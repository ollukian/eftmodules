#pragma once

#ifndef EFT_CORE_IMPACT
#define EFT_CORE_IMPACT

#include <utility>


namespace eft {

struct Impact
{
	double val = 0.;
	double err = 0.;

	explicit Impact(float v) : val(v) {}
	Impact() { val = 0.; err = 0; }
	Impact(double v) : val(v) {}
	Impact(Impact&& other) = default;
	Impact(const Impact & other) { val = other.val; err = other.err; }

	Impact& operator=(Impact other) noexcept;
	Impact& operator=(Impact&& other) noexcept;

	Impact& operator = (const Impact & other) { val = other.val; return *this; }
	Impact& operator = (double other) { val = other; return *this; }
};

} // namespace eft

#endif // !EFT_CORE_IMPACT
