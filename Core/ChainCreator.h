
//
// Created by lukianchuk on 22-Jun-22.
//
// Serves to create a chain of physics modules based on the
// config file, passed to the application
//

#ifndef EFT_CHAINCREATOR_H
#define EFT_CHAINCREATOR_H

#include <string>
#include <utility>
#include <vector>

// TODO: move to a precompiled header

#include "Logger.h"
#include "Profiler.h"

#include "EFTModules.h"



//#include "../Modules/TChainModulesOnePhysics.h"


namespace eft {
namespace inner {

    // servant class used to parse income config file to
    // create a chain of physics modules
class ChainCreator {
public:
    using Chains = std::vector<::eft::modules::TChainModulesOnePhysics*>;

    explicit ChainCreator(std::string  path)
        : path_(std::move(path))
    {
        CreateChains();
    }

    inline const Chains& GetChains() const noexcept { return chains_;}
    inline       Chains& GetChains()       noexcept { return chains_;}

    void CreateChains();
private:
    const std::string path_;
    Chains chains_;
};

} // namespace inner
} // namespace eft


#endif //EFT_CHAINCREATOR_H
