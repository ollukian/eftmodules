
//
// Created by lukianchuk on 22-Jun-22.
//

#include "ChainCreator.h"

// TODO: once c++17 support for root is available, switch from the 3rd party json to an own one, defined in json.h and json.cpp

#include <nlohmann/json.hpp>

#include <iostream>
#include <fstream>

using json = nlohmann::json;

void eft::inner::ChainCreator::CreateChains() {
    EFT_INFO("CreateChains from: {}", path_);

    ifstream fs(path_);

    if ( ! fs.is_open() ) {
        EFT_CRITICAL("Impossible to open {}", path_);
    }

    json j;
    fs >> j;
    EFT_INFO("Successfully read json config from: {}", path_);
    //EFT_DEBUG("Content of this json config: ");
    //for (const auto& key_val : j.items()) {
    //    EFT_DEBUG("{} ==> {}", key_val.key(), key_val.value());
    //}

}
