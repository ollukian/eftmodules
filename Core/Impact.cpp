#include "Impact.h"

 
namespace eft {

Impact& Impact::operator=(Impact other) noexcept {
	std::swap(val, other.val); // exchange resources between *this and other
	std::swap(err, other.err);
	return *this;
}
Impact& Impact::operator=(Impact&& other) noexcept
{
	if (this == &other)
		return *this;

	val = other.val;
	err = other.err;
	return *this;
}


} // namespace eft