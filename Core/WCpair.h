#pragma once

#ifndef EFT_CORE_WCPAIR_H
#define EFT_CORE_WCPAIR_H

// root/Core

#include "../EFTManager/WilsconCoefficients.h"

namespace eft {

// used to  parametrise the BSM contribution:
// in this way, one doesn't need to care about 
// the order of the WC in the map:
// WCpair(wc1, wc2) == WCpair(wc2, wc1)
class WCpair
{
public:
	WilsonCoefficientsCode wc1;
	WilsonCoefficientsCode wc2;

	WCpair(WilsonCoefficientsCode wc1_, WilsonCoefficientsCode wc2_)
		: wc1(wc1_)
		, wc2(wc2_)
	{}
};

inline WCpair make_wcPair(WilsonCoefficientsCode wc1, WilsonCoefficientsCode wc2);

// the operators will likely be inlined by the compiler by the default, since
// their code is a "one-liner"
bool   operator == (const WCpair& l, const WCpair& r);
bool   operator != (const WCpair& l, const WCpair& r);
bool   operator <  (const WCpair& l, const WCpair& r);
bool   operator <= (const WCpair& l, const WCpair& r);
bool   operator >= (const WCpair& l, const WCpair& r);
bool   operator >  (const WCpair& l, const WCpair& r);

::std::ostream& operator << (::std::ostream& os, const WCpair& wc);

} // namespace eft

template<>
struct std::hash<eft::WCpair>
{
	std::size_t operator()(const eft::WCpair& wcpair) const noexcept
	{
		std::size_t h1 = std::hash<int>{}(static_cast<int>(wcpair.wc1));
		std::size_t h2 = std::hash<int>{}(static_cast<int>(wcpair.wc2));
		return std::hash<size_t>{}(h1 + h2);
	}
};

namespace eft {
WCpair make_wcPair(WilsonCoefficientsCode wc1, WilsonCoefficientsCode wc2)
{
	return WCpair(wc1, wc2);
}
}


#endif // !EFT_CORE_WCPAIR_H