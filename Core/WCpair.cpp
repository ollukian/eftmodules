#include "WCpair.h"

namespace eft {
/////////////////////////////////////////////////////////////////////
bool operator==(const WCpair& l, const WCpair& r)
{
	if (l.wc1 == r.wc1 && l.wc2 == r.wc2) return true;
	if (l.wc1 == r.wc2 && l.wc2 == r.wc1) return true;
	return false;
}
/////////////////////////////////////////////////////////////////////
bool operator!=(const WCpair& l, const WCpair& r)
{
	return ! (l == r);
}
/////////////////////////////////////////////////////////////////////
bool operator<(const WCpair& l, const WCpair& r)
{
	auto h1 = std::hash<WCpair>{}(l);
	auto h2 = std::hash<WCpair>{}(r);
	return h1 < h2;
}
/////////////////////////////////////////////////////////////////////
bool operator<=(const WCpair& l, const WCpair& r)
{
	auto h1 = std::hash<WCpair>{}(l);
	auto h2 = std::hash<WCpair>{}(r);
	return h1 <= h2;
}
/////////////////////////////////////////////////////////////////////
bool operator>(const WCpair& l, const WCpair& r)
{
	return !(l <= r);
}
/////////////////////////////////////////////////////////////////////
bool operator>=(const WCpair& l, const WCpair& r)
{
	return !(l < r);
}
/////////////////////////////////////////////////////////////////////
::std::ostream& operator<<(::std::ostream& os, const WCpair& wc)
{
	return os << '{' << wcToStr(wc.wc1) << ", " << wcToStr(wc.wc2) << '}';
}
/////////////////////////////////////////////////////////////////////

} // namespace eft
