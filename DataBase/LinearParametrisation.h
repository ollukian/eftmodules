#pragma once

#ifndef LINEAR_PARAMETRISATION_H
#define LINEAR_PARAMETRISATION_H


#include <vector>

#include "TEntry.h"
//#include "EntryParser.h"
//#include "WilsconCoefficients.h"
#include "../EFTManager/WilsonCoefficients.h"

#include "../Utils/test_runner.h"
#include "../Utils/profile.h"

namespace eft {
namespace serialisation {


class LinearParametrisationInitialiser {
public:
	LinearParametrisationInitialiser();
};

enum class ParametrisationType {
	XS,
	WIDTH,
	MU,
	BR,
	PRODUCTION,
	DECAY,
	NONE
};

std::ostream& operator << (std::ostream& os, const ParametrisationType type);

class LinearParametrisation : public TEntry
{
public:
	void        serialise(std::ostream& os) const override;
	explicit    LinearParametrisation(std::istream& is);
	LinearParametrisation() { this->setEntryType(EntryType::LINEAR_PAREMETRISATION); }
	~LinearParametrisation() override = default;

	//inline tePtr create(std::istream& is) const override;
	inline LinearParametrisation* create(std::istream& is) const override;
	inline LinearParametrisation* clone() const override;
public:
	inline const std::vector<std::string>			  getNames() const { return  names_; }
	inline       std::vector<std::string>			  getNames()       { return  names_; }
	inline const std::vector<WilsonCoefficientsCode>  getWC() const { return  wcCodes_; }
	inline       std::vector<WilsonCoefficientsCode>  getWC()       { return  wcCodes_; }
	inline const std::vector<double>				  getCoeffs() const { return vals_; }
	inline       std::vector<double>				  getCoeffs() { return vals_; }
	inline		 DecayChannel						  getDecayChannel() const { return channel_; }
	inline		 DecayChannel						  getDecayChannel()       { return channel_; }
	inline		 ParametrisationType				  getParametrisationType() const { return parametrisationType_; }
	inline		 ParametrisationType				  getParametrisationType()       { return parametrisationType_; }

	inline void setNames(const std::vector<std::string>& names) { names_ = names; }
	inline void setWC(const std::vector<WilsonCoefficientsCode>& names) { wcCodes_ = names; }
	inline void setCoeffs(const std::vector<double>& coeffs) { vals_ = coeffs; }
	inline void setParametrisationType(ParametrisationType type) { parametrisationType_ = type; }
	inline void setChannel(DecayChannel channel) { channel_ = channel; }
private:
	std::vector<std::string> names_;
	std::vector<double>      vals_;
	std::vector<WilsonCoefficientsCode> wcCodes_;
	ParametrisationType parametrisationType_ = ParametrisationType::NONE;
	DecayChannel        channel_			 = DecayChannel::NONE;
}; // class LinearParametrisation


//template <typename TVals>
//inline tePtr LinearParametrisation::create(std::istream& is) const {
inline LinearParametrisation* LinearParametrisation::create(std::istream& is) const {
	return new LinearParametrisation(is);
}

inline LinearParametrisation* LinearParametrisation::clone() const {
	return new LinearParametrisation(*this);
}


} // namespace serialisation
} // namespace eft


#endif // !LINEAR_PARAMETRISATION_H
