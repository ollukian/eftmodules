#include "ImpactPlotter.h"
#include "EntryParser.h"
#include "TEntry.h"
#include "WIdthOrXSsEntry.h"


#ifndef _MSC_VER
#include "../EFTManager/EFTManager.h"
#include "../EFTManager/Parser.h"
#else
//#include "../EFTManager/EFTManager.h"
//#include "../EFTManager/Parser.h"
//#include "EFTManager/EFTManager.h"
//#include "EFTManager/Parser.h"
#endif

#ifndef	_MSC_VER
#include "TStyle.h"
#endif

using namespace std;

//eft::plot::ImpactPlotter::ImpactPlotter(ImpactPlotConfig& config)
//	: config_(move(config))
//{
//}
//////////////////////////////////////////////////////////////////////////////////////
eft::plot::ImpactPlotter::ImpactPlotter(const ImpactPlotConfig* const config)
	: config_(config)
{
	CheckInit();
}
//////////////////////////////////////////////////////////////////////////////////////
void eft::plot::ImpactPlotter::CheckInit() const {
	LOG_DURATION("ImpactPlotter::Check");
	// check if all required values are set up in the config file
}
//////////////////////////////////////////////////////////////////////////////////////
void eft::plot::ImpactPlotter::Plot() const
{
	const string& encodingMode = config_->encodingMode;

	if (encodingMode == "serialised")
		PlotFromSerialised();
	else if (encodingMode == "linparam")
		PlotFromModulesLinParametrisation();
	else
		throw std::runtime_error("ImpactPlotter::Plot(): encodingMode: " + encodingMode
			+ " is not known. Available: serialised, linparam");
}
//////////////////////////////////////////////////////////////////////////////////////
void eft::plot::ImpactPlotter::PlotFromModulesLinParametrisation() const {
	cout << "[ImpactPlotter] from Modules Lin Parametrisation" << endl;
	cout << "[ImpactPlotter] available: " << config_->groupsWCcodes.size() << " groups of WC with: ";
	for (const auto& wcs : config_->groupsWCcodes) { cout << wcs.size() << " "; }
	cout << " elements in these groups. \n";

#define DEBUG
#ifdef DEBUG
#define OUT std::cout
#define FOUT std::cout << "[ImpactPlotter::Plot]"
#else
	stringstream __sMYname;
#define OUT __s
#define FOUT __s
#endif // DEBUG


	// During development, the MSVS @ Windows 10 was used, which has no idea about root
	// to prevent difficulties, any root-related code is screened from the MSVS
#ifndef _MSC_VER 

	// number of vertical groups, in each a dedicated set of the WC is shown
	size_t nb_groups = config_->groupsWCcodes.size();
	// in each group we use a THStack
	vector<unique_ptr<THStack>> h_stacks(nb_groups);

	size_t nbX = config_->bins_.size();// +config_.channels.size();

	//vector<vector<shared_ptr<TH1D>>> h_impact(nb_groups);
	// for each group, a map: [WC] -> [impact]
	vector<map<WilsonCoefficientsCode, unique_ptr<TH1D>>> h_impact(nb_groups);


	////////////////////////////////////////////////////
	// Convert the SM XS to TH1D ///////////////////////
	// /////////////////////////////////////////////////
	// 
	// SM cross-section -> get it from from config->smXSS
	unique_ptr<TH1D> h_xs_sm = make_unique<TH1D>("h_xs_sm", "", nbX, 0, nbX);
	cout << "[ImpactPlotter] convert the sm xs into TH1D" << endl;
	size_t idx = 1;
	for (const auto& label_xs : config_->smXSS_) {
		const string&         label = label_xs.first;
		const ValErr<double>& xs    = label_xs.second;

	//for (const string& label : config_->bins_) {
		cout << "\t label: " << label;

		if (label == "NONE") {
			cout << " skip \n";
			continue;
		}

		try {
			h_xs_sm->SetBinContent(idx, xs.val);
			h_xs_sm->SetBinError(idx,   xs.err);
			h_xs_sm->GetXaxis()->SetBinLabel(idx, label.c_str());
			cout << "h_xs_sm[" << setw(20) << label << "] successfully set to " << xs << endl;
		}
		catch (std::out_of_range& error) {
			cout << "[ImpactPlotter] ERROR: bin: " << label << " is not present in smXSs!" << endl;
			cout << "present labels: \n";
			for (const auto& bin_xs : config_->smXSS_) {
				cout << "|" << bin_xs.first << "| => " << bin_xs.second << endl;
			}
		}
		idx++;
	}

	{
		shared_ptr<TCanvas> c_tmp = make_shared<TCanvas>("tmp", "tmp", 1200, 800);
		h_xs_sm->GetXaxis()->SetTitle("Truth bin");
		h_xs_sm->GetYaxis()->SetTitle("#sigma_{SM}");
		h_xs_sm->SetTitle("SM XS");
		h_xs_sm->Draw("hist e text");
		c_tmp->SaveAs("figures/SM_xs.pdf");
	}

	// draw stack
	const vector<size_t> colors{
		kRed,
		kBlue,
		kGreen,
		kYellow,
		kBlack,
		kMagenta
	};

	//////////////////////////////////////////////////////
	// Convert the Interf XS to h_impact[idx_group][wc] //
	// and create the stacks /////////////////////////////
	//////////////////////////////////////////////////////

	int idx_group = -1;
	for (const auto& wcs : config_->groupsWCcodes) {
		idx_group++;
		OUT << " idx_group: " << idx_group << endl;
		
		h_stacks[idx_group] = make_unique<THStack>("hs", "");

		//size_t idx_wc = 0;
		for (const auto wc : wcs) { // not ref, since (wc)'s are enums - int objects
			OUT << " looping over wc: " << wc << endl;
			const string name = "h_" + to_string(idx_group) + "_" + wcToStr(wc);
			cout << "name: " << name << " is formed" << endl;
			h_impact[idx_group][wc] = make_unique<TH1D>(name.c_str(), "", nbX, 0, nbX);
			cout << "h_impact[" << idx_group << "][" << wc << "] is created" << endl;
			size_t idx_bin = 1;
			cout << "start looping over indexes" << endl;

			if (config_->interfXS_.find(wc) != config_->interfXS_.end()) {
				//for (const auto& label_xs : config_->interfXS_.at(wc)) {
				for (const auto& label_xs : config_->smXSS_) { // take labels from the full list: sm XSs
					const string& label = label_xs.first;
					if (label == "NONE") {
						cout << " none - skip" << endl;
						continue;
					}

					//const ValErr<double>& xs = label_xs.second;
					ValErr<double> xs = 0;
					cout << "xs[" << setw(5) << wc << "][" << setw(25) << label << "] = ";
					 // check if there's an induced xs in this truth bin
					if (config_->interfXS_.at(wc).count(label) != 0) {
						xs = config_->interfXS_.at(wc).at(label);
						cout << xs << endl;
					}
					else {
						xs = 0;
						cout << '0' << endl;
					}



					//cout << "label: " << label << endl;
					//for (const string& label : config_->bins_) {
					try {
						h_impact[idx_group][wc]->SetFillColor(colors[idx_group]);
						h_impact[idx_group][wc]->SetLineColor(colors[idx_group]);
						h_impact[idx_group][wc]->SetBinContent(idx_bin, xs.val);
						h_impact[idx_group][wc]->SetBinError(idx_bin, xs.err);
						h_impact[idx_group][wc]->GetXaxis()->SetBinLabel(idx_bin, label.c_str());
						cout << "h_impact[" << idx_group << "]["
							 <<setw(20) << label << "] successfully set to " << xs << endl;
					}
					catch (std::out_of_range& error) {
						cout << "[ImpactPlotter] ERROR: bin: " << label << " is not present in smXSs!" << endl;
					}
					idx_bin++;
				} // labesl == bins
				cout << "[ImpactPlotter] make the ratio to the SM for the group: " << idx_group
					<< " for the wc: " << setw(10) << wc << endl;
				h_impact[idx_group][wc]->Divide(h_xs_sm.get());

				{
					shared_ptr<TCanvas> c_tmp = make_shared<TCanvas>("tmp", "tmp", 1200, 800);

					c_tmp->SetBottomMargin(0.3);

					h_impact[idx_group][wc]->GetXaxis()->SetTitle("Truth bin");
					h_impact[idx_group][wc]->GetYaxis()->SetTitle("#sigma_{int} / #sigma_{SM}");
					const string wcStr(wcToStr(wc));
					h_impact[idx_group][wc]->SetTitle( ("Impact of " + wcStr + " on the xs").c_str() );
					h_impact[idx_group][wc]->Draw("hist e text");
					c_tmp->SaveAs( ("figures/Impact_" + wcStr + ".pdf").c_str() );
				}
			} // if this wc is present
			else {
				cout << "wc: " << wc << " is not present in the interfxs" << endl;
			}
		} // wc

		cout << "[ImpactPlotter] make the ratio to the SM for the group: " << idx_group << endl;
	} // groups of wc


	


	unique_ptr<TCanvas> c = make_unique<TCanvas>("c", "c", 1800, 1800);
	c->Divide(1, nb_groups, 0, 0);
	size_t idx_canvas = 1;
	FOUT << " start drawing" << endl;
	for (const auto& wcs : config_->groupsWCcodes) {
		idx_group = idx_canvas - 1;
		OUT << "idx_canvas = " << idx_canvas << endl;
		OUT << " idx_group: " << idx_group << endl;
		c->cd(idx_canvas);
		c->SetGrid();
		cout << "cd done" << endl;
		++idx_canvas;
		//size_t size_this_group = wcs.size();
		//size_t idx_wc = 0;
		cout << "form stack" << endl;
		size_t idx_wc = 0;
		for (const auto wc : wcs) {
			cout << " add " << wc;
			h_impact[idx_group][wc]->SetLineColor(colors[idx_wc]);
			h_impact[idx_group][wc]->SetFillColor(colors[idx_wc]);
			//h_impact[idx_group][wc]->SetFillStyle(3001);
			h_impact[idx_group][wc]->GetYaxis()->SetRangeUser(-0.5, 0.5);
			h_stacks[idx_group]->Add(h_impact[idx_group][wc].get()); // b or hist bar
			cout << " done" << endl;
			idx_wc++;
		}
		cout << "after" << endl;
		//h_stacks[idx_group]->GetYaxis()->SetRangeUser(-1., 1.);
		//cout < "axis range is set" << endl;
		h_stacks[idx_group]->Draw("hist nostackb"); //pfc //h
		cout << "after draw" << endl;
		gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
		cout << "after building leg" << endl;
	}
	c->SaveAs("figures/Stack.pdf");


#endif // !_MSC_VER 
}
//////////////////////////////////////////////////////////////////////////////////////
void eft::plot::ImpactPlotter::PlotFromSerialised() const {
//	using eft::serialisation::EntryType;
//	using eft::serialisation::WidthOrXSsEntry;
//
//	cout << "[ImpactPlotter] init..." << endl;
//	cout << "[ImpactPlotter] available: " << config_->groupsWCcodes.size() << " groups of WC with: ";
//	for (const auto& wcs : config_->groupsWCcodes) { cout << wcs.size() << " "; }
//	cout << " elements in these groups. \n";
//
//
//	//#undef _MSC_VER
//#ifdef	_MSC_VER
//	cout << "==== NOT g++ or clang -> no way to include ROOT libraries -> do not execute ImpactPlotter::Plot()" << endl;
//	return;
//#else
//
//#define DEBUG
//#ifdef DEBUG
//#define OUT std::cout
//#define FOUT std::cout << "[ImpactPlotter::Plot()]"
//#else
//	stringstream __sMYname;
//#define OUT __s
//#define FOUT __s
//#endif // DEBUG
//
//
//	//vector<size_t> colors {kRed, kBlue, kGreen};
//	//gStyle->SetPalette(kOcean);
//
//
//	size_t nb_groups = config_->groupsWCcodes.size();
//	vector<shared_ptr<THStack>> h_stacks(nb_groups);
//
//	//size_t nbX = config_.truth_bins.size();// +config_.channels.size();
//	size_t nbX = config_->bins_.size();// +config_.channels.size();
//
//	//vector<vector<shared_ptr<TH1D>>> h_impact(nb_groups);
//	vector<map<WilsonCoefficientsCode, shared_ptr<TH1D>>> h_impact(nb_groups);
//	shared_ptr<TH1D> h_xs_SM = make_shared<TH1D>("h_xs_sm", "", nbX, 0, nbX);
//
//	//const float h_yy_br = 0.00227;
//	//const vector<float> sm_xs{ //fb
//	//	15.09,
//	//	46.92,
//	//	14.78,
//	//	10.24,
//	//	1.70,
//	//	6.74,
//	//	2.14,
//	//	1.99,
//	//	1.04,
//	//	0.24,
//	//	0.04,
//	//	1.16,
//	//	6.59,
//	//	1.22,
//	//	0.58,
//	//	1.00,
//	//	0.17,
//	//	0.17,
//	//	0.79,
//	//	0.12,
//	//	0.45,
//	//	0.09,
//	//	0.27,
//	//	0.40,
//	//	0.28,
//	//	0.12,
//	//	0.06,
//	//	0.19
//	//};
//
//	FOUT << " available: " << nb_groups << " groups" << endl;
//
//	// create stacks and histograms[group][wc]: impact on a truth bin
//	size_t idx_canvas = 1;
//	int idx_group = -1;
//	for (const auto& wcs : config_.groupsWCcodes) {
//		idx_group++;
//		OUT << " idx_group: " << idx_group << endl;
//		//c->cd(idx_canvas++);
//		//size_t size_this_group = wcs.size();
//
//		//h_impact[idx_group] = vector<shared_ptr<TH1D>>(size_this_group);
//		h_stacks[idx_group] = make_shared<THStack>(*new THStack("hs", ""));
//
//		//size_t idx_wc = 0;
//		for (const auto wc : wcs) {
//			OUT << " looping over wc: " << wc << endl;
//			string name = "h_" + to_string(idx_group) + "_" + wcToStr(wc);
//			h_impact[idx_group][wc] = make_shared<TH1D>(name.c_str(), "", nbX, 0, nbX);
//			//[idx_group][idx_wc++] = make_shared<TH1D>(name.c_str(), "", nbX, 0, nbX);
//		}
//
//	}
//
//	// run over the database to fill the entries
//	FOUT << setfill('*') << setw(45) << '*' << setfill(' ') << endl;
//	FOUT << " *** Run over the database **** " << endl;
//	FOUT << setfill('*') << setw(45) << '*' << setfill(' ') << endl;
//	for (const auto& entry : config_->db_) {
//		OUT << " ==> " << entry->getEntryType();
//
//		auto type = entry->getEntryType();
//
//		if (type != EntryType::XSS) {
//			//if (!(type == EntryType::XSS || type == EntryType::WIDTH)) {
//			OUT << " --> NOT XSS ==> get rid of it\n";
//			continue;
//		}
//
//		OUT << " ==> keep it" << endl;
//		using eft::serialisation::ePtr;
//		//WidthOrXSsEntry* typed_entry = 
//		WidthOrXSsEntry* typed_entry = reinterpret_cast<WidthOrXSsEntry*>(entry);
//		//cout << "here we created a typed entry." << endl;
//		//cout << typed_entry->getChannel() << endl;
//		//typed_entry->serialise(cout);
//		//cout << "serial done" << endl;
//		//const ePtr<WidthOrXSsEntry> typed_entry = make_shared<WidthOrXSsEntry>(*entry.get());
//		//const ePtr<WidthOrXSsEntry> typed_entry = make_shared<WidthOrXSsEntry>(entry);
//
//		WCvalue wcVal = typed_entry->getWCvalue();
//		OUT << "wcValue = " << wcVal;
//
//		if (wcVal != WCvalue::ONE) {
//			OUT << " != ONE --> skip it\n";
//			continue;
//		}
//		OUT << " - keep it" << endl;
//
//		WilsonCoefficientsCode wc = typed_entry->getWCcode();
//		OUT << " This entry stands to wc: " << wc << endl;
//		if (wc == WilsonCoefficientsCode::SM) {
//			OUT << "SM - skip, since we take it from the paper\n";
//			/*OUT << "SM - fill the histo" << endl;
//			for (const auto& bin_xs : typed_entry->getXss()) {
//				OUT << setw(5) << " -> " << bin_xs.second << endl;
//				h_xs_SM->SetBin
//				(bin_xs.first + 1, bin_xs.second.val);
//				h_xs_SM->SetBinError(bin_xs.first + 1, bin_xs.second.err);
//			}*/
//		}
//		else { // NOT SM
//			idx_group = GetWCgroupIdx(wc);
//			if (idx_group == 999)
//				continue;
//
//			OUT << " *** in the group: " << idx_group << endl;
//			for (const auto& bin_xs : typed_entry->getXss()) {
//				OUT << " loop over atruth bins" << endl;
//				auto bin = bin_xs.first;
//				auto xs = bin_xs.second;
//				OUT << " * bin: " << bin << " -> " << xs << endl;
//				OUT << " try to set: " << bin + 1 << " to " << xs.val << endl;
//				h_impact[idx_group][wc]->SetBinContent(bin + 1, xs.val);
//				//h_impact[idx_group][wc]->SetBinError(bin + 1, xs.err);
//			} // bins in  a given entry
//		} // NOT SM case
//		OUT << " ==> go to the next entry" << endl;
//	} // entries
//	FOUT << " Finished running over entries" << endl;
//
//	for (size_t idx_bin = 0; idx_bin < config_->interfXS_.size(); ++idx_bin) {
//		h_xs_SM->SetBinContent(idx_bin + 1, sm_xs.at(idx_bin) / h_yy_br / 1000);
//	}
//
//	{
//		shared_ptr<TCanvas> c_tmp = make_shared<TCanvas>("tmp", "tmp", 1200, 800);
//		h_xs_SM->GetXaxis()->SetTitle("Truth bin");
//		h_xs_SM->GetYaxis()->SetTitle("#sigma_{SM}");
//		h_xs_SM->SetTitle("SM XS");
//		h_xs_SM->Draw("hist e text");
//		c_tmp->SaveAs("figures/impact_SM.pdf");
//	}
//
//	FOUT << " Make the ratio to the SM" << endl;
//	for (idx_group = 0; idx_group < static_cast<int> (nb_groups); ++idx_group) {
//		for (auto& wc_hist : h_impact[idx_group]) {
//			auto wc = wc_hist.first;
//			auto hist = wc_hist.second;
//			OUT << " scale wc: " << wc << " to the SM" << endl;
//			OUT << " integral before: " << hist->Integral() << endl;
//			OUT << "content before scaling: " << endl;
//
//			for (size_t idx_bin = 1; idx_bin <= nbX; ++idx_bin) {
//				OUT << setw(15) << hist->GetBinContent(idx_bin)
//					<< " / " << setw(15)
//					<< h_xs_SM->GetBinContent(idx_bin)
//					<< " = " << (hist->GetBinContent(idx_bin) / h_xs_SM->GetBinContent(idx_bin))
//					<< endl;
//			}
//			hist->Divide(h_xs_SM.get());
//			OUT << "content after scaling: " << endl;
//			for (size_t idx_bin = 1; idx_bin <= nbX; ++idx_bin) {
//				OUT << setw(15) << hist->GetBinContent(idx_bin) << endl;
//			}
//
//			OUT << " intergral after: " << hist->Integral() << endl;
//		}
//	}
//
//
//	// check if scaling of some WC is required...
//	FOUT << " check if scaling is required..." << endl;
//	for (idx_group = 0; idx_group < (static_cast<int> (nb_groups)); ++idx_group) {
//		OUT << " check if scaling of the group: " << idx_group << " is required..." << endl;
//		for (auto& wc_hist : h_impact[idx_group]) {
//			auto wc = wc_hist.first;
//			auto hist = wc_hist.second;
//			//for (const auto& [wc, hist] : h_impact) {
//			OUT << " check wc: " << wc;
//			if (config_.wcScaling_.count(wc) != 0) {
//				auto coef = config_.wcScaling_.at(wc);
//				OUT << " by " << coef << endl;
//				hist->Scale(config_.wcScaling_.at(wc));
//			}
//			else {
//				OUT << " not required.." << endl;
//			}
//		}
//	}
//
//	idx_group = 0;
//	for (const auto& wcs : config_.groupsWCcodes) {
//		for (const auto wc : wcs) {
//			shared_ptr<TCanvas> c_tmp = make_shared<TCanvas>("tmp", "tmp", 1200, 800);
//			h_impact[idx_group][wc]->GetXaxis()->SetTitle("Truth bin");
//			h_impact[idx_group][wc]->GetYaxis()->SetTitle("#sigma_{int}/#sigma_{SM}");
//			h_impact[idx_group][wc]->SetTitle(("scaled Impact of the WC: " + wcToStr(wc)).c_str());
//			//h_impact[idx_group][wc]->Draw("h e");
//			h_impact[idx_group][wc]->Draw("h");
//			SetLabels(h_impact[idx_group][wc].get(), mergedBinsName, 'x');
//			c_tmp->SaveAs(("figures/impact_" + wcToStr(wc) + ".pdf").c_str());
//		}
//		idx_group++;
//	}
//
//	const vector<size_t> colors{
//		kRed,
//		kBlue,
//		kGreen,
//		kYellow,
//		kBlack,
//		kMagenta
//	};
//
//
//	shared_ptr<TCanvas> c = make_shared<TCanvas>("c", "c", 1800, 1800);
//	c->Divide(1, nb_groups, 0, 0);
//	idx_canvas = 1;
//	FOUT << " start drawing" << endl;
//	for (const auto& wcs : config_.groupsWCcodes) {
//		idx_group = idx_canvas - 1;
//		OUT << "idx_canvas = " << idx_canvas << endl;
//		OUT << " idx_group: " << idx_group << endl;
//		c->cd(idx_canvas);
//		c->SetGrid();
//		cout << "cd done" << endl;
//		++idx_canvas;
//		//size_t size_this_group = wcs.size();
//		//size_t idx_wc = 0;
//		cout << "form stack" << endl;
//		size_t idx_wc = 0;
//		for (const auto wc : wcs) {
//			cout << " add " << wc;
//			h_impact[idx_group][wc]->SetLineColor(colors[idx_wc]);
//			h_impact[idx_group][wc]->SetFillColor(colors[idx_wc]);
//			//h_impact[idx_group][wc]->SetFillStyle(3001);
//			h_impact[idx_group][wc]->GetYaxis()->SetRangeUser(-0.5, 0.5);
//			h_stacks[idx_group]->Add(h_impact[idx_group][wc].get(), "bar"); // b or hist
//			cout << " done" << endl;
//			idx_wc++;
//		}
//		cout << "after" << endl;
//		//h_stacks[idx_group]->GetYaxis()->SetRangeUser(-1., 1.);
//		//cout << "axis range is set" << endl;
//		h_stacks[idx_group]->Draw("nostackb h"); //pfc
//		cout << "after draw" << endl;
//		gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
//		cout << "after building leg" << endl;
//	}
//	c->SaveAs("figures/Stack.pdf");
//
//	// draw impact of one wc
//
//#endif
//
//
////#endif // __GNUC__
}
