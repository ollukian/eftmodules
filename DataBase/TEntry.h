#pragma once

#ifndef TENTRY_H
#define TENTRY_H


#include <string>
#include <vector>
#include <map>
#include <set>

#include <memory>
#include <utility>

#include <iostream>
#include <sstream>

//#include "WilsconCoefficients.h"
#include "../EFTManager/WilsonCoefficients.h"

#include "Serialisable.h"

using std::vector;
using std::string;


namespace eft {
namespace serialisation {
	//const static std::string tab = "    ";
	const static std::string tab = "\t";


class TEntry;

using tePtr = std::shared_ptr<::eft::serialisation::TEntry>;

enum class EntryType {
	NOT_DEFINED,
	XSS,
	WIDTH,
	BR,  // branching ratio
	XS, // single cross-section
	PAREMETRISATION,
	LINEAR_PAREMETRISATION,
	QUAD_PARAMETRISATION,
	EIGENVECTORS,
	TRANSITIONMATRIX
};
std::string entryToStr(const EntryType type);

class TEntry : public Serialisable
{
public:
	//static  tePtr unserialise(std::istream& is); 
	static TEntry* unserialise(std::istream& is);
	static  const vector<TEntry*> readDB(const string& filename);
	//static  const set<tePtr> readDB(const string& filename);
	virtual void  serialise(std::ostream& is) const;
	inline TEntry* clone() const override;
	inline TEntry* create(std::istream& is) const override;
	
	TEntry() = default;
	explicit TEntry(std::istream& is);
	virtual ~TEntry() = default;

	inline void setVersion(const std::string& version = "") { version_  = version; }
	inline void setThRegime(const TheoryRegime reg)    { thRegime_ = reg; }
	inline void setEntryType(const EntryType type) { entryType_ = type; }

	virtual inline const std::string getVersion() const { return version_; }
	virtual inline       std::string getVersion()       { return version_; }

	virtual inline TheoryRegime getThRegime() const { return thRegime_; }
	virtual inline TheoryRegime getThRegime()       { return thRegime_; }

	virtual inline EntryType getEntryType() const { return entryType_; }
	virtual inline EntryType getEntryType()       { return entryType_; }

	//inline void serialiseVersion();

protected:
	std::string  version_ = "";
	std::string  className_ = "unknown";
	TheoryRegime thRegime_  = TheoryRegime::NOT_DEFINED;
	EntryType    entryType_ = EntryType::NOT_DEFINED;
private:

public:
	static std::map<std::string, TEntry*> mapEntries;
};

inline TEntry* TEntry::create(std::istream& is) const {
	return new TEntry(is);
}


inline TEntry* TEntry::clone() const {
	return new TEntry(*this);
}

//static std::map<string, TEntry*> mapEntries;

class SerialisationError : public std::runtime_error {
public:
	SerialisationError(const std::string& msg = "")
		: runtime_error("[SerialisationError]: " + msg) {}
};



} // namespace serialisation
} // namespace eft


#endif // !TENTRY_H