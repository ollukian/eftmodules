#pragma once
#include "TEntry.h"

namespace eft {
namespace serialisation {

using matrix = std::vector<std::vector<double>>;

class QuadParametrisationInitialiser;


class QuadParametrisation : public TEntry
{
public:
	void        serialise(std::ostream& os) const override;
	explicit    QuadParametrisation(std::istream& is);
	QuadParametrisation() = default;
	~QuadParametrisation() override = default;

	inline QuadParametrisation* clone() const override;
	inline QuadParametrisation* create(std::istream & is) const override;
public:
	inline const std::vector<std::string>  getNames() const { return  names_; }
	inline       std::vector<std::string>  getNames()       { return  names_; }
	inline const matrix getCoeffs() const { return vals_; }
	inline       matrix getCoeffs()       { return vals_; }

	inline void setNames(std::vector<std::string>& names) { names_ = move(names); }
	inline void setCoeffs(matrix& coeffs)				  { vals_ = move(coeffs); }
private:
	std::vector<std::string> names_;
	matrix vals_;
};

//inline tePtr QuadParametrisation::create(std::istream& is) const {
inline QuadParametrisation* QuadParametrisation::create(std::istream& is) const {
	//return std::make_shared<QuadParametrisation>(
	//	*new QuadParametrisation(is)
	//);
	return new QuadParametrisation(is);
}

inline QuadParametrisation* QuadParametrisation::clone() const {
	//return std::make_shared<QuadParametrisation>(
	//	*new QuadParametrisation(is)
	//);
	return new QuadParametrisation(*this);
}

class QuadParametrisationInitialiser {
public:
	QuadParametrisationInitialiser();
};

} // namespace serialisation
} // namespace eft

