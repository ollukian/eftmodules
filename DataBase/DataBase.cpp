#include "DataBase.h"
#include "TEntry.h"
#include "EntryParser.h"
#include "WIdthOrXSsEntry.h"
#include "Merger.h"
//#include "WilsconCoefficients.h"
#include "../EFTManager/WilsonCoefficients.h"

#include <sstream>

#include "../Utils/test_runner.h"

#include "PythonTest.h"

void RunDataBaseTests(TestRunner& tr);
void TestDB();
void TestParseFunctions();
void TestEntries();

int main() {
	using namespace eft;
	using namespace std;

	//cout << "fineNames[87]: " << fineNames[87] << endl;
	//cout << "fineNames[92]: " << fineNames[92] << endl;
	//cout << strToWC("CG") << endl;
	//cout << strToWC("WilsonCoefficientsCode::CG");
	//return 0;

	//TestParseFunctions();
	//TestEntries();
	//TestDB();

	//testBoost();

	istringstream is(R"(
	{	"mu_GG2H_0J_PTH_0_10_ZZ"                 : ["GG2H_0J_PTH_0_10"],
	"mu_GG2H_0J_PTH_GT10_ZZ"                 : ["GG2H_0J_PTH_GT10"],
	"mu_GG2H_1J_PTH_0_60_ZZ"                 : ["GG2H_1J_PTH_0_60"],
	"mu_GG2H_1J_PTH_120_200_ZZ"              : ["GG2H_1J_PTH_120_200"],
	"mu_GG2H_1J_PTH_60_120_ZZ"               : ["GG2H_1J_PTH_60_120"],
	"mu_GG2H_GE2J_PTH_LE_200_ZZ"             : ["GG2H_GE2J_MJJ_0_350_PTH_0_60","GG2H_GE2J_MJJ_0_350_PTH_60_120","GG2H_GE2J_MJJ_0_350_PTH_120_200","GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25","GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25","GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25","GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25"],
	"mu_GG2H_PTH_GT200_ZZ"                   : ["GG2H_PTH_200_300","GG2H_PTH_300_450","GG2H_PTH_450_650","GG2H_PTH_GT650"],
	"mu_QQ2HQQ_GE2J_MJJ_60_120_ZZ"           : ["QQ2HQQ_GE2J_MJJ_60_120"],
	"mu_QQ2HQQ_GE2J_MJJ_GT350_PTH_GT200_ZZ"  : ["QQ2HQQ_GE2J_MJJ_350_700_PTH_GT200_PTHJJ_GT25","QQ2HQQ_GE2J_MJJ_700_1000_PTH_GT200_PTHJJ_GT25","QQ2HQQ_GE2J_MJJ_GT1500_PTH_GT200_PTHJJ_GT25","QQ2HQQ_GE2J_MJJ_700_1000_PTH_GT200_PTHJJ_0_25","QQ2HQQ_GE2J_MJJ_1000_1500_PTH_GT200_PTHJJ_0_25","QQ2HQQ_GE2J_MJJ_GT1500_PTH_GT200_PTHJJ_0_25","QQ2HQQ_GE2J_MJJ_1000_1500_PTH_GT200_PTHJJ_GT25","QQ2HQQ_GE2J_MJJ_350_700_PTH_GT200_PTHJJ_0_25"],
	"mu_QQ2HQQ_VBF_ZZ"                       : ["QQ2HQQ_GE2J_MJJ_0_60","QQ2HQQ_GE2J_MJJ_120_350","QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25","QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25","QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25","QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25"],
	"mu_TOP_ZZ"                              : ["TH", "TTH_PTH_0_60", "TTH_PTH_60_120", "TTH_PTH_120_200", "TTH_PTH_200_300","TTH_PTH_300_450","TTH_PTH_GT450"],
	"mu_VHLEP_ZZ"                            : ["QQ2HLNU_PTV_0_75","QQ2HLNU_PTV_75_150","QQ2HLNU_PTV_150_250_0J","QQ2HLNU_PTV_150_250_GE1J","QQ2HLNU_PTV_GT250","QQ2HLL_PTV_0_75","QQ2HLL_PTV_75_150","QQ2HLL_PTV_150_250_0J","QQ2HLL_PTV_150_250_GE1J","QQ2HLL_PTV_GT250","GG2HLL_PTV_0_75","GG2HLL_PTV_75_150","GG2HLL_PTV_150_250_0J","GG2HLL_PTV_150_250_GE1J","GG2HLL_PTV_GT250"]
}
)");

	eft::MergeData::ReadFromJSON(is);


	return 0;
}
