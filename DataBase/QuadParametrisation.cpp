#include "QuadParametrisation.h"
#include "TEntry.h"
#include "EntryParser.h"

using namespace std;

namespace eft {
namespace serialisation {

eft::serialisation::QuadParametrisationInitialiser::QuadParametrisationInitialiser() {
	using namespace std;
	TEntry::mapEntries["QUADRATIC_PARAMETRISATION"] = new QuadParametrisation();
	//TEntry::mapEntries["PAREMETRISATION"]           = new QuadParametrisation();

	std::cout << "[STATIC] {Register types: QUADRATIC_PARAMETRISATION}" << endl;
}

void QuadParametrisation::serialise(std::ostream& os) const
{
	using ::eft::serialisation::tab;

	std::cout << "[serialise]{QuadParametrisation}" << std::endl;
	TEntry entry(*this);
	entry.serialise(os);

	os << tab << "Names {";
	for (const auto& name : names_) {
		os << ' ' << name;
	}
	os << '}' << endl;

	os << tab << "Coefficients {\n";
	for (const auto& line : vals_) {
		os << "\t{";
		for (const double val : line) {
			os << ' ' << val;
		}
		os << "}\n";
	}
	os << '}' << endl;
	os << "QuadParametrisation::End" << '\n';
}

inline eft::serialisation::QuadParametrisation::QuadParametrisation(std::istream& is)
	: TEntry(is)
{
	cout << "[QuadParametrisation] {ctor}" << endl;

	using namespace std;

	string line, token;

	while (is >> token) {
	//while (getline(is, line)) {
		//istringstream is(line);
		//is >> token;
		cout << "[LinearParametrisation::ctor] token: " << token << endl;
		setEntryType(EntryType::PAREMETRISATION);

		if (token == "LinearParametrisation::End")
			break;
		if (token == "Names")
			this->names_ = parseVector<string>(is);
		else if (token == "Coefficients")
			this->vals_ = parseMatrix<double>(is);
		else
			throw std::runtime_error("Unknown token: " + token
				+ ". \n Available: Names, Coefficients, Expression \n");

	}

}






}
}