#pragma once

#ifndef PARSE_RESULTS_H
#define PARSE_RESULTS_H



#include "../EFTManager/CrossSections.h"
#include "../EFTManager/TruthBin.h"
#include "../EFTManager/WilsonCoefficients.h"


#include <string>
#include <vector>
#include <iostream>

using truth_bin = eft::Label;
//using cross_section = double;
using cross_section = ValErr<double>;

using Width = ValErr<double>;


namespace eft {

struct ParseResults {
    size_t                      nbGeneratedEvents = 0;
    std::string                 filename;

    eft::Regime                 regime = eft::Regime::NONE; // PRODUCTION / DECAY
    eft::ProdMode               prodMode = eft::ProdMode::NONE;
    eft::DecayChannel           decayChannel = eft::DecayChannel::NONE;
    eft::TheoryRegime           theoryRegime = eft::TheoryRegime::SM;

    CrossSections               xsPerTB{};
    CrossSections               xsPerTBcoarse{};

    CrossSections               xsErrorPerTB{};
    Width                       width_{};

    double                      wcValue = 0;
    eft::WCvalue                wcValueCode = eft::WCvalue::SM;
    eft::WilsonCoefficientsCode wcCode = eft::WilsonCoefficientsCode::NONE;
    eft::WilsonCoefficient      wc{};

    double                      wcValue2 = 0;
    eft::WCvalue                wcValueCode2 = eft::WCvalue::SM;
    eft::WilsonCoefficientsCode wcCode2 = eft::WilsonCoefficientsCode::NONE;
    eft::WilsonCoefficient      wc2{};
};

std::ostream& operator << (ostream& os, const ParseResults& res);

}

#endif // !PARSE_RESULTS_H

