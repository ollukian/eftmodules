#pragma once

#ifndef WIDTH_OR_XS_ENTRY_H
#define WIDTH_OR_XS_ENTRY_H


#include "TEntry.h"
#include "ValErr.h"
#include <map>

#include <iomanip>

using ::eft::serialisation::tab;

namespace eft {
namespace serialisation {

class WidthXsEntryInialiser {
public:
	WidthXsEntryInialiser(); 
};

class WidthOrXSsEntry : public TEntry
{
public:
	inline void serialiseWidth(std::ostream& os) const;
	inline void serialiseXss(std::ostream& os) const;
	void        serialise(std::ostream& os) const override;
	explicit    WidthOrXSsEntry(std::istream& is);
	WidthOrXSsEntry() { setEntryType(EntryType::WIDTH); }
	~WidthOrXSsEntry() override = default;

	inline WidthOrXSsEntry* create(std::istream& is) const override;
	inline WidthOrXSsEntry* clone() const override;

	//inline tePtr create(std::istream& is) const override;
	//inline ePtr<WidthOrXSsEntry> clone() const { return make_shared<WidthOrXSsEntry>(*new WidthOrXSsEntry(this)); }
public:
	inline const std::string getChannel() const { return channel_; }
	inline       std::string getChannel() { return channel_; }
	inline const std::string getFileName() const { return filename_; }
	inline       std::string getFileName() { return filename_; }
	inline       std::size_t getNbEbents() const { return nbEvents_; }
	inline       std::size_t getNbEbents() { return nbEvents_; }
	inline const ValErr<double> getWidth() const { return width_; }
	inline       ValErr<double> getWidth() { return width_; }
	inline const std::map<size_t, ValErr<double>> getXss() const { return xss_; }
	inline	     std::map<size_t, ValErr<double>> getXss()       { return xss_; }
	inline const std::vector<std::string> getXsNames() const { return xsNames_;  }
	inline       std::vector<std::string> getXsNames()       { return xsNames_; }
	inline       WilsonCoefficientsCode   getWCcode() const { return wcCode1_; }
	inline	     WilsonCoefficientsCode   getWCcode()       { return wcCode1_; }
	inline       WilsonCoefficientsCode   getWCcode2() const { return wcCode2_; }
	inline	     WilsonCoefficientsCode   getWCcode2()		 { return wcCode2_; }
	inline       WCvalue				  getWCvalue() const { return wcVal1_; }
	inline       WCvalue				  getWCvalue2() const{ return wcVal2_; }
	inline		WCvalue				  getWCvalue()		     { return wcVal1_; }
	inline		WCvalue				  getWCvalue2()		     { return wcVal2_; }

	inline void  setChannel(const std::string& ch = "") { channel_ = ch; }
	inline void  setFileName(const std::string& ch = "") { filename_ = ch; }
	inline void  setNbEbents(size_t nb) { nbEvents_ = nb; }
	inline void  setWidth(const ValErr<double> width) { width_ = width; }

	inline void  setXss(std::map<size_t, ValErr<double>>& xss) { xss_ = std::move(xss); }
	inline void  setXsNames(const std::vector<std::string>& names) { xsNames_ = names; }
	inline void  setWCcode(const WilsonCoefficientsCode code) { wcCode1_ = code; }
	inline void  setWCcode2(const WilsonCoefficientsCode code) { wcCode2_ = code; }
	inline void  setWCvalue(const WCvalue val)  { wcVal1_ = val; }
	inline void  setWCvalue2(const WCvalue val) { wcVal2_ = val; }

private:
	WilsonCoefficientsCode wcCode1_ = WilsonCoefficientsCode::SM;
	WilsonCoefficientsCode wcCode2_ = WilsonCoefficientsCode::SM;
	WCvalue wcVal1_ = WCvalue::SM;
	WCvalue wcVal2_ = WCvalue::SM;

	std::string channel_ = "";
	size_t      nbEvents_ = 0;
	std::string filename_ = ""; 

	ValErr<double> width_ {0., 0.};
	std::map<std::size_t, ValErr<double>> xss_{};
	std::vector<std::string> xsNames_ {};
};

void WidthOrXSsEntry::serialiseWidth(std::ostream& os) const {
	std::cout << "[serialiseWidth]" << std::endl;
	os << tab << "WIDTH " << width_.val << " +- " << width_.err << '\n';
}

void WidthOrXSsEntry::serialiseXss(std::ostream& os) const {
	using std::string;

	std::cout << "[serialiseXss]" << std::endl;
	os << "XSS {" << '\n';
	for (const auto& bin_xs : xss_) {
		os << std::setw(5) << bin_xs.first << ' '
			<< bin_xs.second.val << " +- "
			<< bin_xs.second.err << std::endl;
	}
	os << "}" << '\n';

	if (!xsNames_.empty()) {
		std::cout << "[serialiseXss] names are present. Serialise them..." << std::endl;
		os << "XS_NAMES { ";
		for (const string& name : xsNames_) {
			os << name << " ";
		}
		os << "} \n";
	}

}

inline WidthOrXSsEntry* WidthOrXSsEntry::create(std::istream& is) const {
	return new WidthOrXSsEntry(is);
//inline tePtr WidthOrXSsEntry::create(std::istream& is) const {
	//return std::make_shared<WidthOrXSsEntry>(
	//	*new WidthOrXSsEntry(is)
	//	);
}

inline WidthOrXSsEntry* WidthOrXSsEntry::clone() const {
	return new WidthOrXSsEntry(*this);
	//inline tePtr WidthOrXSsEntry::create(std::istream& is) const {
		//return std::make_shared<WidthOrXSsEntry>(
		//	*new WidthOrXSsEntry(is)
		//	);
}

} // namespace serialisation
} // namespace eft

#endif // !WIDTH_OR_XS_ENTRY_H

