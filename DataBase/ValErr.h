#pragma once

#ifndef VALERR_H
#define VALERR_H



//#include "EntryParser.h"

#include <iostream>
#include <math.h>
#include <sstream>

template <typename T>
struct ValErr {
    T val;
    T err;
};

template <typename T>
ValErr<T> operator + (ValErr<T>& l, const ValErr<T>& r) {
    l.val += r.val;
    l.err = std::sqrt(l.err * l.err + r.err * r.err);
    return l;
}

template <typename T>
ValErr<T>& operator += (ValErr<T>& l, const ValErr<T>& r) {
    l.val += r.val;
    l.err = std::sqrt(l.err * l.err + r.err * r.err);
    return l;
}



template <typename T>
std::istream& operator >> (std::istream& is, ValErr<T>& res) {
    //std::cout << "[operator >>] for VallErr" << std::endl;
    std::string val, trash, err;
    //std::cout << "start reading chars until a meaningful one:" << std::endl;

    /*char c = static_cast<char>(is.peek());
    while (!isdigit(is.peek())) {
        std::cout << "got: |" << c << "| -> clean. The next one is: |";
        is.get();
        c = static_cast<char>(is.peek());
        std::cout << c << "|" << std::endl;
    }*/
    //std::cout << "got rid of not digits. " << std::endl;
    //std::string line;
    //std::getline(is, line);
    //std::istringstream ss(line);
    /*char c = static_cast<char>(is.peek());
    std::cout << "[operator >>] first char = |" << c << "|" << std::endl;
    while (c == ' ' || c == '\t' || c == '\n') {
        std::cout << "[operator >>] bad first char -> get next: |";
        is.get();
        c = static_cast<char>(is.peek());
        std::cout << c << "| \n";
    }*/
    is >> val >> trash >> err;

    std::string line = val + trash + err;

    if (is.bad()) {
        throw std::runtime_error("impossible to parse: " + line + ". Not in the form: val +- err");
    }

    /*for (const char c : val) {
        if (!isdigit(c) && c != '.' )
            throw std::runtime_error("Format of the value: " + val + " is not correct - not a double");
    }

    for (const char c : err) {
        if (!isdigit(c) && c != '.' )
            throw std::runtime_error("Format of the error: " + err + " is not correct - not a double");
    }*/

    /*std::cout << "to parse: |" << val << "| as val" << std::endl;
    std::cout << "to parse: |" << err << "| as err" << std::endl;
    std::cout << "trash:    |" << trash << "|" << std::endl;
    std::cout << "is.peek() = " << static_cast<char>(is.peek()) << std::endl;*/

    if (trash != "+-")
        throw std::runtime_error("[VallErr::operator<<] -> no +- found");

    res.val = stof(val);
    res.err = stof(err);
    return is;
}

template <typename T>
inline std::ostream& operator << (std::ostream& os, const ValErr<T>& res) {
    return os << res.val << " +- " << res.err;
}

template <typename T>
inline bool operator == (const ValErr<T>& l, const ValErr<T>& r) {

    return (l.val == r.val) && (l.err == r.err);
}

inline bool operator == (const ValErr<double>& l, const ValErr<double>& r) {
    return (abs(l.val - r.val) < 1E-8) && (abs(l.err - r.err) < 1E-8);
}

//template <typename T>
//T& operator / (ValErr<T>& l, const ValErr<T>& r) {
//    l.val /= r.val;
//    //l.err = sqrt(l.err * l.err + r.err * r.err);
//    return l;/
//}





//template <typename T>
//ValErr<T>& operator + (ValErr<T>& l, const ValErr<T>& r) {
//  l.val += r.val;
//  l.err = sqrt
//}

//template <typename T>
//ValErr<T>& operator + (ValErr<T>& l, const TH* h) {
//  l.val += h->GetBinContent(
//}


#endif // !VALERR_H

