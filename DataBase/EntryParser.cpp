#include "EntryParser.h"

namespace eft {
namespace serialisation {

	using namespace std;
///////////////////////////////////////////////////////////////////////////////////
	EntryType parseEntryType(istream& is, string& type)
	{
		absorbSpaces(is);
		std::string line;
		std::getline(is, line);
		std::cout << "[eft::parseEntryType]: " << line;

		size_t pos = line.find("::");
		if (pos == std::string::npos)
			throw ParseException("no :: in the header: |"
				+ line + "|. Impossible to parse entry type");

		const std::string token = line.substr(pos + 2, line.length());
		type = token;
		std::cout << "[eft::parseEntryType] token: " << token << std::endl;

		if (token == "XSS")						    return EntryType::XSS;
		else if (token == "WIDTH")				    return EntryType::WIDTH;
		else if (token == "PAREMETRISATION")	    return EntryType::PAREMETRISATION;
		else if (token == "LINEAR_PAREMETRISATION")	return EntryType::LINEAR_PAREMETRISATION;
		else if (token == "QUAD_PARAMETRISATION")   return EntryType::QUAD_PARAMETRISATION;
		else if (token == "EIGENVECTORS")		    return EntryType::EIGENVECTORS;
		else if (token == "TRANSITIONMATRIX")	    return EntryType::TRANSITIONMATRIX;
		else if (token == "NOT_DEFINED")		    return EntryType::NOT_DEFINED;

		throw ParseException("entry type: " + token + " is not known. "
			+ "Cannot construct an object of this type. Available: \n"
			+ "XSS, WIDTH, PAREMETRISATION, EIGENVECTORS, TRANSITIONMATRIX" 
			+ "LINEAR_PARAMETRISATION, LINEAR_PARAMETRISATION\n");
	}

///////////////////////////////////////////////////////////////////////////////////
	std::ostream& operator << (std::ostream& os, const EntryType& entry) {
		const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
		switch (entry) {
			PROCESS_VAL(EntryType::NOT_DEFINED);
			PROCESS_VAL(EntryType::BR);
			PROCESS_VAL(EntryType::LINEAR_PAREMETRISATION);
			PROCESS_VAL(EntryType::QUAD_PARAMETRISATION);
			PROCESS_VAL(EntryType::XSS);
			PROCESS_VAL(EntryType::XS);
			PROCESS_VAL(EntryType::WIDTH);
			PROCESS_VAL(EntryType::PAREMETRISATION);
			PROCESS_VAL(EntryType::EIGENVECTORS);
			PROCESS_VAL(EntryType::TRANSITIONMATRIX);
		default: throw std::runtime_error("an attempt to used unknown enttry type");
		}
#undef PROCESS_VAL

		return os << s;
	}
///////////////////////////////////////////////////////////////////////////////////
	DecayChannel parseChannel(std::istream& is) {
		string channel;
		is >> channel;

		size_t pos = channel.find("::");
		if (pos != std::string::npos)
			channel = channel.substr(pos + 2, channel.length());

		static std::map<std::string, DecayChannel> __tableWCperName;
#define PROCESS_CASE(__wc) __tableWCperName[#__wc] = DecayChannel::__wc;
		
		PROCESS_CASE(TOTAL);
		PROCESS_CASE(L_NU_L_NU);
			PROCESS_CASE(TAU_NU_TAU_NU);
			PROCESS_CASE(L_NU_TAU_NU);
			PROCESS_CASE(L_NU_J_J);
			PROCESS_CASE(QQQQ);
			PROCESS_CASE(LLLL);
			PROCESS_CASE(TAU_TAU_TAU_TAU);
			PROCESS_CASE(NU_NU_NU_NU);
			PROCESS_CASE(L_L_TAU_TAU);
			PROCESS_CASE(NU_NU_TAU_TAU);
			PROCESS_CASE(J_J_L_L);
			PROCESS_CASE(J_J_NU_NU);
			PROCESS_CASE(GAM_GAM);
			PROCESS_CASE(B_B);
			PROCESS_CASE(C_C);
			PROCESS_CASE(S_S);
			PROCESS_CASE(E_E);
			PROCESS_CASE(MU_MU);
			PROCESS_CASE(TAU_TAU);
			PROCESS_CASE(Z_GAM);
			PROCESS_CASE(G_G);
			PROCESS_CASE(NONE);

#undef PROCESS_CASE

		if (__tableWCperName.count(channel) < 1)
			throw std::runtime_error("Impossible to convert: [" + channel
				+ "] to a given Decay Channel coefficient code. This channel is not present");

		return __tableWCperName.at(channel);
	}
///////////////////////////////////////////////////////////////////////////////////
	TheoryRegime parseThRegime(std::istream& is) {
		std::cout << "[parseThRegime]... ";
		std::string res;
		is >> res;
		size_t pos = res.find("::");

		if (pos == std::string::npos)
			throw ParseException("[parseThRegime] no :: found in the name - cannot parse the th regime");

		res = res.substr(pos + 2, res.length());

		std::cout << res << std::endl;
		if (res == "SM")
			return TheoryRegime::SM;
		if (res == "INT")
			return TheoryRegime::INT;
		if (res == "BSM")
			return TheoryRegime::BSM;
		else
			throw ParseException("[parseThRegime] unknown type: " + res
				+ ". available: SM, INT, BSM");
	}

///////////////////////////////////////////////////////////////////////////////////
	void parseExpression(std::istream& is, std::vector<std::string>& names, std::vector<double>& vals) {
		static const set<char> operators { '-', '+', '*' };
		string wcString {};
		double coeff = -999.999;
		char delim;
		char sign = '+';

		/*cout << "parse expression. All tokens:" << endl;
		string line;
		while (is >> line)
			cout << "|" << line << "|" << endl;
		throw ParseException("stpo");*/

		stringstream already_parsed;

		absorbSpaces(is);

		char c = static_cast<char>(is.peek());
		//is >> c;

		if (c != '{')
			throw ParseException("expected first { character. Got: " + c);


		is.get(c); // to read the first '{' character
		absorbSpaces(is);
		bool toBreak = false;
		while (is.peek() != '}') {
		//while (c != '}') {
			//cout << "first symb: |" << static_cast<int>(is.peek()) << "|" << endl;
			already_parsed << "{";
			//cout << "-----------" << endl;
			is >> coeff >> delim >> wcString;
			//cout << "delim: |" << delim << "|" << endl;
			//cout << "coeff without sign: " << coeff << endl;

			if (sign == '-')
				coeff = - coeff;

			already_parsed << coeff;
			//cout << "coef: |" << coeff << "|" << endl;
			if (delim != '*') {
				//if (operators.find(delim) == operators.end())
				throw ParseException("expected in a form: val1 * name1 + val2 * name2 - ..." + 
				string("| succesfully parsed: \n") + already_parsed.str() + "");
			}
			already_parsed << delim;
			if (wcString[wcString.length() - 1] == '}') {
				//cout << "wcString = " << wcString << endl;
				wcString.erase(wcString.length() - 1);
				toBreak = true;
			}
			already_parsed << wcString;
			//cout << "wc string: |" << wcString << "|" << endl;
			names.emplace_back(std::move(wcString));
			vals.push_back(coeff);

			if (toBreak)
				break;

			is >> sign;
			if (sign == '}')
				break;

			//cout << "sign: |" << sign << "|" << endl;	
			//is >> c;
			//cout << "is eof? " << is.eof() << endl;
			absorbSpaces(is);
		}


		/*cout << " what we got: " << endl;
		for (size_t idx = 0; idx < names.size(); ++idx) {
			cout << setw(10) << names[idx] << " -> " << vals[idx] << endl;
		}*/
	}
///////////////////////////////////////////////////////////////////////////////////
	void CompareStrings(const string& s1, const string& s2) {
		size_t size_1 = s1.length();
		size_t size_2 = s2.length();
		size_t smaller = size_1;
		//size_t greater = size_2;
		if (size_1 != size_2) {
			cout << "size of left: " << size_1 << " != size of right: " << size_2 << endl;
			if (size_1 > size_2) {
				smaller = size_2;
				//greater = size_1;
			}

			for (size_t idx = 0; idx < smaller; ++idx) {
				char c1 = s1[idx];
				char c2 = s2[idx];
				cout << "|" << c1 << "| ";
				if (c1 == c2)
					cout << " == ";
				else
					cout << " != ";
				cout << "|" << c2 << "|" << endl;
			}

			if (size_1 > size_2) {
				for (size_t idx = size_2; idx < size_1; ++idx) {
					cout << "|" << s1[idx] << "|" << endl;
				}
			}
			else {
				for (size_t idx = size_1; idx < size_2; ++idx) {
					cout << "|" << s2[idx] << "|" << endl;
				}
			}

		}
	}
///////////////////////////////////////////////////////////////////////////////////

} // namespace serialisation
} // namespace eft
