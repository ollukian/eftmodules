#pragma once

#ifndef IMPACT_PLOTTER_H
#define IMPACT_PLOTTER_H

#include <set>
#include <vector>
#include <string>

using std::set;
using std::vector;
using std::string;

#ifdef	_MSC_VER
#include "../EFTManager/TruthBin.h"
#else
#include "../EFTManager/TruthBin.h"
#endif
//#include "WilsconCoefficients.h"	
#include "../EFTManager/WilsonCoefficients.h"

#include "TEntry.h"
#include "WIdthOrXSsEntry.h"

#include "../Modules/EFTModules.h"

#ifndef	_MSC_VER
//#pragma warning(push, 0)
#include "TH1D.h"
#include "THStack.h"
#include "TPad.h"
#include "TCanvas.h"
//#pragma warning(pop)
#endif

namespace eft {
namespace plot {

using WC      = WilsonCoefficientsCode;
using WCcodes = vector<WC>;

class  ImpactPlotter;
struct ImpactPlotConfig; // to config the impact plotter

// service class to set up bins and channels for which the impact is
// to be plotted. 
// Data: either serialised database == vector<TEntry*>
//       or     SMxs, intXS, smWidths, intWidths
struct ImpactPlotConfig {
	using Widths      = map<string,         Width>;
	using WidthsPerWc = map<string, map<WC, Width>>;
	using XSS         = map<string,         ValErr<double>>;
	using XSperWC     = map<WC,             XSS>;

	//vector<size_t>   truth_bins {};
	vector<string>     bins_;
	vector<string>     channels {};
	vector<WCcodes>    groupsWCcodes {};
	map<WC, float>     wcScaling_{};

	string encodingMode { "" }; // serialisation, linearParametrisation <- from modules 

	vector<eft::serialisation::TEntry*> db_{};

	//eft::modules::LinearParametrisation linParam_;
	map<string, ValErr<double>> smXSS_;
	Widths           SMwidths_;

	map<WC, map<string, ValErr<double>>> interfXS_;
	WidthsPerWc       interfWidths_;
	//vector<eft::serialisation::tePtr>     db_;
};



class ImpactPlotter
{
public:
	//explicit ImpactPlotter(ImpactPlotConfig& config); // not const, so we can move it
	explicit ImpactPlotter(const ImpactPlotConfig* const config);

	inline size_t GetWCgroupIdx(const WC code) const;
	void Plot() const;
private:
	void CheckInit() const;
	void PlotFromSerialised() const;
	void PlotFromModulesLinParametrisation() const;

private:
	const ImpactPlotConfig* const config_;
	//const std::unique_ptr<ImpactPlotConfig> config_;
};

inline size_t ImpactPlotter::GetWCgroupIdx(const WC code) const {
	if (config_->groupsWCcodes.empty())
		throw std::runtime_error("[ImpactPlotter::GetWCgroupIdx] config is not set up");

	const vector<WCcodes>& data = config_->groupsWCcodes;

	for (size_t idx = 0; idx < data.size(); ++idx) {
		for (const auto this_code: data.at(idx)) {
			if (this_code == code)
				return idx;
		}
	}
	return 999;
}

} // namespace plot
} // namespace eft

#endif // !IMPAT_PLOTTER_H
