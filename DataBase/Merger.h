#pragma once
#ifndef MERGER_H
#define MERGER_H

#include "TEntry.h"

#include <map>
#include <string>
#include <vector>
#include <set>

#include <fstream>

#include "Core.h"



using std::map;
using std::string;
using std::vector;
using std::set;

namespace eft {

struct MergeData {
	map<size_t, size_t> mergingIdxIdx_;
	map<string, string> mergingStrings_;
	vector<string>      mergedNames_;
	vector<string>      initNames_;

	MergeData() = default;
	~MergeData() {}

	inline const string GetMergedFromInit(const string& raw) const;
	inline const string GetMergedFromInit(size_t idx) const;
	inline const string GetMergedStrNb(size_t idx) const { return mergedNames_[idx]; }
	inline const string GetInitStrNb(size_t idx) const;


	MergeData(const map<size_t, size_t>& merging,
		const vector<string>& mergedNames,
		const vector<string>& initNames);

	explicit MergeData(const map<string, vector<string>>& merging);
	//explicit MergeData(istream& is);
	static        MergeData ReadFromJSON(istream& is); 
	static inline MergeData ReadFromJSON(const string& path);
	void                    WriteToJSON(std::ostream& os);

	void SetMerging(const map<size_t, size_t>& merging);
	inline void SetInitNames(const vector<string>& names)   { initNames_ = names; }
	inline void SetMergedNames(const vector<string>& names) { mergedNames_ = names; }
};


class Merger 
{
public:
	Merger() = default;
	~Merger() {}

	size_t GetMergedIdx(size_t idx_raw, const string& mergingScheme = "") const;
	string GetMergedStr(const string& raw, const string& mergingScheme = "") const;
	string PrintAvailableIdxAsString() const;

	inline void               AddMergingScheme(const string& name, MergeData& merging);
	inline void               AddMergingScheme(const string& name, 
											   const map<size_t, size_t>& merging,
											   const vector<string>& mergedNames,
											   const vector<string>& initNames);
	inline void               AddMergingScheme(const string& name, const string& jsonPath);
	inline void               SetInitNames(const vector<string>& names) { initNames_ = names; }
	inline void               SetCurrentMerging(const string& merging);
	inline void               SetMergedNames(const string& scheme, const vector<string>& names);

	inline const set<string>& GetMergingSchemes() const { return mergingsRegistry_; }
	inline       set<string>& GetMergingSchemes()       { return mergingsRegistry_; }
	inline const MergeData& GetMergingScheme(const string& name) const { return mergingPerScheme_.at(name); }
	inline       MergeData& GetMergingScheme(const string& name) { return mergingPerScheme_[name]; }

	inline       vector<string>& GetInitNames()       { return initNames_; }
	inline const vector<string>& GetInitNames() const { return initNames_; }
private:
	map<string, MergeData> mergingPerScheme_{};
	vector<string>         initNames_{};
	set<string>            mergingsRegistry_{};
	string                 currentMerging_{""};
	//map<size_t, size_t> merging_;
	//map<string, string> mergingStrings_;
	//vector<string>      mergedNames_;
	
};

inline const string eft::MergeData::GetInitStrNb(size_t idx) const {
    //EFT_PROFILE_FN();
	if (idx >= initNames_.size())
		return "NONE";
	return initNames_[idx]; 
}

inline const string eft::MergeData::GetMergedFromInit(const string& raw) const {
	if (mergingStrings_.find(raw) == mergingStrings_.end()) {
		string allInit;
		for (const auto& init_merged : mergingStrings_) {
			allInit += " ";
			allInit += init_merged.first;
		}
		//throw eft::serialisation::SerialisationError("idx: " + raw + " is not in the map."
		// + " Available: " + allMerged);
		//std::cout << "[GetMergedFromInit{" << raw << "} WARNING: is not present."
		//	<< " available: " << allInit;
		EFT_ERROR(raw + " is not present in the merging scheme");
		return "NONE";
	}

	return mergingStrings_.at(raw);
}

inline const string eft::MergeData::GetMergedFromInit(size_t idx) const
{
	return MergeData::GetMergedFromInit(initNames_[idx]);
}

inline MergeData MergeData::ReadFromJSON(const string& path)
{
	std::ifstream fs(path);
	return MergeData::ReadFromJSON(fs);
}


inline void Merger::AddMergingScheme(const string& name, MergeData& merging) {
	mergingPerScheme_[name] = std::move(merging);
	mergingsRegistry_.insert(name);
}

inline void Merger::AddMergingScheme(const string& name, const map<size_t, size_t>& merging, const vector<string>& mergedNames, const vector<string>& initNames)
{
	MergeData mergeData(merging, mergedNames, initNames);
	AddMergingScheme(name, mergeData);
}

inline void Merger::AddMergingScheme(const string& name, const string& jsonPath) {
	mergingPerScheme_[name] = std::move(MergeData::ReadFromJSON(jsonPath));
	mergingsRegistry_.insert(name);
}

inline void Merger::SetCurrentMerging(const string& merging) {
	if (mergingPerScheme_.find(merging) == mergingPerScheme_.end())
		std::cout << "[WARNING] set merging scheme: |" << merging << "|"
					<< " is not present in the registry" << std::endl;

	currentMerging_ = merging;
}

inline void Merger::SetMergedNames(const string& scheme, const vector<string>& names) {
	if (mergingPerScheme_.find(scheme) == mergingPerScheme_.end())
		std::cout << "[WARNING] set merging scheme: |" << scheme << "|"
		<< " is not present in the registry" << std::endl;

	mergingPerScheme_[scheme].SetMergedNames(names);
}


} // namespace eft

#endif
