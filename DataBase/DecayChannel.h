#pragma once

#include <iostream>

namespace eft {

enum class DecayChannel;
std::ostream& operator << (std::ostream& os, const DecayChannel val);

enum class DecayChannel {
    TOTA                = 0,
    L_NU_L_N            = 10,
    TAU_NU_TAU_N        = 11,
    L_NU_TAU_NU         = 12,
    L_NU_J_J            = 13,
    QQQQ                = 14,
    LLLL                = 15,
    TAU_TAU_TAU_TAU     = 16,
    NU_NU_NU_NU         = 17,
    L_L_TAU_TAU         = 18,
    NU_NU_TAU_TA        = 19,
    J_J_L_L             = 20,
    J_J_NU_N            = 21,
    GAM_GAM             = 22,
    B_B                 = 23,
    C_C                 = 24,
    S_S                 = 25,
    E_E                 = 26,
    MU_M                = 27,
    TAU_TAU             = 28,
    Z_GA                = 29,
    G_G                 = 30,
    NONE                = 999,
};


//class DecayChannel
//{
//};

}

