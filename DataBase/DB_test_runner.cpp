#include "../Utils/test_runner.h"

#include "TEntry.h"
#include "WIdthOrXSsEntry.h"
#include "LinearParametrisation.h"
#include "QuadParametrisation.h"

#include "EntryParser.h"

#include <sstream>
#include <ostream>
#include <string>
#include <fstream>

using namespace std;
using namespace eft;
using namespace eft::serialisation;

void TestTEntrySimpleSerialisation() {
	using ::eft::serialisation::tab;
	ostringstream os;
	TEntry entry;
	entry.setVersion("version1");
	entry.setThRegime(TheoryRegime::BSM);

	entry.serialise(os);

	istringstream expected(R"(EntryType::NOT_DEFINED
	Version version1
	ThRegime TheoryRegime::BSM
TEntry::End
)");

	CompareStrings(expected.str(), os.str());
	//ASSERT_EQUAL_SSTREAMS(os, expected);
	ASSERT_EQUAL(os.str(), expected.str());
}

void TestTEntrySimpleUnserialisation() {
	istringstream data(R"(
EntryType::NOT_DEFINED
	Version version1
	ThRegime TheoryRegime::BSM
TEntry::End)");
	auto entry = TEntry::unserialise(data);
}

void TestTEntryWriteRead() {
	{
		
	}
}

void TestWidhtAnXsSimpleSerialisation() {
	ostringstream os;
	WidthOrXSsEntry entry;
	entry.setEntryType(EntryType::WIDTH);
	entry.setVersion("version_test");
	entry.setThRegime(TheoryRegime::INT);
	entry.setChannel("yy");
	entry.setFileName("myFile.root");
	entry.setNbEbents(228);
	entry.setWidth({ 0.1, 0.02 });
	ofstream ofs("tests___.txt");

	cout << " is fs open? " << boolalpha << ofs.is_open() << endl;

	entry.serialise(ofs);
	ofs.close();

	ifstream fs("tests___.txt");
	auto entry_read = TEntry::unserialise(fs);
	fs.close();
	ASSERT_EQUAL(entry_read->getEntryType(), entry.getEntryType());

	WidthOrXSsEntry* read = static_cast<WidthOrXSsEntry*>(entry_read);

	ASSERT_EQUAL(read->getThRegime(), entry.getThRegime());
	ASSERT_EQUAL(read->getVersion(), entry.getVersion());
	ASSERT_EQUAL(read->getChannel(), entry.getChannel());
	ASSERT_EQUAL(read->getWCcode(), entry.getWCcode());
	ASSERT_EQUAL(read->getWCvalue(), entry.getWCvalue());
	ASSERT_EQUAL(read->getWidth(), entry.getWidth());
	remove("tests___.txt");
}

void TestParseMap() {
	using ::eft::serialisation::tab;

	istringstream is(R"(
{
	1 one
	2 two
	3 three
}
)");


	auto res = ::eft::serialisation::parseMap<int, string>(is);
	ASSERT(res.count(1) != 0);
	ASSERT(res.count(2) != 0);
	ASSERT(res.count(3) != 0);
	ASSERT(res.size() == 3);

	ASSERT_EQUAL(res[1], "one");
	ASSERT_EQUAL(res[2], "two");
	ASSERT_EQUAL(res[3], "three");
}

void TestParseVector() {

	{	
		istringstream is(R"(
{ 1 2 3 4 5 6 7 8 9 }
)");
		using ::eft::serialisation::tab;

		auto res = ::eft::serialisation::parseVector<size_t>(is);
		ASSERT(res.size() == 9);

		ASSERT_EQUAL(res[0], 1);
		ASSERT_EQUAL(res[1], 2);
		ASSERT_EQUAL(res[2], 3);
	}
	{
		istringstream is(R"(
{1 2 3 4 5 6 7 8 9 }
)");
		using ::eft::serialisation::tab;

		auto res = ::eft::serialisation::parseVector<size_t>(is);
		ASSERT(res.size() == 9);

		ASSERT_EQUAL(res[0], 1);
		ASSERT_EQUAL(res[1], 2);
		ASSERT_EQUAL(res[2], 3);
	}
	{
		istringstream is(R"(
{1 2 3 4 5 6 7 8      9}
)");
		using ::eft::serialisation::tab;

		auto res = ::eft::serialisation::parseVector<size_t>(is);
		ASSERT(res.size() == 9);

		ASSERT_EQUAL(res[0], 1);
		ASSERT_EQUAL(res[1], 2);
		ASSERT_EQUAL(res[2], 3);
	}
	{
		istringstream is(R"(
{
	1

	2
	3
}
)");


		auto res = ::eft::serialisation::parseVector<size_t>(is);
		ASSERT(res.size() == 3);

		ASSERT_EQUAL(res[0], 1);
		ASSERT_EQUAL(res[1], 2);
		ASSERT_EQUAL(res[2], 3);
	}
	{
		using ::eft::serialisation::tab;

		istringstream is(R"(
{
	one two three
}
)");

		auto res = ::eft::serialisation::parseVector<string>(is);
		ASSERT(res.size() == 3);

		ASSERT_EQUAL(res[0], "one");
		ASSERT_EQUAL(res[1], "two");
		ASSERT_EQUAL(res[2], "three");
	}
}

void TestParseMatrix() {
	{
		//stringstream is;
		using ::eft::serialisation::tab;

		istringstream is(R"(
{
	{1 2 3}
	{4 5 6}
}
)");

		auto res = ::eft::serialisation::parseMatrix<size_t>(is);
		ASSERT(res.size() == 2);

		ASSERT_EQUAL(res[0][0], 1);
		ASSERT_EQUAL(res[0][1], 2);
		ASSERT_EQUAL(res[0][2], 3);

		//cout << "res: " << res << endl;
	}
}


void TestLinearParametrisatinSerialisation() {
	using namespace eft::serialisation;
	ostringstream os;

	{
		LinearParametrisation entry;
		entry.setVersion("version_test");
		entry.setThRegime(TheoryRegime::INT);
		entry.setChannel(DecayChannel::GAM_GAM);
		entry.setParametrisationType(ParametrisationType::BR);
		vector<double> coeffs{ 0.1, 0.2, 0.3 };
		vector<string> names{ "first", "second", "third"};
		entry.setCoeffs(coeffs);
		entry.setNames(names);

		ofstream ofs("tests___.txt");

		cout << " is fs open? " << boolalpha << ofs.is_open() << endl;
		entry.serialise(ofs);
		ofs.close();

		ifstream fs("tests___.txt");

		/*cout << "what we got: " << endl;
		string line;
		while (getline(fs, line)) {
			cout << line << endl;
		}*/

		auto entry_read = TEntry::unserialise(fs);
		fs.close();
		ASSERT_EQUAL(entry_read->getEntryType(), entry.getEntryType());

		LinearParametrisation* read = static_cast<LinearParametrisation*>(entry_read);

		//ASSERT_EQUAL(read->getThRegime(), entry.getThRegime());
		ASSERT_EQUAL(read->getVersion(), entry.getVersion());
		ASSERT_EQUAL(read->getNames(), entry.getNames());
		remove("tests___.txt");
	}
}

void TestTypeReading() {
	using eft::serialisation::tab;
	{
		stringstream ss;
		istringstream is(R"(
EntryType::WIDTH
	Version 1
TEntry::End
	WIDTH 1.2 +- 3.45
WidthOrXssEntry::End
)");
		auto entry = TEntry::unserialise(is);
		ASSERT_EQUAL(entry->getEntryType(), EntryType::WIDTH);
	}
	{
		istringstream is(R"(
EntryType::XSS
	Version 1
TEntry::End
	XSS {
		1 23.4 +- 5.6
		2 34.5 +- 7.8
		3 45.6 +- 8.90 
	}
WidthOrXssEntry::End
)");
		/*;
		ss << "EntryType::XSS" << endl;
		ss << "Version 1" << endl;
		ss << "TEntry::End" << endl;
		ss << tab << "XSS { "
			<< "1 " << " 23.4 +- 5.6"
			<< tab << tab << "2 " << " 34.5 +- 5.6"
			<< tab << tab << "3 " << " 45.6 +- 5.6"
			<< tab << tab << "}" << endl;
		ss << "WidthOrXssEntry::End";*/
		/*cout << "to unserialise: \n" << endl;
		cout << ss.str() << endl;
		cout << "start unserialiasion" << endl;

		cout << "symbols: " << endl;
		for (const char c : ss.str()) {
			cout << "|" << c << "|" << endl;
		}*/

		auto entry = TEntry::unserialise(is);
		ASSERT_EQUAL(entry->getEntryType(), EntryType::XSS);
	}

}


void TestParseExpression() {
	stringstream is;
	is << "{ 0.25 * n1 + 0.37 * n2 - 0.4 * n3}";

	vector<string> names;
	vector<double> vals;
	parseExpression(is, names, vals);
}

void TestLinearExpressionWidth() {
	istringstream is(R"(
{
	-40.15 * CHB - 13.08 * CHW + 22.40 * CHB - 0.9463 * CHW + 0.1212 * CHBOX - 0.2417 * CHD
	+ 0.03447 * CUH - 1.151 * CUW - 2.150 * CUB - 0.3637 * CHL3 + 0.1819 * CLL
}
)");

	vector<string> names;
	vector<double> vals;
	parseExpression(is, names, vals);

	ASSERT_EQUAL(names.size(), 11);
	ASSERT_EQUAL(vals.size(), 11);
	ASSERT_EQUAL(names[0], "CHB");
	ASSERT_EQUAL(vals[0], -40.15);

	/*cout << "names: " << endl;
	for (const string& name : names) {
		cout << name << endl;
	}
	for (const auto val : vals) {
		cout << val << endl;
	}*/
}

void TestLinParam() {
	istringstream is(R"(
EntryType::LINEAR_PAREMETRISATION
	Version try_this_version
	ThRegime ThRegime::INT
TEntry::End
	Expression { -40.15 * CHB - 13.08 * CHW + 22.40 * CHB - 0.9463 * CHW + 0.1212 * CHBOX - 0.2417 * CHD + 0.03447 * CUHRE - 1.151 * CUWRE - 2.150 * CUBRE - 0.3637 * CHL3 + 0.1819 * CLL1 }
LinearParametrisation::End
)");
	auto entry = TEntry::unserialise(is);
	ASSERT_EQUAL(entry->getEntryType(), EntryType::LINEAR_PAREMETRISATION);
}

void TestParseFunctions() {
	TestRunner tr;
	RUN_TEST(tr, TestParseMap);
	RUN_TEST(tr, TestParseVector);
	RUN_TEST(tr, TestParseMatrix);
	RUN_TEST(tr, TestParseExpression);
}

void TestRealDB() {
	ifstream fs("test_db_3.txt");
	//TEntry* res = TEntry::unserialise(fs).get();
	auto res = TEntry::unserialise(fs);

	throw std::runtime_error("*********** FINISH TestRealDB() TEST****");
	//cout << "what we got after unserialisation: " << endl;
	//res->serialise(cout);
	//cout << "----" << endl;
	//cout << "try to change type...." << endl;
	//WidthOrXSsEntry* res_typed = (WidthOrXSsEntry*) res->clone();
	//WidthOrXSsEntry* res_typed = reinterpret_cast<WidthOrXSsEntry*>(res.get());
	//res_typed->serialise(cout);
}

void TestEntries() {
	TestRunner tr;
	RUN_TEST(tr, TestTEntrySimpleSerialisation);
	RUN_TEST(tr, TestTEntrySimpleUnserialisation);
	RUN_TEST(tr, TestWidhtAnXsSimpleSerialisation);
	RUN_TEST(tr, TestTypeReading);
	RUN_TEST(tr, TestLinearExpressionWidth);
	RUN_TEST(tr, TestLinParam);
	//RUN_TEST(tr, TestLinearParametrisatinSerialisation);
}


void TestDB() {
	TestRunner tr;
	RUN_TEST(tr, TestRealDB);
}