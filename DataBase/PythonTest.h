//#pragma once

//#include <boost/python.hpp>
//#define BOOST_ALL_DYN_LINK
//#define BOOST_PYTHON_DYNAMIC_LIB
#include <boost/regex.hpp>
//#include <boost/python.hpp>

int testBoost() {
    std::string line;
    boost::regex pat("^Subject: (Re: |Aw: )*(.*)");

    while (std::cin)
    {
        std::getline(std::cin, line);
        boost::smatch matches;
        if (boost::regex_match(line, matches, pat))
            std::cout << matches[2] << std::endl;
    }
    return 0;
}

//namespace eft {
	const char* example() {
		return "hi";
	}
//}

//BOOST_PYTHON_MODULE(eft) {
//	using namespace boost::python;
//	def("example", example);
//}