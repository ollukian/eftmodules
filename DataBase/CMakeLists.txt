#add_library(DataBase STATIC TEntry EntryParser WIdthOrXSsEntry QuadParametrisation LinearParametrisation ImpactPlotter Merger)

set(database_source_files
        EntryParser.cpp
        EntryParser.h
        WIdthOrXSsEntry.cpp
        WIdthOrXSsEntry.h
        QuadParametrisation.cpp
        QuadParametrisation.h
        LinearParametrisation.cpp
        LinearParametrisation.h
        ImpactPlotter.cpp
        ImpactPlotter.h
        ParseResults.cpp
        ParseResults.h
        #Merger.cpp
        #Merger.h
        ../Core/Core.h)

add_library(database ${database_source_files} ../Core/Core.h)
target_link_libraries(database core)