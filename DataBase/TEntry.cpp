#include "TEntry.h"
#include "EntryParser.h"
//#include "EFTManager/profile.h"
#include "../Utils/profile.h"

#include <sstream>
#include <fstream>

using namespace std;
using namespace eft::serialisation;

namespace eft {
//namespace serialisation {


std::map<string, serialisation::TEntry*> serialisation::TEntry::mapEntries;

namespace serialisation {
//tePtr TEntry::unserialise(istream& is)
TEntry* TEntry::unserialise(istream& is)
{
	cout << "[TEntry::unserialise] detect type... " << endl;
	string type;
	parseEntryType(is, type);
	cout << "[TEntry::unserialise] type --> " << type << endl;

	if (type == "NOT_DEFINED")
		return TEntry(is).clone();
		//return std::make_shared<eft::serialisation::TEntry>( * new TEntry(is) );

	if (mapEntries.count(type) == 0) {
		throw SerialisationError("for the entry type: "
			+ type
			+ " there's no serialisatin ctor --> add mapEntries["
			+ type
			+ "] = new "
			+ type
			+ "()"
			+ " to the source file - to the ctor of the class"
		);
	}
	
	//return mapEntries[type]->create(is)->clone();
	return mapEntries[type]->create(is);
	
	
	//return std::make_shared<eft::serialisation::TEntry>(* mapEntries[type]->create(is));
	//eturn std::make_shared<eft::TEntry>();
}

void TEntry::serialise(std::ostream& os) const
{
	//using ::eft::serialisation::tab;
	//using tab = "\t";
	//char tab = '\t';

	std::cout << "[TEntry::serialise]" << endl;
	os  << entryType_ << '\n'
	    << tab << "Version "<< version_  << '\n'
		<< tab << "ThRegime "   << thRegime_ << '\n'
		<< "TEntry::End" << '\n';
}

TEntry::TEntry(std::istream& is)
{
#undef DEBUG
//#define DEBUG
#ifdef DEBUG
#define OUT std::cout
#define FOUT std::cout << "[TEntry::ctor] "
#else
	stringstream __stringStreamMineInner;
#define OUT __stringStreamMineInner 
#define FOUT __stringStreamMineInner
	//#define OUT __stringStreamMineInner stringstream;
#endif // DEBUG
	FOUT << "{TEntry} part...." << endl;

	string line, token;
	while (is >> token) {
	//while (getline(is, line)) {
		//istringstream ss(line);
		//ss >> token;
		OUT << " token: " << token << endl;
		if (token == "TEntry::End") {
			OUT << "end forming" << endl;
			return;
			break;
		}
		
		if (token == "Version") 
			this->version_ = parseName(is);
		else if (token == "ThRegime")
			this->thRegime_ = parseThRegime(is);
		else
			throw ParseException("[TEntry::ctor] -> can't parse token: |"
				+ token + "|. available: "
				+ " version, thRegime, TEntry::End");
	}

#undef OUT
#undef FOUT
}

std::string entryToStr(const EntryType type)
{
	std::ostringstream os;
	os << type;
	return os.str();
}



const vector<TEntry*> TEntry::readDB(const string& filename) {
#define DEBUG
#ifdef DEBUG
#define OUT std::cout
#define FOUT std::cout << "[readDB] "
#else
	stringstream __stringStreamMineInner;
#define OUT __stringStreamMineInner 
//#define OUT __stringStreamMineInner stringstream;
#endif // DEBUG

	FOUT << "read the database from the file: " << filename << endl;
	ifstream fs(filename, std::ios_base::in);

	if (!fs.is_open())
		throw std::runtime_error("impossible to open: " + filename);

	std::vector<TEntry*> res;
	//std::vector<tePtr> res;
	//size_t nb = 1;
	map<EntryType, size_t> stats;
	while (fs) {
		//OUT << " deal with obj #" << nb++ << endl;
		auto obj = TEntry::unserialise(fs);
		//OUT << " type of obj# " << nb - 1 << " -> " << obj->getEntryType() << endl;
		//OUT << "lets' see what we got: " << endl;
		//obj->serialise(cout);
		res.push_back(obj->clone());
		//res.push_back(make_shared<TEntry>(*obj->clone()));
		//OUT << "let's see what happens after push_back: " << endl;
		//res.back()->serialise(cout);
		//cout << "****" << endl;
		//res.push_back(obj->clone());
		stats[obj->getEntryType()]++;
		absorbSpaces(fs);
	}
	FOUT << " extracting from the file: " << filename << " is done"
		<< res.size() << "objects are extracted:" << endl;

	for (const auto& type_nb : stats) {
		OUT << "*" << setw(15) << type_nb.first << " "
			<< setw(5) << type_nb.second << "*" << endl;
	}

	return res;
#undef FOUT
#undef OUT
}

} // namespace serialisation
} // namespace eft