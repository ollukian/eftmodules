
#include "WIdthOrXSsEntry.h"
#include "TEntry.h"
#include "ValErr.h"
#include "EntryParser.h"
#include "LinearParametrisation.h"
#include "QuadParametrisation.h"

#include <sstream>

using namespace std;
using namespace eft;
using namespace eft::serialisation;

eft::serialisation::WidthXsEntryInialiser::WidthXsEntryInialiser() {
	::eft::serialisation::TEntry::mapEntries["WidthXsEntry"] = new ::eft::serialisation::WidthOrXSsEntry();
	::eft::serialisation::TEntry::mapEntries["WIDTH"]        = new ::eft::serialisation::WidthOrXSsEntry();
	::eft::serialisation::TEntry::mapEntries["XSS"]          = new ::eft::serialisation::WidthOrXSsEntry();
	std::cout << "[STATIC] {Register types: WIDTH, XSS}" << endl;
}

static WidthXsEntryInialiser __WidthXsEntryInialiser;
static LinearParametrisationInitialiser __LinearParametrisationInitialiser;
static QuadParametrisationInitialiser __QuadParametrisationInitialiser;

namespace eft {
namespace serialisation {

	void ::eft::serialisation::WidthOrXSsEntry::serialise(std::ostream& os) const
	{
		using ::eft::serialisation::tab;
		std::cout << "[serialise]{WidthOrXSsEntry}" << std::endl;
		//os << "EntryType::WidthOrXSsEntry\n";
		TEntry entry(*this);
		//TEntry* entry = this->clone();
		entry.serialise(os);
		//return;
		//os << tab << "WidthOrXS " << entryType_ << '\n';
		//os << "\t ThRegime " << thRegime_ << endl;
		os << tab << "CHANNEL " << channel_ << '\n';

		if (thRegime_ == TheoryRegime::BSM) {
			os << tab << "WC_code " << wcCode1_ << '\n';
			os << tab << "WC_code " << wcCode2_ << '\n';

			os << tab << "WC_val " << wcVal1_ << '\n';
			os << tab << "WC_val " << wcVal2_ << '\n';
		}
		else if (thRegime_ == TheoryRegime::INT) {
			os << tab << "WC_code " << wcCode1_ << '\n';
			os << tab << "WC_val " << wcVal1_ << '\n';
		}

		
		if (entryType_ == EntryType::WIDTH) {
			serialiseWidth(os);
		}
		else if (entryType_ == EntryType::XSS) {
			serialiseXss(os);
		}

		if ( ! filename_.empty() )
			os << tab << "FileName " << filename_ << '\n';
		os << "WidthOrXSsEntry::End" << '\n';
		os << "\n\n";
	}

	WidthOrXSsEntry::WidthOrXSsEntry(std::istream& is)
		: TEntry(is)
	{
		cout << "[WidthOrXSsEntry::ctor]" << endl;
		
		string line, token;
		while (is >> token) {
		//while (getline(is, line)) {
			//istringstream ss(line);
			//ss >> token;
			cout << "[WidthOrXSsEntry::ctor] token: " << token << " ==> ";
			if (token == "WidthOrXSsEntry::End" || token == "WidthOrXssEntry::End") {
				//cout << "end!" << endl;
				//cout << "let's see what we got: " << endl;
				//this->serialise(cout);
				//cout << "---" << endl;
				return;
			}
			if (token == "XSS") {
				setEntryType(EntryType::XSS);
				xss_ = parseMap<size_t, ValErr<double>>(is);  //parseXsMap(is);
				cout << xss_.size() << " entries to the XS\n";
			}
			else if (token == "WIDTH") {
				setEntryType(EntryType::WIDTH);
				width_ = parseValErr(is);
				cout << width_ << '\n';
			}
			else if (token == "XS_NAMES") {
				xsNames_ = parseVector<string>(is);
				cout << xsNames_.size() << " entries to the XS names\n";
			}
			else if (token == "CHANNEL" || token == "Channel") {
				is >> channel_;
				cout << channel_ << '\n';
			}
			else if (token == "WC_code") {
				wcCode1_ = parseWCcode(is);
				cout << wcCode1_ << '\n';
			}
			else if (token == "WC_val") {
				wcVal1_ = parseWCvalue(is);
				cout << wcVal1_ << '\n';
			}
			else if (token == "FileName") {
				is >> filename_;
				cout << filename_ << '\n';
			}
			else
				throw ParseException("got token: " + token
					+ ". Available: WIDTH, XSS, XS_NAMES, WidthOrXSsEntry::End, CHANNEL, FileName, WC_val, WC_code");

		}
		throw ParseException("No WidthOrXSsEntry::End token found at the end of the file.");
	}

} // namespace serialisation
} // namespace eft
