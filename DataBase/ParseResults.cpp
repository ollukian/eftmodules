#include "ParseResults.h"

#include <iomanip>

using namespace std;

namespace eft {

//////////////////////////////////////////////////////////////////////////////////////
ostream& operator << (ostream& os, const ParseResults& res) { // TODO: add printing of the WC 
    os << setfill('=') << setw(50) << '=' << endl << setfill(' ');
    os << "| Parse Results: " << setw(33) << "|" << endl;
    os << setfill('=') << setw(50) << '|' << endl << setfill(' ');
    os << "| " << setw(20) << "events: " << setw(15) << res.nbGeneratedEvents << setw(14) << " | " << endl;
    os << "| " << setw(20) << "regime: " << setw(15) << res.regime << setw(14) << " | " << endl;
    os << "| " << setw(20) << "TheoryRegime: " << setw(15) << res.theoryRegime << setw(14) << " | " << endl;

    if (res.theoryRegime == eft::TheoryRegime::INT) {
        //os << "| " << setw(20) << "wcvalue: "     << setw(15) << res.wcValue           << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcvalueCode: " << setw(15) << res.wcValueCode << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcid: " << setw(15) << res.wc.GetId() << setw(14) << " | " << endl;
        os << "| " << setw(35) << res.wcCode << setw(14) << " | " << endl;
    }
    if (res.theoryRegime == eft::TheoryRegime::BSM) {
        //os << "| " << setw(20) << "wcvalue1: "    << setw(15) << res.wcValue           << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcvalueCode1: " << setw(15) << res.wcValueCode << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcid1: " << setw(15) << res.wc.GetId() << setw(14) << " | " << endl;
        os << "| " << setw(35) << res.wcCode << setw(14) << " | " << endl;
        //os << "| " << setw(20) << "wcvalue2: "    << setw(15) << res.wcValue           << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcvalueCode:2 " << setw(15) << res.wcValueCode << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcid2: " << setw(15) << res.wc.GetId() << setw(14) << " | " << endl;
        os << "| " << setw(35) << res.wcCode2 << setw(14) << " | " << endl;
    }
    if (res.regime == eft::Regime::DECAY) {
        os << "| " << setw(20) << "decayChannel: " << setw(26) << res.decayChannel << setw(1) << "| " << endl;
        os << "| " << setw(20) << "Width: " << setw(15) << res.width_.val << " +-" << res.width_.err << setw(2) << "| " << endl;
    }
    else {
        os << "| " << setw(20) << "prodMode: " << setw(15) << res.prodMode << setw(14) << " | " << endl;
    }
    os << setfill('=') << setw(50) << '=' << endl << setfill(' ');
    return os;
}
//////////////////////////////////////////////////////////////////////////////////////
}