#pragma once

#ifndef SERIALISABLE_H
#define SERIALISABLE_H

#include <iostream>

using std::istream;


namespace eft {
namespace serialisation {

template <class Entry>
using ePtr = std::shared_ptr<Entry>;

class Serialisable
{
public:
	virtual ~Serialisable() {}
	virtual Serialisable* clone() const = 0;
	virtual Serialisable* create(std::istream& is) const = 0;
};


} // namespace serialise
} // namespace eft
#endif // !SERIALISABLE_H