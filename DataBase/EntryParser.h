#pragma once

#ifndef ENTRY_PARSER_H
#define ENTRY_PARSER_H


#include "TEntry.h"
#include "WIdthOrXSsEntry.h"
#include "ValErr.h"
#include "LinearParametrisation.h"

#ifdef	_MSC_VER
#include "../EFTManager/Parser.h"
#else
#include "../EFTManager/Parser.h"
#endif

#include <stdexcept>
#include <iostream>
#include <istream>
#include <iomanip>

namespace eft {
namespace serialisation {
    //using namespace serialisation;



class ParseException : public std::runtime_error {
public:
    ParseException(const std::string& what = "")
        : std::runtime_error("[ParseException]: " + what) {}
};

enum class ParseTokens {
    VERSION,
    TH_REGIME,
    CLASS_NAME,
    WC_CODE,
    WC_VAL,
    WIDTH,
    XSS,
    NB_EVENTS,
    FILENAME,
    COMP_ORDER,
    CHANNEL,
    TYPE
};

struct TokenBase {};



EntryType                parseEntryType(std::istream& is, std::string& type); // type <- here the type will be also written
void                     parseTokens(std::vector<ParseTokens>& tokens, tePtr& entry);
inline const std::string parseName(std::istream& is);
TheoryRegime             parseThRegime(std::istream& is);
DecayChannel             parseChannel(std::istream& is);
inline WilsonCoefficientsCode   parseWCcode(std::istream& is);
inline WCvalue                  parseWCvalue(std::istream& is);
inline ParametrisationType parseParametrisatinType(std::istream& is);
inline std::map<size_t, ValErr<float>> parseXsMap(std::istream& is);
inline ValErr<double>    parseValErr(std::istream& is);
void                     parseExpression(std::istream& is, 
                            std::vector<std::string>& names, 
                            std::vector<double>& vals);
inline void              absorbSpaces(std::istream& is);

template <typename Key, typename Val>
std::map<Key, Val>       parseMap(std::istream& is);

template <typename T>
std::vector<T>           parseVector(std::istream& is);

template <typename T>
std::vector<std::vector<T>> parseMatrix(std::istream& is);

void CompareStrings(const std::string& s1, const std::string& s2);

std::ostream& operator << (std::ostream& os, const EntryType& entry);

///////////////////////////////////////////////////////////////////////////////////
inline const std::string parseName(std::istream& is) {
    std::cout << "[parseName]... ";
    std::string res;
    is >> res;
    std::cout << res << std::endl;
    return res; // mo move to allow copy ellision
}
///////////////////////////////////////////////////////////////////////////////////
inline ParametrisationType parseParametrisationType(std::istream& is) {
    std::string type;
    is >> type;
    if      (type == "ParametrisationType::BR"    || type == "BR")      return ParametrisationType::BR;
    else if (type == "ParametrisationType::MU"    || type == "MU")      return ParametrisationType::MU;
    else if (type == "ParametrisationType::WIDTH" || type == "WIDTH")   return ParametrisationType::WIDTH;
    else if (type == "ParametrisationType::PRODUCTION"|| type == "PRODUCTION") return ParametrisationType::PRODUCTION;
    else if (type == "ParametrisationType::DECAY" || type == "DECAY")   return ParametrisationType::DECAY;
    else if (type == "ParametrisationType::XS"    || type == "XS")      return ParametrisationType::XS;
    else if (type == "ParametrisationType::NONE"  || type == "NONE")    return ParametrisationType::NONE;
    throw std::runtime_error("[parseParametrisatinType] impossible to parse: " + type
        + ". available: BR, MU, WIDTH, XS, NONE, DECAY, PRODUCTION");
}
///////////////////////////////////////////////////////////////////////////////////
template <typename Key, typename Val>
std::map<Key, Val> parseMap(std::istream& is) {
    std::map<Key, Val> res;
    std::cout << "[parseMap]" << std::endl;
    absorbSpaces(is);
    Key key{};
    Val val{};
    char c;
    is >> c;

    if (c != '{')
        throw ParseException("[parseMap] expected first { character. Got: " + static_cast<char>(c));
    is.get();

    while (static_cast<char>(is.peek()) != '}') {
        is >> key >> val;
   
        res[key] = std::move(val);
        absorbSpaces(is);
    }
    is.get(); // to get the last '}' character
    return res; // no move to allow elision
}
///////////////////////////////////////////////////////////////////////////////////
std::map<size_t, ValErr<float>> parseXsMap(std::istream& is) {
    std::map<size_t, ValErr<float>> res;

    size_t key;
    ValErr<float> val;

    absorbSpaces(is);

    char c;
    is.get(c); // to read the first '{' character
    if (c != '}')
        throw ParseException("[parseXsMap] expected first '}' character. Got: " + c);

    while (is.peek() != '}') {
        is >> key >> val;
        res[key] = std::move(val);
        absorbSpaces(is);
    }
    is.get(); // to read '}' character
    return res; // mo move to allow copy ellision
}
///////////////////////////////////////////////////////////////////////////////////
template <typename T>
std::vector<T> parseVector(std::istream& is) {
    std::vector<T> res;

    T elem{};

    absorbSpaces(is);

    char c;
    is.get(c); // to read the first '{' character
    if (c != '{') {
        std::string error_char{'|'};
        error_char.push_back(c);
        error_char.push_back('|');
        throw ParseException("[parseVector] expected first |{| character. Got: " + error_char);
    }

    while (static_cast<char>(is.peek()) != '}') {
        //char nextChar = static_cast<char>(is.peek());
        is >> elem;
        //nextChar = static_cast<char>(is.peek());
        absorbSpaces(is);

        res.push_back(std::move(elem));
    }
    is.get(); // to read '}' character

    return res; // no move to allow elision
}
///////////////////////////////////////////////////////////////////////////////////
template <typename T>
std::vector<std::vector<T>> parseMatrix(std::istream& is) {
    using namespace std;
    std::vector<std::vector<T>> res;
    absorbSpaces(is);

    char c;
    is.get(c); // to read the first '{' character
    absorbSpaces(is);

    while (static_cast<char>(is.peek()) != '}') { // over lines
        std::vector<T> line = parseVector<T>(is);
        absorbSpaces(is);
        res.push_back(std::move(line));
    }
    is.get(); // to read '}' character*/

    return res; // no move to allow copy elision
}
///////////////////////////////////////////////////////////////////////////////////
inline ValErr<double> parseValErr(std::istream& is) {
    ValErr<double> res;
    is >> res;
    return res; // no move to allow copy ellistion
}
///////////////////////////////////////////////////////////////////////////////////
inline void absorbSpaces(std::istream& is) {
    while (is.peek() == '\n' || is.peek() == ' ' || is.peek() == '\t') {
        is.get();
    }
}
///////////////////////////////////////////////////////////////////////////////////
WilsonCoefficientsCode parseWCcode(std::istream& is)
{
    string line;
    is >> line;
    return strToWC(move(line));
}
///////////////////////////////////////////////////////////////////////////////////
WCvalue parseWCvalue(std::istream& is)
{
    cout << "parseWCvalue: ";
    string line;
    is >> line;
    cout << line << '\n';

    if (line == "WCvalue::SM" || line == "SM")
        return WCvalue::SM;
    else if (line == "WCvalue::ONE" || line == "ONE")
        return WCvalue::ONE;
    else if (line == "WCvalue::MINHALF" || line == "MINHALF")
        return WCvalue::MINHALF;
    else if (line == "WCvalue::DOTONE" || line == "DOTONE")
        return WCvalue::DOTONE;
    else if (line == "WCvalue::TOTAL" || line == "TOTAL")
        return WCvalue::TOTAL;

    throw std::runtime_error("an attempt to parse wrong: " + line + " value of the WC val");
    return WCvalue::NONE;
}
///////////////////////////////////////////////////////////////////////////////////
} // namespace serialisation
} // namespace eft

#endif // ! ENTRY_PARSER_H
