#include "TEntry.h"
#include "WIdthOrXSsEntry.h"
#include "LinearParametrisation.h"
#include "EntryParser.h"

#include <iostream>

using namespace std;
using namespace eft;

eft::serialisation::LinearParametrisationInitialiser::LinearParametrisationInitialiser() {
	TEntry::mapEntries["LINEAR_PAREMETRISATION"] = new LinearParametrisation();
	//TEntry::mapEntries["PAREMETRISATION"]        = new LinearParametrisation();

	std::cout << "[STATIC] {Register types: LINEAR_PAREMETRISATION}" << endl;
}


namespace eft {
namespace serialisation {

void LinearParametrisation::serialise(std::ostream & os) const
{
	using ::eft::serialisation::tab;

	std::cout << "[serialise]{LinearParametrisation}" << std::endl;
	TEntry entry(*this);
	entry.serialise(os);

	os << "Channel " << channel_ << endl;
	os << "ParametrisationType " << parametrisationType_ << endl;

	os << tab << "Names {";
	//for (const auto& name : wcCodes_) {
	for (const auto& name : names_) {
		os << ' ' << name;
	}
	os << " }" << endl;

	os << tab << "Coefficients {";
	for (const auto val : vals_) {
		os << ' ' << val;
	}
	os << " }" << endl;
	os << "LinearParametrisation::End" << '\n';
}


LinearParametrisation::LinearParametrisation(std::istream& is)
	: TEntry(is)
{
	using namespace std;
	using namespace eft::serialisation;
	cout << "[LinearParametrisation] {ctor} start parsing..." << endl;
	string line, token;

	while (is >> token) {
	//while (getline(is, line)) {
		//istringstream is(line);
		//is >> token;
		cout << "[LinearParametrisation::ctor] token: " << token << endl;
		setEntryType(EntryType::LINEAR_PAREMETRISATION);

		if (token == "LinearParametrisation::End")
			break;
		if (token == "Names")
			this->names_ = parseVector<string>(is);
		else if (token == "Coefficients")
			this->vals_ = parseVector<double>(is);
		else if (token == "Expression")
			parseExpression(is, names_, vals_);
		else if (token == "Channel")
			this->channel_ = parseChannel(is);
		else if (token == "ParametrisationType")
			this->parametrisationType_ = parseParametrisationType(is);
		else
			throw std::runtime_error("Unknown token: " + token
				+ ". \n Available: Names, Coefficients, Expression, LinearParametrisation::End. Channel, ParamType \n");

	}
	cout << "[LinearParametrisation] {ctor} object is unserialised " << endl;
	cout << "[LinearParametrisation] {ctor} recover WC " << endl;
	wcCodes_.reserve(names_.size());
	for (const string& name : names_) {
		wcCodes_.push_back(strToWC(name));
	}
	cout << "[LinearParametrisation] {ctor} recover WC is done " << endl;
} // ctor




	} // namespace serialisation
} // namespace eft

std::ostream& eft::serialisation::operator<<(std::ostream& os, const ParametrisationType type)
{
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch (type) {
        PROCESS_VAL(ParametrisationType::XS);
        PROCESS_VAL(ParametrisationType::MU);
        PROCESS_VAL(ParametrisationType::WIDTH);
        PROCESS_VAL(ParametrisationType::BR);
        PROCESS_VAL(ParametrisationType::DECAY);
        PROCESS_VAL(ParametrisationType::PRODUCTION);
		PROCESS_VAL(ParametrisationType::NONE);
	default:
		throw std::runtime_error(string("operator<<(ParametrisationType) an attempt to print a value not in:")
			+ string("XS, MU, WIDTH, BR, DECAY, PRODUCTION, NONE"));
    }
#undef PROCESS_VAL

    return os << s;
}
