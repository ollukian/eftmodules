//
// Created by Aleksei Lukianchuk on 22-Jun-22.
// lukianchuk@lal.in2p3.fr
//
// Main object to be executed
// It parses commnand-line arguments and launched required
// functions
//
//


#include "../Core/Core.h"

#include "../Modules/SandboxHiggsChain.h"

#include "../Core/ChainCreator.h"

#include "ConfigWriter.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace eft;

int main(int argc, char** argv) {
    Logger::Init();

    ofstream fs;
    fs.open("eft.json", ios_base::out);
    if (fs.is_open())
        ConfigWriter(fs);
    else {
        EFT_CRITICAL("file {} is not open!", "config/eft.json");
        //throw std::runtime_error("error creating file");
    }

    EFT_INFO("[Sandbox][Higgs] got {} parameters", argc);
    for (size_t idx = 0; (int) idx < argc; ++idx) {
        EFT_INFO("\t {}", argv[idx]);
        //cout << "\t {" << argv[idx] << "}\n";
    }

    //const string path = argv[1];
    const string path = "../config/eft_config.json";
    //auto& chains = inner::ChainCreator(path).GetChains();

    /*if (argc >= 1) {
        EFT_INFO("[Sandbox][Higgs] got {} parameters", argc);
        for (size_t idx = 0; (int) idx < argc; ++idx) {
            EFT_INFO("\t {}", argv[idx]);
            //cout << "\t {" << argv[idx] << "}\n";
        }
        path = argv[1];
        inner::ChainCreator(path).GetChains();
    }
    else {
        EFT_CRITICAL("No config file provided");
        throw std::runtime_error("");
    }*/

    // first step: parse config files to create a chain
    // second step: perform desired studies with it - by cmd arguments

    //cin.get();

    RunSandboxHiggsChain();

    return 0;
}

