
//
// Created by lukianchuk on 23-Jun-22.
//
/*
 * Serves to allow simple json (de-) serialisation
 * of TChainModules via the nlohmann::json
 */

//#include <nlohmann/json.hpp>
//#include "../Modules/EFTModules.h"
//#include "../Modules/HiggsChainModules.h"

//#if 0

#include "JsonHelper.h"

//using json = nlohmann::ordered_json;
using json = nlohmann::json;
using namespace eft::modules;

namespace eft {
namespace modules {


void to_json(json& j, const HiggsChainModules& higgsChain) {
    json j_chain;
    j_chain["physics"] = "Higgs";
    j_chain["channels"];

    EFT_PROFILE_FN();
    EFT_DEBUG("in to_json(j, higgsChain)");

    EFT_DEBUG("Start looping over modules");
    /*for (const auto absHiggsModule : higgsChain.GetChainModules()) {
        const auto higgsModule = reinterpret_cast<HiggsModule*>(absHiggsModule);
        const auto higgsConfig = reinterpret_cast<HiggsConfig*>(higgsModule->GetConfig());

        const string& name = higgsModule->GetChannel();

        EFT_DEBUG("module: {}", name);
        EFT_DEBUG("module varnName: {}", higgsConfig->GetObsName());
        EFT_DEBUG("module dir: {}", higgsConfig->GetDirectory());
        EFT_DEBUG("module readbr: {}", higgsConfig->GetReadBrStatus());

        json j_this_channel;
        //nlohmann::ordered_json j_this_channel;
        j_this_channel["name"]      = name;
        j_this_channel["varName"]   = higgsConfig->GetObsName();
        j_this_channel["directory"] = higgsConfig->GetDirectory();

        bool readBr = higgsConfig->GetReadBrStatus();
        if (readBr) {
            const auto& br_param = higgsModule->GetBrParametrisation();
            json j_br_param;

            for (const auto& wc_val : br_param) {
                j_br_param[wcToStr(wc_val.first)] = wc_val.second;
            }

            j_this_channel["br"] = {
                    {"readBr", true},
                    {"parametrisation", j_br_param}
            };
        } // if read br
        else {
            j_this_channel["br"] = {
                    {"readBr", false}
            };
        } // else (if(readBr))

        j_this_channel["readBr"]    = higgsConfig->GetReadBrStatus();

        j_chain["channels"].push_back(j_this_channel);
        EFT_DEBUG("j_chain {} has been added", name);
        EFT_DEBUG("What we got:");
        //cout << setw(2) << j_this_channel << endl;
        EFT_DEBUG("Full j:");
        cout << setw(2) << j_chain << endl;
    }*/

    for (const auto absHiggsModule : higgsChain.GetChainModules()) {
        const auto higgsModule = reinterpret_cast<HiggsModule *>(absHiggsModule);
        json j_this_channel;
        to_json(j_this_channel, *higgsModule);
        j_chain["channels"].push_back(j_this_channel);
    }

    j = j_chain;
}

void from_json(const json& j, HiggsChainModules& higgsChain) {

    const auto& higgsModulesJson = j.at("channels");
    EFT_DEBUG("[omJson]{{HiggsChain}} {0} elements found", higgsModulesJson.size());

    for (const auto& higgsModulePtr : higgsModulesJson) {
        HiggsModule mdl(higgsModulePtr.get<HiggsModule>());
        EFT_DEBUG("[JSON]{{HiggsChain}} retrieved {} channel ", mdl.GetChannel());
        higgsChain.AddModule(&mdl);
    }

    //cout << "hey, implement: void from_json(const json& j, HiggsChainModules& higgsChain)" << endl;
    //auto modules = j.at("name");
    //j.at("name").get_to(higgsChain.)
}

void from_json(const json& j, HiggsModule& higgsModule) {
    EFT_LOG_DURATION("[JSON]{HiggsModule}");
    EFT_DEBUG("[JSON]{HiggsModule}");
    const string& name = j.at("name");

    // NOT smart pointer, because this object shall be owned
    // by the chain
    auto* cfg = new HiggsConfig(name);
    EFT_DEBUG("[JSON]{{HiggsModule}} retrieved name: {}", name);
    const string& directory = j.at("directory");
    EFT_DEBUG("[JSON]{{HiggsModule}} retrieved dir: {}", directory);
    const string& varName = j.at("varName");
    EFT_DEBUG("[JSON]{{HiggsModule}} retrieved varname: {}", varName);
    bool brStatus = j.at("readBr");
    EFT_DEBUG("[JSON]{{HiggsModule}} retrieved br status: {}", brStatus);
    cfg->SetDirectory(directory);
    cfg->SetObservableName(varName);
    cfg->SetReadBr(brStatus);

    if (brStatus) {

    EFT_DEBUG("[JSON]{{HiggsModule}} read the be parametrisation...");
        const auto& brParam = j.at("br").at("parametrisation");
        higgsModule.SetBrParametrisation(brParam);
        EFT_DEBUG("[JSON]{{HiggsModule}} read the be parametrisation DONE");
    }

    higgsModule.SetCfg(cfg);
    //j.at("name").get_to(higgsModule.GetConfig().S)
}

void to_json(nlohmann::json& j, const HiggsModule& higgsModule) {
    const auto higgsConfig = reinterpret_cast<HiggsConfig*>(higgsModule.GetConfig());

    const string& name = higgsModule.GetChannel();

    EFT_DEBUG("module: {}", name);
    EFT_DEBUG("module varnName: {}", higgsConfig->GetObsName());
    EFT_DEBUG("module dir: {}", higgsConfig->GetDirectory());
    EFT_DEBUG("module readbr: {}", higgsConfig->GetReadBrStatus());

    json j_this_channel;
    //nlohmann::ordered_json j_this_channel;
    j_this_channel["name"]      = name;
    j_this_channel["varName"]   = higgsConfig->GetObsName();
    j_this_channel["directory"] = higgsConfig->GetDirectory();

    bool readBr = higgsConfig->GetReadBrStatus();
    if (readBr) {
        const auto& br_param = higgsModule.GetBrParametrisation();
        json j_br_param;

        for (const auto& wc_val : br_param) {
            j_br_param[wcToStr(wc_val.first)] = wc_val.second;
        }

        j_this_channel["br"] = {
                {"readBr", true},
                {"parametrisation", j_br_param}
        };
    } // if read br
    else {
        j_this_channel["br"] = {
                {"readBr", false}
        };
    } // else (if(readBr))

    j_this_channel["readBr"] = higgsConfig->GetReadBrStatus();

    EFT_DEBUG("CHeck available merging schemes:");
    const auto& schemes = higgsConfig->GetMerger()->GetMergingSchemes();
    for (const auto& scheme : schemes) {
        EFT_DEBUG(scheme);
    }

    const auto& mergings = higgsConfig->GetMerger()->GetMergingScheme(name).mergingStrings_;
    EFT_DEBUG("mergings.size() = {}", mergings.size());
    const auto& init_names = higgsConfig->GetMerger()->GetInitNames();
    EFT_DEBUG("inin_name.size() = {}", init_names.size());


    json j_merging;

    higgsConfig->GetMerger()->GetInitNames();

    //const auto& merging = higgsConfig->GetMergingScheme();

    //EFT_DEBUG("merging scheme: higgsConfig->GetMergingScheme().size() = ", merging.size());

    //for (const auto& init_fin : merging) {
     //   EFT_DEBUG("{} ==> {}", init_fin.first, init_fin.second);
     //   j_merging[init_fin.first] = init_fin.second;
    //}

    for (const auto& init_name: init_names) {

        if (init_name == "UNKNOWN")
            continue;

        EFT_DEBUG("init name: {}", init_name);
        EFT_DEBUG("try to get the merged name..");
        const string& fin_name = higgsConfig->GetMerger()->GetMergedStr(init_name, name);
        EFT_DEBUG(fin_name);
        //EFT_DEBUG("Write down: {} ==> {}", init_fin.first, init_fin.second);
        j_merging[init_name] = fin_name;
    }
    j_this_channel["merging"]["scheme"] = j_merging;

    j = j_this_channel;
    //j_chain["channels"].push_back(j_this_channel);
    //EFT_DEBUG("j_chain {} has been added", name);
    //EFT_DEBUG("What we got:");
    //cout << setw(2) << j_this_channel << endl;
    //EFT_DEBUG("Full j:");
    //cout << setw(2) << j_chain << endl;
}

} // namespace modules
} // namespace eft





//#endif