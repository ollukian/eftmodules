#pragma once

#ifndef UTILS_PROFILE_H
#define UTILS_PROFILE_H

#include <chrono>
#include <iostream>
#include <string>
#include <iomanip> // to use std::put_time
#include <sstream>
#include <map>
#include <time.h>

using namespace std;
using namespace std::chrono;

class LogDuration;

inline ostream& putTime(std::ostream& os) {
    // LOG_DURATION("putTime");
    std::time_t t = std::time(nullptr);
    //std::tm tm_ = localtime_s(&t);
    std::tm tm_;
    localtime_s(&tm_, &t);
    return os << '[' << std::put_time(&tm_, "%T") << ']';
}

class LogDuration {
public:
    explicit LogDuration(const string& msg = "")
        : message(msg)
        , start(steady_clock::now())
    {
    }

    ~LogDuration() {
        auto finish = steady_clock::now();
        auto dur = finish - start;
        putTime(cout) << '[' << setw(15) << message << "] DONE in "
            << duration_cast<milliseconds>(dur).count()
            << " ms" << endl;

        durations_[message] += duration_cast<microseconds>(dur).count();
        nbCalls_[message]++;
    }

    static void GetStats() {
        cout << std::right;
        cout << setfill('*') << setw(70) << '*' << setfill(' ') << endl;
        cout << "* Durations of Functions: " << setw(44) << '*' << endl;
        cout << setfill('*') << setw(70) << '*' << setfill(' ') << endl;

        cout << '*'
            << setw(25) << "Function Name"
            << setw(11) << "mcs"
            << setw(10) << "ms"
            << setw(10) << "calls"
            << setw(15) << "avg per call"
            << setw(4) << "*\n";
        cout << setfill('-') << setw(70) << '-' << setfill(' ') << endl;

        for (const auto& name_dur : durations_) {
            cout << '*'
                << setw(25) << name_dur.first << ':'
                << setw(10) << name_dur.second
                << setw(10) << name_dur.second / 1000
                << setw(10) << nbCalls_[name_dur.first]
                << setw(10) << name_dur.second / nbCalls_[name_dur.first]
                << setw(4) << "*\n";
        }
        cout << setfill('*') << setw(70) << '*' << setfill(' ') << endl;
    }
private:
    const string             message;
    steady_clock::time_point start;

    static map<string, long> durations_;
    static map<string, long> nbCalls_;
};

//map<string, long> LogDuration::durations_;
//map<string, long> LogDuration::nbCalls_;

#ifndef UNIQ_ID_IMPL
#define UNIQ_ID_IMPL(lineno) _a_local_var_##lineno
#define UNIQ_ID(lineno) UNIQ_ID_IMPL(lineno)

#define LOG_DURATION(message) \
  LogDuration UNIQ_ID(__LINE__){message};

#endif // ifndef UNIQ_ID_IMPL

//#endif


// struct __putTime {
//   putTime() {
//     t   = std::time(nullptr);
//     tm_ = *std::localtime(&t);
//   }
//   std::time_t t;
//   std::tm_;
// };

// std::ostream& operator << (ostream& os, __putTime& pt) {
//   std::time_t t = std::time(nullptr);
//   std::tm tm_ = *std::localtime(&t);
//   return os << '[' << std::put_time(&tm_, "%T") << ']';
// }


// #define TIME					\
//   __putTime UNIQ_ID(__LINE__);			\
//   UNIQ_ID(__LINE__)

#endif // UTILS_PROFILE_H

