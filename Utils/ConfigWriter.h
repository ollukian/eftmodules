
//
// Created by lukianchuk on 23-Jun-22.
//

#ifndef EFT_CONFIGWRITER_H
#define EFT_CONFIGWRITER_H

#include <iostream>

int ConfigWriter(std::ostream& os);

#endif //EFT_CONFIGWRITER_H
