
//
// Created by lukianchuk on 22-Jun-22.
//

#ifndef EFT_JSON_H
#define EFT_JSON_H

#ifdef HASCXX17 // it would be an elegant and short solution
//                 but current root is not compatible with

#include <istream>
#include <map>
#include <string>
#include <variant>
#include <vector>

namespace Json {

    class Node : std::variant<std::vector<Node>,
            std::map<std::string, Node>,
            float,
            std::string> {
    public:
        using variant::variant;

        const auto& AsArray() const {
            return std::get<std::vector<Node>>(*this);
        }
        const auto& AsMap() const {
            return std::get<std::map<std::string, Node>>(*this);
        }
        float AsFloat() const {
            return std::get<float>(*this);
        }
        const auto& AsString() const {
            return std::get<std::string>(*this);
        }
    };

    class Document {
    public:
        explicit Document(Node root) : root(move(root)) {
        }

        const Node& GetRoot() const;

    private:
        Node root;
    };

    Document Load(std::istream& input);

}

#endif // HASCXX17



#endif //EFT_JSON_H
