//
// Created by lukianchuk on 21-Jun-22.
//

#include "Logger.h"
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <vector>

using namespace std;

namespace eft {

    shared_ptr<spdlog::logger> Logger::logger_;

    void Logger::Init()
    {
        vector<spdlog::sink_ptr> logSinks;
        logSinks.emplace_back(make_shared<spdlog::sinks::stdout_color_sink_mt>());
        logSinks.emplace_back(make_shared<spdlog::sinks::basic_file_sink_mt>("eft_log.log", true));

        logSinks[0]->set_pattern("%^[%T] %n: %v%$");
        logSinks[1]->set_pattern("[%T] [%l] %n: %v");

        logger_ = std::make_shared<spdlog::logger>("eft", begin(logSinks), end(logSinks));
        spdlog::register_logger(logger_);
        logger_->set_level(spdlog::level::trace);
        logger_->flush_on(spdlog::level::trace);

        logger_->info("********************************************************************");
        logger_->info("**************** Welcome to EftModules *****************************");
        logger_->info("********************************************************************");
        logger_->info("To log, use one of the following macros, defined in Utils/Logger.h:");

        logger_->trace("EFT_TRACE(\"message to trace\");");
        logger_->info("EFT_INFO(\"message to info\");");
        logger_->warn("EFT_WARN(\"message to warn\");");
        logger_->error("EFT_ERROR(\"message to error\");");
        logger_->critical("EFT_CRITICAL(\"message to critical\");");
        logger_->debug("EFT_DEBUG(\"message to debug\");");

        logger_->info("To profile a function, use:  EFT_PROFILE_FN();");
        logger_->info("To profile a scope, use: EFT_LOG_DURATION(msg)");
        logger_->info("********************************************************************");
    }

} // namespace eft
