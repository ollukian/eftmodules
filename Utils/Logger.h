//
// Created by lukianchuk on 21-Jun-22.
//

#ifndef EFT_LOGGER_H
#define EFT_LOGGER_H

#include <memory>

#pragma warning(push, 0)
#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>
#pragma warning(pop)

namespace eft {

class Logger {
public:
    static void Init();

    static inline std::shared_ptr<spdlog::logger>& GetLogger() { return logger_; }

private:
    static std::shared_ptr<spdlog::logger> logger_;
};
} // namespace eft

#define EFT_TRACE(...)      ::eft::Logger::GetLogger()->trace(__VA_ARGS__)
#define EFT_INFO(...)       ::eft::Logger::GetLogger()->info(__VA_ARGS__)
#define EFT_WARN(...)       ::eft::Logger::GetLogger()->warn(__VA_ARGS__)
#define EFT_ERROR(...)      ::eft::Logger::GetLogger()->error(__VA_ARGS__)
#define EFT_CRITICAL(...)   ::eft::Logger::GetLogger()->critical(__VA_ARGS__)
#define EFT_DEBUG(...)      ::eft::Logger::GetLogger()->debug(__VA_ARGS__)





#endif //EFT_LOGGER_H
