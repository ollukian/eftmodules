#include "Merger.h"
#include <iomanip>

#include <sstream>

using namespace std;

namespace eft {

size_t Merger::GetMergedIdx(size_t idx_raw, const string& mergingScheme) const
{

    if (mergingPerScheme_.find(mergingScheme) == mergingPerScheme_.end())
        throw std::runtime_error("[ERROR] scheme: |" + mergingScheme + "|"
            + " is not present.");

    const map<size_t, size_t>& merging = mergingPerScheme_.at(mergingScheme).mergingIdxIdx_;

    if (merging.find(idx_raw) == merging.end())
        throw eft::serialisation::SerialisationError("idx: " + std::to_string(idx_raw) + " is not in the map. Available"
            + PrintAvailableIdxAsString());

     return merging.at(idx_raw); 
}

string Merger::GetMergedStr(const string& raw, const string& mergingScheme) const
{
    //cout << "in get merged str for raw: {" << raw << "} scheme: {" << mergingScheme << "}" << endl;
    if (mergingPerScheme_.find(mergingScheme) == mergingPerScheme_.end()) {
        stringstream schemes;
        for (const auto& scheme_data : mergingPerScheme_) {
            schemes << " " << scheme_data.first;
        }
        throw std::runtime_error("[ERROR] scheme: |" + mergingScheme + "|"
            + " is not present. Available: " + schemes.str());
    }
    //cout << "after testing that this scheme is present" << endl; 
    const map<string, string>& merging = mergingPerScheme_.at(mergingScheme).mergingStrings_;

    if (merging.find(raw) == merging.end())
        return "UNKNOWN";
       // throw std::invalid_argument("Category: " + raw + " is not in the map of merging");
        //throw eft::serialisation::SerialisationError("idx: " + raw + " is not in the map");

    return merging.at(raw);
}

string Merger::PrintAvailableIdxAsString() const {
    ostringstream os;
    for (const auto& idxRaw_idxTarget : mergingIdxIdx) {
        os << idxRaw_idxTarget.first << " ";
    }
    return os.str();
}

MergeData::MergeData(const map<size_t, size_t>& merging, 
    const vector<string>& mergedNames, 
    const vector<string>& initNames)
    : mergingIdxIdx_(merging)
    , mergedNames_(mergedNames)
    , initNames_(initNames)
{
    EFT_INFO("Form MergeData from a map of merging");
    //cout << "[MergeData] from map of merging" << endl;
    string init, merged;
    for (const auto& i_f : mergingIdxIdx_) {
        merged = mergedNames_[i_f.second];
        init   = initNames_  [i_f.first];
        mergingStrings_[init] = merged;
        EFT_DEBUG("Register: indexes: {} ==> {}", init, merged);
        //cout << "indexes: " << i_f.first << " ==> " << i_f.second << endl;
        //cout << "[MergeData] register:" << setw(45) << init
        //    << " ==> " << merged << endl;
    }
    EFT_DEBUG("[MergeData] created: ");
    cout << "[MergeData] created: " << initNames_.size() << " init names, "
        << mergedNames_.size() << " merged names, mapIdxIdx has size: " << mergingIdxIdx_.size()
        << ", map StrStr: " << mergingStrings_.size() << endl;
}

MergeData::MergeData(const map<string, vector<string>>& merging)
{
    string mergedName;
    vector<string> toMerge;
    set<string> merged;
    set<string> init;

    EFT_INFO("Form MergeData from a map of merging: map<string, vector<string>> with size: {}", merging.size());
    //cout << "[MergeData] from map<string, vector<string>>" << endl;

    // initialise map [init_name] -> [merged_name]
    // and fill set of all init_names and merged_names
    // later on it will become a vector to make a map
    // idx_init -> idx_merged

    cout << setw(string("[MergeData] register: )").size() + 44) << " init name" << "   merged Name" << endl;
    cout << "---------------------------------------------------------------------------------" << endl;
    for (const auto& mergedName_toMerge : merging) {
        mergedName = mergedName_toMerge.first;
        toMerge = mergedName_toMerge.second;

        merged.insert(mergedName);

        for (const string& initName : toMerge) {
            init.insert(initName);
            mergingStrings_[initName] = mergedName;
            EFT_DEBUG("[MergeData] register: {:25} ==> {:20}", initName, mergedName);
            //cout << "[MergeData] register: " << setw(45) << initName
            //     << " ==> " << mergedName << endl;
        } // init bins to get this merged
    } // merged bins



    for (const string& initName : init) {
        initNames_.push_back(initName);
    }

    for (const string& mergedName_ : merged) {
        mergedNames_.push_back(mergedName_);
    }

    // fill the map of merging with indexes
    //cout << "[MergeData] fill map of indexes..." << endl;
    for (const auto& init_merged : mergingStrings_) {
        const string& init_   = init_merged.first;
        const string& merged_ = init_merged.second;

        size_t idx_init   = 0;
        size_t idx_merged = 0;

        for (size_t idx = 0; idx < initNames_.size(); ++idx) {
            if (initNames_.at(idx) == init_) {
                idx_init = idx;
                break;
            }
        }

        for (size_t idx = 0; idx < mergedNames_.size(); ++idx) {
            if (mergedNames_.at(idx) == merged_) {
                idx_merged= idx;
                break;
            }
        }

        EFT_DEBUG("[MergeData] register idx: {:3} ==> {:3}", idx_init, idx_merged);
        //cout << "[MergeData] register: " << idx_init << " -> " << idx_merged << endl;
        mergingIdxIdx_[idx_init] = idx_merged;
    } // merging map in strings
    //cout << "[MergeData] DONE: " << mergingIdxIdx_.size() << " merged elems." << endl;
    EFT_INFO("[MergeData] DONE: obtained " + to_string(mergingIdxIdx_.size()) + " merged elements");
}

MergeData MergeData::ReadFromJSON(istream& is_)
{
    EFT_INFO("Form MergeData from JSON");
    //cout << "[MergeData::FromJSON]" << endl;
    char brace;
    is_ >> brace;
    string line;

    string init_bin;
    string merged_bin;

    map<string, vector<string>> merging;

    while (getline(is_, line)) {
        //EFT_DEBUG("read line: {}", line);
        //cout << "*** read line: |" << line << "|" << endl;

        if (line == "}")
            break;

        istringstream is(line);
        //cout << "stringstream: |" << is.str() << "|" << endl;
        is >> merged_bin;

        merged_bin.erase(0, 1);
        merged_bin.erase(merged_bin.size() - 1, 1);

        //cout << "merged_bin: " << merged_bin << endl;

        char next;
        is >> next;

        if (next != ':')
            throw std::runtime_error("WARNING! JSON is not good: expected colon after bin to merged");

        is >> next;
        if (next != '[')
            throw std::runtime_error("WARNING! JSON is not good: expected [ after bin to merged and colon");

        //cout << "go parsing elements" << endl;
        vector<string> toBeMerged;
        while (is.peek() != ']') {      
            is >> next; // "
            //cout << "here next: |" << next << "|" << endl;
            string buf;
            if (next != ',' && next != '"')
                buf.push_back(next);
            //cout << "fill buf: ";
            while (is.peek() != '"') {
                while (is.peek() == ' ')
                    is >> next;
                is >> next;
                buf.push_back(next);
                //cout << next;
            }
            //cout << " DONE: buf: |" << buf << "]" << endl;
            //cout << "is it empty? ==> " << buf.empty() << endl;

            is >> next; // last "
            init_bin = buf;

            if (!buf.empty()) {
                toBeMerged.push_back(init_bin);
                //cout << " to add [" << init_bin << "] to merge to: " << merged_bin << endl;
            }
            //is >> init_bin;
            //cout << "read init_bin: |" << init_bin << "]" << endl;
        } // raeding init_bins
        merging[merged_bin] = toBeMerged;
    } // merged bins

    /*cout << "[MergeData::FromJSON] what we've got:" << endl;
    for (const auto& merged_init : merging) {
        cout << "[" << merged_init.first << "] ==> ";
        for (const string& init : merged_init.second) {
            cout << init << ", ";
        }
        cout << endl;
    }*/

    return MergeData(merging);
}

void MergeData::WriteToJSON(std::ostream& os)
{
    os << "{\n";

    //////////////////////
    // form the map:
    // [string]   [set<string>]
    //  merged      init bins to be merged to this merged one

    map<string, set<string>> initMergedToGivenMerged;

    for (const auto& init_merged : mergingStrings_) {
        const string& init = init_merged.first;
        const string& merged = init_merged.second;
        initMergedToGivenMerged[merged].insert(init);
    }

    ///////////////////////
    // print the map
    for (const auto& merged_inits : initMergedToGivenMerged) {
        const string& merged = merged_inits.first;
        const set<string> inits = merged_inits.second;
        os << "\t \"" << merged << "\" : [";

        bool isFirst = true;
        for (const string& init : inits) {
            if (!isFirst)
                os << ", ";
            isFirst = false;
            os << '\"' << init << '\"';
        }
        os << "],\n";
    }
    os << "}\n";
    /*
    map<size_t, size_t> mergingIdxIdx_;
	map<string, string> mergingStrings_;
	vector<string>      mergedNames_;
	vector<string>      initNames_;
    */
}

} // namespace eft
