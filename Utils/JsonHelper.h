
//
// Created by lukianchuk on 23-Jun-22.
//

#ifndef EFT_JSONHELPER_H
#define EFT_JSONHELPER_H

#include <nlohmann/json.hpp>
#include "../Modules/EFTModules.h"
#include "../Modules/HiggsChainModules.h"


namespace eft {
namespace modules {

//namespace ns {
    void to_json(nlohmann::json& j, const HiggsChainModules& higgsChain);
    void from_json(const nlohmann::json& j, HiggsChainModules& higgsChain);

    void to_json(nlohmann::json& j, const HiggsModule& higgsModule);
    void from_json(const nlohmann::json& j, HiggsModule& higgsModule);
//}

} // namespace modules
} // namespace eft

#endif //EFT_JSONHELPER_H
