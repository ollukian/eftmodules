//
// Created by Aleksei Lukianchuk on 20-Jun-22.
// 
// Defines macros for profiling:
// 
//	* EFT_PROFILE_FN()      => profiles execution of a function, which name is automaticlally deduced
//  * EFT_LOG_DURATION(msg) => profiles one scope
// 
//

#ifndef EFT_CORE_PROFILER_H
#define EFT_CORE_PROFILER_H

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <string>
#include <thread>
#include <mutex>
#include <sstream>

#include <ctime>

using namespace std;
using namespace std::chrono;

#include "../Utils/Logger.h"

//#include "../Vendors/spdlog/spdlog/include/spdlog/spdlog.h"

#include <string>


namespace eft {

	//struct ProfileResult;
	class  Profiler;

#define EFT_PROFILE
#ifdef EFT_PROFILE
#if defined(__GNUC__)
#define EFT_FN_SIG _func_
#elif (defined(__FUNCSIG__) || (_MSC_VER))
#define EFT_FN_SIG __FUNCSIG__
#elif defined(__cplusplus) && (__cplusplus >= 201103)
    #define EFT_FN_SIG __func__
	#else
		#define EFT_FN_SIG "EFT_FN_SIG is not known. Visit Core/Profiler.h"
#endif

    //#define EFT_PROFILE_SCOPE_LINE_IMPL(name, line) EFT::Profiler UNIQ_ID()
    //#define EFT_PROFILE_SCOPE_LINE(name, line) EFT_PROFILE_SCOPE_LINE_IMPL(name, line)
    //#define EFT_PROFILE_SCOPE(name) EFT_PROFILE_SCOPE_LINE(name, __LINE__)


#define EFT_LOG_DURATION(message) ::eft::Profiler UNIQ_ID(__LINE__){message};
#define EFT_PROFILE_FN()          EFT_LOG_DURATION(EFT_FN_SIG)
#else
    #define EFT_LOG_DURATION(message)
	#define EFT_PROFILE_FN()
#endif // EFT_PROFILE

	/*struct ProfileResult {
        std::chrono::duration<double, std::micro>   start;
        std::chrono::microseconds                   elapsedTime;
        std::thread::id                             threadID;

        ProfileResult()
            : start(steady_clock::now())
            , threadID(std::thread::get_id())
            , elapsedTime(0)
        {}

        std::ostream& operator << (ostream& os, const ProfileResult& res);
	};*/

	class Profiler {
    public:
        explicit Profiler(const string& msg = "")
                : message(msg)
                //, prefRes_()
                , start(steady_clock::now())
                , threadID(std::this_thread::get_id())
        {
        }

        ~Profiler() {
            auto finish = steady_clock::now();
            elapsedTime = duration_cast<milliseconds>(finish - start);
            //auto dur = finish - start;
            std::stringstream message_to_display;
            message_to_display << "[" << message << "] done in " << elapsedTime.count()
                 << "ms, thread id: " << threadID << endl;
            EFT_DEBUG(message_to_display.str());
            //EFT_INFO(message);
        }
	    //Profiler(const Profiler&&) = delete;
	    //Profiler(Profiler&&) = delete;

        //static Profiler& Get();
        void WriteProfile() const;
        //void WriteProfile(const ProfileResult& result);

    private:
        const std::string  message;
	    std::mutex    mutex_;
        std::ofstream os_;
        //ProfileResult prefRes_;
        //std::chrono::duration<double, std::micro>   start;
        steady_clock::time_point    start;
        std::chrono::microseconds   elapsedTime;
        std::thread::id             threadID;
    private:
	   //expProfiler() = default;
	    //~Profiler()
	};


#ifndef EFT_UNIQ_ID_IMPL
#define EFT_UNIQ_ID_IMPL(lineno) _eft_a_local_var_##lineno
#define EFT_UNIQ_ID(lineno) EFT_UNIQ_ID_IMPL(lineno)

//#define LOG_DURATION(message) \
 // LogDuration EFT_UNIQ_ID(__LINE__){message};

#endif // ifndef UNIQ_ID_IMPL




}

#endif // !EFT_CORE_PROFILER_H