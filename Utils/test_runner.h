#pragma once

#ifndef EFT_UTILS_TEST_RUNNER_H
#define EFT_UTILS_TEST_RUNNER_H

#include <sstream>
#include <stdexcept>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "Logger.h"

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
    os << "{";
    bool first = true;
    for (const auto& x : s) {
        if (!first) {
            os << ", ";
        }
        first = false;
        os << x;
    }
    return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
    os << "{";
    bool first = true;
    for (const auto& x : s) {
        if (!first) {
            os << ", ";
        }
        first = false;
        os << x;
    }
    return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
    os << "{";
    bool first = true;
    for (const auto& kv : m) {
        if (!first) {
            os << ", ";
        }
        first = false;
        os << kv.first << ": " << kv.second;
    }
    return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
    if (!(t == u)) {
        ostringstream os;
        os << "Assertion failed: " << t << " != " << u;
        if (!hint.empty()) {
            os << " hint: " << hint << '\n';
        }
        throw runtime_error(os.str());
    }
}

inline void Assert(bool b, const string& hint) {
    AssertEqual(b, true, hint);
}

class TestRunner {
public:
    template <class TestFunc>
    void RunTest(TestFunc func, const string& test_name) {
        try {
            func();
            EFT_DEBUG("{:15} unit test is OK", test_name);
            //cerr << test_name << " OK" << endl;
        }
        catch (exception& e) {
            ++fail_count;
            EFT_CRITICAL("{:15} unit test FAILED: {}", test_name, e.what());
            //cerr << test_name << " fail: " << e.what() << endl;
        }
        catch (...) {
            ++fail_count;
            EFT_CRITICAL("Unknown exception caught");
            //cerr << "Unknown exception caught" << endl;
        }
    }

    ~TestRunner() {
        if (fail_count > 0) {
            EFT_CRITICAL("{} unit tests failed. Terminate", fail_count);
            //cerr << fail_count << " unit tests failed. Terminate" << endl;
            exit(1);
        }
    }

private:
    size_t fail_count = 0;
};

#define ASSERT_EQUAL_SSTREAMS(x, y) {   \
   ostringstream __os;                  \
  __os << #x << " != " << #y << ", "      \
    << __FILE__ << ":" << __LINE__;     \
    size_t lSize = x.str().length();    \
    size_t rSize = y.str().length();    \
    if (lSize != rSize) {               \
        __os << "\n they have different size: "    \
           << lSize << " and " << rSize << '\n'; \
        throw std::runtime_error(__os.str());     \
    }                                            \
    for (size_t idx = 0; idx < lSize; ++idx) {   \
        char l = x.str().c_str()[idx];           \
        char r = y.str().c_str()[idx];           \
        if (l != r) {                            \
            __os << "symbols #" << idx           \
            << " are different: "                \
            << l << " and " << r << '\n';        \
            AssertEqual(l, r, __os.str());       \
        }                                        \
    }                                            \
}


#define ASSERT_EQUAL(x, y) {            \
  ostringstream __os;                   \
  __os << #x << " != " << #y << ", "    \
    << __FILE__ << ":" << __LINE__;     \
  AssertEqual(x, y, __os.str());        \
}

#define ASSERT(x) {                     \
  ostringstream __os;                     \
  __os << #x << " is false, "             \
    << __FILE__ << ":" << __LINE__;     \
  Assert(x, __os.str());                  \
}

#define RUN_TEST(tr, func) \
  tr.RunTest(func, #func)

#endif // EFT_UTILS_TEST_RUNNER_H