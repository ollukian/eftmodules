
//
// Created by lukianchuk on 22-Jun-22.
//

#pragma once

#ifdef HASCXX17

#include "json.h"
#include <iostream>

using namespace std;

namespace Json {

    const Node& Document::GetRoot() const {
        return root;
    }

    Node LoadNode(istream& input);

    Node LoadArray(istream& input) {
        vector<Node> result;

        for (char c; input >> c && c != ']'; ) {
            if (c != ',') {
                input.putback(c);
            }
            result.push_back(LoadNode(input));
        }
        return Node(move(result));
    }

    Node LoadFloat(istream& input) {
        float result;
        input >> result;
        return Node(result);
    }

    Node LoadString(istream& input) {
        string line;
        getline(input, line, '"');
        return Node(move(line));
    }

    Node LoadDict(istream& input) {
        map<string, Node> result;

        for (char c; input >> c && c != '}'; ) {
            if (c == ',') {
                input >> c;
            }
            string key = Json::LoadString(input).AsString();
            if (key != "is_roundtrip") {
                input >> c;
                if (key.size() != 0) {
                    result.emplace(move(key), LoadNode(input));
                }
            }
            else {
                string value;
                input >> value >> value;

                int to_return;
                if (value == "false") to_return = 0;
                else to_return = 1;

                result.emplace(move(key), to_return);
            }
        }
        return Node(move(result));
    }

    Node LoadNode(istream& input) {
        char c;
        input >> c;

        if (c == '[') {
            return LoadArray(input);
        }
        else if (c == '{') {
            return LoadDict(input);
        }
        else if (c == '"') {
            return LoadString(input);
        } else {
            if (c == EOF) cerr << "takoe \n";
            if (!isdigit(c)) cerr << "takoe - ne chislo \n";
            input.putback(c);
            return LoadFloat(input);
        }
    }

    Document Load(istream& input) {
        return Document{LoadNode(input)};
    }

}

#endif // #ifdef HASCXX17
