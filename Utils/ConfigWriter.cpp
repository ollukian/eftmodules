
//
// Created by lukianchuk on 23-Jun-22.
//

#include <nlohmann/json.hpp>

#include "Logger.h"

#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;
using json = nlohmann::json;

int ConfigWriter(ostream& os) {

    EFT_INFO("Config Writer: write down to a json file hardcode user-defined config");

    json j_root;
    json j_chains;

    json j_higgs;

    json j_higgs_yy;
    json j_higgs_bb;

    j_higgs_yy["name"] = "yy";
    j_higgs_yy["varName"] = "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_fine_pTjet30";
    j_higgs_yy["directory"] = "/sps/atlas/o/ollukian/ATLAS_HGam/EFT/HyyEFT/MyEFT/FromEleonora/Rivet_samples_Aleksei_prod";
    j_higgs_yy["mergingScheme"];
    j_higgs_yy["br"] = json::object( { {"readBr", true}, {"brPath", "here"} } );
    j_higgs_yy["nbEvents"] = 100'000;

    j_higgs_bb["name"] = "bb";
    j_higgs_bb["varName"] = "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30";
    j_higgs_bb["directory"] = "/sps/atlas/o/ollukian/ATLAS_HGam/EFT/HyyEFT/MyEFT/FromEleonora/Rivet_samples_Aleksei_prod";
    j_higgs_bb["mergingScheme"];
    j_higgs_bb["br"] = json::object( { {"readBr", false} } );

    j_higgs["chainName"] = "Higgs";
    j_higgs["channels"].push_back(j_higgs_yy);
    j_higgs["channels"].push_back(j_higgs_bb);

    j_chains.push_back(j_higgs);

    j_root["chains"].push_back(j_chains);

    //os << j_root;
    //cout << j_root;

    //std::cout << std::setw(2) << j_root << std::endl;

    return 0;
}

