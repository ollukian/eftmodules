#pragma once
#ifndef EFT_TEFTMODULE_H
#define EFT_TEFTMODULE_H

//#include "../EFTManager/Parser.h"
#include "../DataBase/ParseResults.h"
#include "TParametrisation.h"

#include "TModuleConfig.h"
#include "HiggsConfig.h"
//#include "../Serialisable.h"

#include <string>
#include <vector>
#include <memory>

using std::shared_ptr;

namespace eft {
namespace modules {

// interface for various physics: Higgs, EW, top
// provides common handles:
// readFiles(path)
// GetParametrisation() => return parametrisation to be written
// 	   inner functions/utilits to be overload for each instance:
// 	   schematic inteface, only type, name and args, no constness and other things
// - void		   register(ParseResults&)
// - ParseResults& parse(filename) => gets info from filename and its content
// - void          processFiles(path)
// - getXSs()
class TEFTModule
{
public:
	virtual       void				  Register(const ParseResults& res)           = 0; // the result to be moved
	virtual const ParseResults 		  Parse(const string& filename) const         = 0; // extract all info from file: name + content
	virtual		  ParseResults	      ParseMetaInfo(const string& filename) const = 0; // parses header
	virtual		  ParseResults	      ParseContent(ParseResults& res) const       = 0; // extract XS/Width/whatever
	//virtual       void				  ProcessFiles()							  = 0; // make calculations
	virtual       void				  ProcessFiles()                              = 0; // make calculations
	virtual       void                ComputeParametrisation()                    = 0;
	//virtual       void                ReadFiles(const string& dir)                 = 0; // extracts names of needed file from the dir
	inline const  TParametrisation       GetParametrisation()     const { return parametrisation_; }
	inline const  LinearParametrisation  GetLinParametrisation()  const { 
		/*std::cout << "[GetLinParametrisation] return object with keys: " << std::endl;
		for (const auto& key_val : parametrisation_.linear) {
			std::cout << key_val.first << std::endl;
		}*/
		return parametrisation_.linear; 
	}
	inline const  QuadraticParametrisation GetQuadParametrisation() const { return parametrisation_.quad; }
	//inline        unique_ptr<TModuleConfig>& GetConfig()             { return cfg_; }
	//inline const  unique_ptr<TModuleConfig>& GetConfig()       const { return cfg_; }
	inline        TModuleConfig* GetConfig()             { return cfg_; }
	inline        TModuleConfig* GetConfig()       const { return cfg_; }
	inline const  std::string GetChannel() const { return channel_; }
	inline        std::string GetChannel()       { return channel_; }
	inline        void        SetChannel(const std::string& channel) { channel_ = channel; }
	inline        void        SetChannel(      std::string&& channel) { channel_ = move(channel); }


	inline void   SetConfig(TModuleConfig* cfg) { /*cfg_.reset(cfg);*/ cfg_ = cfg; }

	TEFTModule() = default;
	explicit TEFTModule(TModuleConfig* cfg) : cfg_(cfg) { parametrisation_.linear[cfg_->GetName()];  parametrisation_.quad[cfg_->GetName()];
	}
	//inline static unique_ptr<TModuleConfig> create(TModuleConfig* cfg);
	virtual ~TEFTModule() {}
protected:
	//virtual void Init() const = 0;
protected:
	//shared_ptr<TModuleConfig> cfg_;  //!< allows user-defined reading of files
	//unique_ptr<TModuleConfig> cfg_;  //!< allows user-defined reading of files
	TModuleConfig* cfg_ = nullptr;
	TParametrisation          parametrisation_ {}; //!< stores linear and quad parametrsisations. Same for all modules
	std::string               channel_ {};
};

} // namespace modules
} // namespace eft

#endif // !EFT_TEFTMODULE_H

