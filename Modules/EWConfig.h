#pragma once
#include "TModuleConfig.h"

#include <memory>

namespace eft {
namespace modules {


class EWConfig final : public TModuleConfig
{
public:
	explicit EWConfig(std::string& name_)  : TModuleConfig(move(name_)) {}
	explicit EWConfig(std::string&& name_) : TModuleConfig(name_) {}
	explicit EWConfig(const char* name)    : TModuleConfig(name) {}
	~EWConfig() override {}
	static inline std::unique_ptr<EWConfig> create(std::string& name_);
	static inline std::unique_ptr<EWConfig> create(std::string&& name_);

	//inline void Init() const;

	// from the base class to be overloaded:
	const std::vector<std::string> GetFiles() const noexcept override;
	void                           PrintInfo(std::ostream& os) const override;
private:
};


}
}
