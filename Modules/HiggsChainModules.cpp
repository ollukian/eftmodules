#include "HiggsChainModules.h"

#include "../Utils/profile.h"
#include "../EFTManager/WilsonCoefficients.h"
#include "../EFTManager/Parser.h"
//#include "HiggsModule.h"

#include "../Utils/Logger.h"
#include "../Utils/Profiler.h"

using namespace std;
//////////////////////////////////////////////////////////////////////////////////////
void eft::modules::HiggsChainModules::Run()
{
    EFT_PROFILE_FN();
	LOG_DURATION("HiggsChainModules::Run");
	EFT_INFO("[HiggsChain]{Run} initiate processing files");
	cout << "[HiggsChain]{Run} initiate processing files...\n";
	for (auto& physModule : chain_) {
	    EFT_INFO("[HiggsChain]{" + physModule->GetConfig()->GetName() + "} process...");
		//cout << "[HiggsChain]{" << physModule->GetConfig()->GetName() << "} process files..." << endl;
		physModule->ProcessFiles();
        EFT_INFO("[HiggsChain]{" + physModule->GetConfig()->GetName() + "} process DONE");
		//cout << "[HiggsChain]{" << physModule->GetConfig()->GetName() << "} process files DONE" << endl;
	}
	EFT_INFO("[HiggsChain]{Run} all physics components have finished. Combine parametrisations...");
	CombineParametrisationFromChannels();
}
//////////////////////////////////////////////////////////////////////////////////////
eft::modules::HiggsChainModules::~HiggsChainModules()
{
	////cout << "[HiggsChain] release modules and info...\n";
	/*for (auto& physModule : chain_) {
		delete physModule.get();
		//physModule.operator~();
	}*/
}
//////////////////////////////////////////////////////////////////////////////////////
void eft::modules::HiggsChainModules::CombineParametrisationFromChannels() {
    EFT_PROFILE_FN();
    EFT_INFO("[HiggsChainModules]{CombineParametrisationFromChannels}");
    EFT_INFO("[HiggsChainModules] get parametrisation from sub-channels...");
	//cout << "[HiggsChainModules] CombineParametrisationFromChannels from:\n";
	for (const auto& higgsModule : chain_) {
	    EFT_INFO("\t" + higgsModule->GetConfig()->GetName());
		//cout << "\t" << higgsModule->GetConfig()->GetName() << endl;
		//cout << "get channel ...";
		const string& channel = higgsModule->GetChannel();
		//cout << channel << endl;
		parametrisation_->linear[channel] = higgsModule->GetLinParametrisation().begin()->second;
	}
    EFT_INFO("[HiggsChainModules] parametrisations are obtained");

	cout << "[HiggsChainModules] create registry of important WC" << endl;
	// collect all WC which play a role in the linear parametrisatin
	// => the impact and parametrisation can be printed for all
	// this Wilson Coefficients
	for (const auto& ch_linparam : parametrisation_->linear) {
		//const string& ch = ch_linparam.first;
		for (const auto& wc_rest : ch_linparam.second) {
			//WC wc = wc_rest.first;
			registryWClinear_.insert(wc_rest.first);
		}
	}

	cout << "[HiggsChainModules] registry of important WC:" << endl;
	for (const auto wc : registryWClinear_) {
		cout << '\t' << wc << '\n';
	}
	//throw std::runtime_error("FINALISE: {HiggsChainModules::Run} combining parametrisations from various channels");
}
//////////////////////////////////////////////////////////////////////////////////////