#pragma once

//#include "DecayChannel.h"
#include "../EFTManager/WilsonCoefficients.h"
#include "../DataBase/ValErr.h"

#include "../Core/WCpair.h"
#include "../Core/Impact.h"

#include <map>
#include <string>


namespace eft {
namespace modules {

//struct Impact;
struct TParametrisation;
struct Bin;
//struct WCPair;
//struct Impact;
class LinParametrisation;
class QuadParametrisation;


//class LinParametrisation {
//public:
//	LinParametrisation() = default;
//	~LinParametrisation() {}
//	inline const std::map<std::string, 
//		std::map<WilsonCoefficientsCode, 
//		std::map<Bin, Impact>>> 
//		Get() const { return data_; }
//	inline std::map<std::string, std::map<WilsonCoefficientsCode, std::map<Bin, Impact>>> Get() { return data_; }
//
//
//	inline void Add(const string& ch, const WilsonCoefficientsCode wc, const Bin& bin, const Impact& other);
//private:
//	std::map<std::string,
//		std::map<WilsonCoefficientsCode,
//			std::map<Bin,
//				Impact
//			>
//		>
//	> data_{};
//};

//using Impact = ValErr<double>;


// map: [ch][wc][bin] => Impact
using LinearParametrisation
		//= std::map<DecayChannel,
		= std::map<std::string, 
			std::map<WilsonCoefficientsCode,
				std::map<Bin,
					Impact
				>
			>
		>;



using QuadraticParametrisation
		//= std::map<DecayChannel,
		= std::map<std::string,
			std::map<WCpair,
				std::map<Bin,
					Impact
				>
			>
		>;



/*struct Impact {
	double value;
	double error;
};*/

struct Bin {
	std::string name{"unset"};

	Bin() = default;
	~Bin() {}
	Bin(std::string _n) : name(std::move(_n)) {}
	Bin(const Bin& other) { name = other.name; }
	Bin(Bin&& other) = default;
	Bin& operator=(Bin& right)  = default;
	Bin& operator=(Bin&& right) = default;

	inline bool operator < (const Bin& other) { return name < other.name; }
};

inline bool operator < (const Bin& l, const Bin& r) { return l.name < r.name; }

// used to  parametrise the BSM contribution:
// in this way, one doesn't need to care about 
// the order of the WC in the map:
// WCpair(wc1, wc2) == WCpair(wc2, wc1)
//struct WCPair {
//	WilsonCoefficientsCode wc1;
//	WilsonCoefficientsCode wc2;
//
//	WCPair(WilsonCoefficientsCode wc1_, WilsonCoefficientsCode wc2_)
//		: wc1(wc1_)
//		, wc2(wc2_)
//	{}
//};
//
//inline WCPair make_wcPair(WilsonCoefficientsCode wc1, WilsonCoefficientsCode wc2) {
//	return WCPair(wc1, wc2);
//}
//
//inline bool operator == (const WCPair& l, const WCPair& r) {
//	if (l.wc1 == r.wc1 && l.wc2 == r.wc2) return true;
//	if (l.wc1 == r.wc2 && l.wc2 == r.wc1) return true;
//	return false;
//}
//
//inline ::std::ostream& operator << (::std::ostream& os, const WCPair& wc) {
//	const std::string wc1 = wcToStr(wc.wc1);
//	const std::string wc2 = wcToStr(wc.wc2);
//	return os << '{' << wc1 << ", " << wc2 << '}';
//}

} // namespace modules
} // namespace eft

//template<>
//struct std::hash<eft::modules::WCPair>
//{
//	std::size_t operator()(const eft::modules::WCPair& wcpair) const noexcept
//	{
//		std::size_t h1 = std::hash<int>{}(static_cast<int>(wcpair.wc1));
//		std::size_t h2 = std::hash<int>{}(static_cast<int>(wcpair.wc2));
//		std::size_t h = std::hash<int>{}(h1 + h2);
//		return h;
//	}
//};

namespace eft {

//namespace std {
//}

//inline bool operator < (const WCPair& l, const WCPair& r) {
//	auto h1 = std::hash<WCPair>{}(l);
//	auto h2 = std::hash<WCPair>{}(r);
//	return h1 < h2;
//	//auto hash_1 = std::hash<int> {} (static_cast<int>(l.wc1));
//	//auto hash_2 = std::hash<int>{}  (static_cast<int>(l.wc2));
//}

//struct Impact {
//	double val = 0.;
//	double err = 0.;
//
//	explicit Impact(float v) : val(v) {}
//	Impact() { val = 0.; err = 0; }
//	Impact(double v) : val(v) {}
//	Impact(Impact&& other) = default;
//	Impact(const Impact& other) { val = other.val; err = other.err; }
//
//	Impact& operator=(Impact other) noexcept {
//		std::swap(val, other.val); // exchange resources between *this and other
//		std::swap(err, other.err);
//		return *this;
//	}
//	Impact& operator=(Impact&& other) noexcept
//	{
//		if (this == &other)
//			return *this; 
//                         
//		val = other.val;
//		err = other.err;
//		return *this;
//	}
//
//	Impact& operator = (const Impact& other) { val = other.val; return *this; }
//	Impact& operator = (double other) { val = other; return *this; }
//};


namespace modules {
// TODO: check performance with unordered_map for the last bin
struct TParametrisation
{
	LinearParametrisation    linear {};
	QuadraticParametrisation quad  {};
};

} // namespace modules
} // namespace eft


