#pragma once

#ifndef EFT_MODULES_SANDBOX_HIGGS_CHAIN_H
#define EFT_MODULES_SANDBOX_HIGGS_CHAIN_H

#include "HiggsChainModules.h"
#include "HiggsConfig.h"
#include "HiggsModule.h"

int RunSandboxHiggsChain(int argc = 0, char* argv[] = nullptr);

#endif // !EFT_MODULES_SANDBOX_HIGGS_CHAIN_H
