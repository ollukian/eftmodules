#pragma once

#ifndef EFT_MODULES_HIGGSCHAIN_H
#define EFT_MODULES_HIGGSCHAIN_H


#include "TEFTModule.h"
#include "TChainModulesOnePhysics.h"
#include <set>

#include "Logger.h"

namespace eft {
namespace modules {



class HiggsChainModules final : public TChainModulesOnePhysics
{
public:
	static inline std::unique_ptr<HiggsChainModules> create() { return make_unique<HiggsChainModules>(); }
	void Run() override final;
	HiggsChainModules() = default;
	~HiggsChainModules() final;

	void CombineParametrisationFromChannels();

private:
	using WC = eft::WilsonCoefficientsCode;
	std::set<WC> registryWClinear_;
	//TParametrisation parametrisation_;
	//std::vector<std::map<WC, std::map<Bin, Impact>>> linearImpacts_;
};


} // namespace eft
} // namespace modules


#endif // !EFT_MODULES_HIGGSCHAIN_H

