#pragma once
#ifndef EFT_HIGGSMODULE_H
#define EFT_HIGGSMODULE_H

#include "TEFTModule.h"
#include "HiggsConfig.h"

#include "Merger.h"
#include "../Database/ParseResults.h"
//#include "../../EFTManager/Parser.h"

#include "../Utils/profile.h"
#include "TParametrisation.h"

//#include "Core.h"
//#include "Profiler.h"
//#include "Logger.h"

#include "../Core/Core.h"

namespace eft {
namespace modules {

class HiggsModule final : public TEFTModule
{
	using WC = eft::WilsonCoefficientsCode;
	using XS = ValErr<double>;
	using CH = eft::DecayChannel;

	using Widths = std::map<CH, Width>;

	using XSperWC = map<eft::WilsonCoefficientsCode, std::map<eft::WCvalue, CrossSections>>;
	using XsChannelTruthBinWc = 
			std::map<CH,
			std::map<truth_bin,
			std::map<WC,
			std::map<WCvalue, XS>>>>;
	using RegistryWCcode    = std::set<WC>;
	using WidthPerChanWcVal = 
		std::map<CH,
		std::map<WC,
		std::map<eft::WCvalue,
		Width>>>;
	using ValPerChanWc = 
		//std::map<CH,
		std::map<WC,
		double>;

public:
	HiggsModule(std::string& channel, std::string& varNameTTree, bool readBrFile = false);
	explicit HiggsModule(HiggsConfig* cfg);
	HiggsModule() = default;
	~HiggsModule() final {}


    void SetCfg(HiggsConfig* cfg);
	inline static unique_ptr<HiggsModule> create(HiggsConfig* cfg);
	//inline static unique_ptr<HiggsModule> create(unique_ptr<HiggsConfig> cfg);
	inline static unique_ptr<HiggsModule> create(std::string& channel, std::string& varNameTTree, bool readBrFile = false);

	////////////////////////////////////////////////////////
	// Getters
	//inline const CrossSections			 GetRawSmXSs()		     const { return smXSS_; }
	//inline       CrossSections			 GetRawSmXSs()		           { return smXSS_; }

	inline const std::map<std::string, ValErr<double>> GetMergedSmXSs() const { return Merge(smXSS_, channel_); }
	inline       std::map<std::string, ValErr<double>> GetMergedSmXSs()       { return Merge(smXSS_, channel_); }

	inline const map<WilsonCoefficientsCode, map<string, ValErr<double>>> 
		GetAveragedIntXSs() const { return Merge(interfXS_, channel_); }
	inline       map<WilsonCoefficientsCode, map<string, ValErr<double>>> 
		GetAveragedIntXSs()       { return Merge(interfXS_, channel_); }

	//	   const map<WC, CrossSections>  GetAveragedIntXSs() const;
	//		     map<WC, CrossSections>  GetAveragedIntXSs();
	////////////////////////////////////////////////////////
	// overriden functions from the base class
	//void                ReadFiles(const string& dir) override;
	inline void		    Register(const ParseResults& res) override;
	const ParseResults  Parse(const string& filename) const override;
	ParseResults		ParseMetaInfo(const string& filename) const override; // parses header
	inline ParseResults ParseContent(ParseResults& res) const override; // extract XS/Width/whatever
	void				ProcessFiles() override;
	////////////////////////////////////////////////////////
	/// 	custom functions for this class
    static void         ParseDecayContent(const string& filename, ParseResults& res);
    void         ParseProdContent(const string& filename, ParseResults& res) const;
	inline void  SetDir(const string& dir) { directory_ = dir; }
	inline void  SetDir(string&& dir)      { directory_ = dir; }
	/*void         GetXSs(Regime reg = Regime::NONE, ProdMode mode = ProdMode::NONE, WCvalue val = WCvalue::NONE, size_t nbEvents = 0);
	void         GetXSs(const std::vector<Regime>& regimes,
						const std::vector<ProdMode>& modes,
						const std::vector<WCvalue>& vals,
						const std::vector<size_t>& vecNbEvents);*/
	void         ReadBrParametrisation(istream& is);
	void         SetBrParametrisation(const map<string, double>& param);

	inline const map<WC, double> GetBrParametrisation() const noexcept { return  brPerChanWcVal_; }
	inline       map<WC, double> GetBrParametrisation()       noexcept { return  brPerChanWcVal_; }
	//void         PrintStatistics(ostream& os) const;
	//void         PrintXss(ostream& os = std::cout) const;
	//void		AverageWCvalues(XSperWC& interfxs);
	void		AverageWCvalues();
	void        ComputeParametrisation() override;

	//inline const string GetChannel() const { return channel_; }
	//inline       string GetChannel()       { return channel_; }
	//void		PrintParametrisationsXML(ostream& os = std::cout) const;
	//void		PrintParametrisationsXML(const string&& name) const;
private:
	void		RegisterXS(const ParseResults& res);
	void        RegisterSmXs(const ParseResults& res);
	void        RegisterIntXs(const ParseResults& res);
	void        RegisterBsmXs(const ParseResults& res);
	void		RegisterWidth(const ParseResults& res);
	void		AddXS(CrossSections& l, const CrossSections& r);
	void		AddXS(XsChannelTruthBinWc& db,
					  CH ch,
					  WC code,
					  eft::WCvalue wcVal,
					  const CrossSections& xss) const;
	void        AddXS(XsChannelTruthBinWc& db, const ParseResults& res) const;
	void inline AddWidth(Width& l, const Width& r);
	const CrossSections
		AverageWCvaluesOneWC(const vector<eft::WCvalue>& wcVals,
			const vector<CrossSections>& xss
		) const;

	void AverageIntValues();
	void AverageBsmValues();

	map<WC, map<string, ValErr<double>>>
	Merge(const XSperWC& interfXs, const string& channel) const;

	map<string, ValErr<double>>
	Merge(const CrossSections& interfXs, const string& channel) const;
private:
	shared_ptr<eft::Merger> merger_;
	mutable string  varNameFromTTree_;
	string        directory_;
	//const string  channel_;
	bool          toReadBrFile_;

	//map<string, 
	CrossSections    smXSS_;
	Widths           SMwidths_;

	XSperWC          interfXS_;

	std::map<WCpair, std::map<WCvalue, CrossSections>>  bsmXS_;

	//RegistryTruthBins regTB_;          // all available truth bins
	RegistryWCcode    regWCcode_;      // all available WC
	std::set<WCpair>  regWCpairs_;
	WidthPerChanWcVal widthPerChanWcVal_;
	ValPerChanWc      brPerChanWcVal_;
	XsChannelTruthBinWc xsChannelTruthBinWc_;
};

//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::Register(const ParseResults& res) 
{
	//LOG_DURATION("Register");
	//std::cout << "[Register] Dispacth the resul according to the type: production or decay" << endl;
    //EFT_PROFILE_FN();
	if (res.regime == eft::Regime::PRODUCTION)
		RegisterXS(res);
	else
		RegisterWidth(res);
}
//////////////////////////////////////////////////////////////////////////////////////
inline void HiggsModule::AddWidth(Width& l, const Width& r)
{
    //EFT_PROFILE_FN();
	//LOG_DURATION("Higgs::AddWidth");
	l.val += r.val;
	l.err = std::sqrt(l.err * l.err + r.err * r.err);
}
//////////////////////////////////////////////////////////////////////////////////////
ParseResults HiggsModule::ParseContent(ParseResults& res) const
{
	/*********************************************
	* Extracts either cross-section or width
	* from a given file by calling the general
	* ParseContent function, common for all modules
	*
	* In the HiggsModule it will call one of the two:
	* ParseXsContent
	* ParseWidthContent
	**********************************************/
    //EFT_PROFILE_FN();
	if (res.regime == eft::Regime::PRODUCTION)
		ParseProdContent(res.filename, res);
	else
		ParseDecayContent(res.filename, res);
	return res;
}
//////////////////////////////////////////////////////////////////////////////////////
inline unique_ptr<HiggsModule> HiggsModule::create(HiggsConfig* cfg) {
	return make_unique<HiggsModule>(cfg);
}
//////////////////////////////////////////////////////////////////////////////////////
inline unique_ptr<HiggsModule> 
HiggsModule::create(std::string& channel,
					std::string& varNameTTree,
					bool readBrFile)
{
	return make_unique<HiggsModule>(channel, varNameTTree, readBrFile);
}
///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//inline unique_ptr<HiggsModule> HiggsModule::create(unique_ptr<HiggsConfig> cfg) {
//	return HiggsModule::create(cfg.get());
//}
//////////////////////////////////////////////////////////////////////////////////////

} // namespace modules
} // namespace eft

#endif // !EFT_HIGGSMODULE_H
