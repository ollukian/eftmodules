#pragma once
#ifndef EFT_MODULES_HIGGSMODULE_H
#define EFT_MODULES_HIGGSMODULE_H


#include "TModuleConfig.h"
#include "../Database/ParseResults.h"
#include "Merger.h"

namespace eft {
namespace modules {

class HiggsConfig final : public TModuleConfig
{
public:
	explicit HiggsConfig(std::string&  name_) : TModuleConfig(move(name_)) {}
	explicit HiggsConfig(std::string&& name_) : TModuleConfig(name_) {}
	explicit HiggsConfig(const char* name)    : TModuleConfig(name) {}
	explicit HiggsConfig(const std::string& name) : TModuleConfig(name) {};
	~HiggsConfig() override = default;
	static inline unique_ptr<HiggsConfig> create(std::string& name_);
	static inline unique_ptr<HiggsConfig> create(std::string&& name_);


	//inline void Init() const;

	// from the base class to be overloaded:
	const std::vector<std::string> GetFiles() const noexcept override;
	void                           PrintInfo(std::ostream& os) const override;
	//std::stringstream              PrintInfoInSS() const override;

	//////////////////////////////////////////////////////////
	////  specific for the HiggsConfig
	std::vector<std::string> GetListFiles(
		Regime reg    = Regime::NONE,
		ProdMode mode = ProdMode::NONE,
		WCvalue val   = WCvalue::NONE,
		size_t nbEvents = 0
	) const;
	//vector<string>                      GetListFiles(const string& path, WilsonCoefficientsCode code, size_t nbEvents, Regime reg = Regime::PRODUCTION, WCvalue val = WCvalue::NONE);


	inline HiggsConfig& SetRegimes(std::vector<Regime>& regimes);
	inline HiggsConfig& SetModes  (std::vector<ProdMode>& modes);
	inline HiggsConfig& SetVals   (std::vector<WCvalue>& vals);
	inline HiggsConfig& SetNbEvents(std::vector<size_t>& nb);

	inline HiggsConfig& SetRegimes(std::vector<Regime>&& regimes);
	inline HiggsConfig& SetModes  (std::vector<ProdMode>&& modes);
	inline HiggsConfig& SetVals   (std::vector<WCvalue>&& vals);
	inline HiggsConfig& SetNbEvents(std::vector<size_t>&& nb);

	inline HiggsConfig& SetRegimes(Regime regime);
	inline HiggsConfig& SetModes(ProdMode mode);
	inline HiggsConfig& SetVals(WCvalue val);
	inline HiggsConfig& SetNbEvents(size_t nb);


	inline HiggsConfig& SetDirectory(const std::string& dir)  { directory_ = dir; return *this; }
	inline HiggsConfig& SetDirectory(std::string& dir)  { directory_ = move(dir); return *this; }
	inline HiggsConfig& SetDirectory(std::string&& dir) { directory_ = dir;       return *this; }

	inline HiggsConfig& SetReadBr(bool toRead) { readBr = toRead; return *this; }

	inline       shared_ptr<eft::Merger>& GetMerger()       { return merger_; }
	inline const shared_ptr<eft::Merger>& GetMerger() const { return merger_; }
	inline       bool                     GetReadBrStatus() const { return readBr; }
	inline const string&                  GetDirectory()    const { return directory_; }

	inline HiggsConfig& SetCatInitNames(const vector<string>& names) { merger_->SetInitNames(names); return *this; }
	inline const vector<string>& GetCatInitNames() const { return merger_->GetInitNames();}

	inline       HiggsConfig&             AddMergingScheme(const string& json_path);
	inline       HiggsConfig&             AddMergingScheme(MergeData& merging);
	inline       HiggsConfig& AddMergingScheme(const map<size_t, size_t>& merging,
												const vector<string>& mergedNames,
												const vector<string>& initNames);

	inline map<string, string>& GetMergingScheme() { return merger_->GetMergingScheme(moduleName).mergingStrings_;}

private:
	bool readBr { false };

	std::string             directory_ {""};
	shared_ptr<eft::Merger> merger_ { new Merger() };
	mutable std::string     mergingSchemeName_ {""};

	std::vector<Regime>		regimes_ { eft::Regime  ::PRODUCTION, eft::Regime::DECAY };
	std::vector<ProdMode>	modes_   { eft::ProdMode::NONE };
	std::vector<WCvalue>	vals_    { eft::WCvalue ::NONE };
	std::vector<size_t>		vecNbEvents_ {0};
};



inline HiggsConfig& eft::modules::HiggsConfig::SetRegimes(std::vector<Regime>& regimes) {
	regimes_ = move(regimes);
	return *this;
}

inline HiggsConfig& eft::modules::HiggsConfig::SetModes(std::vector<ProdMode>& modes) {
	modes_ = move(modes);
	return *this;
}
inline HiggsConfig& eft::modules::HiggsConfig::SetVals(std::vector<WCvalue>& vals) {
	vals_ = move(vals);
	return *this;
}
inline HiggsConfig& eft::modules::HiggsConfig::SetNbEvents(std::vector<size_t>& nb) {
	vecNbEvents_ = move(nb);
	return *this;
}

inline HiggsConfig& eft::modules::HiggsConfig::SetRegimes(std::vector<Regime>&& regimes) {
	regimes_ = move(regimes);
	return *this;
}

inline HiggsConfig& eft::modules::HiggsConfig::SetModes(std::vector<ProdMode>&& modes) {
	modes_ = move(modes);
	return *this;
}
inline HiggsConfig& eft::modules::HiggsConfig::SetVals(std::vector<WCvalue>&& vals) {
	vals_ = move(vals);
	return *this;
}
inline HiggsConfig& eft::modules::HiggsConfig::SetNbEvents(std::vector<size_t>&& nb) {
	vecNbEvents_ = move(nb);
	return *this;
}

inline HiggsConfig& HiggsConfig::SetRegimes(Regime regime) {
	regimes_ = { regime };
	return *this;
}

inline HiggsConfig& HiggsConfig::SetModes(ProdMode mode) {
	modes_ = { mode };
	return *this;
}

inline HiggsConfig& HiggsConfig::SetVals(WCvalue val) {
	vals_ = { val };
	return *this;
}

inline HiggsConfig& HiggsConfig::SetNbEvents(size_t nb) {
	vecNbEvents_ = { nb };
	return *this;
}

inline HiggsConfig& HiggsConfig::AddMergingScheme(const string& json_path) {
    EFT_DEBUG("AddMergingScheme from {} for {}", json_path, moduleName);
	merger_->AddMergingScheme(moduleName, json_path);
	mergingSchemeName_ = moduleName;
	return *this;
}

inline HiggsConfig& HiggsConfig::AddMergingScheme(MergeData& merging)
{
	merger_->AddMergingScheme(moduleName, merging);
	mergingSchemeName_ = moduleName;
	return *this;
}

inline HiggsConfig& HiggsConfig::AddMergingScheme(const map<size_t, size_t>& merging, const vector<string>& mergedNames, const vector<string>& initNames)
{
	merger_->AddMergingScheme(moduleName, merging, mergedNames, initNames);
	mergingSchemeName_ = moduleName; 
	return *this;
}

inline unique_ptr<HiggsConfig> HiggsConfig::create(std::string& name_) {
	return make_unique<HiggsConfig>(move(name_));
}

inline unique_ptr<HiggsConfig> HiggsConfig::create(std::string&& name_) {
	return make_unique<HiggsConfig>(name_);
}

//std::stringstream HiggsConfig::PrintInfoInSS() const {
//    stringstream ss;
//    PrintInfo(ss);
//    return ss;
//}
//void HiggsConfig::Init() const {
//	moduleName = "Higgs";
//}





} // namespace modules
} // namespace eft

#endif // !EFT_MODULES_HIGGSMODULE_H