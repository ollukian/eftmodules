
#include "SandboxHiggsChain.h"

//#include "../EftDatabase/ImpactPlotter.h"
#include "../DataBase/ImpactPlotter.h"


#include "../Utils/profile.h"

//#include "Logger.h"
//#include "Profiler.h"

#include "Logger.h"
#include "../Utils/Profiler.h"

#include "../Utils/JsonHelper.h"

#include <memory>
#include <fstream>
#include <iostream>

using namespace eft::modules;
using namespace std;

int RunSandboxHiggsChain(int argc, char* argv[]) {
    EFT_PROFILE_FN();
    EFT_INFO("Sandbox::Higgs");
	//cout << "[Sandbox][Higgs]" << endl;
	LOG_DURATION("Sandbox::Higgs"); 

	if (argc >= 1) {
		cout << "[Sandbox][Higgs] got " << argc << " parameters:\n";
		for (size_t idx = 0; (int) idx < argc; ++idx) {
			cout << "\t {" << argv[idx] << "}\n";
		}
	}

	//unique_ptr<HiggsChainModules> higgsChain = make_unique<HiggsChainModules>();
    auto higgsChain = HiggsChainModules::create();
	////// add the yy module
	auto hyyCfg = HiggsConfig::create("yy");

    TCHAR buffer[MAX_PATH] = { 0 };
	GetModuleFileName(nullptr, buffer, MAX_PATH);
	EFT_DEBUG("current dir: {}", buffer);

	hyyCfg->AddMergingScheme("../source/hyy_map_26poi.json")
	//hyyCfg->AddMergingScheme("source/hyy_map_CMS_like.json")
		.SetCatInitNames(eft::fineNames)
		.SetObservableName("HiggsTemplateCrossSectionsStage12/STXS_stage1_2_fine_pTjet30");
	hyyCfg->SetDirectory("/sps/atlas/o/ollukian/ATLAS_HGam/EFT/HyyEFT/MyEFT/res")
	//hyyCfg->SetDirectory("/sps/atlas/o/ollukian/ATLAS_HGam/EFT/HyyEFT/MyEFT/FromEleonora/Rivet_samples_Aleksei_prod")
	.SetReadBr(true);

	hyyCfg->SetNbEvents(100000);

	auto hyyModule = HiggsModule::create(hyyCfg.get());

	std::istringstream ss(R"(
{
	-40.15 * CHB - 13.08 * CHW + 22.40 * CHWB - 0.9463 * CHW + 0.1212 * CHBOX - 0.2417 * CHD
	+ 0.03447 * CUHRE - 1.151 * CUWRE - 2.150 * CUBRE - 0.3637 * CHL3 + 0.1819 * CLL1
}
)");
	hyyModule->ReadBrParametrisation(ss);

	/// <summary>
	/// add the bb module
	/// </summary>
	///
	unique_ptr<HiggsConfig> hbbConfig = make_unique<HiggsConfig>("bb");
	hbbConfig->AddMergingScheme("source/hbb_map.json")
		.SetObservableName("HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30");
	hbbConfig->SetDirectory("/sps/atlas/o/ollukian/ATLAS_HGam/EFT/HyyEFT/MyEFT/FromEleonora/Rivet_samples_Aleksei_prod");
	hbbConfig->SetCatInitNames(eft::coarseNames);

	unique_ptr<HiggsModule> hbbModule = make_unique<HiggsModule>(hbbConfig.get());


	higgsChain->AddModule(hyyModule.get());
    higgsChain->AddModule(hbbModule.get());

    EFT_INFO("try to write down higgsChain to cout");
    nlohmann::json j = *higgsChain;
    cout << setw(2) << j;
    EFT_INFO("DONE");

    ofstream fs("test_obj.txt");

    if ( ! fs.is_open() ) {
        EFT_CRITICAL("file test_obj.txt is not open for writing");
        exit(0);
    }
    else {
        EFT_DEBUG("Write j into test_obj.txt");
        fs << setw(2) << j;
        fs.close();
    }

    EFT_INFO("Try to de-serialise this object");
    nlohmann::json j_read;

    std::ifstream ifs("test_obj.txt");

    if ( ! ifs.is_open() ) {
        EFT_CRITICAL("file test_obj.txt is not open for reading");
        exit(0);
    }

    ifs >> j_read;
    //const auto& chain = j_read.at("channels");
    //EFT_DEBUG("chain contains {} Higgs channels", chain.size());

    const auto& chain = j_read.get<HiggsChainModules>();

    //exit(0);
    //j << *higgsChain;

	higgsChain->Run();

	// make an impact plot
	const auto& higgsParametrisationLinear = higgsChain->GetParametrisation()->linear;
	
	shared_ptr<eft::plot::ImpactPlotConfig> impactCfg = make_shared<eft::plot::ImpactPlotConfig>();
	impactCfg->bins_ = eft::mergedBinsName;
	impactCfg->encodingMode = "linparam";
	impactCfg->smXSS_     = hyyModule->GetMergedSmXSs();
	impactCfg->interfXS_ = hyyModule->GetAveragedIntXSs();

	using WC = eft::WilsonCoefficientsCode;

	impactCfg->groupsWCcodes = {
		{WC::CHG, WC::CUGRE, WC::CUHRE, WC::CG, WC::CQQ1},
		{WC::CHW, WC::CHWB, WC::CHB},
		{WC::CHQ3, WC::CHU, WC::CHQ1}
	};

	eft::plot::ImpactPlotter impactPlotter(impactCfg.get());
	impactPlotter.Plot();
	return 0;
}