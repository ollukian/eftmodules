#pragma once
#ifndef EFT_EFTMODULE_EW_H
#define EFT_EFTMODULE_EW_H


#include "TEFTModule.h"
#include "EWConfig.h"
#include "../EFTManager/CrossSections.h"
#include "../EFTManager/WilsonCoefficients.h"
#include "../Core/WCpair.h"

#include <unordered_map>
#include <map>
#include <string>

namespace eft {
namespace modules {


class EWModule final : public TEFTModule
{
public:
	using XSperWC           = map<eft::WilsonCoefficientsCode, std::map<eft::WCvalue, CrossSections>>;
	using RegistryWCcode    = std::set<WilsonCoefficientsCode>;
	using RegistryTruthBins = std::set<std::string>;

	EWModule(std::string& channel, const std::string& varNameTTree = {}) {};
	explicit EWModule(EWConfig* scfg);
	~EWModule() final {};

	//virtual  void       ReadFiles(const string& dir) override;
	void				Register(const ParseResults& res) override ; // the result to be moved
	const ParseResults  Parse(const string& filename) const override;
	ParseResults		ParseMetaInfo(const string& filename) const override; // parses header
	ParseResults		ParseContent(ParseResults& res) const override; // extract XS/Width/whatever
	void				ProcessFiles() override;


private:
	using WC = WilsonCoefficientsCode;
	using XSS = std::map<std::string, ValErr<double>>;
	using IntXSS = std::map<WC, std::map <WCvalue, XSS>>;
	using BsmXSS = std::map<WC, std::map<WCpair, std::map<WCvalue, XSS>>>;

	const std::string  varNameFromTTree_;
	const std::string  directory_;

	XSS    smXSS_;
	IntXSS interfXS_;
	BsmXSS bsmXSS_;

	//CrossSections    smXSS_;
	
	RegistryTruthBins regTB_;          // all available truth bins
	RegistryWCcode    regWCcode_;
	//static std::unordered_map<std::string, 
};

} // namespace modules
} // namespace eft

#endif // !EFT_EFTMODULE_EW_H