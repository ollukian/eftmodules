#include "HiggsConfig.h"
#include "../Utils/profile.h"
#include "../EFTManager/WilsonCoefficients.h"
#include "../EFTManager/Parser.h"

#include "../Utils/Logger.h"
#include "../Utils/Profiler.h"

#ifndef _MSC_VER
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#include "TSystemDirectory.h"
#pragma GCC diagnostic pop
#endif // !_MSC_VER


#include <iomanip>
#include <fstream>

using namespace std;

namespace eft {
namespace modules {




const vector<string> HiggsConfig::GetFiles() const noexcept
{
    EFT_PROFILE_FN();
	LOG_DURATION("HigCfg::GetFiles");
	cout << "[HiggsConfig]{GetFileList} from: " << directory_ << " for the following settings : \n";
	cout << '\t' << setw(15) << "Regimes: ";
	for (const auto reg : regimes_) { cout << reg << ", "; }
	cout << endl;
	cout << '\t' << setw(15) << "ProdModes: ";
	for (const auto mode : modes_) { cout << mode << ", "; }
	cout << endl;
	cout << '\t' << setw(15) << "WCvalues: ";
	for (const auto val : vals_) { cout << val << ", "; }
	cout << endl;
	cout << '\t' << setw(15) << "NbEvents: ";
	for (const auto nb : vecNbEvents_) { cout << nb << ", "; }
	cout << endl;

	vector<string> files;

    for (const auto& regime : regimes_) {
        for (const auto& mode : modes_) {
            for (const auto& val : vals_) {
                for (const auto& nbEvents : vecNbEvents_) {
					cout << "[HiggsConfig][GetFiles][" 
						 << setw(15) << regime << "]["
                         << setw(5) << mode << "]["
                         << setw(7) << val << "]["
                         << setw(7) << nbEvents << "]... \n";
					size_t size_before = files.size();
					auto tmp = GetListFiles(regime, mode, val, nbEvents);
					files.insert(files.begin(), tmp.begin(), tmp.end());
					//files.push_back(GetListFiles(regime, mode, val, nbEvents));
					size_t size_after = files.size();
					cout << "[HiggsConfig][GetFiles][" 
						 << setw(15) << regime << "]["
						 << setw(5) << mode << "]["
						 << setw(7) << val << "]["
						 << setw(7) << nbEvents << ']'
						 << (size_after - size_before) << " files added \n";
                }
            }
        }
    }
	return files;
} // GetFiles() 
void HiggsConfig::PrintInfo(std::ostream& os) const
{
    EFT_PROFILE_FN();
	os << "+=========================================================+\n";
	os << "| HiggsConfig: " << setw(15) << moduleName << "         |\n";
	os << "| VariableToExctract: " << setw(40) << observableNameTTree_ << " |\n";
	os << "| directory : " << setw(40) << directory_ << " |\n";
	os << "| regimes: ";
	for (const auto reg : regimes_) { cout << " " << reg; }
	os << " |\n";
	os << "| modes_: ";
	for (const auto reg : modes_) { cout << " " << reg; }
	os << " |\n";
	os << "| vals_: ";
	for (const auto reg : vals_) { cout << " " << reg; }
	os << " |\n";
	os << "+==========================================================\n";
}
//////////////////////////////////////////////////////////////////////////////////////
vector<string> HiggsConfig::GetListFiles(Regime reg, ProdMode mode, WCvalue val, size_t nbEvents) const
{
	// TODO: to make it system independent. Currently, works only for Linux
	// if we update to c++17, then one can use std::filepath and iterators over
	// files
	EFT_PROFILE_FN();
	LOG_DURATION("HigCfg::GetListFiles");

	vector<string> specification;

	if (reg == Regime::PRODUCTION) {
		specification.emplace_back("production");
		if (mode == ProdMode::GGF)
			specification[0] += "_ggH";
		else if (mode != ProdMode::NONE)
			specification[0] += "_" + enumToStr<ProdMode>(mode);
	}
	else if (reg == Regime::DECAY)
		specification.emplace_back("decay");

	if (val != WCvalue::NONE) {
		size_t nb = (int)val;
		specification.emplace_back(to_string(nb) + "_WCmode");
	}

	if (nbEvents != 0)
		specification.emplace_back(to_string(nbEvents) + "_events");
	
	string unix_command = "ls " + directory_;
	for (const string& spec : specification) {
		unix_command += " | grep -i  " + spec;
	}

	cout << "unix command: \n";
	cout << unix_command << endl;

	unix_command += " > tmp.txt";

	//gSystem->Exec(unix_command.c_str());
	system(unix_command.c_str());


	string line;
	fstream fs("tmp.txt", std::ios_base::in);
	vector<string> res;
	cout << "content of tmp.txt:\n";
	while (std::getline(fs, line)) {
		cout << line << endl;
		res.emplace_back(line);
	}

	system("rm tmp.txt");
	//gSystem->Exec("rm tmp.txt");

	return res;
}
//////////////////////////////////////////////////////////////////////////////////////

} // namespace modules
} // namespace eft