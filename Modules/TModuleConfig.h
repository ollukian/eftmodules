#pragma once

#ifndef EFT_MODULES_TMODULECONFIG_H
#define EFT_MODULES_TMODULECONFIG_H

#include <vector>
#include <string>
#include <iostream>
#include <sstream>

namespace eft {
namespace modules {

/// <summary>
/// TModuleConfig is a virtual class, which inheritances
/// 	should provide a list of files for analysis
/// 	depending on the individual settings for each
/// 	module. For example, to take only GGH files
/// 	for the Higgs module or so on.
/// 
///		
/// 
/// </summary>

class TModuleConfig
{
	// TODO: to think: to make it static?
public:
	//! Name Getters.
	/*!
	  A more elaborate description of the constructor.
	*/
	explicit TModuleConfig(std::string&& name) : moduleName(name) {}
	explicit TModuleConfig(std::string& name) :  moduleName(std::move(name)) {}
	explicit TModuleConfig(const char* name) :   moduleName(name) {}
	explicit TModuleConfig(const std::string& name) : moduleName(name) {}

	virtual ~TModuleConfig() {}

	const std::string  GetName() const { return moduleName; }
	      std::string  GetName()       { return moduleName; }
	const std::string  GetObsName() const { return observableNameTTree_;  }
	      std::string  GetObsName()       { return observableNameTTree_; }

	void SetObservableName(const std::string& obs)  { observableNameTTree_ = obs; }
	void SetObservableName(std::string& obs)  { observableNameTTree_ = std::move(obs); }
	void SetObservableName(std::string&& obs) { observableNameTTree_ = obs; }

	/**
	* Provides the list of files to be registered by the module.
	* Implementation dependent! 
	* It's up to the user to define the behaviour
	* 
	**/
	virtual const std::vector<std::string> GetFiles() const noexcept = 0;
	virtual       void                     PrintInfo(std::ostream& os) const = 0;
	//virtual std::stringstream PrintInfoInSS() const = 0;

protected:
	const   std::string moduleName           = ""; //!< name of the module
	mutable std::string observableNameTTree_ = "";
};


} // namespace modules
} // namespace eft

#endif // !EFT_MODULES_TMODULECONFIG_H

