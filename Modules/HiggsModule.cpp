#include "HiggsModule.h"
#include "../EFTManager/ParseResults.h"
#include "../EFTManager/Parser.h"
#include "../Utils/profile.h"
#include "../Utils/test_runner.h"

#include "../DataBase/TEntry.h"
#include "../DataBase/EntryParser.h"
#include "../DataBase/LinearParametrisation.h"
#include "../DataBase/WIdthOrXSsEntry.h"

#include "../Core/WCpair.h"
#include "../Core/Core.h"
#include "../Utils/Profiler.h"
#include "../Utils/Logger.h"
//#include "Core.h"
//#include "../ImpactPlotter.h" 

//#ifndef _MSC_VER
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
//#endif // !_MSC_VER


#include <numeric>

namespace eft {
namespace modules {

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////
const ParseResults HiggsModule::Parse(const string& filename) const
{
    EFT_PROFILE_FN();
    auto res = ParseMetaInfo(filename);
    ParseContent(res);
    //auto res = ParseContent(move(ParseMetaInfo(filename)));
    /*if (res.regime == eft::Regime::PRODUCTION)
        ParseProdContent(filename, res);
    else
        ParseDecayContent(filename, res);*/

    EFT_INFO("HiggsModule: parse channel: " + channel_ + " from: " + filename);
    //cout << "[HiggsModule]{" << setw(10) << channel_ << "} parsed: " << filename << '\n';
    cout << res << endl;
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
ParseResults HiggsModule::ParseMetaInfo(const string& filename) const
{
    EFT_PROFILE_FN();
    //LOG_DURATION("Higgs::ParseMetaInfo");
    EFT_INFO("[HiggsModule]::ParseMetaInfo from: " + filename);
    //cout << "[HiggsModule][ParseMetaInfo] from {" << filename << "}\n";
    EFT_DEBUG("directory: " + directory_ + " => full filename: " + directory_ + "//" + filename);
    //cout << "directory: " << directory_ << " => full filename: " << directory_ + "//" + filename << endl;

    ParseResults res;
    res.filename = directory_ + '/' + filename;
    auto tokens = Tokenize(filename); // TODO: put the right tokeniser here 
    size_t idx_token = 0;
    //cout << "[ParseMetaInfo] {" << filename << '}' << endl;
    //cout << "[ParseMetaInfo] determine the name convention: Old, New, Ana, Eleonora..." << endl;
    //EFT_DEBUG("determine t")

    string wilsonCoeffs; // to be set after the regime
    const string regime = CapitaliseString(tokens[idx_token++]); // idx_token -> 1
    if (regime == "PRODUCTION") {
        res.regime = eft::Regime::PRODUCTION;
        //cout << "[ParseMetaInfo] PRODUCTION mode: |";
        cout << "[ParseMetaInfo] Production mode ";
        EFT_INFO("production mode");

        // if(isdigit(*)) is served 
        // to handle old convention in the format:
        //  12300
        //  |\/\/
        //  ||  ^ ---- Wilson Coeff  #2
        //  |^-------- Wilson Coeff  #1
        //  ^--------- prodmode
        //
        // Otherwise:
        //  _ggH_cHq_
        //    |   ^---- WC1
        //    ^-------- production mode
        // 
        // NOTE: format about BSM is not defined yet...

        if (isdigit(tokens[idx_token][0])) { 
            cout << " => from a digit: |";
            char prodModeDigit = tokens[idx_token][0];
            cout << prodModeDigit << "| ==> ";
            res.prodMode = ParseProductionMode(prodModeDigit);
            wilsonCoeffs = tokens[idx_token].substr(1, 4);
            idx_token++;
        }
        else {
            const string prodMode = CapitaliseString(tokens[idx_token++]); // idx_token -> 2
            cout << " => from literals: |" << prodMode << "|\n";
            //cout << prodMode << '|';
            res.prodMode = ParseProductionMode(prodMode);
            cout << " ==> " << res.prodMode << endl;
            wilsonCoeffs = CapitaliseString(tokens[idx_token++]); // idx_token -> 3
            //cout << "[ParseMetaInfo] take: " << wilsonCoeffs << " as WCs" << endl;
        } // parsing of prodmode: from a digit or from literals

    }
    else if (regime == "DECAY") {
        EFT_DEBUG("[ParseMetaInfo] Decay");
        //cout << "[ParseMetaInfo] DECAY" << endl;
        res.regime = eft::Regime::DECAY;
        string& decChannel = tokens[idx_token];  // idx_token = 1 
        // => no icrease, since WC maybe in the same token
        // for old convention
        cout << "[ParseMetaInfo] deal with decayChannel: |" << decChannel << "|" << endl;
        if (isdigit(decChannel[0])) {
            const string& decIdx = decChannel.substr(0, 2);
            cout << "[ParseMetaInfo] parse: |" << decIdx << "| as channel ==> ";

            res.decayChannel = ParseDecayChannel(stoi(decIdx));
            cout << res.decayChannel << endl;
            wilsonCoeffs = decChannel.substr(2, decChannel.length());
            cout << "[ParseMetaInfo] parse remnants: |" << wilsonCoeffs << "| as WC" << endl;
            //cout << "[ParseMetaInfo] decayChannel: |" << res.decayChannel << endl;
            //cout << 
            idx_token++; //  idx_token -> 2
        }
        else {
            CapitaliseString(decChannel);
            idx_token++;
            res.decayChannel = ParseDecayChannelEleonora(decChannel);
            //throw std::logic_error("Parsing of decay channels from name is NOT avaible - add it");
            //res.decayChannel = ParseDecayChannel(decChannel);
            wilsonCoeffs = CapitaliseString(tokens[idx_token++]); // idx_token -> 2
        }
    }
    else {
        throw std::runtime_error("Parsing filename: regime: {" + regime + "}"
            + " is not known. Available: production, decay.");
    }

    //bool isFromEleonora = true;
    cout << "[ParseMetaInfo] parse WCs: |";
    //wilsonCoeffs = tokens[2];
    cout << wilsonCoeffs << '|';
    if (isdigit(wilsonCoeffs[0])) {
        //isFromEleonora = false;
        cout << " from a digit ==> ";
        res.wc. SetId(stoi(wilsonCoeffs.substr(0, 2)));
        res.wc2.SetId(stoi(wilsonCoeffs.substr(2, 2)));
        //res.wc.SetId(ParseWCid(wilsonCoeffs));
      // cout << "parse wc code. Id = " << res.wc.GetId() << endl;
        res.wcCode  = ParseWCcode(res.wc.GetId());
        res.wcCode2 = ParseWCcode(res.wc2.GetId());
        cout << res.wcCode << " and " << res.wcCode2 << endl;
    }
    else if (wilsonCoeffs[0] == 'c' || wilsonCoeffs[0] == 'C') {
        //isFromEleonora = true;
        CapitaliseString(wilsonCoeffs);
        cout << " from a name: " << wilsonCoeffs;
        res.wcCode = strToWC(wilsonCoeffs);
        cout << " ==> " << res.wcCode << endl;
    }
    else if (wilsonCoeffs == "SM") {
        res.wcCode = WilsonCoefficientsCode::SM;
        cout << "SM" << endl;
    }
    else {
        throw std::logic_error("Impossible to parse WC from: |" + wilsonCoeffs
            + "|. Available: number xy or cXX => from the name");
    }

    if (res.wcCode == WilsonCoefficientsCode::SM /* &&! isFromEleonora*/) {
        res.theoryRegime = TheoryRegime::SM;
        cout << "[ParseMetaInfo] no need to parse WC value" << endl;
    }
    else {
        res.theoryRegime = TheoryRegime::INT;
        cout << "[ParseMetaInfo] need to parse WC value: ";
        //cout << "[ParseMetaInfo] parse WC value: ";
        const string wcValStr = CapitaliseString(tokens[idx_token++]);
        cout << wcValStr << " ==> ";
        res.wcValue = stoi(wcValStr);
        res.wcValueCode = ParseWCvalue(res.wcValue);
        cout << res.wcValueCode << endl;

        const string wcValueStr = CapitaliseString(tokens[idx_token++]);
        if (wcValueStr != "WCMODE")
            throw std::logic_error("Expected to get: WCmode at the 4th token, but got: |"
                + wcValueStr + "|");

    }

    const string& nbEvents = tokens[idx_token];
    cout << "[ParseMetaInfo] parse nbEvents: |" << nbEvents;
    try {
        res.nbGeneratedEvents = stoi(nbEvents);
    }
    catch (invalid_argument& exception) {
        cout << " ==> not valid! Error!" << endl;
        throw std::runtime_error("Impossible to convert: |" + nbEvents
            + " to integer! We got: " + exception.what());
    }
    cout << " ==> " << res.nbGeneratedEvents << endl;
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::ParseDecayContent(const string& filename, ParseResults& res)
{
    EFT_PROFILE_FN();
    //LOG_DURATION("Higgs::ParseDecayContent");
    //cout << "[HiggsModule][ParseDecayContent] from " << filename << endl;
    EFT_INFO("HiggsModule::ParseDecayConten fron " + filename);

    fstream f(filename, std::ios_base::in);

    if (!f.is_open()) {
        EFT_CRITICAL("IMPOSSIBLE to open: " + filename);
        throw std::runtime_error(
                "[HiggsModule][ParseDecayContent] Impossible to open: " + filename + " to get the width");
    }
    string val, err, trash;
    f >> val >> trash >> err;
    Width width;
    width.val = stod(val);
    width.err = stod(err);
    cout << "[HiggsModule][ParseDecayContent] Width: " << width << endl;
    res.width_ = width;
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::ParseProdContent(const string& filename, ParseResults& res) const
{
    EFT_PROFILE_FN();
    LOG_DURATION("Higgs::ParseProdContent");
    EFT_DEBUG("[HiggsModule][ParseProdContent] from: {}", filename);
   // cout << "[HiggsModule][ParseProdContent] from: {" << filename << "}\n";
    //CrossSections res;

//#ifndef _MSC_VER // to prevent the MSVS dealing with ROOT files

    unique_ptr<TFile> f= make_unique<TFile>(filename.c_str());
    if ( ! f->IsOpen() )
        throw std::runtime_error("[HiggsModule][GetCrossSection] impossible to open: " + filename);

    if ( f->Get(varNameFromTTree_.c_str() ) == nullptr) {
        cout << "[HiggsModule][GetCrossSection] file: " << filename << " is empty - do NOT consider it" << endl;
        return;
    }

    shared_ptr<TH1D> crossSections = make_shared<TH1D>(*reinterpret_cast<TH1D*>(f->Get(varNameFromTTree_.c_str())));

    size_t nbBins = crossSections->GetNbinsX();
    //cout << " * has " << nbBins << endl;
    //cout << " * their content: " << endl;

    truth_bin     bin;
    cross_section xs;

    for (size_t idx_bin = 0; idx_bin < nbBins; ++idx_bin) {
        bin = idx_bin;
        xs.val = crossSections->GetBinContent(idx_bin + 1);
        xs.err = crossSections->GetBinError(idx_bin + 1);
        //cout << "\t" << setw(15) << bin << setw(5) << " -> " << xs << endl;
        res.xsPerTB[bin] = xs;
    }
    f->Close();
    
//#endif // !_MSC_VER
    /*
        "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30"
    */
} 
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::RegisterXS(const ParseResults& res)
{
    EFT_PROFILE_FN();
  cout << "[HiggsModule][Register] XS" << endl;
  //LOG_DURATION("Higgs::RegisterXS");
  cout << "[Register] wc | id: " << res.wc.GetId()
       << " | value: " << res.wcValueCode
       << " ... " << endl;

  if (res.theoryRegime == eft::TheoryRegime::SM) {
      RegisterSmXs(res);
  }
  else if (res.theoryRegime == eft::TheoryRegime::INT) {
      RegisterIntXs(res);
  } // int xs
  else {
      RegisterBsmXs(res);
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::RegisterBsmXs(const ParseResults& res) {
    EFT_PROFILE_FN();
    const WCpair wcPair = make_wcPair(res.wcCode, res.wcCode2);
    bsmXS_[wcPair][res.wcValueCode] += res.xsPerTB;
    regWCpairs_.insert(wcPair);
    //throw std::runtime_error("HiggsModule::RegisterXS ERROR: impossible to register everyting but: SM, INT");
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::RegisterSmXs(const ParseResults& res) {
    EFT_PROFILE_FN();
    EFT_LOG_DURATION("Higgs::RegisterSmXs");
    cout << "[HiggsModule][Register] SM xs handler" << endl;
    cout << "before adding:" << endl;

    for (const auto& bin_xs : smXSS_) {
        cout << setw(25) << bin_xs.first << " => " << bin_xs.second << endl;
    }

    //cout << "add the following: " << endl;
    for (const auto& bin_xs : res.xsPerTB) {
        //cout << bin_xs.first << ": " << merger_->GetInitNames().at(bin_xs.first)
        //    << " " << bin_xs.second << endl;
        //<< " to " << smXSS_.at(bin_xs.first) << endl;
    }
    AddXS(smXSS_, res.xsPerTB);
    //cout << "after adding:" << endl;

    //for (const auto& bin_xs : smXSS_) {
    //    cout << setw(25) << bin_xs.first << " => " << bin_xs.second << endl;
    //}
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::RegisterIntXs(const ParseResults& res) {
    EFT_PROFILE_FN();
    EFT_DEBUG("[Higgs] {} channel INT XS handler", channel_);
    //cout << "[HiggsModule][Register] INT xs handler" << endl;
    if (interfXS_.find(res.wcCode) != interfXS_.end())
    {
        interfXS_[res.wcCode][res.wcValueCode];

        AddXS(interfXS_[res.wcCode][res.wcValueCode], res.xsPerTB);
        cout << "interfXS_["
            << res.wcCode << "]["
            << res.wcValueCode << "].size() = "
            << interfXS_[res.wcCode][res.wcValueCode].size() << endl;
    }
    else
    {
        cout << " no, it's the first entry" << endl;
        interfXS_[res.wcCode];
        interfXS_[res.wcCode][res.wcValueCode] = res.xsPerTB;
    }

    AddXS(xsChannelTruthBinWc_, res);

    regWCcode_.insert(res.wcCode);
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::RegisterWidth(const ParseResults& res)
{
    EFT_PROFILE_FN();
    EFT_LOG_DURATION("Higgs::RegisterWidth");
    EFT_DEBUG("[HiggsModule]{{Register}} width {0} for decayChannel {1}, wc: {2}, wcVal: {3} ",
              res.decayChannel, res.wcCode, res.wcValueCode);
    //cout << "[HiggsModule][Register] Width " << res.width_.val << " +- " << res.width_.err
    //    << " for the channel: " << res.decayChannel
    //    << " for the WC: " << res.wcCode
    //    << " for its value:" << res.wcValueCode
    //    << endl;

    // no ref, since light numeric types
    eft::DecayChannel           channel = res.decayChannel;
    eft::WilsonCoefficientsCode wc      = res.wcCode;
    eft::WCvalue                wcval   = res.wcValueCode;
 

    if (wc != eft::WilsonCoefficientsCode::SM)
    {
        AddWidth(widthPerChanWcVal_[channel]                 [wc][wcval],                                 res.width_);
        AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][wc][wcval],                                 res.width_);
        AddWidth(widthPerChanWcVal_[channel]                 [eft::WilsonCoefficientsCode::TOTAL][wcval], res.width_);
        AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][eft::WilsonCoefficientsCode::TOTAL][wcval], res.width_);
    
    }
    else { // SM
        // TODO: to get rid of the SM register and just deal with the usual registry with the ::SM code
        // this code is already present, just need to deal with it
        //cout << "[RegisterWidth] SM => add to [ch] and [total]" << endl;
        AddWidth(SMwidths_[channel],                  res.width_);
        AddWidth(SMwidths_[eft::DecayChannel::TOTAL], res.width_);
    }
}
//////////////////////////////////////////////////////////////////////////////////////
HiggsModule::HiggsModule(std::string& channel, std::string& varNameTTree, bool readBrFile)
    : varNameFromTTree_(move(varNameTTree))
    , toReadBrFile_(readBrFile)
{
    EFT_PROFILE_FN();
    SetChannel(move(channel));
}
//////////////////////////////////////////////////////////////////////////////////////
////void HiggsModule::GetXSs(Regime reg, ProdMode mode, WCvalue val, size_t nbEvents)
//{
//    LOG_DURATION("Higgs::GetXSs");
//    cout << "[GetXS]: " << reg << " " << mode << " " << static_cast<int> (val) << " " << nbEvents << endl;
//    cout << "[GetXS]: get list of files fulfilling the criterium.." << endl;
//
//    string specification = "";
//
//    //if (reg == Regime::DECAY)
//    //  specification = "old";
//    const vector<string> files = GetListFiles(directory_, reg, mode, specification, val, nbEvents);
//    cout << "[GetXS] found " << files.size() << " files" << endl;
//
//    if (files.empty())
//        throw std::runtime_error("NO FILES FOUND");
//
//    cout << "[GetXS] parse them..." << endl;
//    for (const string& file : files)
//    {
//        cout << " - " << file << endl;
//        if (reg == Regime::DECAY)
//            Register(Parse(file));
//        else
//            Register(Parse(file));
//    }
//    // cout << " XSs are obtained" << endl;
//}
//////////////////////////////////////////////////////////////////////////////////////
//void HiggsModule::GetXSs(const vector<Regime>& regimes = vector<Regime>{ Regime::NONE },
//    const vector<ProdMode>& modes = vector<ProdMode>{ ProdMode::TOTAL },
//    const vector<WCvalue>& vals = vector<WCvalue>{ WCvalue::NONE },
//    const vector<size_t>& vecNbEvents = vector<size_t>{ 0 })
//{
//    cout << "[GetXSs] for: "
//        << regimes.size() << " regimes, "
//        << modes.size() << " modes, "
//        << vals.size() << " wc vals, "
//        << vecNbEvents.size() << " nb of events"
//        << endl;
//
//    cout << "[GetXSs] launch sequential running for all combintation of parameters..." << endl;
//    for (const auto& regime : regimes)
//    {
//        for (const auto& mode : modes)
//        {
//            for (const auto& val : vals)
//            {
//                for (const auto& nbEvents : vecNbEvents)
//                {
//                    putTime(cout) << "[GetXSs][" << setw(15) << regime << "]["
//                        << setw(5) << mode << "]["
//                        << setw(7) << val << "]["
//                        << setw(7) << nbEvents << "]... \n";
//                    GetXSs(regime, mode, val, nbEvents);
//                }
//            }
//        }
//    }
//}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::ReadBrParametrisation(istream& is) {
    EFT_PROFILE_FN();
    EFT_LOG_DURATION("Higgs::ReadBrParametrisation");
    //using namespace eft::serialisation;
    std::vector<std::string> names;
    std::vector<double> vals;
    eft::serialisation::parseExpression(is, names, vals);
    //cout << "names: " << endl;
    //for (const string& name : names) {
    for (size_t idx = 0; idx < names.size(); ++idx) {
        string& name = names[idx];
        //cout << name << endl;
        auto wc = strToWC(name);
        brPerChanWcVal_[wc] = vals[idx];
        //cout << "assign to: " << setw(15) << name << " -> " << vals[idx] << endl;
    }
}
//////////////////////////////////////////////////////////////////////////////////////
const CrossSections
HiggsModule::AverageWCvaluesOneWC(const vector<eft::WCvalue>& wcVals,
    const vector<CrossSections>& xss) const
{

    /*********************************************
     *  Takes vector of available wcVals among:
     * 		- ONE
     * 		- MINHALF
     * 		- DOTONE
     *
     * 	and vector of CrossSections corresp
     * 	to this values of the WC
     *
     * 	to make a linear fit of
     * 	sigma(wc)
     *
     * 	return
     * 		this averaged value
     *********************************************/

    EFT_PROFILE_FN();
    EFT_LOG_DURATION("AverageWCvaluesOneWC");
    ASSERT_EQUAL(wcVals.size(), xss.size());

    size_t nbPoints = wcVals.size();

    EFT_DEBUG("[HiggsModule][AverageWCvaluesOneWC] for {} values", nbPoints);
    //cout << "[HiggsModule][AverageWCvaluesOneWC] for " << nbPoints << " available values" << endl;

    CrossSections res;
    truth_bin bin;
    // cross_section xs;
#ifndef _MSC_VER
    shared_ptr<TGraphErrors> gr;

    vector<double> wcs;
    vector<double> xsVals;
    vector<double> xsErrs;

    for (const auto& bin_xs : xss[0])
    {
        wcs.clear();
        xsVals.clear();
        xsErrs.clear();

        bin = bin_xs.first;
        cout << "[AverageWCvaluesOneWC] bin: " << bin;

        if (bin.id() == 99) {
            cout << " ==> skip it" << endl;
            continue;
        }

        cout << endl;
        //cout << "[AverageWCvaluesOneWC] transform " << wcVals.size() << " to double...";

        wcs = WcValsToDouble(wcVals);

        cout << "[AverageWCvaluesOneWC] start looping over wc values..." << endl;
        for (size_t idx_wc = 0; idx_wc < xss.size(); ++idx_wc)
        {
            auto& xs = xss[idx_wc][bin];
            xsVals.push_back(xs.val);
            xsErrs.push_back(xs.err);
        } // WC valuse

        //for (size_t idx = 0; idx < xsVals.size(); ++idx) {
            //cout << wcs[idx] << " -> " << xsVals[idx] << " +- " << xsErrs[idx] << endl;
            //if (xsVals[idx] != 0)
              //notZeroPresent = true;
        //}

        bool doNeedFit = true;

        for (const auto val : xsVals) {
            if (val == 0) {
                cout << "WARNING all zeroes! skip this one" << endl;
                doNeedFit = false;
                break;
                res[bin] = { 0.0, 0.0 };
            }
        }

        if (doNeedFit) {
            gr = make_shared<TGraphErrors>(nbPoints,
                wcs.data(),
                xsVals.data(),
                nullptr,
                xsErrs.data());

            cout << "[AverageWCvaluesOneWC] fit...";
            gr->Fit("pol1", "q");



            //TCanvas c("c", "c", 1200, 800);
            //gr->GetYaxis()->SetTitle("#sigma [pb]");
            //gr->GetXaxis()->SetTitle("Wilson coefficient");

            //string merged_name = fineNames[bin];

            ////if (doMerging_)
            //    merged_name = mergedBinsName[bin];

            //const string title = "#sigma " + merged_name + "(" + wcToStr(wc) + ")";
            //const string saveName = "sigma" + merged_name + "(" + wcToStr(wc) + ")";
            //gr->SetTitle(title.c_str());

            //gr->Draw("ALP");
            //c.SaveAs(("figures/xs/fit_" + saveName + ".pdf").c_str());

            res[bin].val = gr->GetFunction("pol1")->GetParameter(1); // slope
            res[bin].err = gr->GetFunction("pol1")->GetParError(1);  // slope
        }
    }
    // throw std::runtime_error("enough");
#endif
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::AverageIntValues()
{
    //
    EFT_PROFILE_FN();
    EFT_LOG_DURATION("Higgs::AverageInt");

    if (interfXS_.empty()) {
        EFT_CRITICAL("Interference XS is empty");
    }

    /// <summary>
    /// average Int values:
    /// </summary>
    for (auto& wc_wcVal_xs : interfXS_)
    { // over wc -> <wcVals --> <bin --> xs>>
        auto wc = wc_wcVal_xs.first;
        EFT_DEBUG("wc");

        //if (wc.GetId() == 0)
        if (wc == WilsonCoefficientsCode::NONE) {
            EFT_DEBUG("skip it");
            //cout << " ==> skip it" << endl;
            continue;
        }

        //if (wc == WilsonCoefficientsCode::SM) {
        //  cout << " ==> 
        //}

        //cout << endl;

        // in vectors to simplify fitting with TGraph, where
        // only c-style arrays can be passed
        std::vector<eft::WCvalue> wcVals;
        std::vector<CrossSections> xsPerWcVals;

        wcVals.reserve(4);
        xsPerWcVals.reserve(4);

        eft::WCvalue wcVal;
        CrossSections xss;

        for (auto& wcVal_xs : wc_wcVal_xs.second)
        { // over the wc vals to fill the vecs
            wcVal = wcVal_xs.first;
            xss = wcVal_xs.second;

            if (wcVal == eft::WCvalue::SM) {
                cout << " value = SM ==> skip" << endl;
                continue;
            }

            cout << wcVal << " -> " << xss.size() << " cross-sections" << endl;

            if (xss.size() != 0)
            {
                wcVals.push_back(wcVal);
                xsPerWcVals.push_back(xss);
            }
        }

        interfXS_[wc];
        interfXS_[wc][eft::WCvalue::AVERAGED];
        cout << "we are about to fill the total for the " << wc << " -> " << wcVals.size() << " elems;" << endl;
        cout << "interfxs[" << setw(20) << wc << "][AVERAGED] is to be averaged over: ";
        if (wcVals.size() > 1) {
            cout << wcVals.size() << " elems: ";
            for (const auto& val : wcVals) { cout << val << ", "; }
            cout << endl;
            interfXS_[wc][eft::WCvalue::AVERAGED] = AverageWCvaluesOneWC(wcVals, xsPerWcVals);
        }
        else if (wcVals.size() == 1) {
            cout << " 1 elem: " << wcVals[0] << endl;
            interfXS_[wc][eft::WCvalue::AVERAGED] = interfXS_[wc][wcVals[0]];
        }
        else {
            cout << " NO elemets --> fix to zero" << endl;
            interfXS_[wc][eft::WCvalue::AVERAGED] = {};
        }
    } // 
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::AverageBsmValues()
{
    EFT_PROFILE_FN();
    EFT_LOG_DURATION("Higgs::AverageBsm");
    // TODO: to use the same function as for averaging the interference cross-sections

    if (bsmXS_.empty()) {
        EFT_WARN("BSM XSS are empty");
    }

    /// <summary>
    /// average Int values:
    /// </summary>
    for (auto& wc_wcVal_xs : bsmXS_)
    { // over wc -> <wcVals --> <bin --> xs>>
        auto wc = wc_wcVal_xs.first;
        cout << wc;

        // in vectors to simplify fitting with TGraph, where
        // only c-style arrays can be passed
        std::vector<eft::WCvalue> wcVals;
        std::vector<CrossSections> xsPerWcVals;

        wcVals.reserve(4);
        xsPerWcVals.reserve(4);

        eft::WCvalue wcVal;
        CrossSections xss;

        for (auto& wcVal_xs : wc_wcVal_xs.second)
        { // over the wc vals to fill the vecs
            wcVal = wcVal_xs.first;
            xss = wcVal_xs.second;

            if (wcVal == eft::WCvalue::SM) {
                cout << " value = SM ==> skip" << endl;
                continue;
            }

            cout << wcVal << " -> " << xss.size() << " cross-sections" << endl;

            if (xss.size() != 0)
            {
                wcVals.push_back(wcVal);
                xsPerWcVals.push_back(xss);
            }
        }

        bsmXS_[wc];
        bsmXS_[wc][eft::WCvalue::AVERAGED];
        cout << "we are about to fill the total for the " << wc << " -> " << wcVals.size() << " elems;" << endl;
        cout << "bsmXS_[" << setw(20) << wc << "][AVERAGED] is to be averaged over: ";
        if (wcVals.size() > 1) {
            cout << wcVals.size() << " elems: ";
            for (const auto& val : wcVals) { cout << val << ", "; }
            cout << endl;
            bsmXS_[wc][eft::WCvalue::AVERAGED] = AverageWCvaluesOneWC(wcVals, xsPerWcVals);
        }
        else if (wcVals.size() == 1) {
            cout << " 1 elem: " << wcVals[0] << endl;
            bsmXS_[wc][eft::WCvalue::AVERAGED] = bsmXS_[wc][wcVals[0]];
        }
        else {
            cout << " NO elemets --> fix to zero" << endl;
            bsmXS_[wc][eft::WCvalue::AVERAGED] = {};
        }
    }
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::AddXS(CrossSections& l, const CrossSections& r)
{
    EFT_PROFILE_FN();
    EFT_LOG_DURATION("Higgs::AddXS");
    cross_section xs1, xs2;

    set<truth_bin> bins;

    for (const CrossSections& xs : { l, r })
    {
        for (const auto& bin_xs : xs)
        {
            bins.insert(bin_xs.first);
        }
    }

    for (auto& bin : bins)
    {
        // cout << "try bin: " << bin << endl;

        if (l.find(bin) != l.end())
            xs1 = l.at(bin);
        else
            xs1 = { 0., 0. };

        if (r.find(bin) != r.end())
            xs2 = r.at(bin);
        else
            xs2 = { 0., 0. };

        l[bin] += xs2;
    }
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::AddXS(XsChannelTruthBinWc& db,
    eft::DecayChannel ch,
    eft::WilsonCoefficientsCode code,
    eft::WCvalue wcVal,
    const CrossSections& xss) const
{
    EFT_PROFILE_FN();
    LOG_DURATION("Higgs::AddXS[ch][wal][xss]");
    cout << "[Higgs][AddXs] add XS for: " << ch << " " << code << " " << wcVal << endl;
    cout << "[Higgs][AddXs] " << xss.size() << " xss available" << endl;
    //truth_bin bin =  Label(99);
    truth_bin bin = (Label)999;
    cross_section xs{};
    for (const auto& bin_xs : xss) {
        bin = bin_xs.first;
        xs = bin_xs.second;
        cout << "[" << bin << "]: " << db[ch][bin][code][wcVal] << " + " << xs;
        db[ch][bin][code][wcVal] += xs;
        cout << " = " << db[ch][bin][code][wcVal] << endl;
    }
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::AddXS(XsChannelTruthBinWc& db, const ParseResults& res) const {
    EFT_PROFILE_FN();
    AddXS(db, res.decayChannel, res.wcCode, res.wcValueCode, res.xsPerTB);
}
//////////////////////////////////////////////////////////////////////////////////////
//void eft::HiggsModule::ReadFiles(const string& dir)
//{
    /*********************************************
    * Extracts required files from a directory 
    * and registers them
    *
    *
    *
    **********************************************/
//}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::AverageWCvalues()
{
    EFT_PROFILE_FN();
    LOG_DURATION("Higgs::AverageWCvalues");
    cout << "[AverageWCvalues] average cross-sections..." << endl;


    


    // average BSM values
    AverageIntValues();
    AverageBsmValues();

    cout << "[AverageWCvalues] average cross-sections... DONE" << endl;
    cout << "[AverageWCvalues] average widths..." << endl;
    cout << "[ERROR] currently not averaged! take ONE" << endl;

    for (const auto& ch_wc_wcVal : widthPerChanWcVal_) {
        DecayChannel ch = ch_wc_wcVal.first;
        for (const auto& wc_wcVal : ch_wc_wcVal.second) {
            WilsonCoefficientsCode wc = wc_wcVal.first;
            widthPerChanWcVal_[ch][wc][WCvalue::AVERAGED] = widthPerChanWcVal_[ch][wc][WCvalue::ONE];
            cout << "widthPerChanWcVal_[" << ch << "][" << wc << "][WCvalue::AVERAGED] = "
                << widthPerChanWcVal_[ch][wc][WCvalue::AVERAGED] << endl;
        }
    }
    cout << "[Higgs][AverageWCvalues] print SM xs:" << endl;
    for (const auto& bin_xs : smXSS_) {
        const auto& bin = bin_xs.first;
        const auto& xs  = bin_xs.second;
        const string& name = merger_->GetInitNames().at(bin);
        cout << ' ' << bin << ' ' << setw(25) << name << " ==> " << xs << endl;
    }
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::ComputeParametrisation()
{
    //using WC = WilsonCoefficientsCode;
    //using CH = DecayChannel;
    EFT_PROFILE_FN();
    map<WilsonCoefficientsCode, double>                    width_total_wc;
    double                                                 width_total_sm;
    map<DecayChannel, map<WilsonCoefficientsCode, double>> width_channel_wc;
    map<DecayChannel, double>                              width_channel_sm;

    map<CH, map<WC, map<string, double>>> impact_xs;

    map<CH, map<        string, double>>  xs_ch_sm; // xs per channel per truth bin
    map<CH, map<WC, map<string, double>>> xs_ch_wc; // xs per channel per wc per truth bin

    WidthPerChanWcVal& widths = widthPerChanWcVal_;
    cout << "keys of widhts:" << endl;

    if (SMwidths_.empty()) {
        EFT_CRITICAL("SM Widths are empty");
    }

    for (const auto& key_val : SMwidths_) {
        cout << key_val.first << endl;
    }

    cout << "try to get sm widhts...";
    //width_total_sm = widths[DecayChannel::TOTAL][WilsonCoefficientsCode::SM][WCvalue::AVERAGED].val;
    width_total_sm = SMwidths_.at(DecayChannel::TOTAL).val;

    if (width_total_sm < 1E-9) {
        EFT_CRITICAL("Total Width of Higgs in pure SM is found to be ZERO");
    }

    set<WC> regWC;
    map<CH, set<string>> bins_per_chan;

    eft::DecayChannel decayEnum = DecayChannel::NONE;
    if (channel_ == "yy")
        decayEnum = DecayChannel::GAM_GAM;
    else if (channel_ == "4l")
        decayEnum = DecayChannel::LLLL;
    else if (channel_ == "bb")
        decayEnum = DecayChannel::B_B;

    //const string& channel = channel_;
    //{
    //for (const string& channel : channel_) {
    //for (const string& channel : { "yy", "4l", "bb" }) {
    cout << "[PrintParametrisationsXML] channel: " << channel_ << endl;
    map<WilsonCoefficientsCode, map<string, ValErr<double>>> Xs_wc_bin;
    map<                            string, ValErr<double>>  Xs_sm_bin;

    map<WilsonCoefficientsCode, double> impactXS_wc;
    map<WilsonCoefficientsCode, double> impactBR_wc;

    cout << "Mergefor  " << interfXS_.size() << " wc ==> ";
    Xs_wc_bin = Merge(interfXS_, channel_);
    Xs_sm_bin = Merge(smXSS_,    channel_);
    cout << Xs_wc_bin.size() << endl;

    cout << "[PrintParametrisationsXML] prepare cross-sections..." << endl;
    cout << "channel: |" << channel_ << "|" << endl;
    for (const auto& wc_bin_xs : Xs_wc_bin) {
        WC wc = wc_bin_xs.first;
        regWC.insert(wc);
        for (const auto& bin_xs : wc_bin_xs.second) {
            const string& bin = bin_xs.first;
            bins_per_chan[decayEnum].insert(bin);
            auto xs = bin_xs.second;
            if (wc == WC::SM) {
                Xs_sm_bin[bin] = xs;
                //xs_ch_sm[decayEnum][bin] = xs.val;
                cout << "xs_ch_sm[" << setw(10) << decayEnum << "][" << setw(25) << bin << "] = "
                     << Xs_sm_bin[bin] << endl;
                      //<< xs_ch_sm[decayEnum][bin] << endl;
            }
            else {
                xs_ch_wc[decayEnum][wc][bin] = xs.val;
                cout << "xs_ch_wc[" << setw(10) << decayEnum << "][" << setw(10) << wc
                    << "][" << setw(25) << bin << "] = "
                    << xs_ch_wc[decayEnum][wc][bin] << endl;
            } // not SM
        } // bins
    } // wc

    cout << "[Higgs] prepare sm xs.." << endl;
        // prepare SM xs: 
    for (const auto& bin_xs : smXSS_) {
        //cout << "sm_xs[" << bin_xs.first << "] = " << bin_xs.second << endl;
        //cout << "init names.size() = " << merger_->GetInitNames().size() << endl;
        const string& bin_init = merger_->GetInitNames().at(bin_xs.first);
        cout << "sm_xs[" << setw(25) << bin_init << "] = " << bin_xs.second << endl;
       // cout << "bin init = " << bin_init << endl;

        if (bin_init == "UNKNOWN") {
            cout << "skip UNKNOWN" << endl;
            continue;
        }

        //string name = "UNKNOWN";
        //try {
        //    name = merger_->GetMergedStr(bin_init, cfg_->GetName());
        //    //const string& name = merger_->GetMergedStr(bin_init, cfg_->GetName());
        //}
        //catch (std::invalid_argument& e) {
        //    name = "UNKNOWN";
        //}
        const string& name = merger_->GetMergedStr(bin_init, cfg_->GetName());
        cout << "merge it to: " << name << endl;
        //cout << bin_xs.first << " => bin_init: " << bin_init << " ==> " << name << " ==> xs += " << bin_xs.second
        cout << setw(25) << bin_init << " => " << name << ": " << Xs_sm_bin[name].val << " += " << bin_xs.second.val;
        Xs_sm_bin[name] += bin_xs.second;
        cout << " = " << Xs_sm_bin[name].val << endl;
        cout << "----------------------------------" << endl;
        //cout << "xs_ch_sm[" << setw(10) << decayEnum << "][" << setw(25) << bin_xs.first << "] = ";
        //xs_ch_sm[decayEnum][bin_xs.first] = bin_xs.second.val;
        //cout << xs_ch_sm[decayEnum][bin_xs.first] << endl;
    }

    //WC wc;
    

    ///// xss are ready ==> compute impact on them for each channel
    cout << "[PrintParametrisationsXML] compute impact xs..." << endl;
    //for (const auto ch : { CH::GAM_GAM, CH::LLLL, CH::B_B }) {
    for (const auto ch : {decayEnum}) {
        for (const auto& wc_rest : xs_ch_wc[ch]) {
            auto wc = wc_rest.first;
            for (const auto& bin_xs : xs_ch_wc[ch][wc]) {
                const string& bin = bin_xs.first;

                if (bin == "NONE")
                    continue;

                cout << "impact_xs[" << setw(7) << enumToStr(ch) << "]["
                    << setw(20) << enumToStr(wc) << "]["
                    << setw(35) << bin << "] = "
                    << xs_ch_wc[ch][wc][bin] << " / "
                    << Xs_sm_bin[bin] << " = ";
                    //<< xs_ch_sm[ch][bin] << " = ";
                //impact_xs[ch][wc][bin] = xs_ch_wc[ch][wc][bin] / xs_ch_sm[ch][bin];
                if (Xs_sm_bin[bin].val > 1E-9) // if xs sm != 0 => migth be due to a problem with init files
                    impact_xs[ch][wc][bin] = xs_ch_wc[ch][wc][bin] / Xs_sm_bin[bin].val;
                else
                    impact_xs[ch][wc][bin] = 0.f;
                cout << impact_xs[ch][wc][bin] << endl;
            }
        }
    }

    //cout << "compute width induced by wc...." << endl;
    //    //width_channel_sm[decayEnum] = widths[decayEnum][WilsonCoefficientsCode::SM][WCvalue::AVERAGED].val;
    width_channel_sm[decayEnum] = SMwidths_.at(decayEnum).val;
    cout << "width_channel_sm[" << decayEnum << "] = " << width_channel_sm[decayEnum] << endl;
    //auto wc = WilsonCoefficientsCode::NONE;
    ////for (const auto wc : regWCcode_) {
    //for (const auto& ch_wc_wcval_width : widths) {
    //    auto ch = ch_wc_wcval_width.first;
    //    wc = ch_wc_wcval_width.first;
    //    cout << "wc: {" << wc << "} - check if it's present" << endl;
    ////for (const auto& wc_wcVal_width : widths[decayEnum]) {
    //    //wc = wc_wcVal_width.first;
    //    width_channel_wc[decayEnum][wc] = widths[decayEnum][wc][WCvalue::AVERAGED].val;
    //    width_total_wc[wc]             += widths[decayEnum][wc][WCvalue::AVERAGED].val;
    //    cout << "width_channel_wc[" << decayEnum << "][" << setw(15) << "] = " << width_channel_wc[decayEnum][wc] << endl;
    //    cout << "width_total_wc["   << decayEnum << "] += " << widths[decayEnum][wc][WCvalue::AVERAGED].val <<
    //          " = " << width_total_wc[wc] << endl;
    //} // wc, given channel
    cout << "fill widht_total_wc and width_channel_wc.." << endl;
    for (const auto wc : regWCcode_) {
        cout << "width_total_wc[" << wc << "] = " << widths[CH::TOTAL][wc][WCvalue::AVERAGED].val << endl;
        width_total_wc[wc] = widths[CH::TOTAL][wc][WCvalue::AVERAGED].val;
        width_channel_wc[decayEnum][wc] = widths[decayEnum][wc][WCvalue::AVERAGED].val;
        cout << "width_channel_wc[" << wc << "] = " << widths[decayEnum][wc][WCvalue::AVERAGED].val << endl;
    }



    /////////////////////////////////////////////////////////////////////////////////////////////
    // IMPACT ON THE DECAY //////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    // no we have all:
    // width_total_wc       [wc]  --> width
    // width_total_SM             --> width
    // width_channel_wc [ch][wc]  --> width
    // width_channel_SM [ch]      --> width

    // prepare the impacts on the decay part (RB):
    // for yy, take directly from the paper
    // for the others -> compute as:
    //
    // impact_decay_total       [wc] = width_total_wc           [wc] / width_total_SM
    // impact_decay_channel [ch][wc] = width_channel_wc     [ch][wc] / width_channel_SM [ch]
    // ==>
    // impact_br_channel    [ch][wc] = impact_decay_channel [ch][wc] - impact_decay_total   [wc]



    map<        WC, double>  impact_decay_total;
    map<CH, map<WC, double>> impact_decay_channel;
    //map<CH, map<WC, double>> impact_br_channel;
    map<WC, double> impact_br_channel;

    // compute the impact_decay_total[wc] first:
    for (WC wc_ : regWCcode_) {
        if (wc_ == WC::SM)
            continue;
        cout << "impact_decay_total[" << setw(15) << wc_ << "] = " << setw(10)
            << width_total_wc[wc_]
            << " / "
            << width_total_sm << " = ";
        impact_decay_total[wc_] = width_total_wc[wc_] / width_total_sm;
        cout << impact_decay_total[wc_] << endl;
    }

    cout << "[] prepare information about the decay part..." << endl;
    for (const CH ch : {decayEnum}) {
        cout << "consider the " << ch << " channel" << endl;
        for (const WC wc_ : regWCcode_) {
            //cout << '[' << setw(15) << ch << "][" << setw(25) << wc << "]";
            impact_br_channel[wc_];
            if (toReadBrFile_) {
                cout << "[Higgs] to read br..." << endl;
            //if (ch == CH::GAM_GAM) {
                if (brPerChanWcVal_.count(wc_) != 0)
                    impact_br_channel[wc_] = brPerChanWcVal_.at(wc_);
                else
                    impact_br_channel[wc_] = 0;
                cout << " impact_br_channel[" << setw(15) << ch << "][" << setw(15) << wc_ << "] = "
                    << setw(10) << impact_br_channel[wc_] << endl;
                //cout << " set br to: " << impact_br_channel[ch][wc] << endl;
            }
            else {
                cout << "impact_decay_channel[" << setw(15) << enumToStr(ch)
                    << "][" << setw(15) << enumToStr(wc_) << "] = " << setw(10)
                    << width_channel_wc[ch][wc_] << " / " << setw(10)
                    << width_channel_sm[ch] << " = ";
                if (width_channel_sm[ch] >= 1E-9) // if width SM = 0
                    impact_decay_channel[ch][wc_] = width_channel_wc[ch][wc_] / width_channel_sm[ch];
                else
                    impact_decay_channel[ch][wc_] = 0;
                cout << impact_decay_channel[ch][wc_] << endl;

                cout << " ==> impact_br_channel[" << setw(15) << ch << "][" << setw(15) << wc_ << "] = "
                    << setw(10) << impact_decay_channel[ch][wc_] << " / " << setw(10)
                    << " = ";
                impact_br_channel[wc_] = impact_decay_channel[ch][wc_] - impact_decay_total[wc_];
                cout << impact_br_channel[wc_] << endl;
            } // NOT GAM_GAM channel
        } // wc
    } // channels

    //cout << "[PrintParametrisationsXML] print parametrisation..." << endl;
    cout << "[PrintParametrisationsXML] parametrisations of BR and XS are obtained" << endl;
    cout << "[PrintParametrisationsXML] prepare impacts per [ch][wc][bin] before printing them down" << endl;
    map<CH, map<WC, map<string, double>>> param_ch_wc_bin;

    // impact_xs[ch][wc][bin] 
    // impact_br_channel[ch][wc]
    //string bin;

    auto ch = decayEnum;
    //for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
        //const auto& impact_xs_this_ch = impact_xs.at(ch);
    for (const auto& wc_bin_xs : impact_xs.at(ch)) {
        auto wc = wc_bin_xs.first;
        if (wc == WilsonCoefficientsCode::SM)
            continue;

        for (const auto& bin_xs : wc_bin_xs.second) {
            const string& bin = bin_xs.first;
            double impact_xs_ = bin_xs.second;
            double impact_widht_ = 0.;
            if (bin == "NONE")
                continue;

            //if (impact_br_channel.find(ch) != impact_br_channel.end()) {
            if (impact_br_channel.find(wc) != impact_br_channel.end()) {
                impact_widht_ = impact_br_channel.at(wc);
            } // try to find impact_br[ch] on this wc
            //} // try to find impact_br on this ch
            param_ch_wc_bin[ch][wc][bin] = impact_xs_ + impact_widht_;
            //param_ch_wc_bin[ch][wc][bin] = impact_widht_;
            /*cout << "impact[" << setw(7) << enumToStr(ch) << "][" << setw(8) << enumToStr(wc) << "]["
                << setw(35) << bin << "] = "
                << setw(10) << impact_xs_ << " - "
                << setw(10) << impact_widht_ << " = "
                << param_ch_wc_bin[ch][wc][bin] << endl;*/

            //parametrisation_.linear[ch][wc][bin] = param_ch_wc_bin[ch][wc][bin];
            if (param_ch_wc_bin[ch][wc][bin] > 1E-9) {
                parametrisation_.linear[channel_][wc][bin] = param_ch_wc_bin[ch][wc][bin];
                cout << "parametrisation_.linear[" << channel_ << "][" << setw(15)
                    << wc << "][" << setw(15)
                    << bin << "] = " << parametrisation_.linear[channel_][wc][bin].val << endl;
            }
            else {
                cout << "zero value for: " << channel_ << "][" << setw(15)
                    << wc << "][" << setw(15)
                    << bin << "] => do NOT register it" << endl;
            }
        } // bin
    } // wc
    //} // channel
    //cout << "ok" << endl;


#if 0
    // print parametrisation
    os << "parametrisation:" << endl;
    for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
        for (const string& bin : bins_per_chan[ch]) {
            if (bin == "NONE")
                continue;
            os << " - [";
            bool isFirst = true;
            for (const WC wc : regWC) {
                if (wc == WilsonCoefficientsCode::SM)
                    continue;
                if (param_ch_wc_bin[ch].find(wc) != param_ch_wc_bin[ch].end()) {
                    if (!isFirst)
                        os << ", ";
                    else
                        isFirst = false;

                    os << param_ch_wc_bin[ch][wc][bin];
                }
            } // wc
            os << "]" << endl;
        } // bins 
    } // channels

    // pring xpars...
    os << "xpars: [";
    bool isFirst = true;
    for (const WC wc : regWC) {
        if (wc == WilsonCoefficientsCode::SM)
            continue;
        if (!isFirst)
            os << ", ";
        else
            isFirst = false;

        os << enumToStr(wc);
    } // wc
    os << "]\n";

    // PRINT ypars
    os << "ypars: [";
    isFirst = true;
    for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
        for (const string& bin : bins_per_chan[ch]) {
            if (bin == "NONE")
                continue;
            if (!isFirst)
                os << ", ";
            else
                isFirst = false;

            os << bin;
        } // bins
        os << endl;

    } // ch
    os << "]\n";

    // put the sm predictions...
    os << "SM_predictions: [";
    isFirst = true;
    for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
        for (const string& bin : bins_per_chan[ch]) {
            if (bin == "NONE")
                continue;
            if (!isFirst)
                os << ", ";
            else
                isFirst = false;

            os << xs_ch_sm[ch][bin];
        } // bin
    } // ch
    os << "]\n";
#endif // if false --> part about printing the parametrisation
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::ProcessFiles() {
    EFT_PROFILE_FN();
    // Get List of all files fullfilling the criteria specified in the cfg
    // and register them to the storage
    const vector<string>& files = cfg_->GetFiles(); //cfg->GetFiles();

    if (files.empty()) {
        EFT_CRITICAL("HiggsModule {} NO FILES FOUND", channel_);
    }

    for (const string& file : files) {
        Register(Parse(file));
    }
    // average cross-sections and width (if it's required)
    AverageWCvalues();
    ComputeParametrisation();
    EFT_INFO("[Higgs]{} processing of files is done", channel_);
    //cout << "[Higgs]{" << cfg_->GetName() << "} processing of files is done" << endl;
}
//////////////////////////////////////////////////////////////////////////////////////
HiggsModule::HiggsModule(HiggsConfig* cfg) 
       : TEFTModule(cfg)
       , merger_(cfg->GetMerger())
       , varNameFromTTree_(cfg->GetObsName())
       , directory_(cfg->GetDirectory())
       , toReadBrFile_(cfg->GetReadBrStatus())
{
    EFT_PROFILE_FN();
    SetChannel(cfg->GetName());
    EFT_INFO("[HiggsModule] {} created from configFile :", channel_);
    //EFT_INFO(cfg->PrintInfoInSS().str());
    //cout << "[HiggsModule] created from configFile: " << endl;
    cfg_->PrintInfo(cout);
    if (cfg->GetMerger()->GetInitNames().empty()) {
        EFT_CRITICAL("[HiggsModule {{}} no init names set]", cfg->GetName());
        //cout << "[HiggsModule]{" << cfg->GetName() << "} ERROR: no init names set! " << endl;
        //throw std::logic_error(string("Set initial names of the categories, extracted from the histograms.")
        //    + string("To do it, use: config->SetCatInitNames(const std::vector<string>& names)"));
    }
    //cout << "ctor HiggsModule(cfg) try to initiate parametrisatin_.linear for name: " << cfg->GetName() << endl;
    parametrisation_.linear[cfg->GetName()];
}
//////////////////////////////////////////////////////////////////////////////////////
void HiggsModule::SetCfg(HiggsConfig* cfg) {
    cfg_ = cfg;
    merger_ = cfg->GetMerger();
    varNameFromTTree_ = cfg->GetObsName();
    directory_  =cfg->GetDirectory();
    toReadBrFile_ = cfg->GetReadBrStatus();
    SetChannel(cfg_->GetName());

    EFT_INFO("[HiggsModule] {} channel. Config is set up. Config: ", channel_);
    EFT_PROFILE_FN();

    if (cfg->GetMerger()->GetInitNames().empty()) {
        EFT_CRITICAL("[HiggsModule {{}} no init names set]", cfg->GetName());
        //cout << "[HiggsModule]{" << cfg->GetName() << "} ERROR: no init names set! " << endl;
        //throw std::logic_error(string("Set initial names of the categories, extracted from the histograms.")
        //    + string("To do it, use: config->SetCatInitNames(const std::vector<string>& names)"));
    }
            //cout << "ctor HiggsModule(cfg) try to initiate parametrisatin_.linear for name: " << cfg->GetName() << endl;
    parametrisation_.linear[channel_];

}
////////////////////////////////////////////////////////////////////////////////
void HiggsModule::SetBrParametrisation(const map<string, double>& param) {
    EFT_LOG_DURATION("[HiggsModule]{SetBrParametrisation}");
    // NOTE: since we're obliged to stay now with c++14 (since root has been
    // compiled like this, we CANNOT use structured-binding and
    // need to use this awful unpacking
    for (const auto& wcAsStr_val : param) {
        // no refs, since enums and double are trivially-copyable
        WC wc = strToWC(wcAsStr_val.first);
        double val = wcAsStr_val.second;
        brPerChanWcVal_[wc] = val;
    }
}
///////////////////////////////////////////////////////////////////////////////////
map<WilsonCoefficientsCode, map<string, ValErr<double>>>
HiggsModule::Merge(const XSperWC& interfXs,
    const string& channel) const
{
    EFT_PROFILE_FN();
    EFT_LOG_DURATION("Merge");

    const auto& merger = merger_->GetMergingScheme(channel);
    //cout << "[Merge] for " << channel << " channel" << endl;
    EFT_DEBUG("[Merge] {} channel", channel);

    map<WilsonCoefficientsCode, map<string, ValErr<double>>> res;

    CrossSections xss;
    //cross_section xs;
    truth_bin     bin;
    WilsonCoefficientsCode wc;
    size_t idx_bin;

    string bin_init, bin_merged;

    for (const auto& wc_wcVal_xss : interfXs) {
        wc = wc_wcVal_xss.first;
        cout << "wc: " << wc << " available: " << wc_wcVal_xss.second.at(WCvalue::AVERAGED).size()
            << " truth bins" << endl;
        res[wc];
        //if (wc == WilsonCoefficientsCode::SM)
        //  continue;

        size_t maxBins = 0;
        if (channel == "yy")
            maxBins = eft::fineNames.size();
        else
            maxBins = eft::coarseNames.size();


        cout << " - loop over " << wc_wcVal_xss.second.at(WCvalue::AVERAGED).size() << " bins" << endl;
        for (const auto& bin_xs : wc_wcVal_xss.second.at(WCvalue::AVERAGED)) {
            idx_bin = bin_xs.first;
            cout << "idx_bin = " << idx_bin << endl;

            if (idx_bin >= maxBins)
                continue;

            if (channel == "yy")
                bin_init = eft::fineNames.at(idx_bin);
            else
                bin_init = eft::coarseNames.at(idx_bin);

            bin_merged = merger.GetMergedFromInit(bin_init);
            //bin_init   = merger.GetInitStrNb(idx_bin);
            //bin_merged = merger.GetMergedFromInit(bin_init);
            cout << setw(25) << bin_init << " --> " << setw(25) << bin_merged << "| ==> ";
            //cout << " * idx: " << idx_bin << " --> " << bin_init << " ===> " << bin_merged;
            cout << " val: |" << res[wc][bin_merged] << "| + |" << bin_xs.second << "| ==> ";
            res[wc][bin_merged] += bin_xs.second;
            cout << res[wc][bin_merged] << endl;
            cout << "*****************************" << endl;
        } // bins
    } // wc

    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
map<string, ValErr<double>>
HiggsModule::Merge(const CrossSections& smXs, const string& channel) const {
    EFT_PROFILE_FN();
    EFT_DEBUG("Merge XS xs for {}", channel);
    //cout << "Merge SM xs for " << channel << " channel" << endl;
    XSperWC xsPerWC_;
    xsPerWC_[WilsonCoefficientsCode::SM];
    xsPerWC_[WilsonCoefficientsCode::SM][WCvalue::AVERAGED] = smXs;

    auto res = HiggsModule::Merge(xsPerWC_, channel);
    return res[WilsonCoefficientsCode::SM];
}
//////////////////////////////////////////////////////////////////////////////////////
//void HiggsModule::PrintParametrisationsXML(ostream& os) const {
//    cout << "[Higgs::PrintParametrisation]\n";
//    LOG_DURATION("Higgs::PrintParametrisation");
//
//    // need to compute:
//    // [ch] ==> {bins} to know what bins to loop over for each channel
//    // {wc} ==> they are shared among all channels
//
//    set<WC> regWC;
//    map<CH, set<string>> bins_per_chan;
//
//    for (const auto& ch_wc_bin_impact : parametrisation_.linear) {
//
//    }
//
//    os << "parametrisation:" << endl;
//    for (const auto& ch_rest : parametrisation_.linear) {
//        CH ch = ch_rest.first;
//        for (const string& bin : parametrisation_.linear.at(ch)) {
//            if (bin == "NONE")
//                continue;
//            os << " - [";
//            bool isFirst = true;
//            for (const WC wc : regWC) {
//                if (wc == WilsonCoefficientsCode::SM)
//                    continue;
//                if (parametrisation_.linear.at(ch).find(wc) != parametrisation_.linear.at(ch).end()) {
//                    if (!isFirst)
//                        os << ", ";
//                    else
//                        isFirst = false;
//
//                    os << parametrisation_.linear.at(ch).at(wc).at(bin).val;
//                }
//            } // wc
//            os << "]" << endl;
//        } // bins 
//    } // channels
//
//    // pring xpars...
//    os << "xpars: [";
//    bool isFirst = true;
//    for (const WC wc : regWC) {
//        if (wc == WilsonCoefficientsCode::SM)
//            continue;
//        if (!isFirst)
//            os << ", ";
//        else
//            isFirst = false;
//
//        os << enumToStr(wc);
//    } // wc
//    os << "]\n";
//
//    // PRINT ypars
//    os << "ypars: [";
//    isFirst = true;
//    for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
//        for (const string& bin : bins_per_chan[ch]) {
//            if (bin == "NONE")
//                continue;
//            if (!isFirst)
//                os << ", ";
//            else
//                isFirst = false;
//
//            os << bin;
//        } // bins
//        os << endl;
//
//    } // ch
//    os << "]\n";
//
//    // put the sm predictions...
//    os << "SM_predictions: [";
//    isFirst = true;
//    for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
//        for (const string& bin : bins_per_chan[ch]) {
//            if (bin == "NONE")
//                continue;
//            if (!isFirst)
//                os << ", ";
//            else
//                isFirst = false;
//
//            os << xs_ch_sm[ch][bin];
//        } // bin
//    } // ch
//    os << "]\n";
//}
//////////////////////////////////////////////////////////////////////////////////////
/*const map<WilsonCoefficientsCode, CrossSections> HiggsModule::GetAveragedIntXSs() const {
    //map<WilsonCoefficientsCode, CrossSections> res;
    //cout << "[Higgs] GetAveragedIntXs" << endl;
    //LOG_DURATION("Higgs::GetAveragedIntXs");
    //for (const auto& wc_wcval_xss : interfXS_) {
    //    const auto wc = wc_wcval_xss.first;
    //    cout << "\t wc: " << wc << endl;
    //    const auto& xss = wc_wcval_xss.second.at(WCvalue::AVERAGED);
    //                             //<WCvalue> -> xss
    //    //                                              xss
    //    res[wc] = xss;
    //}
    //return res;
    return const_cast<HiggsModule*>(this)->GetAveragedIntXSs();
}*/
//////////////////////////////////////////////////////////////////////////////////////
/*map<WilsonCoefficientsCode, CrossSections> HiggsModule::GetAveragedIntXSs() {
    map<WilsonCoefficientsCode, CrossSections> res;
    cout << "[Higgs] GetAveragedIntXs" << endl;
    LOG_DURATION("Higgs::GetAveragedIntXs");
    for (const auto& wc_wcval_xss : interfXS_) {
        const auto wc = wc_wcval_xss.first;
        cout << "\t wc: " << wc << endl;
        const auto& xss = wc_wcval_xss.second.at(WCvalue::AVERAGED);
        //                          <WCvalue> -> xss
        //                                              xss
        res[wc] = xss;
    }
    return res;
}*/

} // namespace modules
} // namespace eft
