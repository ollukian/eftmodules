#pragma once

#include "TEFTModule.h"

namespace eft {
namespace modules {

// serves to collect together modules of the same physics and then
// to handle them together for the final results
// example (and the cause of creation):
// Higgs physiscs, where yy channel requires fine granularity of STXS
// while bb and 4l - coarse one, therefore they require different
// variables from the files and independent treatment. 
class TChainModulesOnePhysics
{
public:
	//virtual inline std::shared_ptr<TChainModulesOnePhysics> create() = 0;

	TChainModulesOnePhysics() : parametrisation_(new TParametrisation()) {}
	TChainModulesOnePhysics(const TChainModulesOnePhysics& other) = default;
	TChainModulesOnePhysics(TChainModulesOnePhysics&& other) = default;
	//{
	//    parametrisation_ = std::move(other.parametrisation_);
	//}

	// add module of the same physics to the chain, which can be handled simultaneously
	//inline  void AddModule(unique_ptr<TEFTModule> physModule);
	inline  void AddModule(TEFTModule* physModule);
    inline const std::vector<TEFTModule*> GetChainModules() const { return chain_; }
	// run all modules in the chain, so that they prepare their own parametrisation
	// then these parametrisations are combined into a single onw
	virtual void Run() = 0;
	inline        shared_ptr<TParametrisation>& GetParametrisation()       { return parametrisation_; }
	inline  const shared_ptr<TParametrisation>& GetParametrisation() const { return parametrisation_; }
	virtual ~TChainModulesOnePhysics() {}
protected:
	//std::vector<shared_ptr<TEFTModule>> chain_{};
	std::vector<TEFTModule*> chain_{};
	shared_ptr<TParametrisation>        parametrisation_{};
	//unique_ptr<TParametrisation> parametrisation_;
};

//inline void TChainModulesOnePhysics::AddModule(unique_ptr<TEFTModule> module) {
inline void TChainModulesOnePhysics::AddModule(TEFTModule* physModule) {
	//std::cout << "[Modules] adding the {" << physModule->GetConfig()->GetName() << "} module to the chain" << std::endl;
	EFT_INFO("[Modules] adding {} module to the chain", physModule->GetConfig()->GetName());
	chain_.push_back(physModule);
	//chain_.push_back(std::move(physModule));
}

} // namespace eft
} // namespace modules

