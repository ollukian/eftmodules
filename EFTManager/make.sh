blocks=("core" "modules" "database" "manager")

## compile all sub-components
for block in ${blocks[@]}; do
    if ! sh make_$block.sh; then
	echo "ERROR in compilation of ${block}"
	exit
    fi
done

## compile the 'main' file
if ! g++ -c -g @../Utils/compilerFlags.sh `root-config --cflags` RunEFT.C; then
    echo "ERROR in compilation of RunEFT.C"
    exit
fi

## link them
g++ @../Utils/compilerFlags.sh `root-config --glibs` $( echo *.o  )
