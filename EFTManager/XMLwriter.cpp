#include "XMLwriter.h"

using namespace std;

void XMLwriter::Write(ostringstream& os, 
                      const XMLConfig& config, 
                      const map<string, XMLwriteRequest>& reqPerChannnel) 
{
    LOG_DURATION("XMLWriter::Write");
    cout << "[XMLwriter::Write] results for " << reqPerChannnel.size()
         << " channels: " << endl;
    for (const auto& ch_req : reqPerChannnel) {
        cout << " - " << ch_req.first << endl;
    }

    cout << "available " << config.POIsName.size() << " wc: " <<  endl;
    for (const string& wc : config.POIsName) {
        cout << wc << endl;
    }

    //reqPerChannnel.at("yy").parametrisation.Print();

    cout << "size of trans matrix: " << reqPerChannnel.at("yy").parametrisation.GetNcols() << " * " << reqPerChannnel.at("yy").parametrisation.GetNrows() << endl;
    //exit(1);
    //#define XML_ITEM(x) "
    
    os << "<Organisation " 
       << "Infile=\"" << config.inFile   << "\"" << endl 
       << "\t \t Outfile=\"" << config.outFile << "\"" << endl
       << "\t \t workspaceName=\"" << config.WSname << "\"" << endl
       << "\t \t modelConfigName=\"" << config.modelConfigName << "\"" << endl
       << "\t \t dataName=\"" << config.dataName << "\"" << endl
       << "\t \t modelName=\"" << config.modelName << "\"" << endl
       << "\t \t POINames=\"";


    bool isFirst = true;
    for (const string& poi : config.POIsName) {
        if (!isFirst)
            os << ",";
        os << poi;
        isFirst = false;
    }
    os << "\"" << endl;
    os << "\\>" << endl;

    os << endl << endl;

    //// set all WC to zero -> print: <Item Name="wc[0]">
    for (const string& poi : config.POIsName) {
        os << "<Item Name=\"" << poi << "[0]\"" << "\\>" << endl;
    }

    os << endl << endl;
    //// create: <Item Name="expr::eft_int_GG2H_PTH_200_300_YY('0.111000 * @0 - 0.030000 * @1', cHbox, cHDD)"/>)


    for (const auto& ch_req : reqPerChannnel) {
        const string ch = ch_req.first;
        cout << "dealing with: " << ch << " channnel." << endl;
        size_t nbPois = ch_req.second.muNames.size();
        for (size_t idx_mu = 0; idx_mu < nbPois; ++idx_mu) {
            const string mu = ch_req.second.muNames.at(idx_mu); 
            cout << "mu: " << setw(15) << mu << endl;

            os << "<Item Name=\"expr::eft_int_" << mu << "_" << ch << "(\'";

            //cout << "available:"

            vector<string> poisToUse;
            for (size_t idx_poi = 0; idx_poi < ch_req.second.wcNames.size(); ++idx_poi) {
                cout << "wc = " << idx_poi << " -> " << setw(15) << ch_req.second.wcNames.at(idx_poi) << endl;
                const string poi = ch_req.second.wcNames.at(idx_poi);
                //double coeff = ch_req.second.parametrisation(idx_mu, idx_poi);
                double coeff = ch_req.second.parametrisation(idx_poi, idx_mu);
                if (abs(coeff) < 1e-3)
                    continue;

                poisToUse.emplace_back(poi);
                if (coeff > 0)
                    os << '+';
                else if (coeff < 0)
                    os << '-';
                os << ' ' << abs(coeff) << " * @" << idx_poi << ' ';
            }

            os << "', ";
            isFirst = true;
            for (const string& poi : poisToUse) {
                if ( !isFirst ) {
                    os << ", ";
                }
                os << poi;
                isFirst = false;
            }

            os << ")\"" << "/>" << endl;
        } // over mu
    } // over channels


    // write down total parametrisation: 1 + sum over channels

    os << endl << endl;
    os << "<Item Name=\"unit[1]\"\\>";
    os << endl << endl;
    os << "<!-- Final EFT parametrisation --!>" << endl;
    os << endl;

    string channel;
    for (const auto& ch_req : reqPerChannnel) {
        channel = ch_req.first;
        for (const string& poi : config.muNames) {
            os << "<Item Name=\"sum::" << poi << "_" << channel 
               << "(unit, eft_int_" << poi << "_" << channel 
               << ")\"/>" << endl;
        }
    } // channel

    os << endl << endl;
    os << "<!-- Re-parametrise final pdf --!>" << endl;
    os << endl;

    os << "<Map Name=\"EDIT::NEWPDF(OLDPDF, " << endl;

    for (const auto& ch_req : reqPerChannnel) {
        channel = ch_req.first;
        for (const string& poi : config.muNames) {
            os << "\t\t mu_" << poi << "=eft_" << poi << "_" << channel << "," <<endl;
        }
    }
    os << "\")";
    os << "/>" << endl;
    os << endl;

    os << "<Asimov Name=\"asimovData\"/>" << endl;
    os << endl;
    os << "</Organisation>" << endl;
}
//////////////////////////////////////////////////////////////////////////////////////
XMLwriteRequest& XMLwriteRequest::operator = (const XMLwriteRequest& other) {
    //cout << "[+]" << endl;
    //cout << "l.parametrisation.size: " << endl;
    this->parametrisation.Print();
    //cout << "r.parametrisation size: " << endl;
    other.parametrisation.Print();
    this->parametrisation.ResizeTo(other.parametrisation);
    this->channel       = other.channel;
    this->wcNames       = other.wcNames;
    this->muNames       = other.muNames;
    this->includeSM     = other.includeSM;
    this->includeBSM    = other.includeBSM;
    this->includeInterf = other.includeInterf;
    this->parametrisation += other.parametrisation;
    //cout << "after adding: " << endl;
    this->parametrisation.Print();

    return *this;
}
//////////////////////////////////////////////////////////////////////////////////////