#pragma once

#ifndef EFTMANAGER_H
#define EFTMANAGER_H

#include "../Utils/test_runner.h"
#include "../Utils/profile.h"

#include "Parser.h"
#include "WilsonCoefficients.h"
#include "MatrixManager.h"
#include "XMLwriter.h"

#include "../DataBase/Merger.h"

#include <string>
#include <memory>

#include <iostream>
#include <fstream>

//#pragma warning(push, 0)
//#ifndef	_MSC_VER
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TMatrixD.h"
#include "TVectorT.h"
#include "TFile.h"
#include "TAxis.h"

#include "TStyle.h"
//#endif // if not MSVS
//#pragma warning(pop)

using std::map;
using std::set;
using namespace ROOT;

// WC->xs, regardless the value of the WC
//using XSperWC          = std::map<eft::WilsonCoefficient, CrossSections>; 
//using XSperWC          = map<eft::WilsonCoefficient, map<eft::WCvalue, CrossSections>>; 

using WC        = eft::WilsonCoefficientsCode;
using truth_bin = eft::Label;
using XS        = ValErr<double>;
using XsChannelTruthBinWc = map<eft::DecayChannel, 
                            map<truth_bin, 
                            map<WC, 
                            map<WCvalue, XS>>>>;

using XSperWC          = map<eft::WilsonCoefficientsCode, map<eft::WCvalue, CrossSections>>; 

// all available WC
using RegistryWC       = std::set<eft::WilsonCoefficient>;      
using RegistryWCcode   = std::set<eft::WilsonCoefficientsCode>; 
using RegistryTruthBins = std::set<size_t>;     

using RegistryWCperVal = std::map<eft::WCvalue,           std::set<eft::WilsonCoefficient>>;

using RegistryValperWC = std::map<eft::WilsonCoefficient, std::set<eft::WCvalue>>;

using XSperWCperVal    = std::map<eft::WilsonCoefficient, std::map<eft::WCvalue,           CrossSections>>;

using XSperValperWC    = std::map<eft::WCvalue,           std::map<eft::WilsonCoefficient, CrossSections>>;

using WidthPerChanWcVal = std::map<eft::DecayChannel,     
                          std::map<eft::WilsonCoefficientsCode,
                          std::map<eft::WCvalue,
                          Width>>>;

using ValPerChanWc      = std::map<eft::DecayChannel,
                          std::map<eft::WilsonCoefficientsCode,
                          double>>;

using Widths            = std::map<eft::DecayChannel, Width>;

using TransitionMatrix  = std::map<eft::WilsonCoefficientsCode,
                          std::map<truth_bin,
                          double>>;

using WCbyCodes         = std::map<eft::WilsonCoefficientsCode, eft::WilsonCoefficient>;


using VecIdx = std::vector<double>;
using VecVecVals = std::vector<vector<double>>;

//#define INFO(x) std::cout << "[INFO] " << x << std::endl;

class EFTManager {
public:
    EFTManager()  = default;
    ~EFTManager() { LogDuration::GetStats(); }

    ParseResults Parse(const string& filename, const string& specification = {}); // not const and not ref, so that it can be cut and modified
    ParseResults ParseMetaInfo(const string& filename);
    void         ParseDecayContent(const string& filename, ParseResults& res);
    void         ParseProdContent(const string& filename, ParseResults& res);
    ParseResults ParseOldConvention(const string& filename);
    ParseResults ParseNewConvention(const string& filename);
    ParseResults ParseAna(string filename) const;
    inline void  SetDir(const string& dir) { directory_ = dir; }
    void         GetSmXs();
    void         GetSmWidths();
    void         GetXSs(Regime reg = Regime::NONE,   ProdMode mode = ProdMode::NONE, WCvalue val = WCvalue::NONE, size_t nbEvents = 0);
    void         GetXSs(const std::vector<Regime>& regimes, 
                        const std::vector<ProdMode>& modes, 
                        const std::vector<WCvalue>& vals, 
                        const std::vector<size_t>& vecNbEvents);
    void         GetXssAna(const string& path, ProdMode mode = ProdMode::NONE, WCvalue val = WCvalue::NONE);
    void         ReadBrParametrisation(istream& is, DecayChannel chan);
    void         DrawSmXs() const;
    void         Draw2DImpact();
    void         DrawTransitionMatrix() const; 
    void         DrawXS(TH1D& h, const WCvalue val = WCvalue::NONE, const WilsonCoefficientsCode code = WilsonCoefficientsCode::NONE) const;

    void         PrintStatistics(ostream& os) const;
    void         PrintXss(ostream& os = std::cout) const;
    //void         PrintYaml(ostream& os, map<CH, map<WC, map<string, double>>>& impact) const;

    void         ComputeTransitionMatrix(bool doTakeDecayFromPaper = false);

    //void static SetLabels(TH1* h, const vector<string>& labels, char axis = 'x');
    void static SetOutput(std::ostream& os) { os_ = &os; }

    inline eft::WilsonCoefficient const GetWcByCode(const eft::WilsonCoefficientsCode code) const { return wCbyCode_.at(code); }
    inline eft::WilsonCoefficient       GetWcByCode(const eft::WilsonCoefficientsCode code)       { return wCbyCode_[code]; }

    inline static void EnableMerging(bool b) { doMerging_ = b;}
    inline const TransitionMatrix& GetTransitionMatrix() const { return transMatWcBin_; }
    inline       TransitionMatrix& GetTransitionMatrix()       { return transMatWcBin_; }

    inline       void     ReadSTXScov(const string& path, const string& varName);
    inline       TMatrixD& GetSTXScov()             { return *STXScov_; }
    inline const TMatrixD& GetSTXScov() const       { return *STXScov_; }

    inline void  ComputeHessianEFT();
    void         ComputeEV();
    void         WriteXMLwsReparametrisation(const string& filename, XMLConfig config) const;
    void         SaveDataBase(const string& path) const;
    void         SaveSmXs(const string& path) const;
    void         SaveIntXs(const string& path) const;
    void         SaveIntWidths(const string& path) const;
    void         DrawImpactComparison() const;

    void        AverageWCvalues(XSperWC& interfxs);
    void        AverageWCvalues();
    void        PrintParametrisationsXML(ostream& os = std::cout) const;
    void        PrintParametrisationsXML(const string&& name) const;

    inline       eft::Merger& GetMerger()       { return merger_; }
    inline const eft::Merger& GetMerger() const { return merger_; }

    map<WilsonCoefficientsCode, map<string, ValErr<double>>> 
      Merge(const XSperWC& interfXs, const string& channel) const;
    
    map<string, ValErr<double>> 
      Merge(const CrossSections& interfXs, const string& channel) const;
      

private:
    static std::ostream*  os_;
    static bool      doMerging_;
    static size_t    nbEvents_;

    string           directory_;

    CrossSections    SMxs_;
    XSperWC          interfXS_; 
    XSperWC          interfXScoarse_; 
    
    XsChannelTruthBinWc xsChannelTruthBinWc_;  

    shared_ptr<TH1D> hSM_;            // SM xs
    shared_ptr<TH2D> hInterf_;        // Interference xs
    shared_ptr<TH2D> hImpactInterf_;  // ratio interference/SM 

    mutable shared_ptr<TH2D> hTransMatrix_;

    RegistryWC        regWC_;          // all available WC
    RegistryTruthBins regTB_;          // all available truth bins
    RegistryWCcode    regWCcode_;      // all available WC
    RegistryWCperVal  regWCperVal_;    // for all WC     -> availalbe values (1, -0.5, 0.1, 0, ...)
    RegistryValperWC  regValperWc_;    // for all values -> availalbe WC

    WCbyCodes        wCbyCode_;

    XSperWCperVal    xsPerWCperVal_;  // [WC][value][bin] -> xs
    XSperValperWC    xsPerValPerWc_;  // [value][WC][bin] -> xs

    WidthPerChanWcVal widthPerChanWcVal_;
    ValPerChanWc      brPerChanWcVal_;
    Widths            SMwidths_;

    TransitionMatrix transMatWcBin_;

    bool             wcAreAveraged_ = false;

    TMatrixD         transWcBin_;
    TMatrixD*        STXScov_;
    TMatrixD         EFTHessian_;

    TMatrixD         EFTevecs_;
    TVectorD         EFTevals_;

    eft::Merger  merger_;
 private:
    void        Register(const ParseResults& res);
    void        RegisterXS(const ParseResults& res);
    void        RegisterWidth(const ParseResults& res);
    void        AddXS(CrossSections& l, const CrossSections& r);
    void        AddXS(XsChannelTruthBinWc &db, 
                      eft::DecayChannel ch, 
                      eft::WilsonCoefficientsCode code,
                      eft::WCvalue wcVal, 
                      const CrossSections &xss) const;
    void        AddXS(XsChannelTruthBinWc &db, const ParseResults& res) const;
    void inline AddWidth(Width& l, const Width& r); 
    const CrossSections 
                AverageWCvaluesOneWC(const vector<eft::WCvalue>& wcVals, 
                                     const vector<CrossSections>& xss,
                                     const eft::WilsonCoefficientsCode& wc
                                    ) const;
};

//////////////////////////////////////////////////////////////////////////////////////
inline void EFTManager::AddWidth(Width &l, const Width &r)
{
  LOG_DURATION("AddWidth");
  l.val += r.val;
  l.err = std::sqrt(l.err * l.err + r.err * r.err);
}
//////////////////////////////////////////////////////////////////////////////////////
inline void EFTManager::ReadSTXScov(const string &path, const string &varName)
{
  LOG_DURATION("ReadSTXScov");
  TFile f(path.c_str(), "READ");
  if (!f.IsOpen())
    throw std::runtime_error("impossible to open: " + path);

  STXScov_ = reinterpret_cast<TMatrixD *>(f.Get(varName.c_str())->Clone());
  MatrixManager::DrawMatrix(*STXScov_, "figures/STXScov.pdf", "colz text");
  f.Close();
}
//////////////////////////////////////////////////////////////////////////////////////
inline void EFTManager::ComputeHessianEFT()
{
  LOG_DURATION("ComputeHessianEFT");

  cout << "try change basis: " << endl;
  TMatrixD STXSHessian = MatrixManager::GetInverted(*STXScov_);
  EFTHessian_.ResizeTo(transWcBin_.GetNrows(), transWcBin_.GetNrows());
  // cout << "change size to: " << transWcBin_.GetNrows() << " * " << transWcBin_.GetNrows() << endl;
  EFTHessian_ = MatrixManager::ChangeBasis(*STXScov_, transWcBin_);
  // MatrixManager::ChangeBasis(*STXScov_, transWcBin_);
  // return;

  auto STXS_unity = STXSHessian * (*STXScov_);

  MatrixManager::DrawMatrix(*STXScov_, "figures/STXScov.pdf", "colz text");
  MatrixManager::DrawMatrix(STXSHessian, "figures/STXSHessian.pdf", "colz text");
  MatrixManager::DrawMatrix(STXS_unity, "figures/STXS_unity.pdf", "colz text");
  MatrixManager::DrawMatrix(EFTHessian_, "figures/EFTHessian.pdf", "colz text");
}
//////////////////////////////////////////////////////////////////////////////////////

#endif
