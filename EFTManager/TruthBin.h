#pragma once

#ifndef TRUTH_BIN_H
#define TRUTH_BIN_H

#include "WilsonCoefficients.h"

#include <string>
#include <vector>

#include <iostream>

using std::string;
using namespace std;

namespace eft {

class Label {
private:
    string          name_           = "unknown";
    string          latexName_      = "unknown";
    string          rootLabelName_  = "unknown";
    size_t          id_             = 99;
    vector<size_t>  mergedFrom_     = {99};
    size_t          mergeTo_        = 99;

    static const vector<string>& tableNames_;
public:
    Label() = default;
    /*explicit Label(size_t id)
        : id_(id)
    {}*/
    explicit Label(int id)
        : id_(id)
    {}
    Label(size_t id)
        : id_(id)
    {}
    ~Label() {}


    inline       size_t id()            const { return id_; }
    inline const string name()          const { return name_; }
    inline       string name()                { return name_; }
    inline const string latexName()     const { return latexName_; }
    inline       string latexName()           { return latexName_; }
    inline const string rootLabelName() const { return rootLabelName_; }
    inline       string rootLabelName()       { return rootLabelName_; }

    inline const vector<size_t> mergedFrom() const { return mergedFrom_; }
    inline       vector<size_t> mergedFrom()       { return mergedFrom_; }

    inline       size_t         mergeTo()          { return mergeTo_; }
    inline       size_t         mergeTo()    const { return mergeTo_; }


    inline       void SetId(size_t idx)                 { id_ = idx; }
    inline       void SetName(const string& name)       { name_ = name; }
    inline       void SetLatexName(const string& name)  { latexName_ = name; }
    inline       void SetRootLabel(const string& name)  { rootLabelName_ = name; }

public:
    Label& operator = (size_t id) { id_ = id; return *this; }
    Label& operator = (size_t&& id) { id_ = id; return *this; }
    //Label& operator = (int id)    { id_ = id; return *this; }

    bool operator <  (const Label& other) { return this->id_ <  other.id_; }
    bool operator == (const Label& other) { return this->id_ == other.id_; }
    bool operator != (const Label& other) { return this->id_ != other.id_; }

    bool operator <  (const size_t other) { return this->id_ <   other; }
    bool operator >  (const size_t other) { return this->id_ >   other; }
    bool operator >= (const size_t other) { return this->id_ >=  other; }
    bool operator <= (const size_t other) { return this->id_ <=  other; }
    bool operator == (const size_t other) { return this->id_ ==  other; }
    bool operator != (const size_t other) { return this->id_ !=  other; }


    //operator size_t () { return id_; }
    //operator size_t () const { return id_; }
    operator int ()    const { return id_; }
    //std::ostream& operator << (std::ostream& os) { return os << id_; }
};

    //inline std::ostream& operator << (std::ostream& os, const Label& label) {
    //    return os << label.id();
    //}

} // namespace eft

#endif
