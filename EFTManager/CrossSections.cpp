#include "CrossSections.h"

CrossSections& CrossSections::operator+= (const CrossSections& other) {

    cross_section xs1, xs2;
    set<truth_bin> bins;

    for (const CrossSections& xs : { *this, other })
    {
        for (const auto& bin_xs : xs)
        {
            bins.insert(bin_xs.first);
        }
    }

    for (auto& bin : bins)
    {
        // cout << "try bin: " << bin << endl;

        if (this->find(bin) != other.end())
            xs1 = this->at(bin);
        else
            xs1 = { 0., 0. };

        if (other.find(bin) != this->end())
            xs2 = other.at(bin);
        else
            xs2 = { 0., 0. };

        this->operator[](bin) += xs2;
    }
    return *this;
}

CrossSections CrossSections::operator+(const CrossSections& other)
{
    // identify all bins for which xs should be added
    // it may happen, that other contains bins not present
    // in the lhc => loop over this( == lhc) and other ( == rhc)
    set<truth_bin> bins;
    CrossSections res;
    for (const CrossSections& xs : { *this, other })
    {
        for (const auto& bin_xs : xs)
        {
            bins.insert(bin_xs.first);
        }
    }

    for (const auto& bin : bins) {

        if (this->find(bin) != this->end())
            res[bin] = this->at(bin);
        else
            res[bin] = { 0., 0. };

        if (other.find(bin) != other.end())
            this->operator[](bin) += other[bin];
    }
    return res;
}
