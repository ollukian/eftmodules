#include "Parser.h" 

#include "CrossSections.h"
#include "../Utils/profile.h"
#include "../Utils/test_runner.h"
//#include <vector>

//#pragma warning(push, 0)
#include "TH2D.h"
#include "TStyle.h"
//#pragma warning(pop)

//#include "WilsonCoefficients.cxx"
//////////////////////////////////////////////////////////////////////////////////////
vector<string> Tokenize(string line) {
  LOG_DURATION("Tokenize");
  //cout << "[Tokenize]: " << line << endl;
    if (line.empty())
        throw std::runtime_error("Impossible to tokenise an empty string");

    vector<string> res;
    //size_t len = line.size();
    size_t pos = line.find('_');

    while (pos != std::string::npos) {
      //cout << " * pos = " << pos << endl;
      //cout << "token = " << line.substr(0, pos) << endl;
        res.push_back(move(line.substr(0, pos)));
        //res.push_back(move(line.substr(0, pos)));
        //cout << " --> token = " << line.back() << endl;
        //line = line.substr(pos + 1, len);
        line.erase(0, pos + 1);
        while (line[0] == '_')
            line.erase(0, 1);
        pos = line.find('_');
    }

    res.push_back(move(line));
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
vector<string> Tokenize(string line, const vector<string>& delims) {
   LOG_DURATION("Tokenize");
   if (line.empty())
        throw std::runtime_error("Impossible to tokenise an empty string");

    vector<string> res;
    //size_t len = line.size();

    size_t pos = std::string::npos;
    //cout << "[Tokenize] start searching for the closest token...." << endl;
    //size_t pos = line.find('_');
    //cout << "[Tokenize] try |_| -> " <<  pos << endl;
    for (const string& delim : delims) {
      //cout << "[Tokenize] try |" << delim << "| -> ";
      size_t pos_candidate = line.find(delim);
      //cout << pos << endl;
      if (pos_candidate < pos)
        pos = pos_candidate;
    }
    //cout << "[Tokenize] the closest delim is at the: " << pos << " position" << endl;

    while (pos != std::string::npos) {
      //cout << " * pos = " << pos << endl;
        //cout << "token = " << line.substr(0, pos) << endl;
        std::string toAdd = line.substr(0, pos);
        DeleteBlankChars(toAdd);
        res.push_back(move(toAdd));
        //res.push_back(move(line.substr(0, pos)));
        //cout << " --> token = " << line.back() << endl;
        //line = line.substr(pos + 1, len);
        //cout << "line before erasing: " << line << endl;
        line.erase(0, pos + 1);
        //cout << "line after erasing: " << line << endl;
        while (line[0] == '_')
            line.erase(0, 1);
        
        //cout << "find a new candidate" << endl;
        pos = std::string::npos;
        for (const string& delim : delims) {
          size_t pos_candidate = line.find(delim);
          //cout << " for |" << delim << "| -> " << pos_candidate << endl;
          if (pos_candidate < pos)
            pos = pos_candidate;
        }
        //cout << "go to the next round with pos = " << pos << endl;
    }

    DeleteBlankChars(line);
    res.push_back(move(line));
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
CrossSections GetCrossSection(
        const string& filename,
        bool  doDraw,
        const string& varName
        ) 
{
    cout << "[GetCrossSection]: " << filename << " file" << endl;
    LOG_DURATION("GetCrossSection");
    CrossSections res;

    TFile f(filename.c_str());

    if (! f.IsOpen() )
        throw std::runtime_error("impossible to open: " + filename);

    if ( f.Get(varName.c_str()) == nullptr ) {
      cout << "[GetCrossSection] file: " << filename << " is empty - do NOT consider it" << endl;
      return {};
    }

    shared_ptr<TH1D> crossSections = make_shared<TH1D>(*reinterpret_cast<TH1D*>(f.Get(varName.c_str())));

    size_t nbBins = crossSections->GetNbinsX();
    //cout << " * has " << nbBins << endl;
    //cout << " * their content: " << endl;

    truth_bin     bin; 
    cross_section xs;

    for (size_t idx_bin = 0; idx_bin < nbBins; ++idx_bin) {
        bin = idx_bin;
        xs.val = crossSections->GetBinContent(idx_bin + 1);
	      xs.err = crossSections->GetBinError(idx_bin + 1);
        //cout << "\t" << setw(15) << bin << setw(5) << " -> " << xs << endl;
        res[bin] = xs;
    }

    f.Close();

    if (doDraw) {
      const auto tokens = Tokenize(filename.substr(filename.rfind('/'), filename.size()));
      //cout << "[Parse] found " << tokens.size() << " tokens: " << endl;
      //for (const string& token : tokens) { cout << token << endl; }

      if (tokens.size() < 5)
          throw std::runtime_error("less than 5 tokens found in: " + filename);

      const string regime = tokens[0];
      const string mode   = tokens[1];
      const string wcs    = tokens[2];
      const string wc1    = wcs.substr(0, 2);
      const string wc2    = wcs.substr(2, 2);
      const string wilsonCoeffVal = tokens[3];
      const string nbEvents       = tokens[5];

      // for old version
      /*const string regime         = tokens[0];
      const string wilsonCoeff    = tokens[1];
      const string wilsonCoeffVal = tokens[2];
      const string nbEvents       = tokens[4];
      */
      const string wcName  = wcToStr(ParseWCcode(wc1));
      const string wcName2 = wcToStr(ParseWCcode(wc2));
      const string title = "Interference XS induced by " + wcName + 
                            + " and " + wcName2 + " at "
                            + wilsonCoeffVal + " for " + mode;
      
      {
        LOG_DURATION("DrawingIntXS");
	
        TCanvas c("c", "c", 1200, 800);
	
        c.SetBottomMargin(0.30);
        c.SetGrid();  
        //EFTManager::SetLabels(crossSections.get(), eft::binsNamesLatex, 'x');
        if (nbBins == fineNames.size())
          SetLabels(crossSections.get(), fineNames, 'x');
        //crossSections->SetTitle(("Interference XS induced by: " + wcName).c_str());
        crossSections->SetTitle(title.c_str());
        crossSections->GetXaxis()->SetLabelSize(0.02);
        crossSections->GetYaxis()->SetTitle("Interference Cross-section [pb]");
        crossSections->Draw("HIST E text");
        const string name = filename.substr(filename.rfind("/") + 1, filename.rfind(".root") - filename.rfind("/") - 1);
        cout << "[INFO] save as: " << name << endl;
        c.SaveAs(("figures/res_" + name + ".pdf").c_str());
      }
    }
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
shared_ptr<TH1D> MapToHisto(const CrossSections& mapXS) {
    LOG_DURATION("MapToHisto");
    cout << "[MapToHisto] for a map with " << mapXS.size() << "elements:" << endl;
	  //size_t nbBins = eft::mergedBinsName.size();
    size_t nbBins = mapXS.size();
    cout << "[MapToHisto] init a map with " << nbBins << "elements" << endl;
	//for (const auto& tb_xs : mapXS) { cout << tb_xs.first << " -> " << tb_xs.second << endl;}

    shared_ptr<TH1D> h = make_shared<TH1D>("h_tmp", "h_tmp", nbBins, 0.5, nbBins + 0.5);
    truth_bin     bin;
    cross_section xs;
    
    for (size_t idx_bin = 0;  idx_bin < nbBins; ++idx_bin) {
		  if (mapXS.find(idx_bin) == mapXS.end()) {
		  	  cout << " -  deal with: " << setw(15) << idx_bin << " not present in the map"
		  		     << " set to zero" << endl; 
		  	  h->SetBinContent(idx_bin + 1, 0);
	        h->SetBinError(idx_bin + 1, 0);
		  }
		  else {
		  	bin = idx_bin;
		  	xs = mapXS[bin];
      //for (const pair<truth_bin, cross_section>& tb_xs : mapXS) {
          //bin = tb_xs.first;
          //xs  = tb_xs.second;
          	cout << " - deal with: " << setw(15) << bin << " -> " << xs << endl;
          //if (bin.id() == 99)
          //  cout << " -- skip 99" << endl;
          //else {
            h->SetBinContent(bin + 1, xs.val);
	        	h->SetBinError(bin + 1, xs.err);
		  }
        //}
    }

    cout << "result of map to histo: " << endl;

    for (int idx = 1; idx <= h->GetNbinsX(); ++idx) {
      cout << idx << " -> " << h->GetBinContent(idx) << endl;
    }

    return h;
}
//////////////////////////////////////////////////////////////////////////////////////
// shared_ptr<TH2D> MapToHist(const XSperWC& xsPerWc) {
//   cout << "[MapToHisto]" << endl;
//   size_t nbWc   = xsPerWc.size();
//   size_t nbBins = xsPerWc.begin()->size();
//     shared_ptr<TH2D> h = make_shared<TH2D>("h_tmp", "h_tmp", 
//                                             nbBins, 
//                                             0, 
//                                             nbBins - 1, 
//                                             nbWc,
//                                             0,
//                                             nbWc - 1);
//     truth_bin     bin;
//     cross_section xs;
//     for (const auto& wc_tb_xs : xsPerWc) {
//         auto wc = tb_xs.first;
//
//      // for (const auto& tb_xs : tb_xs.second) {
//      //   h->SetBinContent()
//      // }
//
//         //xs  = tb_xs.second;
//         //cout << " - deal with: " << setw(15) << bin << " -> " << xs << endl;
//         //h->SetBinContent(bin, xs);
//     }
//     return move(h);
// }

using namespace eft;
//////////////////////////////////////////////////////////////////////////////////////
vector<string>  GetListFiles( const string& path, 
                              Regime reg, 
                              ProdMode mode,
                              string parserCfg,
                              /*bool isModeAsNb,*/
                              WCvalue val, 
                              size_t nbEvents
) {
  vector<string> specification;
  cout << "[GetListFiles] in " << path << ", regime = " << reg << ", mode = " << mode << ", val = " << val << ", nbev = " << nbEvents << endl;
  LOG_DURATION("GetListFiles");
  
  if (reg == Regime::PRODUCTION)
    specification.push_back("production");
  else if (reg == Regime::DECAY)
    specification.push_back("decay");
  
  if (reg == Regime::PRODUCTION) {
    if (parserCfg == "old") {
      if (mode != ProdMode::NONE) {
	size_t nb = (int) mode;
	specification[0] += "_" + to_string(nb);
      }
    }
    else { // so, parse prod mode as name: GGF, ...
      if (mode == ProdMode::GGF)
	specification[0] += "_ggH";
      else
	specification[0] += "_" + enumToStr<ProdMode>(mode);
    }
  } // production

  if (val != WCvalue::NONE) {
    size_t nb = (int) val;
    specification.emplace_back(to_string(nb) + "_WCmode");
  }

  if (nbEvents != 0)
    //specification.emplace_back("__" + to_string(nbEvents) + "_events");
    specification.emplace_back(to_string(nbEvents) + "_events");

  string unix_command = "ls " + path; // " > tmp.txt";

  //if ( ! specification.empty() )
  //  unix_command += " | grep ";

  for (const string& spec : specification) {
    unix_command += " | grep -i  " + spec;
  }

  unix_command += " > tmp.txt";

  cout << "[INFO] unix_command = " << unix_command << endl;
  gSystem->Exec(unix_command.c_str());
  
  string line;
  fstream fs("tmp.txt", std::ios_base::in);
  vector<string> res;

  while(std::getline(fs, line)) {
    res.push_back(line);
  }
  
  gSystem->Exec("rm tmp.txt");

  //cout << "result:" << endl;
  //for (const string& f : res) { cout << f << endl; }
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
vector<string> GetListFilesAna(const string& path, ProdMode mode, WCvalue val) {
  cout << "[GetListFilesAna] in " << path << ", mode = " << mode << ", val = " << val << endl;
  LOG_DURATION("GetListFilesAna");

  string folder = "/Samples_int_ci";
  switch (val)
  {
    case WCvalue::ONE     : folder += "1";     break;
    case WCvalue::MINHALF : folder += "05neg"; break;
    case WCvalue::DOTONE  : folder += "01";    break;
    case WCvalue::AVERAGED : throw std::runtime_error("AVERAGED is not available "); break;
    case WCvalue::SM      : throw std::runtime_error("SM is not available now"); break;
    case WCvalue::TOTAL   : throw std::runtime_error("TOTAL is not available now"); break;
    case WCvalue::NONE    : throw std::runtime_error("NONE is not available now"); break;
    default:                folder += "*";     break;
  }

  folder += '/';

  static const map<ProdMode, string> mapProdModesPerCode {
      {ProdMode::GGF,   "ggH"},
      {ProdMode::VBF,   "VBF"},
      {ProdMode::ZH,    "ZHlep"},
      {ProdMode::WH,    "WHlep"},
      {ProdMode::TTH,   "ttH"},
      {ProdMode::THJB,  "tHjb"},
      {ProdMode::THW,   "tHW"},
    };


  static const map<size_t, string> mapProdModes {
    {1, "ggH"},
    {2, "VBF"},
    {3, "ZHlep"},
    {4, "WHlep"},
    {5, "ttH"},
    {6, "tHjb"},
    {7, "tHW"},
  };

  cout << "folder: " << folder << endl;

  const string specification = mapProdModesPerCode.at(mode) + "*";
  cout << "specification: " << specification << endl;
  const string unix_command = "ls " + path + folder + " | grep -i " + specification + " > tmp.txt";
  gSystem->Exec(unix_command.c_str());

  cout << "unix command: " << unix_command << endl;
  
  string line;
  fstream fs("tmp.txt", std::ios_base::in);
  vector<string> res;

  while(std::getline(fs, line)) {
    cout << "dealing with: " << line << endl;
    res.push_back(path + folder + line);
  }
  
  gSystem->Exec("rm tmp.txt");
  cout << "result:" << endl;
  for (const string& f : res) { cout << f << endl; }

  return res;

}
//////////////////////////////////////////////////////////////////////////////////////
vector<string> GetListFiles(const string& path, 
  WilsonCoefficientsCode code, 
  size_t nbEvents,
  Regime reg, 
  WCvalue val) 
{
  vector<string> specification;
  cout << "[GetListFilesGivenWC] in " << path << ", regime = " << reg << ", wc = " << code << ", val = " << val << ", nbev = " << nbEvents << endl;
  LOG_DURATION("GetListFilesGivenWC");
  
  if (reg == Regime::PRODUCTION)
    specification.push_back("production");
  else if (reg == Regime::DECAY)
    specification.push_back("decay");

  string wcSpec = to_string(static_cast<int>(code)) + "00_";
  specification.emplace_back(move(wcSpec));

  if (val != WCvalue::NONE) {
    size_t nb = (int) val;
    specification.emplace_back(to_string(nb) + "_WCmode");
  }

  if (nbEvents != 0)
    specification.emplace_back(to_string(nbEvents) + "_events");
    //specification.emplace_back("__" + to_string(nbEvents) + "_events");

  string unix_command = "ls " + path; // " > tmp.txt";

  //if ( ! specification.empty() )
  //  unix_command += " | grep ";

  for (const string& spec : specification) {
    unix_command += " | grep -i " + spec;
  }

  unix_command += " > tmp.txt";

  cout << "[INFO] unix_command = " << unix_command << endl;
  gSystem->Exec(unix_command.c_str());
  
  string line;
  fstream fs("tmp.txt", std::ios_base::in);
  vector<string> res;

  while(std::getline(fs, line)) {
    res.push_back(path + "/" + line);
  }
  
  gSystem->Exec("rm tmp.txt");

  cout << "result:" << endl;
  for (const string& f : res) { cout << f << endl; }
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
eft::WCvalue ParseWCvalue(float val) {
  int i_val = static_cast<int>(val);
  if (i_val == 0) 
      return WCvalue::SM;    
  else if (i_val == 1) 
      return WCvalue::ONE;   
  else if (i_val == 2) 
      return WCvalue::MINHALF;
  else if (i_val == 3) 
      return WCvalue::DOTONE;
  
  throw std::runtime_error("an attempt to parse wrong: " + to_string(val) + " value of the WC val");
  return WCvalue::NONE;
}
//////////////////////////////////////////////////////////////////////////////////////
eft::DecayChannel ParseDecayChannelEleonora(const string& name) {
  static const map<string, string> map_channels_Elenora_mine = {
    {"4J"        , "QQQQ"},
    {"4L"        , "LLLL"},
    {"4TAU"      , "TAU_TAU_TAU_TAU"},
    {"4V"        , "NU_NU_NU_NU"},
    {"AA"        , "GAM_GAM"},
    {"BB"        , "B_B"},
    {"CC"        , "C_C"},
    {"EE"        , "E_E"},
    {"GG"        , "G_G"},
    {"JJLL"      , "J_J_L_L"},
    {"JJVV"      , "J_J_NU_NU"},
    {"LLTAUTAU"  , "L_L_TAU_TAU"},
    {"LVJJ"      , "L_NU_J_J"},
    {"LVTAUV"    , "L_NU_TAU_NU"},
    {"MUMU"      , "MU_MU"},
    {"SS"        , "S_S"},
    {"TAUTAU"    , "TAU_TAU"},
    {"TAUVTAUV"  , "TAU_NU_TAU_NU"},
    {"VVTAUTAU"  , "NU_NU_TAU_TAU"},
    {"ZA"        , "Z_GAM"},
    {"LVLV"      , "L_NU_L_NU"}
};

  //auto name_big = CapitaliseString(name);
  string name_big = name.substr(1, name.length());;
  CapitaliseString(name_big);

  if (map_channels_Elenora_mine.find(name_big) == map_channels_Elenora_mine.end())
    throw std::runtime_error("NO " + name_big + " in the channels name");

  return strToChan(map_channels_Elenora_mine.at(name_big));
}
//////////////////////////////////////////////////////////////////////////////////////
Width GetWidth(const string& filename) {
    cout << "[GetWidth] from " << filename << endl;
    LOG_DURATION("GetWidth");
    fstream f(filename, std::ios_base::in);

    if ( ! f.is_open()) 
      throw std::runtime_error("impossible to open: " + filename + " to get the width");
    
    string val, err, trash;
    f >> val >> trash >> err;
    Width res;
    res.val = stod(val);
    res.err = stod(err);
    cout << "[GetWidth] found: " << res.val << " +- " << res.err << endl;
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
CrossSections Merge(CrossSections& xss, const std::map<size_t, size_t> mg) {
  putTime(std::cout) << "[Merge] xs with " << xss.size() <<  "elements"<< endl;
  LOG_DURATION("Merge");

  CrossSections res;

  truth_bin binInitial;
  truth_bin binMerged;
  cross_section xs;

  for (const auto& bin_xs : xss) {
    binInitial = bin_xs.first;
    binMerged  = mg.at(binInitial);
    xs = bin_xs.second;
    /*cout << setw(5) << binInitial << " -> " << setw(5) << binMerged 
	       << " | Add: " << setw(15) << xs << " to taddwihe xs: "
	       << setw(15) << res[binMerged] << " -> ";*/
    
    res[binMerged] += xs;
    //cout << setw(15) << res[binMerged] << endl;
  }
  putTime(std::cout) << "[Merge] is done. Result contains " << res.size() << " bins" << endl;
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
vector<double>  WcValsToDouble(const vector<eft::WCvalue>& vals) {
  LOG_DURATION("WcValsToDouble");
  cout << "wcvals to double for " << vals.size() << " elements" << endl;

  vector<double> res;
  res.reserve(vals.size());

  for (const auto val : vals) {
      if (val == eft::WCvalue::ONE)
          res.push_back(1.);
      else if (val == eft::WCvalue::DOTONE)
          res.push_back(0.1);
      else if (val == eft::WCvalue::MINHALF)
          res.push_back(-0.5);
      else if (val == eft::WCvalue::SM)
          res.push_back(-0.0);
      else {
          ostringstream os;
          os << val;
          throw std::runtime_error("impossible to convert the" + os.str() + " to a value");
      }   
  }
  cout << " to return: " << res.size() << " elements" << endl;
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
ostream& operator << (ostream& os, const ParseResults& res) { // TODO: add printing of the WC 
    os << setfill('=') << setw(50) << '=' << endl << setfill(' ');
    os << "| Parse Results: " << setw(33) << "|" << endl;
    os << setfill('=') << setw(50) << '=' << endl << setfill(' ');
    os << "| " << setw(20) << "events: "      << setw(15) << res.nbGeneratedEvents << setw(14) << " | " << endl;
    os << "| " << setw(20) << "regime: "      << setw(15) << res.regime            << setw(14) << " | " << endl;
    os << "| " << setw(20) << "TheoryRegime: "<< setw(15) << res.theoryRegime      << setw(14) << " | " << endl;

    if (res.theoryRegime == eft::TheoryRegime::INT) {
        //os << "| " << setw(20) << "wcvalue: "     << setw(15) << res.wcValue           << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcvalueCode: " << setw(15) << res.wcValueCode       << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcid: "        << setw(15) << res.wc.GetId()        << setw(14) << " | " << endl;
        os << "| " << setw(35) << res.wcCode      << setw(14) << " | " << endl;
    }
    if (res.theoryRegime == eft::TheoryRegime::BSM) {
        //os << "| " << setw(20) << "wcvalue1: "    << setw(15) << res.wcValue           << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcvalueCode1: "<< setw(15) << res.wcValueCode       << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcid1: "       << setw(15) << res.wc.GetId()        << setw(14) << " | " << endl;
        os << "| " << setw(35) << res.wcCode      << setw(14) << " | " << endl;
        //os << "| " << setw(20) << "wcvalue2: "    << setw(15) << res.wcValue           << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcvalueCode:2 "<< setw(15) << res.wcValueCode       << setw(14) << " | " << endl;
        os << "| " << setw(20) << "wcid2: "       << setw(15) << res.wc.GetId()        << setw(14) << " | " << endl;
        os << "| " << setw(35) << res.wcCode2     << setw(14) << " | " << endl;
    }
    if (res.regime == eft::Regime::DECAY) {
        os << "| " << setw(20) << "decayChannel: "<< setw(26) << res.decayChannel      << setw(1) << " | " << endl;
        os << "| " << setw(20) << "Width: "   << setw(15) << res.width_.val << " +-" << res.width_.err   << setw(2) << " | " << endl; 
    }
    else {
        os << "| " << setw(20) << "prodMode: "    << setw(15) << res.prodMode          << setw(14) << " | " << endl;
    }
    os << setfill('=') << setw(50) << '=' << endl << setfill(' ');
    return os;  
}
//////////////////////////////////////////////////////////////////////////////////////
std::vector<eft::ProdMode> ParseProductionModesFromNames(const string& names) {
  LOG_DURATION("ParseProductionModesFromNames");

  if (names.empty())
    throw std::runtime_error("[ParseProductionModesFromNames] cannot parse empty string");


  //static set<string> __allProdModesList{"GGH", "VBF", "WHLEP", "ZHLEP", "VBF"}

  auto tokens = Tokenize(names, {",", "|"});
  std::vector<eft::ProdMode> res;
  res.reserve(tokens.size());
  cout << "tokens: " << endl;


  for (string& token : tokens) { // not const, so we can capitalise it
    CapitaliseString(token);
    cout << token << endl;
    if (token == "GGF" || token == "GGH")
      res.push_back(eft::ProdMode::GGF);
    else if (token == "VBF")
      res.push_back(eft::ProdMode::VBF);
    else if (token == "ZHLEP" || token == "ZH")
      res.push_back(eft::ProdMode::ZH);
    else if (token == "WHLEP" || token == "WH")
      res.push_back(eft::ProdMode::WH);
    else if (token == "QQ2ZH")
      res.push_back(eft::ProdMode::QQ2ZH);
    else if (token == "GG2ZH")
      res.push_back(eft::ProdMode::GG2ZH);
    else if (token == "TTH")
      res.push_back(eft::ProdMode::TTH);
    else if (token == "THJB" || token == "THBJ" || token == "THB")
      res.push_back(eft::ProdMode::THJB);
    else if (token == "THW" || token == "TWH")
      res.push_back(eft::ProdMode::THW);
    else if (token == "BBH")
      res.push_back(eft::ProdMode::BBH);
    else {
      throw std::runtime_error("[ParseProductionModesFromNames]: "
        + string("impossible to parse: |") + token 
        + "| available: GGF, GGH, VBF, WHlep, WH, ZH, ZHlep, TTH, THJB, THBJ, THB, THW, TWH, QQ2ZH, GG2ZH, BBH");
    }
  }
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
//#ifndef	_MSC_VER
void SetLabels(TH1 *h, const vector<string> &labels, char axis)
{
  /*
    axis: x or y -> which axis to set
    default: 'x'
  */
  LOG_DURATION("SetLabels");
  putTime(cout) << "[SetLabels] for " << axis << " axis " << endl;
  TAxis *ax;
  // shared_ptr<TAxis> ax;
  if (axis == 'x')
    ax = h->GetXaxis();
  else if (axis == 'y')
    ax = h->GetYaxis();
  else
  {
    string s;
    s.push_back(axis);
    throw std::runtime_error("ERROR: axis: " + s + " is not recognised. Available: x, y");
  }

  size_t nbLabels = ax->GetNbins();
  // cout << "nb labels = " << nbLabels << endl;
  // cout << "nb names in the vec: " << labels.size() << endl;

  ASSERT_EQUAL(nbLabels, labels.size());

  for (size_t idx_label = 0; idx_label < nbLabels; ++idx_label)
  {
    // cout << "dealing with idx = " << setw(5) << idx_label
    //		 << " put label: " << labels[idx_label] << endl;
    ax->SetBinLabel(idx_label + 1, labels[idx_label].c_str());
  }

  if (axis == 'x')
    ax->LabelsOption("v");
}
//#endif
//////////////////////////////////////////////////////////////////////////////////////
//#ifndef	_MSC_VER
TH1D* GetHistFromFile(const string& path, const string& name) {
  LOG_DURATION("GetHistFromFile");
  TFile f( path.c_str() );
  TH1D* res;

  if (! f.IsOpen() )
    throw std::runtime_error("impossible to open: " + name);

  if ( f.Get(name.c_str()) == nullptr ) {
    cout << "[GetCrossSection] file: " << name << " is empty - do NOT consider it" << endl;
    return {};
  }

  TH1D* cloned =  static_cast<TH1D*> ( (f.Get<TH1D>(name.c_str())) );
  cout << "cloned: ";
  cloned->Print();

  res = new TH1D(path.c_str(), "", cloned->GetNbinsX(), 0, cloned->GetNbinsX());

  for (int idx_bin = 1; idx_bin <= cloned->GetNbinsX(); ++idx_bin) {
    res->SetBinContent(idx_bin, cloned->GetBinContent(idx_bin));
    res->SetBinError(idx_bin, cloned->GetBinError(idx_bin));
  }

  cout << "res in function: " << endl;
  res->Print();
  delete cloned;

  f.Close();
  /*res->Add(cloned);
  //res.reset( cloned );
  cout << "res: ";
  res->Print();
  f.Close();
  cout << "after closing, res: ";
  res->Print();*/
  return res;
}
//#endif
//////////////////////////////////////////////////////////////////////////////////////
//#ifndef	_MSC_VER
shared_ptr<TH1D> GetTotalXsGivenWC(
      const string& path, 
      WilsonCoefficientsCode code, 
      size_t nbEvent, 
      WCvalue wc_value) 
{
  LOG_DURATION("GetTotalXsGivenWC");
  const auto files = GetListFiles(path, code, nbEvent, Regime::PRODUCTION, wc_value);

  size_t nbBins = fineNames.size();
  shared_ptr<TH1D> h = make_shared<TH1D>("h", "h", nbBins, 0, nbBins);

  vector<CrossSections> xss;

  for (const string& file : files) {
    //cout << "file: " << file << endl;
    //cout << "content: " << endl;
    
    //TH1D* h_tmp = GetHistFromFile(file);
    //cout << "after get hist from file" << endl;
    //h_tmp->Print();
    //for (int idx = 1; idx <= h_tmp->GetNbinsX(); ++idx) {
    //  cout << idx << " -> " << h_tmp->GetBinContent(idx) << " +- " << h_tmp->GetBinError(idx) << endl;
    //}

    /*cout << "what the function returns: " << endl;
    TH1D* h_ = GetHistFromFile(file);
    cout << "after" << endl;
    cout << boolalpha;
    cout << "is zero ptr? " << (h_ == nullptr) << endl;
    cout << "let's pring it " << endl;
    h_->Print();
    cout << "after printing" << endl;*/

    //h->Add(GetHistFromFile(file));
    xss.push_back(move(GetCrossSection(file)));
  }

  

  for (const auto& xs_one_file : xss) {
    for (const auto& bin_xs : xs_one_file) {
      auto bin = bin_xs.first;
      auto xs = bin_xs.second;
      cout << "[" << bin.id() << "] add -> " << xs;

      auto val = h->GetBinContent(bin + 1);
      auto err = h->GetBinError(bin + 1);

      cout << "| val: " << val << " +- " << err << " -> ";

      h->SetBinContent(bin + 1, xs.val + val);
      h->SetBinError(bin + 1, sqrt(xs.err * xs.err + err * err));
      cout << h->GetBinContent(bin+1) << " +- " << h->GetBinError(bin+1) << endl;
    }
  }
  return h;
}
//#endif
//////////////////////////////////////////////////////////////////////////////////////
//#ifndef _MSC_VER
TH1D* ComputeSignificanceDifferencesHistos(const TH1D& h1, const TH1D& h2) {
    ASSERT_EQUAL(h1.GetNbinsX(), h2.GetNbinsX());

    size_t nbBins = h1.GetNbinsX();
    TH1D* res = new TH1D("test", "", nbBins, 0, nbBins);

    for (size_t idx_bin = 1; idx_bin <= nbBins; ++idx_bin) {
        auto val1 = h1.GetBinContent(idx_bin);
        auto err1 = h1.GetBinError(idx_bin);
        auto val2 = h2.GetBinContent(idx_bin);
        auto err2 = h2.GetBinError(idx_bin);

        double signif;

        if (err1 != 0 && err2 != 0)
          signif = abs(val1 - val2) / sqrt(err1 * err1 + err2 * err2);
        else
          signif = 0;
        cout << val1 << " +- " << err1 << " & " << val2 << " +- "  << err2 << " -> ";
        res->SetBinContent(idx_bin, signif);
        cout << signif << endl;
    }
    return res;
}
//#endif
//////////////////////////////////////////////////////////////////////////////////////
void Test() {

  using namespace eft;
  using WC = WilsonCoefficientsCode;

  static const vector<WC> wcs {
    WC::SM,
    WC::CW,
	  WC::CH,
	  WC::CHBOX,
	  WC::CHDD ,
	  WC::CHG  ,
	  WC::CHW  ,
	  WC::CHB  ,
	  WC::CHWB ,
	  WC::CUHRE,
	  WC::CTHRE,
	  WC::CDHRE,
	  WC::CBHRE,
	  WC::CUGRE,
	  WC::CTGRE,
	  WC::CUWRE,
	  WC::CTWRE,
	  WC::CUBRE,
	  WC::CTBRE,
	  WC::CDGRE,
	  WC::CBGRE,
	  WC::CDWRE,
	  WC::CBWRE,
	  WC::CDBRE,
	  WC::CBBRE,
	  WC::CHJ1, 
	  WC::CHQ1, 
	  WC::CHJ3, 
	  WC::CHQ3,
	  WC::CHU ,
	  WC::CHT ,
	  WC::CHD ,
	  WC::CHBQ,
	  WC::CHUDRE,
	  WC::CHTBRE,
	  WC::CJJ11,
	  WC::CJJ18,
	  WC::CJJ31,
	  WC::CJJ38,
	  WC::CQJ11,
	  WC::CQJ18,
	  WC::CQJ31,
	  WC::CQJ38,
	  WC::CQQ1,
	  WC::CQQ8,
	  WC::CUU1,
	  WC::CUU8,
	  WC::CTT ,
	  WC::CTU1,
	  WC::CTU8,
	  WC::CDD1,
	  WC::CDD8,
	  WC::CBB, 
	  WC::CBD1,
	  WC::CBD8,
	  WC::CUD1,
	  WC::CTB1,
	  WC::CTD1,
	  WC::CBU1,
	  WC::CUD8,
	  WC::CTB8,
	  WC::CTD8,
	  WC::CBU8,
	  WC::CJU1,
	  WC::CQU1,
	  WC::CJU8,
	  WC::CQU8,
	  WC::CTJ1,
	  WC::CTJ8,
	  WC::CQT1,
	  WC::CQT8,
	  WC::CJD1,
	  WC::CJD8,
	  WC::CQD1,
	  WC::CQD8,
	  WC::CBJ1,
	  WC::CBJ8,
	  WC::CQB1,
	  WC::CQB8,
	  WC::CEHRE,
	  WC::CEWRE,
	  WC::CEBRE,
	  WC::CHL1 ,
	  WC::CHL3 ,
	  WC::CHE ,
	  WC::CLL ,
	  WC::CLL1,
	  WC::CLJ1,
	  WC::CLJ3,
	  WC::CQL1,
	  WC::CQL3,
	  WC::CEE,
	  WC::CEU,
	  WC::CTE,
	  WC::CED,
	  WC::CBE,
	  WC::CJE,
	  WC::CQE,
	  WC::CLU,
	  WC::CTL,
	  WC::CLD,
	  WC::CBL,
	  WC::CLE     
  };


  TH2D* h_signif = new TH2D("signif", "", 
    fineNames.size(), 0, fineNames.size(),
    wcs.size(), 0, wcs.size()
  );

  size_t idx_wc = 0;
   bool first = true;
  for (const auto wc : wcs) {
    idx_wc++;

  auto hist_10k = GetTotalXsGivenWC("/sps/atlas/o/ollukian/ATLAS_HGam/EFT/HyyEFT/MyEFT/res",
      wc,
      10000,
      WCvalue::ONE
    );

    auto hist_100k = GetTotalXsGivenWC("/sps/atlas/o/ollukian/ATLAS_HGam/EFT/HyyEFT/MyEFT/res",
      wc,
      100000,
      WCvalue::ONE
    );

    auto res = ComputeSignificanceDifferencesHistos(*hist_10k, *hist_100k);
    h_signif->GetYaxis()->SetBinLabel(idx_wc, wcToStr(wc).c_str());
   
    for (size_t idx_bin = 1; idx_bin <= static_cast<size_t>(res->GetNbinsX()); ++idx_bin) {
      h_signif->SetBinContent(idx_bin, idx_wc, res->GetBinContent(idx_bin));
      
      if (first) {
        h_signif->GetXaxis()->SetBinLabel(idx_bin, fineNames[idx_bin - 1].c_str());
      }
    }
    first = false;
  }

  gStyle->SetPaintTextFormat("2.1f");
  h_signif->SetMarkerSize(0.2);
  h_signif->GetXaxis()->SetLabelSize(0.01);
  h_signif->GetYaxis()->SetLabelSize(0.01);
  h_signif->GetZaxis()->SetRangeUser(0.f, 5.f);


  TCanvas c("c", "c", 1800, 1200);

  c.SetBottomMargin(0.30);


  c.SetGrid();
  h_signif->Draw("colz text");
  c.SaveAs("figures/signif.pdf");
}
