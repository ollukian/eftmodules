import ROOT

list_poi = ["mu_Hll_ptV_0_150"                       
, "mu_Hll_ptV_gt150"                      
, "mu_gg2H_0J_ptH_0_10"                   
, "mu_gg2H_0J_ptH_gt10"                   
, "mu_gg2H_1J_ptH_0_60"                   
, "mu_gg2H_1J_ptH_120_200"                
, "mu_gg2H_1J_ptH_60_120"                 
, "mu_gg2H_ge2J_mJJ_0_350_ptH_0_120"      
, "mu_gg2H_ge2J_mJJ_0_350_ptH_120_200"    
, "mu_gg2H_ge2J_mJJ_gt350_ptH_0_200"      
, "mu_gg2H_ptH_200_300"        
, "mu_gg2H_ptH_300_450"       
, "mu_gg2H_ptH_gt450"  
, "mu_qq2Hlnu_ptV_0_150"   
, "mu_qq2Hlnu_ptV_gt150"  
, "mu_qq2Hqq_VHhad"    
, "mu_qq2Hqq_ge2J_mJJ_350_1000_ptH_gt200" 
, "mu_qq2Hqq_ge2J_mJJ_350_700_ptH_0_200"  
, "mu_qq2Hqq_ge2J_mJJ_700_1000_ptH_0_200" 
, "mu_qq2Hqq_ge2J_mJJ_gt1000_ptH_0_200"   
, "mu_qq2Hqq_ge2J_mJJ_gt1000_ptH_gt200"   
, "mu_qq2Hqq_le1J_VHveto"    
, "mu_tH"      
, "mu_ttH_ptH_0_60"  
, "mu_ttH_ptH_120_200"  
, "mu_ttH_ptH_200_300"   
, "mu_ttH_ptH_60_120"   
, "mu_ttH_ptH_gt300"   
] # ordered list of POI names as in the fit

f = ROOT.TFile("source/Hyy.root", "UPDATE")
#res = ROOT.RooFit.RooFitResult
f.ls()
res = f.Get("fitResultAsimov")
#corr = ROOT.TMatrixDSym()
cor = res.correlationMatrix()
cov = res.covarianceMatrix() # if you need covariance
#cov = f.Get("covariance")
# get the index of the POI from the full list of floating parameters
poiIndex = [res.floatParsFinal().index(p) for p in list_poi]
size = len(poiIndex)

print(size, " POIs detected:")
print(poiIndex)

resCov  = ROOT.TMatrixD(size, size)
resCor  = ROOT.TMatrixD(size, size)
hist = ROOT.TH2D("hCov", "hCov", size, 0, size, size, 0, size)

## extract the values
idx_i = 0
idx_j = 0
for i in poiIndex:
    idx_j = 0
    hist.GetXaxis().SetBinLabel(idx_i + 1, list_poi[idx_i])
    hist.GetYaxis().SetBinLabel(idx_i + 1, list_poi[idx_i])
    for j in poiIndex:
        resCov[idx_i][idx_j] = cov(i, j)
        resCor[idx_i][idx_j] = cor(i, j)
        hist.SetBinContent(idx_i + 1, idx_j + 1, cov(i, j))
        idx_j += 1
        print(  cov( i , j )  )
    idx_i += 1

resCov.Write("stxsCovarianceExp")
resCor.Write("stxsCorrelationExp")
hist.Write("stxsCovarianceExpHist")


f.Close()