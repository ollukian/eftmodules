#ifndef WILSON_COEF_H
#define WILSON_COEF_H


#pragma once

#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <set>

/*using std::string;
using std::vector;
using std::ostream;*/

namespace eft {

    class POI {
    private:
      std::string  name_ {};
      std::string  latexName_ {};
      std::string  rootLabel_ {};
      double  value_ = 0;
      double  error_ = 0;
    public:
        POI() = default;
        //virtual ~POI();
        explicit POI(const std::string& name);
        POI(const std::string& name, double value)
            : name_(name)
            , value_(value) {}

        virtual ~POI() = default;
    public:
        inline       std::string GetName() { return name_; }
        inline const std::string GetName()      const { return name_; }
        inline       std::string GetLatexName() { return latexName_; }
        inline const std::string GetLatexName() const { return latexName_; }
        inline       std::string GetRootLabel() { return rootLabel_; }
        inline const std::string GetRootLabel() const { return rootLabel_; }
        inline       double  GetValue() { return value_; }
        inline       double  GetValue()     const { return value_; }
        inline       double  GetError() { return error_; }
        inline       double  GetError()     const { return error_; }

        inline void SetName(const std::string& name) { name_ = name; }
        inline void SetLatexName(const std::string& name) { latexName_ = name; }
        inline void SetRootLabel(const std::string& name) { rootLabel_ = name; }
        inline void SetValue(double val) { value_ = val; }
        inline void SetError(double val) { error_ = val; }
    };

    /*enum class WilsonCoefficientsCode { // list from 19 to 84
        TOTAL = 100,
        NONE = 999,
        SM = 0,
        CG = 19,
        cGtil = 20,
        CW = 21,
        cWtil = 22,
        CH = 23,
        CHBOX = 24,
        CHDD = 25,
        CHG = 26,
        cHGtil = 27,
        CHW = 28,
        CHWTIL = 29,
        CHB = 30,
        CHBTIL = 31,
        CHWB = 32,
        CHWBTIL = 33,
        CEHRE = 34,
        CUHRE = 35,
        CDHRE = 36,
        CEWRE = 37,
        CEBRE = 38,
        CUGRE = 39,
        CUWRE = 40,
        CUBRE = 41,
        CDGRE = 42,
        CDWRE = 43,
        CDBRE = 44,
        CHL1 = 45,
        CHL3 = 46,
        CHE = 47,
        CHQ1 = 48,
        CHQ3 = 49,
        CHU = 50,
        CHD = 51,
        CHUDRE = 52,
        CL1 = 53,
        CLL1 = 54,
        CQQ1 = 55,
        CQQ11 = 56,
        CQQ3 = 57,
        CQQ31 = 58,
        CLQ1 = 59,
        CLQ3 = 60,
        CEE = 61,
        CUU = 62,
        CUU1 = 63,
        CDD = 64,
        CDD1 = 65,
        CEU = 66,
        CED = 67,
        CUD1 = 68,
        CUD8 = 69,
        CLE = 70,
        CLU = 71,
        CLD = 72,
        CQE = 73,
        CQU1 = 74,
        CQU8 = 75,
        CDQ1 = 76,
        CDQ8 = 77,
        CLEDQRE = 78,
        CQUQD1RE = 79,
        CQUQD11RE = 80,
        CQUQD8RE = 81,
        CQUQD81RE = 82,
        CLEQU1RE = 83,
        CLEQU3RE = 84
    };*/

    enum class WilsonCoefficientsCode {
        NONE = 999,
        TOTAL = 1000,
        SM = 0,
    	CG = 1,
	CW = 2,
	CH = 3,
	CHBOX = 4,
	CHDD = 5,
	CHG = 6,
	CHW = 7,
	CHB = 8,
	CHWB = 9,
	CUHRE = 10,
	CTHRE = 11,
	CDHRE = 12,
	CBHRE = 13,
	CUGRE = 14,
	CTGRE = 15,
	CUWRE = 16,
	CTWRE = 17,
	CUBRE = 18,
	CTBRE = 19,
	CDGRE = 20,
	CBGRE = 21,
	CDWRE = 22,
	CBWRE = 23,
	CDBRE = 24,
	CBBRE = 25,
	CHJ1 = 26,
	CHQ1 = 27,
	CHJ3 = 28,
	CHQ3 = 29,
	CHU = 30,
	CHT = 31,
	CHD = 32,
	CHBQ = 33,
	CHUDRE = 34,
	CHTBRE = 35,
	CJJ11 = 36,
	CJJ18 = 37,
	CJJ31 = 38,
	CJJ38 = 39,
	CQJ11 = 40,
	CQJ18 = 41,
	CQJ31 = 42,
	CQJ38 = 43,
	CQQ1 = 44,
	CQQ8 = 45,
	CUU1 = 46,
	CUU8 = 47,
	CTT = 48,
	CTU1 = 49,
	CTU8 = 50,
	CDD1 = 51,
	CDD8 = 52,
	CBB = 53,
	CBD1 = 54,
	CBD8 = 55,
	CUD1 = 56,
	CTB1 = 57,
	CTD1 = 58,
	CBU1 = 59,
	CUD8 = 60,
	CTB8 = 61,
	CTD8 = 62,
	CBU8 = 63,
	CJU1 = 66,
	CQU1 = 67,
	CJU8 = 68,
	CQU8 = 69,
	CTJ1 = 70,
	CTJ8 = 71,
	CQT1 = 72,
	CQT8 = 73,
	CJD1 = 74,
	CJD8 = 75,
	CQD1 = 76,
	CQD8 = 77,
	CBJ1 = 78,
	CBJ8 = 79,
	CQB1 = 80,
	CQB8 = 81,
	CEHRE = 100,
	CEWRE = 101,
	CEBRE = 102,
	CHL1 = 103,
	CHL3 = 104,
	CHE = 105,
	CLL = 106,
	CLL1 = 107,
	CLJ1 = 108,
	CLJ3 = 109,
	CQL1 = 110,
	CQL3 = 111,
	CEE = 112,
	CEU = 113,
	CTE = 114,
	CED = 115,
	CBE = 116,
	CJE = 117,
	CQE = 118,
	CLU = 119,
	CTL = 120,
	CLD = 121,
	CBL = 122,
	CLE = 123,
    };

    WilsonCoefficientsCode strToWC(const std::string& wc = {});

    enum class Regime {
        PRODUCTION,
        DECAY,
        NONE = 0
    };

    enum class TheoryRegime {
        SM = 0,
        INT = 1,
        BSM = 2,
        NOT_DEFINED = 9
    };

    enum class ProdMode {
        TOTAL = 0,
        GGF   = 1,
        VBF   = 2,
        ZH    = 4,
        WH    = 3,
        QQ2ZH = 5,
        GG2ZH = 6,
        TTH   = 7,
        THJB  = 8,
        THW   = 9,
        BBH   = 10,
        NONE = 99
        //TTH = 5,
        //THJB = 6,
        //THW = 7,
    };

    enum class WCvalue {
        SM = 0,
        ONE = 1,
        DOTONE = 3, // 01
        MINHALF = 2,  // -05
        NONE = 9,
        TOTAL = 10,
        AVERAGED
    };

    enum class DecayChannel {
        TOTAL = 0,
        L_NU_L_NU = 10,
        TAU_NU_TAU_NU = 11,
        L_NU_TAU_NU = 12,
        L_NU_J_J = 13,
        QQQQ = 14,
        LLLL = 15,
        TAU_TAU_TAU_TAU = 16,
        NU_NU_NU_NU = 17,
        L_L_TAU_TAU = 18,
        NU_NU_TAU_TAU = 19,
        J_J_L_L = 20,
        J_J_NU_NU = 21,
        GAM_GAM = 22,
        B_B = 23,
        C_C = 24,
        S_S = 25,
        E_E = 26,
        MU_MU = 27,
        TAU_TAU = 28,
        Z_GAM = 29,
        G_G = 30,
        NONE = 999,
    };

    DecayChannel strToChan(const std::string& chan);
    //bool operator == (enum DecayChannel& left, enum DecayChannel& right) { return ((int)left) == ((int)right); }

    class WilsonCoefficient : public POI {
    public:
        WilsonCoefficient() = default;
	~WilsonCoefficient() final {}

        WilsonCoefficient(const std::string& name)
	  : POI(name) {}
        WilsonCoefficient(const std::string& name, double val)
            : POI(name, val) {}
        WilsonCoefficient(const std::string& name, double val, WCvalue wcVal)
            : POI(name, val)
            , val_(wcVal) {}

        void   SetId(size_t id) { id_ = id; /*Init();*/ }
        size_t GetId() const { return id_; }

        WilsonCoefficientsCode GetWcCode() const { return type_; }
        void                   SetWcCode(WilsonCoefficientsCode code) { type_ = code; id_ = static_cast<int>(code); }

    private:
        WilsonCoefficientsCode  type_ = WilsonCoefficientsCode::NONE;
        WCvalue                 val_ = WCvalue::NONE;
        size_t                  id_ = 0;
    private:
        //void Init(); // TODO: to get the latex name from the table
    };

    bool operator < (const WilsonCoefficient& l, const WilsonCoefficient& r);
    std::ostream& operator << (std::ostream& os, const Regime reg);
    std::ostream& operator << (std::ostream& os, const ProdMode mode);
    std::ostream& operator << (std::ostream& os, const WCvalue val);
    std::ostream& operator << (std::ostream& os, const DecayChannel val);
    std::ostream& operator << (std::ostream& os, const WilsonCoefficientsCode& val);
    std::ostream& operator << (std::ostream& os, const TheoryRegime val);

    inline std::string wcToStr(const WilsonCoefficientsCode& wc) {
        std::ostringstream s;
        s << wc;
        size_t pos = s.str().find("::");
        size_t len = s.str().length();
        return s.str().substr(pos + 2, len);
    }

    template<typename Enum>
    inline std::string enumToStr(const Enum& s) {
        std::ostringstream os;
        os << s;
        size_t pos = os.str().find("::");
        size_t len = os.str().length();
        return os.str().substr(pos + 2, len);
    }

    std::ostream& operator << (std::ostream& os, const WilsonCoefficient& wc);

    const std::vector<std::string> wCnames = { "ceWPh","ceBPh","cuGPh","cuWPh","cuBPh","cdGPh","cdWPh","cdBPh","cHudPh","ceHPh","cuHPh","cdHPh","cledqPh","cquqd1Ph","cquqd8Ph","clequ1Ph","clequ3Ph","LambdaSMEFT","cG","cGtil","cW","cWtil","cH","cHbox","cHDD","cHG","cHGtil","cHW","cHWtil","cHB","cHBtil","cHWB","cHWBtil","ceHAbs","cuHAbs","cdHAbs","ceWAbs","ceBAbs","cuGAbs","cuWAbs","cuBAbs","cdGAbs","cdWAbs","cdBAbs","cHl1","cHl3","cHe","cHq1","cHq3","cHu","cHd","cHudAbs","cll","cll1","cqq1","cqq11","cqq3","cqq31","clq1","clq3","cee","cuu","cuu1","cdd","cdd1","ceu","ced","cud1","cud8","cle","clu","cld","cqe","cqu1","cqu8","cqd1","cqd8","cledqAbs","cquqd1Abs","cquqd8Abs","clequ1Abs","clequ3Abs" };

    const std::vector<std::string> binsNamesLatex = {
      "UNKNOWN",
      "GG2H_FWDH",
      "ggF (200 < p_{T}^{H} < 300 GeV)",
      "ggF (300 < p_{T}^{H} < 450 GeV)",
      "ggF (450 < p_{T}^{H} < 650 GeV)",
      "ggF (p_{T}^{H} > 650 GeV)",
      "ggF (0-jet, p_{T}^{H} < 10 GeV)",
      "ggF (0-jet, p_{T}^{H} > 10 GeV)",
      "ggF (1-jet, p_{T}^{H} < 60 GeV)",
      "ggF (1-jet, 60 < p_{T}^{H} < 120 GeV)",
      "ggF (1-jet, 120 < p_{T}^{H} < 200 GeV)",
      "ggF (#geq2-jet, m_{jj} < 350 GeV, p_{T}^{H} < 60 GeV)",
      "ggF (#geq2-jet, m_{jj} < 350 GeV, 60 < p_{T}^{H} < 120 GeV)",
      "ggF (#geq2-jet, m_{jj} < 350 GeV, 120 < p_{T}^{H} < 200 GeV)",
      "ggF (#geq2-jet, 350 < m_{jj} < 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} < 25 GeV)",
      "ggF (#geq2-jet, 350 < m_{jj} < 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} > 25 GeV)",
      "ggF (#geq2-jet, m_{jj} > 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} < 25 GeV)",
      "ggF (#geq2-jet, m_{jj} > 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} > 25 GeV)",
      "QQ2HQQ_FWDH",
      "qq#rightarrowHqq (0-jet)",
      "qq#rightarrowHqq (1-jet)",
      "qq#rightarrowHqq (#geq2-jet m_{jj} < 60 GeV)",
      "qq#rightarrowHqq (#geq2-jet 60 < m_{jj} < 120 GeV)",
      "qq#rightarrowHqq (#geq2-jet 120 < m_{jj} < 350 GeV)",
      "qq#rightarrowHqq (#geq2-jet m_{jj} > 350 GeV, p_{T}^{H} > 200 GeV)",
      "qq#rightarrowHqq (#geq2-jet 350 < m_{jj} < 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} < 25 GeV)",
      "qq#rightarrowHqq (#geq2-jet 350 < m_{jj} < 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} > 25 GeV)",
      "qq#rightarrowHqq (#geq2-jet m_{jj} > 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} < 25 GeV)",
      "qq#rightarrowHqq (#geq2-jet m_{jj} > 700 GeV, p_{T}^{H} < 200 GeV, p_{T}^{Hjj} > 25 GeV)",
      "QQ2HLNU_FWDH",
      "qq#rightarrowHl#nu (p_{T}^{V} < 75 GeV)",
      "qq#rightarrowHl#nu (75 < p_{T}^{V} < 150 GeV)",
      "qq#rightarrowHl#nu (150 < p_{T}^{V} < 250 GeV, 0-jet)",
      "qq#rightarrowHl#nu (250 < p_{T}^{V} < 250 GeV, #geq1-jet)",
      "qq#rightarrowHl#nu (p_{T}^{V} > 250 GeV)",
      "QQ2HLL_FWDH",
      "qq#rightarrowHll (p_{T}^{V} < 75 GeV)",
      "qq#rightarrowHll (75 < p_{T}^{V} < 150 GeV)",
      "qq#rightarrowHll (150 < p_{T}^{V} < 250 GeV, 0-jet)",
      "qq#rightarrowHll (250 < p_{T}^{V} < 250 GeV, #geq1-jet)",
      "qq#rightarrowHll (p_{T}^{V} > 250 GeV)",
      "GG2HLL_FWDH",
      "gg#rightarrowHll (p_{T}^{V} < 75 GeV)",
      "gg#rightarrowHll (75 < p_{T}^{V} < 150 GeV)",
      "gg#rightarrowHll (150 < p_{T}^{V} < 250 GeV, 0-jet)",
      "gg#rightarrowHll (250 < p_{T}^{V} < 250 GeV, #geq1-jet)",
      "gg#rightarrowHll (p_{T}^{V} > 250 GeV)",
      "TTH_FWDH",
      "ttH (p_{T}^{H} < 60 GeV)",
      "ttH (60 < p_{T}^{H} < 120 GeV)",
      "ttH (120 < p_{T}^{H} < 200 GeV)",
      "ttH (200 < p_{T}^{H} < 300 GeV)",
      "ttH (p_{T}^{H} > 300 GeV)",
      "BBH_FWDH",
      "BBH",
      "TH_FWDH",
      "tH" };


    const std::vector<std::string> mergedBinsName = {
      "GG2H_PTH_200_300",                  // 0
      "GG2H_PTH_300_450",                  // 1
      "GG2H_PTH_GT450",                    // 2
      "GG2H_0J_PTH_0_10",                  // 3
      "GG2H_0J_PTH_GT10",                  // 4
      "GG2H_1J_PTH_0_60",                  // 5
      "GG2H_1J_PTH_60_120",                // 6
      "GG2H_1J_PTH_120_200",               // 7
      "GG2H_GE2J_MJJ_0_350_PTH_0_120",     // 8
      "GG2H_GE2J_MJJ_0_350_PTH_120_200",   // 9
      "GG2H_GE2J_MJJ_GT350_PTHJJ_0_200",   // 10
      "QQ2HQQ_VH_VETO",                    // 11
      "QQ2HQQ_VH_HAD",                     // 12 
      "QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200", // 13  
      "QQ2HQQ_GE2J_MJJ_700_1000_PTH_0_200",// 14   
      "QQ2HQQ_GE2J_MJJ_GT1000_PTH_0_200",  // 15   
      "QQ2HQQ_GE2J_MJJ_350_1000_PTH_GT200",// 16  
      "QQ2HQQ_GE2J_MJJ_GT1000_PTH_GT200",  // 17 
      "QQ2HLNU_PTV_0_150",                 // 18
      "QQ2HLNU_PTV_GT150",                 // 19
      "HLL_PTV_0_150",                     // 20
      "HLL_PTV_GT150",                     // 21
      "TTH_PTH_0_60",                      // 22
      "TTH_PTH_60_120",                    // 23
      "TTH_PTH_120_200",                   // 24
      "TTH_PTH_200_300",                   // 25
      "TTH_PTH_GT300",                     // 26
      "TH",                                // 27
    };

    /*const std::vector<std::string> mergingBins =  {
      "",
      "",
      "GG2H_PTH_200_300",
      "GG2H_PTH_300_450",
      "GG2H_PTH_GT450",
      "GG2H_PTH_GT450",
      "GG2H_0J_PTH_0_10",
      "GG2H_0J_PTH_GT10",
      "GG2H_1J_PTH_0_60",
      "GG2H_1J_PTH_60_120",
      "GG2H_1J_PTH_120_200",
      "GG2H_GE2J_MJJ_0_350_PTH_0_120",
      "GG2H_GE2J_MJJ_0_350_PTH_0_120",
      "GG2H_GE2J_MJJ_0_350_PTH_120_200",
      "GG2H_GE2J_MJJ_GT350_PTH_0_200",
      "GG2H_GE2J_MJJ_GT350_PTH_0_200",
      "GG2H_GE2J_MJJ_GT350_PTH_0_200",
      "GG2H_GE2J_MJJ_GT350_PTH_0_200",
      "",
      "QQ2HQQ_LE1J",
      "QQ2HQQ_LE1J",
      "QQ2HQQ_GE2J_MJJ_0_60_OR_120_350","QQ2HQQ_GE2J_MJJ_60_120","QQ2HQQ_GE2J_MJJ_0_60_OR_120_350","QQ2HQQ_GE2J_MJJ_GT350_PTH_GT200","QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200","QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200","QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200","QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200","","QQ2HLNU_PTV_0_150","QQ2HLNU_PTV_0_150","QQ2HLNU_PTV_GT150","QQ2HLNU_PTV_GT150","QQ2HLNU_PTV_GT150","","ZHLL_PTV_0_150","ZHLL_PTV_0_150","ZHLL_PTV_GT150","ZHLL_PTV_GT150","ZHLL_PTV_GT150","","ZHLL_PTV_0_150","ZHLL_PTV_0_150","ZHLL_PTV_GT150","ZHLL_PTV_GT150","ZHLL_PTV_GT150","","TTH_PTH_0_60","TTH_PTH_60_120","TTH_PTH_120_200","TTH_PTH_GT200","TTH_PTH_GT200","","","","TH"};*/

    const std::map<size_t, size_t> mergingIdxIdx = {
      {    0,  99},
      {    1,  99},
      {    2,  0},
      {    3,  1},
      {    4,  2},
      {    5,  2},
      {    6,  3},
      {    7,  4},
      {    8,  5},
      {    9,  6},
      {   10,  7},
      {   11,  8},
      {   12,  8},
      {   13,  9},
      {   14,  10},
      {   15,  10},
      {   16,  10},
      {   17,  10},
      {   18,  99},
      {   19,  11},
      {   20,  11},
      {   21,  12}, // 
      {   22,  12}, // 
      {   23,  12}, // 
      {   24,  16}, // 16 or 17???
      {   25,  13}, // don't know -
      {   26,  13}, // 
      {   27,  14}, // 14 or 15?
      {   28,  14}, // 14 or 15?
      {   29,  99},
      {   30,  18},
      {   31,  18},
      {   32,  19},
      {   33,  19},
      {   34,  19},
      {   35,  99},
      {   36,  20},
      {   37,  20},
      {   38,  21},
      {   39,  21},
      {   40,  21},
      {   41,  99},
      {   42,  20},
      {   43,  20},
      {   44,  21},
      {   45,  21},
      {   46,  21},
      {   47,  99},
      {   48,  22},
      {   49,  23},
      {   50,  24},
      {   51,  25},
      {   52,  26},
      {   53,  99},
      {   54,  99},
      {   55,  99},
      {   56,  27},
    };

    const std::map<size_t, size_t> mergingFineIdxIdx = {
      //{    0,  999}, // UNKNOWN
      //{    1,  999}, // GG2H_FWDH 
      {    2,  0},  // GG2H_PTH_200_300_PTHJoverPTH_0_15 
      {    3,  1},  // GG2H_PTH_300_450_PTHJoverPTH_0_15 
      {    4,  2},  // GG2H_PTH_450_650_PTHJoverPTH_0_15 
      {    5,  2},  // GG2H_PTH_GT650_PTHJoverPTH_0_15 
      {    6,  0},
      {    7,  1},
      {    8,  2},
      {    9,  2},
      {   10,  3},
      {   11,  4},
      {   12,  5},
      {   13,  6},
      {   14,  7},
      {   15,  8},
      {   16,  8},
      {   17,  9},
      {   18,  8},
      {   19,  8},
      {   20,  9},
      {   21,  10}, // 
      {   22,  10}, // 
      {   23,  10}, // 
      {   24,  10}, // 
      {   25,  10}, // 
      {   26,  10}, // 
      {   27,  10}, // 
      {   28,  10}, // Last GGH
      //{   29,  999}, // 
      {   30,  11},
      {   31,  11},
      {   32,  12},
      {   33,  12},
      {   34,  12},
      {   35,  12},
      {   36,  12},
      {   37,  12},
      {   38,  13},
      {   39,  13},
      {   40,  14},
      {   41,  14},
      {   42,  15},
      {   43,  15},
      {   44,  15},
      {   45,  15},
      {   46,  16},
      {   47,  16},
      {   48,  16},
      {   49,  16},
      {   50,  17},
      {   51,  17},
      {   52,  17},
      {   53,  17}, // last VBF + VH had
      //{   54,  999},
      {   55,  18},
      {   56,  18},
      {   57,  19},
      {   58,  19},
      {   59,  19},
      {   60,  18},
      {   61,  18},
      {   62,  19},
      {   63,  19},
      {   64,  19},
      {   65,  18},
      {   66,  18},
      {   67,  19},
      {   68,  19},
      {   69,  19}, // last WH
      //{   70,  999}, // first ZH
      {   71,  20},
      {   72,  20},
      {   73,  21},
      {   74,  21},
      {   75,  21},
      {   76,  20},
      {   77,  20},
      {   78,  21},
      {   79,  21},
      {   80,  21},
      {   81,  20},
      {   82,  20},
      {   83,  21},
      {   84,  21},
      {   85,  21}, // last ZH
      //{   86,  999}, // first ggZH -> HLL
      {   87,  18},
      {   88,  18},
      {   89,  19},
      {   90,  19},
      {   91,  19},
      {   92,  18},
      {   93,  18},
      {   94,  19},
      {   95,  19},
      {   96,  19},
      {   97,  18},
      {   98,  18},
      {   99,  19},
      {   100, 19},
      {   101, 19}, // last ggZH
      //{   102, 999}, // first ttH
      {   103, 22},
      {   104, 23},
      {   105, 24},
      {   106, 25},
      {   107, 26},
      {   108, 26}, // last ttH
      //{   109, 999}, // bb fwd
      //{   110, 999}, // bb
      //{   111, 999}, // tH fwdh
      {   112, 27}, // tH
    };

    const std::vector<std::string> fineNames {
        "UNKNOWN",
"GG2H_FWDH",
"GG2H_PTH_200_300_PTHJoverPTH_0_15",
"GG2H_PTH_300_450_PTHJoverPTH_0_15",
"GG2H_PTH_450_650_PTHJoverPTH_0_15",
"GG2H_PTH_GT650_PTHJoverPTH_0_15",
"GG2H_PTH_200_300_PTHJoverPTH_GT15",
"GG2H_PTH_300_450_PTHJoverPTH_GT15",
"GG2H_PTH_450_650_PTHJoverPTH_GT15",
"GG2H_PTH_GT650_PTHJoverPTH_GT15",
"GG2H_0J_PTH_0_10",
"GG2H_0J_PTH_GT10",
"GG2H_1J_PTH_0_60",
"GG2H_1J_PTH_60_120",
"GG2H_1J_PTH_120_200",
"GG2H_GE2J_MJJ_0_350_PTH_0_60_PTHJJ_0_25",
"GG2H_GE2J_MJJ_0_350_PTH_60_120_PTHJJ_0_25",
"GG2H_GE2J_MJJ_0_350_PTH_120_200_PTHJJ_0_25",
"GG2H_GE2J_MJJ_0_350_PTH_0_60_PTHJJ_GT25",
"GG2H_GE2J_MJJ_0_350_PTH_60_120_PTHJJ_GT25",
"GG2H_GE2J_MJJ_0_350_PTH_120_200_PTHJJ_GT25",
"GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25",
"GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25",
"GG2H_GE2J_MJJ_700_1000_PTH_0_200_PTHJJ_0_25",
"GG2H_GE2J_MJJ_700_1000_PTH_0_200_PTHJJ_GT25",
"GG2H_GE2J_MJJ_1000_1500_PTH_0_200_PTHJJ_0_25",
"GG2H_GE2J_MJJ_1000_1500_PTH_0_200_PTHJJ_GT25",
"GG2H_GE2J_MJJ_GT1500_PTH_0_200_PTHJJ_0_25",
"GG2H_GE2J_MJJ_GT1500_PTH_0_200_PTHJJ_GT25",
"QQ2HQQ_FWDH",
"QQ2HQQ_0J",
"QQ2HQQ_1J",
"QQ2HQQ_GE2J_MJJ_0_60_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_60_120_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_120_350_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_0_60_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_60_120_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_120_350_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_700_1000_PTH_0_200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_700_1000_PTH_0_200_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_1000_1500_PTH_0_200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_1000_1500_PTH_0_200_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_GT1500_PTH_0_200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_GT1500_PTH_0_200_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_350_700_PTH_GT200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_350_700_PTH_GT200_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_700_1000_PTH_GT200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_700_1000_PTH_GT200_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_1000_1500_PTH_GT200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_1000_1500_PTH_GT200_PTHJJ_GT25",
"QQ2HQQ_GE2J_MJJ_GT1500_PTH_GT200_PTHJJ_0_25",
"QQ2HQQ_GE2J_MJJ_GT1500_PTH_GT200_PTHJJ_GT25",
"QQ2HLNU_FWDH",
"QQ2HLNU_PTV_0_75_0J",
"QQ2HLNU_PTV_75_150_0J",
"QQ2HLNU_PTV_150_250_0J",
"QQ2HLNU_PTV_250_400_0J",
"QQ2HLNU_PTV_GT400_0J",
"QQ2HLNU_PTV_0_75_1J",
"QQ2HLNU_PTV_75_150_1J",
"QQ2HLNU_PTV_150_250_1J",
"QQ2HLNU_PTV_250_400_1J",
"QQ2HLNU_PTV_GT400_1J",
"QQ2HLNU_PTV_0_75_GE2J",
"QQ2HLNU_PTV_75_150_GE2J",
"QQ2HLNU_PTV_150_250_GE2J",
"QQ2HLNU_PTV_250_400_GE2J",
"QQ2HLNU_PTV_GT400_GE2J",
"QQ2HLL_FWDH",
"QQ2HLL_PTV_0_75_0J",
"QQ2HLL_PTV_75_150_0J",
"QQ2HLL_PTV_150_250_0J",
"QQ2HLL_PTV_250_400_0J",
"QQ2HLL_PTV_GT400_0J",
"QQ2HLL_PTV_0_75_1J",
"QQ2HLL_PTV_75_150_1J",
"QQ2HLL_PTV_150_250_1J",
"QQ2HLL_PTV_250_400_1J",
"QQ2HLL_PTV_GT400_1J",
"QQ2HLL_PTV_0_75_GE2J",
"QQ2HLL_PTV_75_150_GE2J",
"QQ2HLL_PTV_150_250_GE2J",
"QQ2HLL_PTV_250_400_GE2J",
"QQ2HLL_PTV_GT400_GE2J",
"GG2HLL_FWDH",
"GG2HLL_PTV_0_75_0J",
"GG2HLL_PTV_75_150_0J",
"GG2HLL_PTV_150_250_0J",
"GG2HLL_PTV_250_400_0J",
"GG2HLL_PTV_GT400_0J",
"GG2HLL_PTV_0_75_1J",
"GG2HLL_PTV_75_150_1J",
"GG2HLL_PTV_150_250_1J",
"GG2HLL_PTV_250_400_1J",
"GG2HLL_PTV_GT400_1J",
"GG2HLL_PTV_0_75_GE2J",
"GG2HLL_PTV_75_150_GE2J",
"GG2HLL_PTV_150_250_GE2J",
"GG2HLL_PTV_250_400_GE2J",
"GG2HLL_PTV_GT400_GE2J",
"TTH_FWDH",
"TTH_PTH_0_60",
"TTH_PTH_60_120",
"TTH_PTH_120_200",
"TTH_PTH_200_300",
"TTH_PTH_300_450",
"TTH_PTH_GT450",
"BBH_FWDH",
"BBH",
"TH_FWDH",
"TH",
};

const std::vector<std::string> coarseNames {
      "UNKNOWN",
      "GG2H_FWDH",
      "GG2H_PTH_200_300",
      "GG2H_PTH_300_450",
      "GG2H_PTH_450_650",
      "GG2H_PTH_GT650",
      "GG2H_0J_PTH_0_10",
      "GG2H_0J_PTH_GT10",
      "GG2H_1J_PTH_0_60",
      "GG2H_1J_PTH_60_120",
      "GG2H_1J_PTH_120_200",
      "GG2H_GE2J_MJJ_0_350_PTH_0_60",
      "GG2H_GE2J_MJJ_0_350_PTH_60_120",
      "GG2H_GE2J_MJJ_0_350_PTH_120_200",
      "GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25",
      "GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25",
      "GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25",
      "GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25",
      "QQ2HQQ_FWDH",
      "QQ2HQQ_0J",
      "QQ2HQQ_1J",
      "QQ2HQQ_GE2J_MJJ_0_60",
      "QQ2HQQ_GE2J_MJJ_60_120",
      "QQ2HQQ_GE2J_MJJ_120_350",
      "QQ2HQQ_GE2J_MJJ_GT350_PTH_GT200",
      "QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25",
      "QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25",
      "QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25",
      "QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25",
      "QQ2HLNU_FWDH",
      "QQ2HLNU_PTV_0_75",
      "QQ2HLNU_PTV_75_150",
      "QQ2HLNU_PTV_150_250_0J",
      "QQ2HLNU_PTV_150_250_GE1J",
      "QQ2HLNU_PTV_GT250",
      "QQ2HLL_FWDH",
      "QQ2HLL_PTV_0_75",
      "QQ2HLL_PTV_75_150",
      "QQ2HLL_PTV_150_250_0J",
      "QQ2HLL_PTV_150_250_GE1J",
      "QQ2HLL_PTV_GT250",
      "GG2HLL_FWDH",
      "GG2HLL_PTV_0_75",
      "GG2HLL_PTV_75_150",
      "GG2HLL_PTV_150_250_0J",
      "GG2HLL_PTV_150_250_GE1J",
      "GG2HLL_PTV_GT250",    
      "TTH_FWDH", 
      "TTH_PTH_0_60",
      "TTH_PTH_60_120",
      "TTH_PTH_120_200",
      "TTH_PTH_200_300",
      "TTH_PTH_GT300",
      "BBH_FWDH", 
      "BBH",  
      "TH_FWDH",
      "TH"
};

    // class WilsonCoefficient {
    // private:
    //     string name_;
    //     string latexName_;

    // public:
    // }
} // namespace eft

#pragma once

#endif // !WILSON_COEF_H
