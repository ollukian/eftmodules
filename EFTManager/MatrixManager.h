#pragma once

#ifndef MATRIX_MANAGER_H
#define MATRIX_MANAGER_H

#include "../Utils/profile.h"
#include "../Utils/test_runner.h"

#include <memory>

#include <string>
#include <map>
#include <set>
#include <vector>

//#pragma warning(push, 0)
//#ifndef	_MSC_VER
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TFile.h"
#include "TCanvas.h"

#include "TH2D.h"
#include "TH1D.h"
//#endif
//#pragma warning(pop)

using std::vector;
using std::map;

class MatrixManager;

//#ifndef	_MSC_VER
class MatrixManager {
public:
    static inline TMatrixD GetInverted(const TMatrixD& mat) { 
        auto inv = (TMatrixD*) mat.Clone();
        return move(inv->Invert());
        //return move(mat.Invert());  
    }

    static inline TMatrixD Invert(TMatrixD& mat) { 
        LOG_DURATION("Invert");
        return move(mat.Invert());  
    }

    static inline TMatrixD Transpose(TMatrixD& mat) {
        LOG_DURATION("Transpose");
        return move(mat.Transpose(mat));
    }
    static inline TMatrixD GetTransposed(TMatrixD& mat) {
        LOG_DURATION("GetTransposed");
        auto tr = (TMatrixD*) mat.Clone();
        return move(tr->Transpose(*tr));
    }
    static inline shared_ptr<TMatrixD> GetMatrix(const string& filename, const string& mname) {
        TFile f(filename.c_str());
        return make_shared<TMatrixD>(*f.Get<TMatrixD>(mname.c_str()));
    }
    static inline TMatrixD ChangeBasis(const TMatrixD& source, const TMatrixD& transMat) {
        LOG_DURATION("ChangeBasis");
        const TMatrixD transMat_t = GetTransposed(const_cast<TMatrixD&>(transMat));
        cout << "Size of transmat: " << transMat.GetNrows() << " x " <<  transMat.GetNcols() << endl;
        cout << "Size of transmat_t: "  << transMat_t.GetNrows()  << " x " <<  transMat_t.GetNcols()  << endl;
        cout << "Size of source: "    << source.GetNrows()   << " x " << source.GetNcols()    << endl;
    

        MatrixManager::DrawMatrix(transMat,    "figures/transMat.pdf", "colz text");
        MatrixManager::DrawMatrix(transMat_t,  "figures/transMat_t.pdf", "colz text");
        MatrixManager::DrawMatrix(source,      "figures/source.pdf", "colz text");
 
        auto res = transMat * (source) * (transMat_t);
        MatrixManager::DrawMatrix(res,   "figures/res.pdf", "colz text");
        return res;
    }

    static inline void GetEigenVecsVals(const TMatrixD& mat, TMatrixD& vecs, TVectorD& vals);

    //static TMatrixD MakeTMatrixD(const vector<vector<double>>& mat);

    template<typename Tv>
    static TMatrixD MakeTMatrixD(const vector<vector<Tv>>& mat);

    template<typename Tx, typename Ty, typename Tv>
    static TMatrixD MakeTMatrixD(const map<Tx, map<Ty, Tv>>& mat, vector<Tx>& x, vector<Ty>& y);
    //static vector<vector<double>> Map2DToVec(const map<size_t, map<size_t, double>>& mat);

    template<typename Tx, typename Ty, typename Tv>
    static vector<vector<Tv>> Map2DToVec(const map<Tx, map<Ty, Tv>>& mat, vector<Tx>& x, vector<Ty>& y);

    static TH2D     MakeHistFromTMatrixD(TMatrixD& mat);

    static inline void DrawMatrix(TMatrixD& mat, const string& path, const char* opt = "");
    static inline void DrawMatrix(const TMatrixD& mat, const string& path, const char* opt = "") {
         DrawMatrix(const_cast<TMatrixD&>(mat), path, opt);
    }
    static void        DrawMatrix(TH1* mat, const string& path, const char* opt = "");
private:
    //vector<vector<double>> transMatrix_;
};
//////////////////////////////////////////////////////////////////////////////////////
template<typename Tx, typename Ty, typename Tv>
vector<vector<Tv>> MatrixManager::Map2DToVec(const map<Tx, map<Ty, Tv>>& mat, vector<Tx>& x, vector<Ty>& y) {
    LOG_DURATION("Map2DToVec");
    size_t nbRows = mat.size();
    size_t nbCols = mat.begin()->second.size();
    cout << "nb cols = " << nbCols << ", nbRows = " << nbRows << endl;
    vector<vector<Tv>> res(nbRows, vector<Tv>(nbCols, 0.));
    size_t idx_row = 0;
    size_t idx_col = 0;
    Tv val = 0.;
    for (const auto& key_row : mat) {
        idx_col = 0;
        x.push_back(key_row.first);
        for (const auto& key_elem : key_row.second) {
            val = key_elem.second;
            cout << "[" << idx_row << "][" << idx_col << "] -> " << val << endl;
            res[idx_row][idx_col++] = val;
        }
        idx_row++;
    }

    Tx exampleName = x[0];
    // fill vector of x-names:
    for (const auto& key_elem : mat.at(exampleName)) {
        y.push_back(key_elem.first);
    }

    cout << "[Map2DToVec]{Results of conversion} Names: " << endl;
    cout << "\tx:" << endl;
    for (const auto& elem : x) cout << "\t\t" << elem << endl;
    cout << "\ty:" << endl;
    for (const auto& elem : y) cout << "\t\t" << elem << endl;
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
template<typename Tv>
TMatrixD MatrixManager::MakeTMatrixD(const vector<vector<Tv>>& mat)
{
    LOG_DURATION("MakeTMatrixD");

    size_t nbRows = mat.size();
    size_t nbCols = mat.front().size();
    cout << "make TMatrixD: " << nbRows << " x " << nbCols << endl;
    TMatrixD res(nbRows, nbCols);
    size_t idx_row = 0;
    size_t idx_col = 0;

    Tv val;

    for (const auto& row : mat) {
        idx_col = 0;
        for (const auto& elem : row) {
            val = elem;
            res(idx_row, idx_col++) = val;
            cout << "set [" << setw(2) << idx_row << "][" 
                            << setw(2) << idx_col << "] to " 
                            << val << endl; 
        }  
         idx_row++;  
    }
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////
template<typename Tx, typename Ty, typename Tv>
TMatrixD MatrixManager::MakeTMatrixD(const map<Tx, map<Ty, Tv>>& mat, 
                                     vector<Tx>& xNames, 
                                     vector<Ty>& yNames
                                     ) 
{
    return move(MakeTMatrixD<Tv>(Map2DToVec(mat, xNames, yNames)));
}
//////////////////////////////////////////////////////////////////////////////////////
inline void MatrixManager::GetEigenVecsVals(const TMatrixD& mat, 
                                            TMatrixD& vecs, 
                                            TVectorD& vals
                                            ) 
{
    LOG_DURATION("GetEigenVecsVals");
    cout << "[GetEigenVecsVals] for the matrix: " << mat.GetNrows() 
       << " x " << mat.GetNcols() << endl;
    vecs.ResizeTo(mat.GetNrows(), mat.GetNcols());
    vals.ResizeTo(mat.GetNrows());
    vecs = mat.EigenVectors(vals);
    cout << "[GetEigenVecsVals] found: " << vecs.GetNrows() << " vecs" << endl;
}
//////////////////////////////////////////////////////////////////////////////////////
inline void MatrixManager::DrawMatrix(TH1* mat, const string& path, const char* opt) {
    LOG_DURATION("DrawMatrix");
    TCanvas c("c", "c", 1200, 800);
    c.SetGrid();

    mat->Draw(opt);
    c.SaveAs(path.c_str());
}
//////////////////////////////////////////////////////////////////////////////////////
inline void MatrixManager::DrawMatrix(TMatrixD& mat, const string& path, const char* opt) { 
    LOG_DURATION("DrawMatrix");
    TCanvas c("c", "c", 1200, 800);
    c.SetGrid();

    mat.Draw(opt);
    c.SaveAs(path.c_str());
}
//////////////////////////////////////////////////////////////////////////////////////
//#endif // if not MSVS

#endif
