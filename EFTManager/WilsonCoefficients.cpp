//#include "WilsconCoefficients.h"
#include "../EFTManager/WilsonCoefficients.h"

namespace eft {



bool operator < (const WilsonCoefficient& l, const WilsonCoefficient& r) {
	return l.GetId() < r.GetId();
}

std::ostream& operator << (std::ostream& os, const Regime reg) {
    if (reg == Regime::PRODUCTION)
        os << "Production";
    else if (reg == Regime::DECAY)
        os << "Decay";
    else
        os << "Altogether";
    return os;
}

std::ostream& operator << (std::ostream& os, const ProdMode mode) {
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch (mode) {
        PROCESS_VAL(ProdMode::TOTAL);
        PROCESS_VAL(ProdMode::GGF);
        PROCESS_VAL(ProdMode::VBF);
        PROCESS_VAL(ProdMode::ZH);
        PROCESS_VAL(ProdMode::WH);
        PROCESS_VAL(ProdMode::TTH);
        PROCESS_VAL(ProdMode::THJB);
        PROCESS_VAL(ProdMode::THW);
        PROCESS_VAL(ProdMode::BBH);
        PROCESS_VAL(ProdMode::NONE);
        PROCESS_VAL(ProdMode::QQ2ZH);
        PROCESS_VAL(ProdMode::GG2ZH);
        default: throw std::runtime_error("[WilsonCoefficints.cpp] an attempt to use an unknown prodmode");
    }
#undef PROCESS_VAL

    return os << s;
}

std::ostream& operator << (std::ostream& os, const WCvalue val) {
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch (val) {
        PROCESS_VAL(WCvalue::TOTAL);
        PROCESS_VAL(WCvalue::SM);
        PROCESS_VAL(WCvalue::ONE);
        PROCESS_VAL(WCvalue::DOTONE);
        PROCESS_VAL(WCvalue::MINHALF);
        PROCESS_VAL(WCvalue::NONE);
        PROCESS_VAL(WCvalue::AVERAGED);
    default: throw std::runtime_error("an attempt to use unknown wcvaue");
    }
#undef PROCESS_VAL

    return os << s;
}
std::ostream& operator << (std::ostream& os, const DecayChannel val) {
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch (val) {
        PROCESS_VAL(DecayChannel::TOTAL);
        PROCESS_VAL(DecayChannel::L_NU_L_NU);
        PROCESS_VAL(DecayChannel::TAU_NU_TAU_NU);
        PROCESS_VAL(DecayChannel::L_NU_TAU_NU);
        PROCESS_VAL(DecayChannel::L_NU_J_J);
        PROCESS_VAL(DecayChannel::QQQQ);
        PROCESS_VAL(DecayChannel::LLLL);
        PROCESS_VAL(DecayChannel::TAU_TAU_TAU_TAU);
        PROCESS_VAL(DecayChannel::NU_NU_NU_NU);
        PROCESS_VAL(DecayChannel::L_L_TAU_TAU);
        PROCESS_VAL(DecayChannel::NU_NU_TAU_TAU);
        PROCESS_VAL(DecayChannel::J_J_L_L);
        PROCESS_VAL(DecayChannel::J_J_NU_NU);
        PROCESS_VAL(DecayChannel::GAM_GAM);
        PROCESS_VAL(DecayChannel::B_B);
        PROCESS_VAL(DecayChannel::C_C);
        PROCESS_VAL(DecayChannel::S_S);
        PROCESS_VAL(DecayChannel::E_E);
        PROCESS_VAL(DecayChannel::MU_MU);
        PROCESS_VAL(DecayChannel::TAU_TAU);
        PROCESS_VAL(DecayChannel::Z_GAM);
        PROCESS_VAL(DecayChannel::G_G);
        PROCESS_VAL(DecayChannel::NONE);
    default: throw std::runtime_error("an attempt to use unknown decay channel");
    }
#undef PROCESS_VAL

    return os << s;
}

std::ostream& operator << (std::ostream& os, const WilsonCoefficientsCode& val) {
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch (val) {
        PROCESS_VAL(WilsonCoefficientsCode::TOTAL);
        PROCESS_VAL(WilsonCoefficientsCode::NONE);
        PROCESS_VAL(WilsonCoefficientsCode::SM);
        PROCESS_VAL(WilsonCoefficientsCode::CG);
        PROCESS_VAL(WilsonCoefficientsCode::CW);
        PROCESS_VAL(WilsonCoefficientsCode::CH);
        PROCESS_VAL(WilsonCoefficientsCode::CHBOX);
        PROCESS_VAL(WilsonCoefficientsCode::CHDD);
        PROCESS_VAL(WilsonCoefficientsCode::CHG);
        PROCESS_VAL(WilsonCoefficientsCode::CHW);
        PROCESS_VAL(WilsonCoefficientsCode::CHB);
        PROCESS_VAL(WilsonCoefficientsCode::CHWB);
        PROCESS_VAL(WilsonCoefficientsCode::CUHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CTHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CBHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CUGRE);
        PROCESS_VAL(WilsonCoefficientsCode::CTGRE);
        PROCESS_VAL(WilsonCoefficientsCode::CUWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CTWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CUBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CTBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDGRE);
        PROCESS_VAL(WilsonCoefficientsCode::CBGRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CBWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CBBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CHJ1);
        PROCESS_VAL(WilsonCoefficientsCode::CHQ1);
        PROCESS_VAL(WilsonCoefficientsCode::CHJ3);
        PROCESS_VAL(WilsonCoefficientsCode::CHQ3);
        PROCESS_VAL(WilsonCoefficientsCode::CHU);
        PROCESS_VAL(WilsonCoefficientsCode::CHT);
        PROCESS_VAL(WilsonCoefficientsCode::CHD);
        PROCESS_VAL(WilsonCoefficientsCode::CHBQ);
        PROCESS_VAL(WilsonCoefficientsCode::CHUDRE);
        PROCESS_VAL(WilsonCoefficientsCode::CHTBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CJJ11);
        PROCESS_VAL(WilsonCoefficientsCode::CJJ18);
        PROCESS_VAL(WilsonCoefficientsCode::CJJ31);
        PROCESS_VAL(WilsonCoefficientsCode::CJJ38);
        PROCESS_VAL(WilsonCoefficientsCode::CQJ11);
        PROCESS_VAL(WilsonCoefficientsCode::CQJ18);
        PROCESS_VAL(WilsonCoefficientsCode::CQJ31);
        PROCESS_VAL(WilsonCoefficientsCode::CQJ38);
        PROCESS_VAL(WilsonCoefficientsCode::CQQ1);
        PROCESS_VAL(WilsonCoefficientsCode::CQQ8);
        PROCESS_VAL(WilsonCoefficientsCode::CUU1);
        PROCESS_VAL(WilsonCoefficientsCode::CUU8);
        PROCESS_VAL(WilsonCoefficientsCode::CTT);
        PROCESS_VAL(WilsonCoefficientsCode::CTU1);
        PROCESS_VAL(WilsonCoefficientsCode::CTU8);
        PROCESS_VAL(WilsonCoefficientsCode::CDD1);
        PROCESS_VAL(WilsonCoefficientsCode::CDD8);
        PROCESS_VAL(WilsonCoefficientsCode::CBB);
        PROCESS_VAL(WilsonCoefficientsCode::CBD1);
        PROCESS_VAL(WilsonCoefficientsCode::CBD8);
        PROCESS_VAL(WilsonCoefficientsCode::CUD1);
        PROCESS_VAL(WilsonCoefficientsCode::CTB1);
        PROCESS_VAL(WilsonCoefficientsCode::CTD1);
        PROCESS_VAL(WilsonCoefficientsCode::CBU1);
        PROCESS_VAL(WilsonCoefficientsCode::CUD8);
        PROCESS_VAL(WilsonCoefficientsCode::CTB8);
        PROCESS_VAL(WilsonCoefficientsCode::CTD8);
        PROCESS_VAL(WilsonCoefficientsCode::CBU8);
        PROCESS_VAL(WilsonCoefficientsCode::CJU1);
        PROCESS_VAL(WilsonCoefficientsCode::CQU1);
        PROCESS_VAL(WilsonCoefficientsCode::CJU8);
        PROCESS_VAL(WilsonCoefficientsCode::CQU8);
        PROCESS_VAL(WilsonCoefficientsCode::CTJ1);
        PROCESS_VAL(WilsonCoefficientsCode::CTJ8);
        PROCESS_VAL(WilsonCoefficientsCode::CQT1);
        PROCESS_VAL(WilsonCoefficientsCode::CQT8);
        PROCESS_VAL(WilsonCoefficientsCode::CJD1);
        PROCESS_VAL(WilsonCoefficientsCode::CJD8);
        PROCESS_VAL(WilsonCoefficientsCode::CQD1);
        PROCESS_VAL(WilsonCoefficientsCode::CQD8);
        PROCESS_VAL(WilsonCoefficientsCode::CBJ1);
        PROCESS_VAL(WilsonCoefficientsCode::CBJ8);
        PROCESS_VAL(WilsonCoefficientsCode::CQB1);
        PROCESS_VAL(WilsonCoefficientsCode::CQB8);
        PROCESS_VAL(WilsonCoefficientsCode::CEHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CEWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CEBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CHL1);
        PROCESS_VAL(WilsonCoefficientsCode::CHL3);
        PROCESS_VAL(WilsonCoefficientsCode::CHE);
        PROCESS_VAL(WilsonCoefficientsCode::CLL);
        PROCESS_VAL(WilsonCoefficientsCode::CLL1);
        PROCESS_VAL(WilsonCoefficientsCode::CLJ1);
        PROCESS_VAL(WilsonCoefficientsCode::CLJ3);
        PROCESS_VAL(WilsonCoefficientsCode::CQL1);
        PROCESS_VAL(WilsonCoefficientsCode::CQL3);
        PROCESS_VAL(WilsonCoefficientsCode::CEE);
        PROCESS_VAL(WilsonCoefficientsCode::CEU);
        PROCESS_VAL(WilsonCoefficientsCode::CTE);
        PROCESS_VAL(WilsonCoefficientsCode::CED);
        PROCESS_VAL(WilsonCoefficientsCode::CBE);
        PROCESS_VAL(WilsonCoefficientsCode::CJE);
        PROCESS_VAL(WilsonCoefficientsCode::CQE);
        PROCESS_VAL(WilsonCoefficientsCode::CLU);
        PROCESS_VAL(WilsonCoefficientsCode::CTL);
        PROCESS_VAL(WilsonCoefficientsCode::CLD);
        PROCESS_VAL(WilsonCoefficientsCode::CBL);
        PROCESS_VAL(WilsonCoefficientsCode::CLE);
    default: throw std::runtime_error("an attempt to use unknown wc");
    }
#undef PROCESS_VAL
    return os << s;
}

/*std::ostream& operator << (std::ostream& os, const WilsonCoefficientsCode& val) {
    const char* s = 0;

#define PROCESS_VAL(p) case(p): s = #p; break;
    switch (val) {
        PROCESS_VAL(WilsonCoefficientsCode::TOTAL);
        PROCESS_VAL(WilsonCoefficientsCode::NONE);
        PROCESS_VAL(WilsonCoefficientsCode::SM);
        PROCESS_VAL(WilsonCoefficientsCode::CG);
        PROCESS_VAL(WilsonCoefficientsCode::cGtil);
        PROCESS_VAL(WilsonCoefficientsCode::CW);
        PROCESS_VAL(WilsonCoefficientsCode::cWtil);
        PROCESS_VAL(WilsonCoefficientsCode::CH);
        PROCESS_VAL(WilsonCoefficientsCode::CHBOX);
        PROCESS_VAL(WilsonCoefficientsCode::CHDD);
        PROCESS_VAL(WilsonCoefficientsCode::CHG);
        PROCESS_VAL(WilsonCoefficientsCode::cHGtil);
        PROCESS_VAL(WilsonCoefficientsCode::CHW);
        PROCESS_VAL(WilsonCoefficientsCode::CHWTIL);
        PROCESS_VAL(WilsonCoefficientsCode::CHB);
        PROCESS_VAL(WilsonCoefficientsCode::CHBTIL);
        PROCESS_VAL(WilsonCoefficientsCode::CHWB);
        PROCESS_VAL(WilsonCoefficientsCode::CHWBTIL);
        PROCESS_VAL(WilsonCoefficientsCode::CEHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CUHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDHRE);
        PROCESS_VAL(WilsonCoefficientsCode::CEWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CEBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CUGRE);
        PROCESS_VAL(WilsonCoefficientsCode::CUWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CUBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDGRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDWRE);
        PROCESS_VAL(WilsonCoefficientsCode::CDBRE);
        PROCESS_VAL(WilsonCoefficientsCode::CHL1);
        PROCESS_VAL(WilsonCoefficientsCode::CHL3);
        PROCESS_VAL(WilsonCoefficientsCode::CHE);
        PROCESS_VAL(WilsonCoefficientsCode::CHQ1);
        PROCESS_VAL(WilsonCoefficientsCode::CHQ3);
        PROCESS_VAL(WilsonCoefficientsCode::CHU);
        PROCESS_VAL(WilsonCoefficientsCode::CHD);
        PROCESS_VAL(WilsonCoefficientsCode::CHUDRE);
        PROCESS_VAL(WilsonCoefficientsCode::CL1);
        PROCESS_VAL(WilsonCoefficientsCode::CLL1);
        PROCESS_VAL(WilsonCoefficientsCode::CQQ1);
        PROCESS_VAL(WilsonCoefficientsCode::CQQ11);
        PROCESS_VAL(WilsonCoefficientsCode::CQQ3);
        PROCESS_VAL(WilsonCoefficientsCode::CQQ31);
        PROCESS_VAL(WilsonCoefficientsCode::CLQ1);
        PROCESS_VAL(WilsonCoefficientsCode::CLQ3);
        PROCESS_VAL(WilsonCoefficientsCode::CEE);
        PROCESS_VAL(WilsonCoefficientsCode::CUU);
        PROCESS_VAL(WilsonCoefficientsCode::CUU1);
        PROCESS_VAL(WilsonCoefficientsCode::CDD);
        PROCESS_VAL(WilsonCoefficientsCode::CDD1);
        PROCESS_VAL(WilsonCoefficientsCode::CEU);
        PROCESS_VAL(WilsonCoefficientsCode::CED);
        PROCESS_VAL(WilsonCoefficientsCode::CUD1);
        PROCESS_VAL(WilsonCoefficientsCode::CUD8);
        PROCESS_VAL(WilsonCoefficientsCode::CLE);
        PROCESS_VAL(WilsonCoefficientsCode::CLU);
        PROCESS_VAL(WilsonCoefficientsCode::CLD);
        PROCESS_VAL(WilsonCoefficientsCode::CQE);
        PROCESS_VAL(WilsonCoefficientsCode::CQU1);
        PROCESS_VAL(WilsonCoefficientsCode::CQU8);
        PROCESS_VAL(WilsonCoefficientsCode::CDQ1);
        PROCESS_VAL(WilsonCoefficientsCode::CDQ8);
        PROCESS_VAL(WilsonCoefficientsCode::CLEDQRE);
        PROCESS_VAL(WilsonCoefficientsCode::CQUQD1RE);
        PROCESS_VAL(WilsonCoefficientsCode::CQUQD11RE);
        PROCESS_VAL(WilsonCoefficientsCode::CQUQD8RE);
        PROCESS_VAL(WilsonCoefficientsCode::CQUQD81RE);
        PROCESS_VAL(WilsonCoefficientsCode::CLEQU1RE);
        PROCESS_VAL(WilsonCoefficientsCode::CLEQU3RE);
    default: throw std::runtime_error("an attempt to use unknown wc");
    }
#undef PROCESS_VAL

    return os << s;
}*/

std::ostream& operator << (std::ostream& os, const TheoryRegime val) {
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch (val) {
        PROCESS_VAL(TheoryRegime::SM);
        PROCESS_VAL(TheoryRegime::INT);
        PROCESS_VAL(TheoryRegime::BSM);
        PROCESS_VAL(TheoryRegime::NOT_DEFINED);
    default: throw std::runtime_error("an attempt to use unknown theory regime");
    }
#undef PROCESS_VAL

    return os << s;
}

std::ostream& operator << (std::ostream& os, const WilsonCoefficient& wc) {
    return os << wc.GetId();
}

WilsonCoefficientsCode strToWC(const std::string& wc) {
    //WilsonCoefficientsCode res = WilsonCoefficientsCode::NONE;
    std::string wcToUse = wc;
    if (wc.find("::") != std::string::npos)
        wcToUse = wc.substr(wc.find("::") + 2, wc.length());

    static std::map<std::string, WilsonCoefficientsCode> __tableWCperName;
#define PROCESS_CASE(__wc) __tableWCperName[#__wc] = WilsonCoefficientsCode::__wc;
    PROCESS_CASE(TOTAL);
    PROCESS_CASE(NONE)
    PROCESS_CASE(SM)
    PROCESS_CASE(CG);
    PROCESS_CASE(CW);
    PROCESS_CASE(CH);
    PROCESS_CASE(CHBOX);
    PROCESS_CASE(CHDD);
    PROCESS_CASE(CHG);
    PROCESS_CASE(CHW);
    PROCESS_CASE(CHB);
    PROCESS_CASE(CHWB);
    PROCESS_CASE(CUHRE);
    PROCESS_CASE(CTHRE);
    PROCESS_CASE(CDHRE);
    PROCESS_CASE(CBHRE);
    PROCESS_CASE(CUGRE);
    PROCESS_CASE(CTGRE);
    PROCESS_CASE(CUWRE);
    PROCESS_CASE(CTWRE);
    PROCESS_CASE(CUBRE);
    PROCESS_CASE(CTBRE);
    PROCESS_CASE(CDGRE);
    PROCESS_CASE(CBGRE);
    PROCESS_CASE(CDWRE);
    PROCESS_CASE(CBWRE);
    PROCESS_CASE(CDBRE);
    PROCESS_CASE(CBBRE);
    PROCESS_CASE(CHJ1);
    PROCESS_CASE(CHQ1);
    PROCESS_CASE(CHJ3);
    PROCESS_CASE(CHQ3);
    PROCESS_CASE(CHU);
    PROCESS_CASE(CHT);
    PROCESS_CASE(CHD);
    PROCESS_CASE(CHBQ);
    PROCESS_CASE(CHUDRE);
    PROCESS_CASE(CHTBRE);
    PROCESS_CASE(CJJ11);
    PROCESS_CASE(CJJ18);
    PROCESS_CASE(CJJ31);
    PROCESS_CASE(CJJ38);
    PROCESS_CASE(CQJ11);
    PROCESS_CASE(CQJ18);
    PROCESS_CASE(CQJ31);
    PROCESS_CASE(CQJ38);
    PROCESS_CASE(CQQ1);
    PROCESS_CASE(CQQ8);
    PROCESS_CASE(CUU1);
    PROCESS_CASE(CUU8);
    PROCESS_CASE(CTT);
    PROCESS_CASE(CTU1);
    PROCESS_CASE(CTU8);
    PROCESS_CASE(CDD1);
    PROCESS_CASE(CDD8);
    PROCESS_CASE(CBB);
    PROCESS_CASE(CBD1);
    PROCESS_CASE(CBD8);
    PROCESS_CASE(CUD1);
    PROCESS_CASE(CTB1);
    PROCESS_CASE(CTD1);
    PROCESS_CASE(CBU1);
    PROCESS_CASE(CUD8);
    PROCESS_CASE(CTB8);
    PROCESS_CASE(CTD8);
    PROCESS_CASE(CBU8);
    PROCESS_CASE(CJU1);
    PROCESS_CASE(CQU1);
    PROCESS_CASE(CJU8);
    PROCESS_CASE(CQU8);
    PROCESS_CASE(CTJ1);
    PROCESS_CASE(CTJ8);
    PROCESS_CASE(CQT1);
    PROCESS_CASE(CQT8);
    PROCESS_CASE(CJD1);
    PROCESS_CASE(CJD8);
    PROCESS_CASE(CQD1);
    PROCESS_CASE(CQD8);
    PROCESS_CASE(CBJ1);
    PROCESS_CASE(CBJ8);
    PROCESS_CASE(CQB1);
    PROCESS_CASE(CQB8);
    PROCESS_CASE(CEHRE);
    PROCESS_CASE(CEWRE);
    PROCESS_CASE(CEBRE);
    PROCESS_CASE(CHL1);
    PROCESS_CASE(CHL3);
    PROCESS_CASE(CHE);
    PROCESS_CASE(CLL);
    PROCESS_CASE(CLL1);
    PROCESS_CASE(CLJ1);
    PROCESS_CASE(CLJ3);
    PROCESS_CASE(CQL1);
    PROCESS_CASE(CQL3);
    PROCESS_CASE(CEE);
    PROCESS_CASE(CEU);
    PROCESS_CASE(CTE);
    PROCESS_CASE(CED);
    PROCESS_CASE(CBE);
    PROCESS_CASE(CJE);
    PROCESS_CASE(CQE);
    PROCESS_CASE(CLU);
    PROCESS_CASE(CTL);
    PROCESS_CASE(CLD);
    PROCESS_CASE(CBL);
    PROCESS_CASE(CLE);
#undef PROCESS_CASE
    if (__tableWCperName.count(wcToUse) < 1)
        throw std::runtime_error("Impossible to convert: [" + wc
            + "] to a given Wilson coefficient code. This code is not present");

    return __tableWCperName.at(wcToUse);
}

  DecayChannel strToChan(const std::string& chan) {
    //DecayChannel res = DecayChannel::NONE;
    std::string chanToUse = chan;
    if (chan.find("::") != std::string::npos)
        chanToUse = chan.substr(chan.find("::") + 2, chan.length());

    static std::map<std::string, DecayChannel> __tableChanPerName;
#define PROCESS_CASE(__wc) __tableChanPerName[#__wc] = DecayChannel::__wc;
    PROCESS_CASE(TOTAL);
        PROCESS_CASE(L_NU_L_NU);
        PROCESS_CASE(TAU_NU_TAU_NU);
        PROCESS_CASE(L_NU_TAU_NU);
        PROCESS_CASE(L_NU_J_J);
        PROCESS_CASE(QQQQ);
        PROCESS_CASE(LLLL);
        PROCESS_CASE(TAU_TAU_TAU_TAU);
        PROCESS_CASE(NU_NU_NU_NU);
        PROCESS_CASE(L_L_TAU_TAU);
        PROCESS_CASE(NU_NU_TAU_TAU);
        PROCESS_CASE(J_J_L_L);
        PROCESS_CASE(J_J_NU_NU);
        PROCESS_CASE(GAM_GAM);
        PROCESS_CASE(B_B);
        PROCESS_CASE(C_C);
        PROCESS_CASE(S_S);
        PROCESS_CASE(E_E);
        PROCESS_CASE(MU_MU);
        PROCESS_CASE(TAU_TAU);
        PROCESS_CASE(Z_GAM);
        PROCESS_CASE(G_G);
        PROCESS_CASE(NONE);
	#undef PROCESS_CASE
    if (__tableChanPerName.count(chanToUse) < 1)
      throw std::runtime_error(std::string("Impossible to convert: [") + chan
      + std::string("] to a given Decay Channel. This string is not present"));

    return __tableChanPerName.at(chanToUse);
  }

/*WilsonCoefficientsCode strToWC(const std::string& wc) {
    WilsonCoefficientsCode res = WilsonCoefficientsCode::NONE;

    std::string wcToUse = wc;

    if (wc.find("::") != std::string::npos)
        wcToUse = wc.substr(wc.find("::") + 2, wc.length());

#define PPCAT_NX(A, B) A ## B
#define PPCAT(A, B) PPCAT_NX(A, B)
#define STRINGIZE_NX(A) #A
#define STRINGIZE(A) STRINGIZE_NX(A)

    static std::map<std::string, WilsonCoefficientsCode> __tableWCperName;
#define PROCESS_CASE(__wc) __tableWCperName[#__wc] = WilsonCoefficientsCode::__wc;
    //__tableWCperName[STRINGIZE(PPCAT(WilsonCoefficientsCode::, __wc))] = WilsonCoefficientsCode::__wc;

    PROCESS_CASE(TOTAL);
    PROCESS_CASE(NONE);
    PROCESS_CASE(SM);
    PROCESS_CASE(CG);
    PROCESS_CASE(cGtil);
    PROCESS_CASE(CW); 
    PROCESS_CASE(cWtil);
    PROCESS_CASE(CH);
    PROCESS_CASE(CHBOX);
    PROCESS_CASE(CHDD);
    PROCESS_CASE(CHG);
    PROCESS_CASE(cHGtil);
    PROCESS_CASE(CHW);
    PROCESS_CASE(CHWTIL);
    PROCESS_CASE(CHB);
    PROCESS_CASE(CHBTIL);
    PROCESS_CASE(CHWB);
    PROCESS_CASE(CHWBTIL);
    PROCESS_CASE(CEHRE);
    PROCESS_CASE(CUHRE);
    PROCESS_CASE(CDHRE);
    PROCESS_CASE(CEWRE);
    PROCESS_CASE(CEBRE);
    PROCESS_CASE(CUGRE);
    PROCESS_CASE(CUWRE);
    PROCESS_CASE(CUBRE);
    PROCESS_CASE(CDGRE);
    PROCESS_CASE(CDWRE);
    PROCESS_CASE(CDBRE);
    PROCESS_CASE(CHL1);
    PROCESS_CASE(CHL3);
    PROCESS_CASE(CHE);
    PROCESS_CASE(CHQ1);
    PROCESS_CASE(CHQ3);
    PROCESS_CASE(CHU);
    PROCESS_CASE(CHD);
    PROCESS_CASE(CHUDRE);
    PROCESS_CASE(CL1);
    PROCESS_CASE(CLL1);
    PROCESS_CASE(CQQ1);
    PROCESS_CASE(CQQ11);
    PROCESS_CASE(CQQ3);
    PROCESS_CASE(CQQ31);
    PROCESS_CASE(CLQ1);
    PROCESS_CASE(CLQ3);
    PROCESS_CASE(CEE);
    PROCESS_CASE(CUU);
    PROCESS_CASE(CUU1);
    PROCESS_CASE(CDD);
    PROCESS_CASE(CDD1);
    PROCESS_CASE(CEU);
    PROCESS_CASE(CED);
    PROCESS_CASE(CUD1);
    PROCESS_CASE(CUD8);
    PROCESS_CASE(CLE);
    PROCESS_CASE(CLU);
    PROCESS_CASE(CLD);
    PROCESS_CASE(CQE);
    PROCESS_CASE(CQU1);
    PROCESS_CASE(CQU8);
    PROCESS_CASE(CDQ1);
    PROCESS_CASE(CDQ8);
    PROCESS_CASE(CLEDQRE);
    PROCESS_CASE(CQUQD1RE);
    PROCESS_CASE(CQUQD11RE);
    PROCESS_CASE(CQUQD8RE);
    PROCESS_CASE(CQUQD81RE);
    PROCESS_CASE(CLEQU1RE);
    PROCESS_CASE(CLEQU3RE);
    #undef PROCESS_CASE

#undef PPCAT_NX
#undef PPCAT 
#undef STRINGIZE_NX
#undef STRINGIZE 

    if (__tableWCperName.count(wcToUse) < 1)
        throw std::runtime_error("Impossible to convert: [" + wc
            + "] to a given Wilson coefficient code. This code is not present");


    return __tableWCperName.at(wcToUse);
}*/




} // namespace eft
