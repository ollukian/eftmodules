#include "EFTManager.h"
//#include "Parser.cxx"

#include "../DataBase/TEntry.h"
#include "../DataBase/EntryParser.h"
#include "../DataBase/LinearParametrisation.h"
#include "../DataBase/WIdthOrXSsEntry.h"
#include "../DataBase/ImpactPlotter.h" 

#include <numeric>


using std::cin;
using std::cout;
using std::endl;
using std::move;

bool EFTManager::doMerging_ = false;
std::ostream *EFTManager::os_ = &std::cout;
size_t    EFTManager::nbEvents_ = 0;
//////////////////////////////////////////////////////////////////////////////////////
ParseResults EFTManager::Parse(const string& filename, const string& specification)
{

  static bool isFirst = true;
  if (isFirst) {
    cout << "[WARNING] the parsing is done only in the NEW way. i.e. name "
         << " of the file is in the form: production_GGH_*** <--- "
         << " instead of: production_1***  ---> to switch back to the old format"
         << " pass \"old\" option to the parse command \n";
    isFirst = false;
  }

  cout << "[Parse] " << filename << endl;
  LOG_DURATION("Parse");

  /*if (specification == "old")
    return ParseOldConvention(filename);
  else if (specification == "Eleonora")
    return ParseEleonoraConvention(filename);
  else
    return ParseNewConvention(filename);*/
  auto res = ParseMetaInfo(filename);
  if (res.regime == eft::Regime::PRODUCTION)
    ParseProdContent(filename, res);
  else
    ParseDecayContent(filename, res);

  cout << res << endl;
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
ParseResults EFTManager::ParseMetaInfo(const string& filename) {
  LOG_DURATION("ParseMetaInfo");
  
  ParseResults res;
  auto tokens = Tokenize(filename);
  size_t idx_token = 0;
  cout << "[ParseMetaInfo] {" << filename << '}' << endl;
  cout << "[ParseMetaInfo] determine the name convention: Old, New, Ana, Eleonora..." << endl;

  string wilsonCoeffs; // to be set after the regime
  const string regime = CapitaliseString(tokens[idx_token++]); // idx_token -> 1
  if (regime == "PRODUCTION") {
    res.regime = eft::Regime::PRODUCTION;
    cout << "[ParseMetaInfo] PRODUCTION mode: |";
    const string prodMode = CapitaliseString(tokens[idx_token++]); // idx_token -> 2
    cout << prodMode << '|';
    res.prodMode = ParseProductionMode(prodMode);
    cout << " ==> " << res.prodMode << endl;
    wilsonCoeffs = CapitaliseString(tokens[idx_token++]); // idx_token -> 3
    //cout << "[ParseMetaInfo] take: " << wilsonCoeffs << " as WCs" << endl;
  }
  else if (regime == "DECAY") {
    cout << "[ParseMetaInfo] DECAY" << endl;
    res.regime = eft::Regime::DECAY;
    string decChannel = tokens[idx_token];  // idx_token = 1 
    // => no icrease, since WC maybe in the same token
    // for old convention
    cout << "[ParseMetaInfo] deal with decayChannel: |" << decChannel << "|" << endl;
    if ( isdigit(decChannel[0])) { 
      const string decIdx = decChannel.substr(0, 2);
      cout << "[ParseMetaInfo] parse: |" << decIdx << "| as channel ==> ";
      
      res.decayChannel = ParseDecayChannel(stoi(decIdx));
      cout << res.decayChannel << endl;
      wilsonCoeffs = decChannel.substr(2, decChannel.length());
      cout << "[ParseMetaInfo] parse remnants: |" << wilsonCoeffs << "| as WC" << endl;
      //cout << "[ParseMetaInfo] decayChannel: |" << res.decayChannel << endl;
      //cout << 
      idx_token++; //  idx_token -> 2
    }
    else {
      CapitaliseString(decChannel);
      idx_token++;
      res.decayChannel = ParseDecayChannelEleonora(decChannel);
      //throw std::logic_error("Parsing of decay channels from name is NOT avaible - add it");
      //res.decayChannel = ParseDecayChannel(decChannel);
      wilsonCoeffs = CapitaliseString(tokens[idx_token++]); // idx_token -> 2
    }
   
    //res.decayChannel = ParseDecayChannel(decChannel);
   
    
  }
  else {
    throw std::runtime_error("Parsing filename: regime: {" + regime + "}"
      + " is not known. Available: production, decay.");
  }

  //bool isFromEleonora = true;
  cout << "[ParseMetaInfo] parse WC... |";
  //wilsonCoeffs = tokens[2];
  cout << wilsonCoeffs << '|';
  if ( isdigit(wilsonCoeffs[0])) {
    //isFromEleonora = false;
    cout << " from a digit ==> ";
    res.wc.SetId(stoi(wilsonCoeffs.substr(0, 2)));
    //res.wc.SetId(ParseWCid(wilsonCoeffs));
  // cout << "parse wc code. Id = " << res.wc.GetId() << endl;
    res.wcCode = ParseWCcode(res.wc.GetId());
    cout << res.wcCode << endl;
  }
  else if (wilsonCoeffs[0] == 'c' || wilsonCoeffs[0] == 'C') {
    //isFromEleonora = true;
    CapitaliseString(wilsonCoeffs);
    cout << " from a name: " << wilsonCoeffs;
    res.wcCode = strToWC(wilsonCoeffs);
    cout << " ==> " << res.wcCode << endl;
  }
  else if (wilsonCoeffs == "SM") {
    res.wcCode = WilsonCoefficientsCode::SM;
    cout << "SM" << endl;
  }
  else {
    throw std::logic_error("Impossible to parse WC from: |" + wilsonCoeffs
        + "|. Available: number xy or cXX => from the name" );
  }

  

  if (res.wcCode == WilsonCoefficientsCode::SM /* &&! isFromEleonora*/) {
    res.theoryRegime = TheoryRegime::SM;
    cout << "[ParseMetaInfo] no need to parse WC value" << endl;
  }
  else {
    res.theoryRegime = TheoryRegime::INT;
    cout << "[ParseMetaInfo] need to parse WC value: ";
    //cout << "[ParseMetaInfo] parse WC value: ";
    const string wcValStr = CapitaliseString(tokens[idx_token++]);
    cout << wcValStr << " ==> ";
    res.wcValue     = stoi(wcValStr);
    res.wcValueCode = ParseWCvalue(res.wcValue);
    cout << res.wcValueCode << endl;

    const string wcValueStr =  CapitaliseString(tokens[idx_token++]);
    if (wcValueStr != "WCMODE")
      throw std::logic_error("Expected to get: WCmode at the 4th token, but got: |"
        + wcValueStr + "|");

  }

  

  const string nbEvents = tokens[idx_token];
  cout << "[ParseMetaInfo] parse nbEvents: |" << nbEvents;
  try {
    res.nbGeneratedEvents = stoi(nbEvents);
  } catch(invalid_argument& exception) {
    cout << " ==> not valid! Error!" << endl;
    throw std::runtime_error("Impossible to convert: |" + nbEvents
      + " to integer! We got: " + exception.what());
  }
  cout << " ==> " << res.nbGeneratedEvents << endl;
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::ParseProdContent(const string& filename, ParseResults& res) {
  LOG_DURATION("ParseProdContent");

  res.xsPerTB       = move(GetCrossSection(directory_ + "/" + filename, false));
  res.xsPerTBcoarse = move(GetCrossSection(directory_ + "/" + filename, 
                          false, 
                          "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30"
                          )
                      );
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::ParseDecayContent(const string& filename, ParseResults& res) {
  LOG_DURATION("[ParseDecayContent]");
  res.width_ = move(GetWidth(directory_ + "/" + filename));
}
//////////////////////////////////////////////////////////////////////////////////////
ParseResults EFTManager::ParseNewConvention(const string& filename) {
  LOG_DURATION("ParseNewConvention");
  cout << "[WARNING] DEPRECIATED" << endl;

  auto tokens = Tokenize(filename);
  //cout << "[Parse] found " << tokens.size() << " tokens: " << endl;
  //for (const string& token : tokens) { cout << token << endl; }

  if (tokens.size() < 7)
    throw std::runtime_error("less than 5 tokens found in: " + filename);

  const string regime = tokens[0];
  const string prodMode = tokens[1];
  const string wilsonCoeffs = tokens[2];
  const string wilsonCoeffVal = tokens[3];
  const string nbEvents = tokens[5];

  //cout << "regime: " << regime << endl;
  //cout << "prodMode: " << prodMode << endl;
  //cout << "wilsonCoeffs: " << wilsonCoeffs << endl;
  //cout << "wilsonCoeffVal: " << wilsonCoeffVal << endl;
  //cout << "nbEvents: " << nbEvents << endl;


  ParseResults res;

  if (regime == "production") {
    res.regime = eft::Regime::PRODUCTION;
    res.prodMode = ParseProductionMode(prodMode);
    res.xsPerTB = move(GetCrossSection(directory_ + "/" + filename, true));
    res.xsPerTBcoarse = move(GetCrossSection(directory_ + "/" + filename, 
                          false, 
                          "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30"
                          )
                      );
    if (doMerging_)
    {
      throw std::runtime_error("ERROR: no merging should be specified.. line 78 EFTManager.cxx");
      cout << "[Parse] merging is required. Start it..." << endl;
      //res.xsPerTB = Merge(res.xsPerTB, eft::mergingFineIdxIdx);
    }
  }
  else if (regime == "decay")
  {
    throw std::runtime_error("[ERROR] Parse new convention for [decay] is not implemented yet");
    
    //res.regime = eft::Regime::DECAY;
    //res.decayChannel = ParseDecayChannel(stoi(wilsonCoeffs.substr(0, 2))); // TODO: change this obsolete
    //res.width_ = move(GetWidth(directory_ + "/" + filename));
    //wilsonCoeff.erase(1, 1);
  }
  else
    throw std::runtime_error("regime: " + regime + " is not recognised. Available: decay, production");

  //cout << "before parsing nb events" << endl;
  res.nbGeneratedEvents = stoi(nbEvents);
  //cout << "before parsing wc value" << endl;
  res.wcValue = stoi(wilsonCoeffVal);
  res.wcValueCode = ParseWCvalue(res.wcValue);

  //res.wcValueCode = ParseWCvalue(res.wcValue);
  //res.wc = WilsonCoefficient("", (int)res.wcValue);
  res.wc.SetId(stoi(wilsonCoeffs.substr(0, 2)));

  res.wcCode  = ParseWCcode(stoi(wilsonCoeffs.substr(0, 2)));
  res.wcCode2 = ParseWCcode(stoi(wilsonCoeffs.substr(2, 2)));

  if (res.wcCode == WilsonCoefficientsCode::SM)
    res.theoryRegime = TheoryRegime::SM;
  else if (res.wcCode2 == WilsonCoefficientsCode::SM)
    res.theoryRegime = TheoryRegime::INT;
  else
    res.theoryRegime = TheoryRegime::BSM;

  cout << res << endl;

  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
ParseResults EFTManager::ParseOldConvention(const string& filename) {
  LOG_DURATION("ParseOldConvention");
  cout << "[WARNING] DEPRECIATED" << endl;
  auto tokens = Tokenize(filename);
  // cout << "[Parse] found " << tokens.size() << " tokens: " << endl;
  // for (const string& token : tokens) { cout << token << endl; }

  if (tokens.size() < 5)
    throw std::runtime_error("less than 5 tokens found in: " + filename);

  string regime = tokens[0];
  string wilsonCoeff = tokens[1];
  string wilsonCoeffVal = tokens[2];
  string nbEvents = tokens[4];

  ParseResults res;

  if (regime == "production")
  {
    res.regime = eft::Regime::PRODUCTION;
    res.prodMode = ParseProductionMode(wilsonCoeff[0]);
    res.xsPerTB = move(GetCrossSection(directory_ + "/" + filename, true));
    res.xsPerTBcoarse = move(GetCrossSection(directory_ + "/" + filename, 
                          false, 
                          "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30"
                          )
                      );
    
    if (doMerging_)
    {
      //cout << "[Parse] merging is required. Start it..." << endl;
      //res.xsPerTB = Merge(res.xsPerTB);
      throw std::runtime_error("ERROR: no merging should be specified.. line 151 EFTManager.cxx");
    }
  }
  else if (regime == "decay")
  {
    res.regime = eft::Regime::DECAY;
    res.decayChannel = ParseDecayChannel(stoi(wilsonCoeff.substr(0, 2)));
    res.width_ = move(GetWidth(directory_ + "/" + filename));
    wilsonCoeff.erase(1, 1);
  }
  else
    throw std::runtime_error("regime: " + regime + " is not recognised. Available: decay, production");

  res.nbGeneratedEvents = stoi(nbEvents);
  res.wcValue = stoi(wilsonCoeffVal);
  res.wcValueCode = ParseWCvalue(res.wcValue);
  res.wc = WilsonCoefficient("", (int)res.wcValue);
  res.wc.SetId(ParseWCid(wilsonCoeff));
  // cout << "parse wc code. Id = " << res.wc.GetId() << endl;
  res.wcCode = ParseWCcode(res.wc.GetId());
  if (res.wcCode == WilsonCoefficientsCode::SM)
    res.theoryRegime = TheoryRegime::SM;
  else
    res.theoryRegime = TheoryRegime::INT;

  cout << res << endl;

  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
ParseResults EFTManager::ParseAna(string filename) const
{
  cout << "[ParseAna] " << filename << endl;
  LOG_DURATION("ParseAna");

  string name_to_save = filename;

  auto tokens = Tokenize(filename, {"\\", "_", ".", "/"});
  // cout << "[Parse] found " << tokens.size() << " tokens: " << endl;
  // for (const string& token : tokens) { cout << token << endl; }
  // return {};

  size_t idx_first_meaningful_token = 0;
  for (size_t idx_token = 0; idx_token < tokens.size(); ++idx_token)
  {
    if (tokens[idx_token] == "Samples")
    {
      idx_first_meaningful_token = idx_token;
      break;
    }
  }

  const eft::TheoryRegime theoryRegime = ParseTheoryRegime(tokens[idx_first_meaningful_token + 1]);
  const eft::WCvalue wcValue = ParseWCvalueAna(tokens[idx_first_meaningful_token + 2]);
  const eft::ProdMode prodMode = ParseProductionMode(tokens[idx_first_meaningful_token + 3]);
  const eft::WilsonCoefficientsCode wcCode1 = ParseWCcode(tokens[idx_first_meaningful_token + 4]);
  const eft::WilsonCoefficientsCode wcCode2 = ParseWCcode(tokens[idx_first_meaningful_token + 5]);

  ParseResults res;
  res.wcValueCode = wcValue;
  res.wcValueCode2 = wcValue;

  res.prodMode = prodMode;
  res.theoryRegime = theoryRegime;
  res.regime = Regime::PRODUCTION;

  res.wcCode = wcCode1;
  res.wcCode2 = wcCode2;

  if (wcCode1 == WilsonCoefficientsCode::SM && wcCode2 == WilsonCoefficientsCode::SM)
  {
    res.theoryRegime = TheoryRegime::SM;
  }

  res.wc = WilsonCoefficient("", (int)res.wcValue);
  res.wc.SetWcCode(wcCode1);
  // res.wc.SetId((int)wcCode1);

  cout << "parsing results. Found: " << endl;
  cout << "th regime: " << theoryRegime << endl;
  cout << "wc value: " << wcValue << endl;
  cout << "wc code: " << res.wc.GetWcCode() << endl;
  cout << "wc id: " << res.wc.GetWcCode() << endl;

  res.xsPerTB = move(GetCrossSection(name_to_save));
  res.xsPerTBcoarse = move(GetCrossSection(name_to_save, 
                          false, 
                          "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30"
                          )
                      );

  if (doMerging_)
  {
    throw std::runtime_error("ERROR: no merging should be specified.. line 244 EFTManager.cxx");
    //cout << "[Parse] merging is required. Start it..." << endl;
    //res.xsPerTB = Merge(res.xsPerTB);
  }

  cout << "res: " << endl;
  cout << res << endl;
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::GetSmXs()
{
  cout << "[Get SM xs]" << endl;
  LOG_DURATION("GetSMxs");
  // cout << "add contribution of all production modes." << endl;
  size_t nbBins;
  if (doMerging_) 
    nbBins = eft::mergedBinsName.size();
  else
    nbBins = regTB_.size();

  {
    nbBins = fineNames.size();
  }

  cout << "[GetSMxs] expected: " << nbBins << " bins from " << endl;

  shared_ptr<TH1D> hSM = make_shared<TH1D>("h_SM", "h_SM", nbBins, 0.5, nbBins + 0.5);

  /*const vector<string> files = {
      "production_10000__00_WCmode__10000_events.root",
      "production_20000__00_WCmode__10000_events.root",
      "production_30000__00_WCmode__10000_events.root",
      "production_40000__00_WCmode__10000_events.root",
      "production_50000__00_WCmode__10000_events.root",
      "production_60000__00_WCmode__10000_events.root",
      "production_70000__00_WCmode__10000_events.root",
  };*/


  const vector<string> files = {
    "production_ttH_SM_100000_events.root",
    "production_tHW_SM_100000_events.root",
    "production_VBF_SM_100000_events.root",
    "production_ZHlep_SM_100000_events.root",
    "production_tHjb_SM_100000_events.root",
    "production_ggH_SM_100000_events.root",
    "production_WHlep_SM_100000_events.root"
    /*"production_GGF_0000__00_WCmode__100000_events.root",
    "production_VBF_0000__00_WCmode__100000_events.root",
    "production_ZH_0000__00_WCmode__100000_events.root",
    "production_WH_0000__00_WCmode__100000_events.root",
    "production_GG2ZH_0000__00_WCmode__100000_events.root",
    "production_QQ2ZH_0000__00_WCmode__100000_events.root",
    "production_THJB_0000__00_WCmode__100000_events.root",
    "production_THW_0000__00_WCmode__100000_events.root",
    "production_TTH_0000__00_WCmode__100000_events.root",
    "production_BBH_0000__00_WCmode__100000_events.root"*/
  };

  for (const string &file : files)
  {
    auto res = Parse(file);
    auto h_tmp = MapToHisto(res.xsPerTB);
    hSM->Add(h_tmp.get());
  }

  cout << "read xs: " << endl;
  // int because otherwise CLang complains that we compare
  // uint and int (somehow, nb of bins is int!!!) WTF?
  for (int idx = 1; idx <= hSM->GetNbinsX(); ++idx)
  {
    cout << setw(5) << idx << " -> " << hSM->GetBinContent(idx) << endl;
  }
  // throw std::runtime_error("enough");

  // size_t nbBins = hSM->GetNbinsX();
  cout << "nbbins = " << nbBins << endl;
  for (size_t idx_bin = 0; idx_bin < nbBins; ++idx_bin)
  {
    cout << "set " << setw(5) << idx_bin << " to " << hSM->GetBinContent(idx_bin + 1) << endl;
    SMxs_[idx_bin].val = hSM->GetBinContent(idx_bin + 1);
    SMxs_[idx_bin].err = hSM->GetBinError(idx_bin + 1);
  }

  TCanvas c("cSm", "", 1200, 800);
  c.SetBottomMargin(0.30);
  hSM->Draw("hist e text");
  SetLabels(hSM.get(), fineNames, 'x');
  c.SaveAs("figures/SM_xs.pdf");
  // throw std::runtime_error("enough");
  hSM_ = move(hSM);
  // SetLabels(hSM_.get(), mergedBinsName, 'x');
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::GetSmWidths()
{
  throw std::runtime_error("DON'T USE GetSmWidhts!");
  cout << "[Get SM widths]" << endl;

  for (size_t idx_decay = 10; idx_decay <= 30; ++idx_decay)
  {
    string filename = "decay_" + to_string(idx_decay) + "0000__1_WCmode__10000_events.txt";
    auto res = Parse(filename, "old");

    SMwidths_[res.decayChannel] = res.width_;
    AddWidth(SMwidths_[eft::DecayChannel::TOTAL], res.width_);
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::DrawSmXs() const
{
  TCanvas c("c", "c", 1200, 800);

  c.SetBottomMargin(0.30);
  if (doMerging_)
    SetLabels(hSM_.get(), mergedBinsName, 'x');
  else
    SetLabels(hSM_.get(), fineNames, 'x');

  hSM_->GetYaxis()->SetTitle("SM Cross-section [pb]");
  hSM_->GetXaxis()->SetLabelSize(0.02);
  hSM_->Draw("HIST E text");
  c.SaveAs("figures/SmXS.pdf");
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::GetXSs(Regime reg, ProdMode mode, WCvalue val, size_t nbEvents)
{
  LOG_DURATION("GetXSs");
  cout << "[GetXS]: " << reg << " " << mode << " " << static_cast<int> (val) << " " << nbEvents << endl;
  cout << "[GetXS]: get list of files fulfilling the criterium.." << endl;

  string specification = "";

  //if (reg == Regime::DECAY)
  //  specification = "old";
  const vector<string> files = GetListFiles(directory_, reg, mode, specification, val, nbEvents);
  cout << "[GetXS] found " << files.size() << " files" << endl;

  if (files.empty())
    throw std::runtime_error("NO FILES FOUND");

  cout << "[GetXS] parse them..." << endl;
  for (const string &file : files)
  {
    cout << " - " << file << endl;
    if (reg == Regime::DECAY)
      Register(Parse(file));
    else
      Register(Parse(file));
  }
  // cout << " XSs are obtained" << endl;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::GetXSs(const vector<Regime> &regimes = vector<Regime>{Regime::NONE},
                        const vector<ProdMode> &modes = vector<ProdMode>{ProdMode::TOTAL},
                        const vector<WCvalue> &vals = vector<WCvalue>{WCvalue::NONE},
                        const vector<size_t> &vecNbEvents = vector<size_t>{0})
{
  cout << "[GetXSs] for: "
       << regimes.size() << " regimes, "
       << modes.size() << " modes, "
       << vals.size() << " wc vals, "
       << vecNbEvents.size() << " nb of events"
       << endl;

  cout << "[GetXSs] launch sequential running for all combintation of parameters..." << endl;
  for (const auto &regime : regimes)
  {
    for (const auto &mode : modes)
    {
      for (const auto &val : vals)
      {
        for (const auto &nbEvents : vecNbEvents)
        {
          putTime(cout) << "[GetXSs][" << setw(15) << regime << "]["
                        << setw(5) << mode << "]["
                        << setw(7) << val << "]["
                        << setw(7) << nbEvents << "]... \n";
          GetXSs(regime, mode, val, nbEvents);
        }
      }
    }
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::GetXssAna(const string &path, ProdMode mode, WCvalue val)
{
  LOG_DURATION("GetXssAna");
  cout << "[GetXSsAna]: " << mode << " " << static_cast<int> (val) << endl;
  cout << "[GetXSsAna]: get list of files fulfilling the criterium.." << endl;

  vector<string> files;
  if (val == WCvalue::NONE)
  {
    cout << "[GetXSsAna] add files for all values...." << endl;
    for (const auto vals : {WCvalue::ONE, WCvalue::MINHALF, WCvalue::DOTONE})
    {
      cout << "[GetXSsAna] dealing with: " << vals << " ...." << endl;
      auto tmp = GetListFilesAna(path, mode, vals);
      for (string &s : tmp)
        files.push_back(move(s));
    }
  }
  else
  {
    files = GetListFilesAna(path, mode, val);
  }
  cout << "[GetXSsAna] found " << files.size() << " files" << endl;

  if (files.empty())
    throw std::runtime_error("NO FILES FOUND");

  cout << "[GetXSsAna] parse them..." << endl;
  for (const string &file : files)
  {
    // cout << " - " << file << endl;
    Register(ParseAna(file));
    cout << "after adding this file, the reg of WC contains " << regWCcode_.size() << " wc: " << endl;
    for (const auto &wc : regWCcode_)
      cout << wc << endl;
  }
  // cout << " XSs are obtained" << endl;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::Register(const ParseResults &res)
{
  LOG_DURATION("Register");
  cout << "[Register] Dispacth the resul according to the type: production or decay" << endl;
  if (res.regime == eft::Regime::PRODUCTION)
    RegisterXS(res);
  else
    RegisterWidth(res);
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::RegisterXS(const ParseResults &res)
{ // TODO: to update
  cout << "[Register] XS" << endl;
  LOG_DURATION("RegisterXS");
  cout << "[Register] wc | id: " << res.wc.GetId()
       << " | value: " << res.wcValueCode
       << " ... " << endl;
  // cout << "[Register] check if the wc is already present...";

  cout << "we use the wc: " << res.wc << endl;
  cout << "res.xsPerTB.size() = " << res.xsPerTB.size() << endl;

  if (interfXS_.find(res.wcCode) != interfXS_.end())
  {
    // if (interfXS_.find(res.wc) != interfXS_.end()) {
    // interfxs_[res.wcCode][res.wcValueCode];
    interfXS_[res.wcCode][res.wcValueCode];
    interfXScoarse_[res.wcCode][res.wcValueCode];
    //cout << " yes, it's already present -> Add the content" << endl;
    //cout << " yes, it's already present -> Add the content" << endl;
    // AddXS(interfXS_[res.wc][res.wcValueCode], res.xsPerTB);
    AddXS(interfXS_      [res.wcCode][res.wcValueCode], res.xsPerTB);
    AddXS(interfXScoarse_[res.wcCode][res.wcValueCode], res.xsPerTBcoarse);
    //cout << " what we have after adding: " << endl;
    cout << "interfXS_["       << res.wcCode << "][" << res.wcValueCode << "] = " << interfXS_      [res.wcCode][res.wcValueCode].size() << endl;
    cout << "interfXScoarse_[" << res.wcCode << "][" << res.wcValueCode << "] = " << interfXScoarse_[res.wcCode][res.wcValueCode].size() << endl;
    // throw std::runtime_error("exit");
  }
  else
  {
    cout << " no, it's the first entry" << endl;
    interfXS_[res.wcCode];

    interfXS_      [res.wcCode][res.wcValueCode] = res.xsPerTB;
    interfXScoarse_[res.wcCode][res.wcValueCode] = res.xsPerTBcoarse;
  }

  AddXS(xsChannelTruthBinWc_, res);

  // AddXS(interfXS_[res.wc][res.wcValueCode], res.xsPerTB);

  wCbyCode_[res.wcCode] = res.wc;
  regWCcode_.insert(res.wcCode);

  regWC_.insert(res.wc);
  regWCperVal_[res.wcValueCode].insert(res.wc);
  regValperWc_[res.wc].insert(res.wcValueCode);

  xsPerWCperVal_[res.wc];
  xsPerWCperVal_[res.wc][res.wcValueCode] = res.xsPerTB;

  xsPerValPerWc_[res.wcValueCode];
  xsPerValPerWc_[res.wcValueCode][res.wc] = res.xsPerTB;

  {
    LOG_DURATION("reg_truth_bins");
    for (const auto& tb_xs : res.xsPerTB) {
      size_t size_before = regTB_.size();
      regTB_.insert(tb_xs.first);
      if (regTB_.size() != size_before)
        cout << "[register TB]: " << tb_xs.first << endl;
    }
  }

  // interfXS_[res.wc] = res.xsPerTB;
  cout << "[Register] DONE" << endl;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::RegisterWidth(const ParseResults &res)
{
  LOG_DURATION("RegisterWidth");
  cout << "[Register] Width " << res.width_.val << " +- " << res.width_.err
       << " for the channel: " << res.decayChannel
       << " for the WC: " << res.wcCode
       << " for its value:" << res.wcValueCode
       << endl;
  eft::DecayChannel channel = res.decayChannel;
  eft::WilsonCoefficientsCode wc = res.wcCode;
  eft::WCvalue wcval = res.wcValueCode;
  //widthPerChanWcVal_[channel];
  //widthPerChanWcVal_[channel][wc];
  //widthPerChanWcVal_[channel][wc][wcval] = res.width_;

  //cout << "[WARNING] to revise this section of registering the widhts..." << endl;

  //widthPerChanWcVal_[channel]                 [wc]                                [eft::WCvalue::TOTAL];
  //widthPerChanWcVal_[channel]                 [eft::WilsonCoefficientsCode::TOTAL][eft::WCvalue::TOTAL];
  //widthPerChanWcVal_[eft::DecayChannel::TOTAL][eft::WilsonCoefficientsCode::TOTAL][eft::WCvalue::TOTAL];

  //widthPerChanWcVal_[DecayChannel::TOTAL]     [wc]                                [eft::WCvalue::TOTAL];

  // cout << "[Register Width]. Total width was: "

  //AddWidth(widthPerChanWcVal_[channel]                 [wc][eft::WCvalue::TOTAL], res.width_);
  //AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][wc][eft::WCvalue::TOTAL], res.width_);
  //AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][wc][wcval], res.width_);

  if (wc != eft::WilsonCoefficientsCode::SM)
  {
    cout << "[RegisterWidth] not SM => add to [ch][wc], [total][wc], [ch][total] and [total][total]" << endl;
    AddWidth(widthPerChanWcVal_[channel                 ][wc                                ][wcval], res.width_);
    //AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][wc                                ][eft::WCvalue::TOTAL], res.width_);
    AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][wc                                ][wcval], res.width_);
    AddWidth(widthPerChanWcVal_[channel]                 [eft::WilsonCoefficientsCode::TOTAL][wcval], res.width_);
    AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][eft::WilsonCoefficientsCode::TOTAL][wcval], res.width_);
    //AddWidth(widthPerChanWcVal_[channel]                 [eft::WilsonCoefficientsCode::TOTAL][eft::WCvalue::TOTAL], res.width_);
    //AddWidth(widthPerChanWcVal_[eft::DecayChannel::TOTAL][eft::WilsonCoefficientsCode::TOTAL][eft::WCvalue::TOTAL], res.width_);
  }
  else { // SM
    cout << "[RegisterWidth] SM => add to [ch] and [total]" << endl;
    AddWidth(SMwidths_[channel],                  res.width_);
    AddWidth(SMwidths_[eft::DecayChannel::TOTAL], res.width_);
  }
  // widthPerChanWcVal_[channel][wc][eft::WCvalue::TOTAL].val += res.width_.val;
  // widthPerChanWcVal_[channel][eft::WilsonCoefficientsCode::TOTAL][eft::WCvalue::TOTAL] += res.width_.val;
  /*widthPerChanWcVal_[eft::DecayChannel::TOTAL]
                    [eft::WilsonCoefficientsCode::TOTAL]
                    [eft::WCvalue::TOTAL]
                    += res.width_.val;*/
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::Draw2DImpact()
{
  LOG_DURATION("Draw2DImpact");
  cout << "[Draw2D] for " << interfXS_.size() << " wilson coeff" << endl;
  size_t nbWC = interfXS_.size();

  if (hSM_ == nullptr)
  {
    cout << "[Warning] SM xss are not obtained!" << endl;
    GetSmXs();
    GetSmWidths();
  }

  if (!wcAreAveraged_)
  {
    cout << "[Draw2D] wc are not averaged - compute it" << endl;
    AverageWCvalues();
  }

  size_t nbBins;
  
  if (doMerging_) 
    nbBins = eft::mergedBinsName.size();
  else
    nbBins = regTB_.size();

  cout << "[Draw2D] initialise for " << nbBins << " bins and " << nbWC << " WC...\n"; 

  hInterf_       = make_shared<TH2D>("hInterf", "hInterf", nbBins, 0., nbBins, nbWC, 0, nbWC);
  hImpactInterf_ = make_shared<TH2D>("hImpact", "hImpact", nbBins, 0., nbBins, nbWC, 0, nbWC);

  cout << "[Draw2D] start filling the 2D histo" << endl;

  eft::WilsonCoefficientsCode wc;
  CrossSections xss;
  //size_t wcId;

  double SMxs;
  //double intxs;
  double impact;

  truth_bin bin;
  cross_section xs;

  size_t wcIdx = 0;

  for (const auto &wc_xss : interfXS_)
  {
    cout << "====================================" << endl;
    wc = wc_xss.first;
    cout << "[Draw2D] dealing with: " << wc;

    //if (wc.GetId() == 0)
    //  continue;
    if (wc == WilsonCoefficientsCode::NONE) {
      cout << " ==> NONE - skip it" << endl;
      continue;
    }

    cout << " ==> leav it. " << wc_xss.second.size() << " keys are available: " << endl;
    //cout << " for the wc: " << wc << ", " << wc_xss.second.size() << " keys are available: " << endl;
    for (const auto &key_val : wc_xss.second)
    {
      cout << key_val.first << endl;
    }

    xss = wc_xss.second.at(WCvalue::AVERAGED);
    //wcId = wc.GetId();
    wcIdx++;
    cout << "[Draw2D] corresponindg wc idx: " << wcIdx << endl;

    //const string wcName = wcToStr(ParseWCcode(wcId));
    const string wcName = wcToStr(wc);
    //cout << "parse wc code - done" << endl;
    hInterf_->GetYaxis()->SetBinLabel(wcIdx, wcName.c_str());
    hImpactInterf_->GetYaxis()->SetBinLabel(wcIdx, wcName.c_str());
    // hInterf_      ->GetYaxis()->SetBinLabel(wcIdx, to_string((int)wcId).c_str());
    // hImpactInterf_->GetYaxis()->SetBinLabel(wcIdx, to_string((int)wcId).c_str());
    // hInterf_      ->GetYaxis()->SetBinLabel(wcIdx, wCnames[wcIdx - 1].c_str());
    // hImpactInterf_->GetYaxis()->SetBinLabel(wcIdx, wCnames[wcIdx - 1].c_str());

    // for (const auto& bin_xs : xss) {
    // cout << " ----------" << endl;
    // bin = bin_xs.first;
    for (size_t bin_idx = 0; bin_idx < nbBins; ++bin_idx)
    {
      if (xss.find(bin_idx) != xss.end())
      {
        bin = bin_idx;
        xs = xss.at(bin_idx);
      }
      else
      {
        bin = bin_idx;
        xs.val = 0.f;
        xs.err = 0.f;
      }

      cout << " *** wc:  " << wc << " -> idx = " << wcIdx << " -> " << wcName << endl;
      //cout << " *** bin: " << bin << " -> " << mergedBinsName[bin] << endl;
      //////xs  = bin_xs.second;
      // SMxs = SMxs_.at(bin);
      SMxs = hSM_->GetBinContent(bin + 1);

      
      //if (SMxs != 0)
      if (abs(SMxs) >= 1E-8) {
        impact = xs.val / SMxs;
        cout << "abs SMxs >= 1E-8, so impact = " << xs.val << " / " << SMxs << " = " << impact << endl;
      }
      else
        impact = 0.;

      // impact = xs;

      //if (abs(xs.val) <= 1E-8)
      //if (xs.val == 0)
      //impact = 0.;

      cout << " ** bin: " << setw(5) << bin << endl;
      cout << " -> intxs  = " << setw(5) << xs << endl;
      cout << " -> SMxs   = " << setw(5) << SMxs << endl;
      cout << " -> impact = " << setw(5) << impact << endl;
      cout << "-> [SetBin] [" << bin + 1 << ", " << wcIdx << "] to " << impact << endl;

      hInterf_->SetBinContent(bin + 1, wcIdx, xs.val);
      hInterf_->SetBinError(bin + 1, wcIdx, xs.err);

      hImpactInterf_->SetBinContent(bin + 1, wcIdx, impact * 100);
    }
  }

  // cout << "[SetXlabel]" << endl;
  for (const auto &bin_xs : xss)
  {
    // cout << " - " << bin << endl;
    bin = bin_xs.first;
    xs = bin_xs.second;

    hInterf_->GetXaxis()->SetBinLabel(bin, to_string(bin).substr(0, 4).c_str());
    hImpactInterf_->GetXaxis()->SetBinLabel(bin, to_string(bin).substr(0, 4).c_str());
    // hInterf_      ->GetXaxis()->SetBinLabel(bin, binsNamesLatex[bin - 1].c_str());
    // hImpactInterf_->GetXaxis()->SetBinLabel(bin, binsNamesLatex[bin - 1].c_str());
  }

  hInterf_->SetTitle("#sigma_{interf} [pb]");
  hImpactInterf_->SetTitle("Impact of WC: #sigma_{interf} / #sigma_{SM} [%]");

  hInterf_->GetXaxis()->SetTitle("Truth bin");
  hImpactInterf_->GetXaxis()->SetTitle("Truth bin");

  hInterf_->GetYaxis()->SetTitle("Wilson coeffients driven xs");
  hImpactInterf_->GetYaxis()->SetTitle("Wilson coeffient impact [%]");

  hImpactInterf_->SetMarkerSize(0.2); //0.6
  hInterf_->SetMarkerSize(0.2);  //0.4

  hImpactInterf_->GetXaxis()->SetLabelSize(0.02);
  hInterf_->GetXaxis()->SetLabelSize(0.02);

  hImpactInterf_->GetZaxis()->SetRangeUser(-100, 100);

  shared_ptr<TCanvas> c2 = make_shared<TCanvas>("c2", "c2", 3600, 3600);
  // TCanvas* c2 = new TCanvas("c2", "c2", 3600, 3600);
  c2->SetGrid();

  c2->SetBottomMargin(0.3);
  c2->SetLeftMargin(0.2);

  if (doMerging_) {
    SetLabels(hInterf_.get(), mergedBinsName, 'x');
    SetLabels(hImpactInterf_.get(), mergedBinsName, 'x');
  }
  else {
    SetLabels(hInterf_.get(), fineNames, 'x');
    SetLabels(hImpactInterf_.get(), fineNames, 'x');
  }

  gStyle->SetPaintTextFormat("2.3f");

  hInterf_->Draw("colz text");
  c2->SaveAs("figures/h2InterfXS.pdf");

  //
  // c2 = new TCanvas("c2", "c2", 3600, 2400);
  c2 = make_shared<TCanvas>("c2", "c2", 3600, 3600);
  c2->SetGrid();

  c2->SetBottomMargin(0.3);
  c2->SetLeftMargin(0.2);

  gStyle->SetPaintTextFormat("2.1f");
  hImpactInterf_->Draw("colz text");
  c2->SaveAs("figures/h2ImpactInterft.pdf");
  // delete c2;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::AddXS(CrossSections &l, const CrossSections &r)
{
  LOG_DURATION("AddXS");
  cout << "[AddXS]" << endl;

  //truth_bin bin;
  cross_section xs1, xs2;

  set<truth_bin> bins;

  for (const CrossSections &xs : {l, r})
  {
    for (const auto &bin_xs : xs)
    {
      bins.insert(bin_xs.first);
    }
  }

  for (auto &bin : bins)
  {
    // cout << "try bin: " << bin << endl;

    if (l.find(bin) != l.end())
      xs1 = l.at(bin);
    else
      xs1 = {0., 0.};

    if (r.find(bin) != r.end())
      xs2 = r.at(bin);
    else
      xs2 = {0., 0.};

    // xs2 = r.at(bin);
    /*cout << " bin: [" << setw(3) << bin << "] = " << setw(10) << xs1
         << " -> " << setw(10) << xs1
         << " + "  << setw(10) << xs2
         << " = ";*/

    l[bin] += xs2;
    // cout << l.at(bin) << endl;
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::AddXS(XsChannelTruthBinWc &db, 
    eft::DecayChannel ch, 
    eft::WilsonCoefficientsCode code,
    eft::WCvalue wcVal, 
    const CrossSections &xss) const 
{
  LOG_DURATION("AddXS_ch_wal_xss");
  cout << "[AddXs] add XS for: " << ch << " " << code << " " << wcVal << endl;
  cout << "[AddXs] " << xss.size() << " xss available" << endl;
  //truth_bin bin =  Label(99);
  truth_bin bin = (Label) 99;
  cross_section xs {};
  for (const auto& bin_xs : xss) {
    bin = bin_xs.first;
    xs = bin_xs.second;
    cout << "[" << bin << "]: " << db[ch][bin][code][wcVal] << " + " << xs;
    db[ch][bin][code][wcVal] += xs; 
    cout << " = " << db[ch][bin][code][wcVal] << endl;
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::AddXS(XsChannelTruthBinWc &db, const ParseResults& res) const {
  AddXS(db, res.decayChannel, res.wcCode, res.wcValueCode, res.xsPerTB);
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::PrintStatistics(ostream &os) const
{
  LOG_DURATION("PrintStatistics");
  os << setfill('=') << setw(50) << '=' << setfill(' ') << endl;
  os << "| Statistics" << setw(38) << '=' << endl;
  os << setfill('*') << setw(50) << '=' << setfill(' ') << endl;
  os << '|' << setw(20) << " WC total: " << setw(10) << regWC_.size() << setw(19) << '|' << endl;
  os << '|' << setw(20) << " WC per values: " << setw(29) << '|' << endl;

  for (const auto &val_wc : regWCperVal_)
  {
    os << '|' << setw(25) << val_wc.first << ": " << setw(3) << val_wc.second.size() << setw(19) << '|' << endl;
  }

  os << '|' << setw(20) << " values per WC: " << setw(29) << '|' << endl;

  for (const auto &wc_val : regValperWc_)
  {
    os << '|' << setw(25) << wc_val.first << ": " << setw(3) << wc_val.second.size() << setw(19) << '*' << endl;
  }
  os << setfill('=') << setw(50) << '=' << setfill(' ') << endl;

  // WIDTH
  // cout << setfill('*')   << setw(60) << '*' << setfill(' ') << endl;
  os << "| Width:" << setw(60) << '*' << endl;
  os << setfill('=') << setw(65) << '+' << setfill(' ') << endl;
  os << '|' << setw(10) << " * Total: " << setw(35)
     << widthPerChanWcVal_.at(eft::DecayChannel::TOTAL)
              .at(eft::WilsonCoefficientsCode::TOTAL)
              .at(eft::WCvalue::AVERAGED)
              .val
     << " +- " << setw(12)
     << widthPerChanWcVal_.at(eft::DecayChannel::TOTAL)
              .at(eft::WilsonCoefficientsCode::TOTAL)
              .at(eft::WCvalue::AVERAGED)
              .err
       << setw(1) << '*' << endl;

  os << '|' << setw(10) << " * Per Channel:" << setw(49) << "|" << endl;

  for (const auto &chan_res : widthPerChanWcVal_)
  {
    os << "| " << setw(30) << std::left << chan_res.first << " -> " << setw(12)
         << chan_res.second.at(eft::WilsonCoefficientsCode::TOTAL)
                .at(eft::WCvalue::AVERAGED)
                .val
         << " +- " << setw(12)
         << chan_res.second.at(eft::WilsonCoefficientsCode::TOTAL)
                .at(eft::WCvalue::AVERAGED)
                .err
         << setw(15) << '|' << endl;
  }
  os << setfill('=') << setw(65) << '=' << setfill(' ') << endl;

  // impact on GamGam
  cout << "| * Composition of GamGam widhts: " << setw(60) << "|" << endl;
  for (const auto &wc_res : widthPerChanWcVal_.at(eft::DecayChannel::GAM_GAM))
  {
    os << "| " << setw(35) << wc_res.first << " -> " << setw(11)
         << wc_res.second.at(eft::WCvalue::AVERAGED).val << " +- " << setw(11)
         << wc_res.second.at(eft::WCvalue::AVERAGED).err << setw(13) << '|' << endl;
  }
  os << setfill('=') << setw(65) << '=' << setfill(' ') << endl;

  // SM widhts
  os << "| * SM widths: " << setw(60) << "+" << endl;

  for (const auto &chan_res : SMwidths_)
  {
    os << "| " << setw(30) << std::left << chan_res.first << " -> " << setw(12)
         << chan_res.second.val
         << " +- " << setw(12)
         << chan_res.second.err
         << setw(15) << "|" << endl;
  }
  os << setfill('=') << setw(65) << '=' << setfill(' ') << endl;

  // SM xs
  os << "| * SM Cross-sections: " << setw(60) << '|' << endl;
  // cout << " sm xs size: " << SMxs_.size() << endl;
  // cout << "available keys: " << endl;

  // for (const auto& bin_xs : SMxs_) { cout << bin_xs.first <<  endl;}

  // cout << "available things in the mergedBinsName:" << endl;
  // for (size_t idx = 0; idx < eft::mergedBinsName.size(); ++idx) {
  //  cout << setw(5) << idx << " -> " << eft::mergedBinsName[idx] << endl;
  //}

  for (const auto &bin_xs : SMxs_)
  {

    if (!(bin_xs.first > 27))
    {
      os << "| "
           << setw(5) << bin_xs.first << ": "
           << setw(40) << eft::mergedBinsName[bin_xs.first] << ": "
           << setw(12) << bin_xs.second << setw(30) << '|' << endl;
    }
  }
  os << setfill('=') << setw(65) << '=' << setfill(' ') << endl;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::ComputeTransitionMatrix(bool doTakeDecayFromPaper)
{
  LOG_DURATION("ComputeTransitionMatrix");
  cout << "[ComputeTransitionMatrix]" << endl;

  double smWidthTotal = SMwidths_[DecayChannel::TOTAL].val;
  double smWidthGamGam = SMwidths_[DecayChannel::GAM_GAM].val;

  truth_bin bin;
  cross_section xs;

  cout << "[ComputeTransitionMatrix] " << regWC_.size() << " WC available:" << endl;
  for (const auto &wc : regWCcode_)
  {
    cout << wc << endl;
  }

  for (const auto code : regWCcode_)
  {
    // for (const auto& wc : regWC_) {
    // WilsonCoefficientsCode code = wc.GetWcCode();
    cout << "===============================" << endl;
    cout << "dealing with: " << code << ":" << endl;

    if (code == WilsonCoefficientsCode::SM)
    {
      cout << "==> SM - skip" << endl;
      continue;
    }

    double impact_width = 0.;
    //double impact_xs    = 0.;
    if ( ! doTakeDecayFromPaper ) {
      // DECAY part
      // cout << "extract width for"
      double wcWidthTotal = widthPerChanWcVal_[DecayChannel::TOTAL][code][eft::WCvalue::AVERAGED].val;
      cout << " - interf width Total: " << setw(10) << wcWidthTotal << endl;
      cout << " - SM     widht Total: " << setw(10) << smWidthTotal << endl;
      double impactWidthTotal = 0;
      if (abs(smWidthTotal) >= 1E-8)
      //if (smWidthTotal != 0)
        impactWidthTotal = wcWidthTotal / smWidthTotal;
      cout << " -- impact total widht: " << impactWidthTotal << endl;

      double wcWidthGamGam = widthPerChanWcVal_[DecayChannel::GAM_GAM][code][eft::WCvalue::AVERAGED].val;
      cout << " - interf width GAMGAM: " << setw(10) << wcWidthGamGam << endl;
      cout << " - SM     widht GAMGAM: " << setw(10) << smWidthGamGam << endl;
      double impactWidthGamGam = 0;
      //if (smWidthGamGam != 0)
      if (abs(smWidthGamGam) >= 1E-8)
        impactWidthGamGam = wcWidthGamGam / smWidthGamGam;
      cout << " -- impact GAMGAM widht: " << impactWidthGamGam << endl;

      impact_width = impactWidthGamGam - impactWidthTotal;
    }
    else { // take decay paramertisatin from the paper
      impact_width = brPerChanWcVal_[DecayChannel::GAM_GAM][code];
      cout << "impact_width[" << code << "] = " << impact_width << endl;
	  } // else -> if we take input decay paramestrisation from a paper

    
    
    transMatWcBin_[code];

    for (const auto &bin_xs : interfXS_[code][WCvalue::AVERAGED])
    {
      bin = bin_xs.first;
      xs = bin_xs.second;

      if (bin.id() == 99)
        continue;

      // cout << "dealing with bin: " << bin << endl;
      // cout << "its xs: " << xs << endl;
      // cout << "SM xs:  " << SMxs_[bin] << endl;
      //cout << "bin: " << setw(5) << bin << " | SMxs: " << setw(12) << SMxs_[bin] << " | intXS: " << setw(12) << xs;
      // cout << " ----- bin: " << setw(5) << bin << " -> " << setw(12)
      //	   << xs << " xs. SM xs: " << setw(12)
      double impactXS = 0;
      if (SMxs_[bin].val != 0)
        impactXS = xs.val / SMxs_[bin].val;

      //cout << " | impact: " << setw(12) << impactXS << " + " << setw(12) << impactWidthGamGam << " - " << setw(12) << impactWidthTotal << " = ";
      double impact = impactXS + impact_width;
      cout << impact << endl;
      transMatWcBin_[code][bin] = impact;
    } // over bins
  }   // over WC

  cout << "transition matrix is obtained. It's size: "
       << transMatWcBin_.size() << " x "
       << transMatWcBin_.begin()->second.size() << endl;

  vector<eft::WilsonCoefficientsCode> wcNames;
  vector<truth_bin> binNames;
  transWcBin_.ResizeTo(transMatWcBin_.size(), transMatWcBin_.begin()->second.size());
  transWcBin_ = MatrixManager::MakeTMatrixD<eft::WilsonCoefficientsCode,
                                            truth_bin,
                                            double>(transMatWcBin_,
                                                    wcNames,
                                                    binNames);
} // ComputeTransitionMatrix()
//////////////////////////////////////////////////////////////////////////////////////
// void EFTManager::SetLabels(TH1 *h, const vector<string> &labels, char axis)
// {
//   /*
//     axis: x or y -> which axis to set
//     default: 'x'
//   */
//   LOG_DURATION("SetLabels");
//   putTime(cout) << "[SetLabels] for " << axis << " axis " << endl;
//   TAxis *ax;
//   // shared_ptr<TAxis> ax;
//   if (axis == 'x')
//     ax = h->GetXaxis();
//   else if (axis == 'y')
//     ax = h->GetYaxis();
//   else
//   {
//     string s;
//     s.push_back(axis);
//     throw std::runtime_error("ERROR: axis: " + s + " is not recognised. Available: x, y");
//   }

//   size_t nbLabels = ax->GetNbins();
//   // cout << "nb labels = " << nbLabels << endl;
//   // cout << "nb names in the vec: " << labels.size() << endl;

//   ASSERT_EQUAL(nbLabels, labels.size());

//   for (size_t idx_label = 0; idx_label < nbLabels; ++idx_label)
//   {
//     // cout << "dealing with idx = " << setw(5) << idx_label
//     //		 << " put label: " << labels[idx_label] << endl;
//     ax->SetBinLabel(idx_label + 1, labels[idx_label].c_str());
//   }

//   if (axis == 'x')
//     ax->LabelsOption("v");
// }
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::DrawTransitionMatrix() const
{
  LOG_DURATION("DrawTransMatrix");

  cout << "[DrawTransMatrix] for " << regWC_.size() << " wc and"
       << mergedBinsName.size() << " truth bins" << endl;

  hTransMatrix_ = make_shared<TH2D>("htrans", "htrans",
                                    mergedBinsName.size(),
                                    0.,
                                    mergedBinsName.size(),
                                    regWC_.size(),
                                    0.,
                                    regWC_.size());

  size_t idx_wc = 1;
  size_t idx_bin = 1;

  eft::WilsonCoefficientsCode wc;
  truth_bin bin;
  double xs;

  for (const auto &wc_tb_val : transMatWcBin_)
  {
    idx_bin = 1;
    wc = wc_tb_val.first;
    // cout << "[wc]: " << setw(30) << wc << endl;
    hTransMatrix_->GetYaxis()->SetBinLabel(idx_wc, wcToStr(wc).c_str());

    for (const auto &tb_val : wc_tb_val.second)
    {
      bin = tb_val.first;
      xs = tb_val.second;
      // cout << "[" << setw(15) << wcToStr(wc) << "]["
      //      << setw(2) << bin << "] -> " << xs.val << "+- "<< xs.err << endl;
      // cout << " [tb]: " << setw(5) << bin << " -> " << xs << endl;

      if (bin.id() == 99)
        continue;

      hTransMatrix_->GetXaxis()->SetBinLabel(idx_bin, mergedBinsName[bin.id()].c_str());
      // hTransMatrix_->GetXaxis()->SetBinLabel(idx_bin, to_string(bin.id()).c_str());

      hTransMatrix_->SetBinContent(idx_bin, idx_wc, xs);
      idx_bin++;
    }
    idx_wc++;
  }

  TCanvas c("c", "c", 2400, 2400);
  c.SetBottomMargin(0.30);
  c.SetLeftMargin(0.20);

  gStyle->SetPaintTextFormat("2.2f");

  // SetLabels(hTransMatrix_.get(), mergedBinsName, 'x');

  hTransMatrix_->GetXaxis()->SetLabelSize(0.02);
  hTransMatrix_->GetXaxis()->LabelsOption("v");
  hTransMatrix_->GetZaxis()->SetRangeUser(-1., 1.);
  // hTransMatrix_->GetYaxis()->SetRangeUser(0, transMatWcBin_.at(wc).size());
  hTransMatrix_->SetMarkerSize(0.4);

  hTransMatrix_->Draw("colz text");
  c.SaveAs("figures/TransitionMatrix.pdf");
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::PrintXss(ostream& os) const {
  LOG_DURATION("PrintXss");

  //eft::WCvalue wcVal = eft::WCvalue::AVERAGED;

  eft::WilsonCoefficientsCode wc = eft::WilsonCoefficientsCode::NONE;
  //truth_bin bin = 99;

  for (const auto& wc_wcVal_xss : interfXS_) {
    wc = wc_wcVal_xss.first;
    for (const auto& bin_xs : wc_wcVal_xss.second.at(eft::WCvalue::AVERAGED)) {
      os << "[" << wc << "][" << bin_xs.first << "] = " << bin_xs.second << endl;
    }
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::AverageWCvalues() {
  AverageWCvalues(interfXS_);
  AverageWCvalues(interfXScoarse_);
}
  //////////////////////////////////////////////////////////////////////////////////////
void EFTManager::AverageWCvalues(XSperWC& interfXS)
{
  LOG_DURATION("AverageWCvalues");
  cout << "[AverageWCvalues] average cross-sections..." << endl;


  for (auto &wc_wcVal_xs : interfXS)
  { // over wc -> <wcVals --> <bin --> xs>>
    auto wc = wc_wcVal_xs.first;
    cout << wc;

    //if (wc.GetId() == 0)
    if (wc == WilsonCoefficientsCode::NONE) {
      cout << " ==> skip it" << endl;
      continue;
    }

    //if (wc == WilsonCoefficientsCode::SM) {
    //  cout << " ==> 
    //}

    cout << endl;

    // in vectors to simplify fitting with TGraph, where
    // only c-style arrays can be passed
    std::vector<eft::WCvalue> wcVals;
    std::vector<CrossSections> xsPerWcVals;

    wcVals.reserve(4);
    xsPerWcVals.reserve(4);

    eft::WCvalue wcVal;
    CrossSections xss;

    for (auto &wcVal_xs : wc_wcVal_xs.second)
    { // over the wc vals to fill the vecs
      wcVal = wcVal_xs.first;
      xss = wcVal_xs.second;

      if (wcVal == eft::WCvalue::SM) {
        cout << " value = SM ==> skip" << endl;
        continue;
      }

      cout << wcVal << " -> " << xss.size() << " cross-sections" << endl;

      if (xss.size() != 0)
      {
        wcVals.push_back(wcVal);
        xsPerWcVals.push_back(xss);
      }
    }

    interfXS[wc];
    interfXS[wc][eft::WCvalue::AVERAGED];
    cout << "we are about to fill the total for the " << wc << " -> " << wcVals.size() << " elems;" << endl;
    cout << "interfxs[" << setw(20) << wc << "][AVERAGED] is to be averaged over: ";
    if (wcVals.size() > 1) {
      cout << wcVals.size() << " elems: ";
      for (const auto& val : wcVals) { cout << val << ", ";}
      cout << endl;
      interfXS[wc][eft::WCvalue::AVERAGED] = AverageWCvaluesOneWC(wcVals, xsPerWcVals, wc);
    }
    else if (wcVals.size() == 1) {
      cout << " 1 elem: " << wcVals[0] << endl;
      interfXS[wc][eft::WCvalue::AVERAGED] = interfXS_[wc][wcVals[0]];
    }
    else {
      cout << " NO elemets --> fix to zero" << endl;
      interfXS[wc][eft::WCvalue::AVERAGED] = {};
    }
  } // 
  cout << "[AverageWCvalues] average cross-sections... DONE" << endl;
  cout << "[AverageWCvalues] average widths..." << endl;
  cout << "[ERROR] currently not averaged! take ONE" << endl;

  for (const auto& ch_wc_wcVal : widthPerChanWcVal_) {
    DecayChannel ch = ch_wc_wcVal.first;
    for (const auto& wc_wcVal : ch_wc_wcVal.second) {
      WilsonCoefficientsCode wc = wc_wcVal.first;
      widthPerChanWcVal_[ch][wc][WCvalue::AVERAGED] = widthPerChanWcVal_[ch][wc][WCvalue::ONE];
      cout << "widthPerChanWcVal_[" << ch << "][" << wc << "][WCvalue::AVERAGED] = "
           << widthPerChanWcVal_[ch][wc][WCvalue::AVERAGED] << endl;
    }
  }

}
//////////////////////////////////////////////////////////////////////////////////////
const CrossSections
EFTManager::AverageWCvaluesOneWC(const vector<eft::WCvalue> &wcVals,
                                 const vector<CrossSections> &xss,
                                 const eft::WilsonCoefficientsCode &wc) const
{

  /*********************************************
   *  Takes vector of available wcVals among:
   * 		- ONE
   * 		- MINHALF
   * 		- DOTONE
   *
   * 	and vector of CrossSections corresp
   * 	to this values of the WC
   *
   * 	to make a linear fit of
   * 	sigma(wc)
   *
   * 	return
   * 		this averaged value
   *********************************************/

  LOG_DURATION("AverageWCvaluesOneWC");
  ASSERT_EQUAL(wcVals.size(), xss.size());

  size_t nbPoints = wcVals.size();

  cout << "[AverageWCvaluesOneWC] for " << nbPoints << " available values" << endl;

  CrossSections res;
  truth_bin bin;
  // cross_section xs;

  shared_ptr<TGraphErrors> gr;

  vector<double> wcs;
  vector<double> xsVals;
  vector<double> xsErrs;

  for (const auto &bin_xs : xss[0])
  {
    wcs.clear();
    xsVals.clear();
    xsErrs.clear();

    bin = bin_xs.first;
     cout << "[AverageWCvaluesOneWC] bin: " << bin;

    if (bin.id() == 99) {
      cout << " ==> skip it" << endl;
      continue;
    }

    cout << endl;
    //cout << "[AverageWCvaluesOneWC] transform " << wcVals.size() << " to double...";
    // xs  = bin_xs.second;
    // cout << " -- bin = " << bin << endl;
    wcs = WcValsToDouble(wcVals);
    //cout << " DONE" << endl;

    //cout << "contenf of xss:" << endl;
    //for (size_t i = 0; i < xss.size(); ++i)
    //{
      // cout << "xss[" << i << "].size() -> " << xss[i].size() << endl;
      //for (const auto &bin_xs : xss[i])
      //{
       // // cout << "xss[" << i << "][" << bin_xs.first << "] = " << bin_xs.second <<  endl;
      //}
    //}
    cout << "[AverageWCvaluesOneWC] start looping over wc values..." << endl;
    for (size_t idx_wc = 0; idx_wc < xss.size(); ++idx_wc)
    {
      // for (size_t idx_wc = 0; idx_wc < nbPoints; ++idx_wc) {
      //cout << "try to get access to: xss[" << idx_wc << "][" << bin << "]...";
      // cout << "xss[" << idx_wc << "] exists? -> " << boolalpha << (xss.find(idx_wc) != xss.end()) << endl;
      // cout << "xss[" << idx_wc << "][" << bin << "] exists? -> " << boolalpha << (xss[idx_wc].find(idx_wc) != 0) << endl;
      auto xs = xss[idx_wc][bin];
      //cout << " DONE...";
      xsVals.push_back(xs.val);
      xsErrs.push_back(xs.err);
      //cout << " ==> pushed!" << endl;
    } // WC valuse

    //cout << "fit the following points: " << endl;
    //bool notZeroPresent = false;
    for (size_t idx = 0; idx < xsVals.size(); ++idx) {
      //cout << wcs[idx] << " -> " << xsVals[idx] << " +- " << xsErrs[idx] << endl;
      //if (xsVals[idx] != 0)
        //notZeroPresent = true;
    }

    //if ( ! notZeroPresent ) {
    //  cout << " only zeroes! skip it" << endl;
    //  continue;
    //}

    //cout << " there's what to fit!" << endl;
    //cout << "try to make graph from: " << wcs.size() << " " << xsVals.size() << " " << xsErrs.size() << endl;

    bool doNeedFit = true;

    for (const auto val : xsVals) {
      if (val == 0) {
        cout << "WARNING all zeroes! skip this one" << endl;
        doNeedFit = false;
        break;
        res[bin] = {0.0, 0.0};
      }
    }

    if (doNeedFit) {
      gr = make_shared<TGraphErrors>(nbPoints,
                                     wcs.data(),
                                     xsVals.data(),
                                     nullptr,
                                     xsErrs.data());

      cout << "[AverageWCvaluesOneWC] fit...";
      gr->Fit("pol1", "q");

      //cout << " DONE" << endl;

      TCanvas c("c", "c", 1200, 800);
      gr->GetYaxis()->SetTitle("#sigma [pb]");
      gr->GetXaxis()->SetTitle("Wilson coefficient");
      //cout << "title is set" << endl;
      //cout << "try wctostr(" << wc << ") ...";
      //cout << wcToStr(wc) << " ==> done!" << endl;

      string merged_name = fineNames[bin];

      if (doMerging_)
        merged_name = mergedBinsName[bin];

      //const string title = "#sigma " + mergedBinsName[bin] + "(" + wcToStr(ParseWCcode(wc.GetId())) + ")";
      //const string saveName = "sigma" + mergedBinsName[bin] + "(" + wcToStr(ParseWCcode(wc.GetId())) + ")";
      const string title = "#sigma " + merged_name + "(" + wcToStr(wc) + ")";
      //cout << "title: " << title << endl;
      const string saveName = "sigma" + merged_name + "(" + wcToStr(wc) + ")";
      //cout << "savename: " << saveName << endl;
      gr->SetTitle(title.c_str());
      //cout << " glob title is set" << endl;

      gr->Draw("ALP");
      //cout << "drawn" << endl;
      c.SaveAs(("figures/xs/fit_" + saveName + ".pdf").c_str());
      //cout << " saved" << endl;
      // throw std::runtime_error("enough");

      res[bin].val = gr->GetFunction("pol1")->GetParameter(1); // slope
      //cout << "val extracted " << endl;
      res[bin].err = gr->GetFunction("pol1")->GetParError(1);  // slope
      //cout << "err extracted " << endl;
      //cout << "done..." << endl;
      // res[bin].val = 1;
      // res[bin].err = 1;
      // exit(0);
    }
  }
  // throw std::runtime_error("enough");
  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::PrintParametrisationsXML(const string&& name) const {
  ofstream fs(name, std::ios_base::out);
  PrintParametrisationsXML(fs);
  fs.close();
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::PrintParametrisationsXML(ostream& os) const {
  LOG_DURATION("PrintParametrisationsXML");

  /*size_t nbWC = interfXS_.size();
  size_t nbBins = fineNames.size();
  size_t nbChannels = widthPerChanWcVal_.size();

  if (doMerging_) {
    nbBins = mergedBinsName.size();
  }

  cout << "[PrintParametrisationsXML] for " << nbWC << " wc and " << nbBins << " truth bins" << endl;
  cout << "[PrintParametrisationsXML] " << nbChannels << " are present" << endl;

  vector<vector<double>> xsWcTb(nbWC, vector<double>(nbBins, 0.));
  vector<double>         xsSM(nbBins, 0.);
  vector<vector<double>> widthChanWC(nbChannels, vector<double>(nbWC, 0.));
  vector<double>         widthSM(nbChannels);


  vector<size_t> numbersOfBins (nbBins);
  vector<string> namesOfBins   (nbBins);
  vector<string> namesOfWC     (nbWC);

  os << "parametrisation:" << endl;

  WilsonCoefficientsCode code =  WilsonCoefficientsCode::NONE;
  truth_bin bin = 99;
  cross_section xs{};

  size_t idx_wc = 0;
  size_t idx_bin = 0;

  // fill info on the cross-sections
  bool first_loop = true;
  for (const auto& wc_wcval_xss : interfXS_) {
    code = wc_wcval_xss.first;
    string wcName = wcToStr(code);
    namesOfWC.push_back(move(wcName));

    idx_bin = 0;
    for (const auto& bin_xs : wc_wcval_xss.second.at(WCvalue::AVERAGED)) {
        bin = bin_xs.first;
        xs = bin_xs.second;
        cout << "[" << code << "][" << bin << "] ==> " << xs << endl;
        cout << "xsWcTb[" << idx_wc << "][" << idx_bin << "] = " << xsWcTb[idx_wc][idx_bin];
        xsWcTb[idx_wc][idx_bin] = xs.val;
        cout << " ==> " << xsWcTb[idx_wc][idx_bin] << endl;

        if (first_loop) {
          numbersOfBins.push_back(bin);
          if (doMerging_)
            namesOfBins.push_back(mergedBinsName.at(bin));
          else
            namesOfBins.push_back(fineNames.at(bin));
        }
      ++idx_bin;
    } // bins
    ++idx_wc;
    first_loop = false;
  } // wc

  // fill info on the decays

  for (const auto& ch_wc_wcval_widhts : widthPerChanWcVal_) {
     
  }


  // map xsWcTb[idx_wc][idx_bin] is filled out
  // now we should write it down

  //for (const auto&)*/

  //using WC = WilsonCoefficientsCode;
  using CH = DecayChannel;

  map<WilsonCoefficientsCode, double>                    width_total_wc;
  double                                                 width_total_sm;
  map<DecayChannel, map<WilsonCoefficientsCode, double>> width_channel_wc;
  map<DecayChannel, double>                              width_channel_sm;

  map<CH, map<WC, map<string, double>>> impact_xs;

  map<CH, map<        string, double>>  xs_ch_sm; // xs per channel per truth bin
  map<CH, map<WC, map<string, double>>> xs_ch_wc; // xs per channel per wc per truth bin

  auto widths = widthPerChanWcVal_;
  cout << "keys of widhts:" << endl;
  for (const auto& key_val : SMwidths_) {
    cout << key_val.first << endl;
  }

  cout << "try to get sm widhts...";
  //width_total_sm = widths[DecayChannel::TOTAL][WilsonCoefficientsCode::SM][WCvalue::AVERAGED].val;
  width_total_sm = SMwidths_.at(DecayChannel::TOTAL).val;
  cout << "it's ok" << endl;

  set<WC> regWC;
  //regWC.reserve(200);
  map<CH, set<string>> bins_per_chan;

  for (const string& channel : {"yy", "4l", "bb"}) {
    cout << "[PrintParametrisationsXML] channel: " << channel << endl;
    map<WilsonCoefficientsCode, map<string, ValErr<double>>> Xs_wc_bin;
    map<                            string, ValErr<double>>  Xs_sm_bin;
    
    map<WilsonCoefficientsCode, double> impactXS_wc;
    map<WilsonCoefficientsCode, double> impactBR_wc;

    //map<WilsonCoefficientsCode, map<string, ValErr<double>>>

    
    //if (true) {
    if (channel != "yy") {
      cout << "Merge for " << interfXScoarse_.size() << " wc ==> "; 
      Xs_wc_bin = Merge(interfXScoarse_, channel);
      Xs_sm_bin = Merge(SMxs_, channel);
      cout << Xs_wc_bin.size() << endl;
    }
    else {
      cout << "Mergefor  " << interfXS_.size() << " wc ==> "; 
      Xs_wc_bin = Merge(interfXS_, "yy");
      Xs_sm_bin = Merge(SMxs_, "yy");
      cout << Xs_wc_bin.size() << endl;
    }
   
    eft::DecayChannel decayEnum = DecayChannel::NONE;
    if (channel == "yy")
      decayEnum = DecayChannel::GAM_GAM;
    else if (channel == "4l")
      decayEnum = DecayChannel::LLLL;
    else if (channel == "bb")
      decayEnum = DecayChannel::B_B;

    cout << "[PrintParametrisationsXML] prepare cross-sections..." << endl;
    for (const auto& wc_bin_xs : Xs_wc_bin) {
      WC wc = wc_bin_xs.first;
      regWC.insert(wc);
      for (const auto& bin_xs : wc_bin_xs.second) {
        string bin = bin_xs.first;
	      bins_per_chan[decayEnum].insert(bin);
        auto xs = bin_xs.second;
        if (wc == WC::SM) {
          xs_ch_sm[decayEnum]    [bin] = xs.val;
          cout << "xs_ch_sm[" << setw(10) << decayEnum << "][" << setw(25) << bin << "] = "
               <<  xs_ch_sm[decayEnum]    [bin] << endl;
        }
        else {
          xs_ch_wc[decayEnum][wc][bin] = xs.val;
          cout << "xs_ch_wc[" << setw(10) << decayEnum << "][" << setw(10) << wc 
               << "][" << setw(25) << bin << "] = "
               <<  xs_ch_wc[decayEnum][wc][bin] << endl;
        } // not SM
      } // bins
    } // wc

    // prepare SM xs:
    for (const auto& bin_xs : Xs_sm_bin) {
      cout << "xs_ch_sm[" << setw(10) << decayEnum << "][" << setw(25) << bin_xs.first << "] = ";
      xs_ch_sm[decayEnum][bin_xs.first] = bin_xs.second.val;
      cout << xs_ch_sm[decayEnum][bin_xs.first] << endl;
    }

    WC wc;
    string bin;

    ///// xss are ready ==> compute impact on them for each channel
    cout << "[PrintParametrisationsXML] compute impact xs..." << endl;
    for (const auto ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
      for (const auto& wc_rest : xs_ch_wc[ch]) {
        wc = wc_rest.first;
        for (const auto& bin_xs : xs_ch_wc[ch][wc]) {
          bin = bin_xs.first;

          if (bin == "NONE")
            continue;

          cout << "impact_xs[" << setw(7) << enumToStr(ch) << "]["
               << setw(20) << enumToStr(wc) << "][" 
               << setw(35) << bin << "] = "
               << xs_ch_wc[ch][wc][bin] << " / "
               << xs_ch_sm[ch]    [bin] << " = ";
          impact_xs[ch][wc][bin] = xs_ch_wc[ch][wc][bin] / xs_ch_sm[ch][bin];
          cout << impact_xs[ch][wc][bin] << endl;
        }
      }
    }

    
    //width_channel_sm[decayEnum] = widths[decayEnum][WilsonCoefficientsCode::SM][WCvalue::AVERAGED].val;
    width_channel_sm[decayEnum] = SMwidths_.at(decayEnum).val;
    // double widthTotalInduced   = 0.;
    // double widthTotalSM        = widthPerChanWcVal_[DecayChannel::TOTAL][WilsonCoefficientsCode::SM][wcValue::TOTAL];
    // double impactWidthTotal    = 0.;

    // double widthChannelInduced = 0.; // depends on the wc
    // double widthChannelSM      = widthPerChanWcVal_[decayEnum][WilsonCoefficientsCode::SM][wcValue::TOTAL];
    // double impactWidthChannel  = 0.;

    // double xsInduced           = 0.;
    // double xsSM                = 0.;

    // double impactBR    = 0.;
    // double impactXs    = 0.;
    // double impact      = 0.;

    if (channel == "yy") {
    
    }
    else {
      wc =  WilsonCoefficientsCode::NONE;
      //truth_bin bin = 999;
      //cross_section xs{};
      //Width width{};

      for (const auto& wc_wcVal_width : widths[decayEnum]) {
        wc = wc_wcVal_width.first;
        //widthChannelInduced = widthPerChanWcVal_[decayEnum][code][wcValue::TOTAL];
        width_channel_wc[decayEnum][wc]  = widths[decayEnum][wc][WCvalue::AVERAGED].val;
        width_total_wc             [wc] += widths[decayEnum][wc][WCvalue::AVERAGED].val;
        //impactWidthChannel = widthChannelInduced / widthChannelSM;
        //width_total_wc[code] += 
      } // wc, given channel
    } // not yy
  } // channels


  /////////////////////////////////////////////////////////////////////////////////////////////
  // IMPACT ON THE DECAY //////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  // no we have all:
  // width_total_wc       [wc]  --> width
  // width_total_SM             --> width
  // width_channel_wc [ch][wc]  --> width
  // width_channel_SM [ch]      --> width

  // prepare the impacts on the decay part (RB):
  // for yy, take directly from the paper
  // for the others -> compute as:
  //
  // impact_decay_total       [wc] = width_total_wc           [wc] / width_total_SM
  // impact_decay_channel [ch][wc] = width_channel_wc     [ch][wc] / width_channel_SM [ch]
  // ==>
  // impact_br_channel    [ch][wc] = impact_decay_channel [ch][wc] - impact_decay_total   [wc]

  

  map<        WC, double>  impact_decay_total;
  map<CH, map<WC, double>> impact_decay_channel;
  map<CH, map<WC, double>> impact_br_channel;

  // compute the impact_decay_total[wc] first:
  for (const WC wc : regWCcode_) {
    if (wc == WC::SM)
      continue;
    cout << "impact_decay_total[" << setw(15) << wc << "] = " << setw(10) 
         << width_total_wc[wc]
         << " / " 
         << width_total_sm << " = ";
    impact_decay_total[wc] = width_total_wc[wc] / width_total_sm;
    cout << impact_decay_total[wc] << endl;
  }

  cout << "[] prepare information about the decay part..." << endl;
  for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
    cout << "consider the " << ch << " channel" << endl;
    for (const WC wc : regWCcode_) {
      //cout << '[' << setw(15) << ch << "][" << setw(25) << wc << "]";
      impact_br_channel[ch][wc];
      if (ch == CH::GAM_GAM) {
        if (brPerChanWcVal_.at(DecayChannel::GAM_GAM).count(wc) != 0)
          impact_br_channel[ch][wc] = brPerChanWcVal_.at(DecayChannel::GAM_GAM).at(wc);
        else
          impact_br_channel[ch][wc] = 0;
        cout << " impact_br_channel[" << setw(15) << ch << "][" << setw(15) << wc << "] = "
             << setw(10) << impact_br_channel[ch][wc] << endl;
        //cout << " set br to: " << impact_br_channel[ch][wc] << endl;
      }
      else {
        cout << "impact_decay_channel[" << setw(15) << enumToStr(ch) 
             << "][" << setw(15) << enumToStr(wc) << "] = " << setw(10) 
             << width_channel_wc[ch][wc] << " / "  << setw(10)
             << width_channel_sm[ch] << " = ";
        impact_decay_channel[ch][wc] = width_channel_wc[ch][wc] / width_channel_sm[ch]; 
        cout << impact_decay_channel[ch][wc] << endl;

        cout << " ==> impact_br_channel[" << setw(15) << ch << "][" << setw(15) << wc << "] = "
             << setw(10) << impact_decay_channel[ch][wc] << " / " << setw(10)
             << " = ";
        impact_br_channel[ch][wc] = impact_decay_channel[ch][wc] - impact_decay_total[wc];
        cout << impact_br_channel[ch][wc] << endl;
      } // NOT GAM_GAM channel
    } // wc
  } // channels

  //cout << "[PrintParametrisationsXML] print parametrisation..." << endl;
  cout << "[PrintParametrisationsXML] parametrisations of BR and XS are obtained" << endl;
  cout << "[PrintParametrisationsXML] prepare impacts per [ch][wc][bin] before printing them down" << endl;
  map<CH, map<WC, map<string, double>>> param_ch_wc_bin;
  
  // impact_xs[ch][wc][bin] 
  // impact_br_channel[ch][wc]
  string bin;

  for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
    //const auto& impact_xs_this_ch = impact_xs.at(ch);
    for (const auto& wc_bin_xs : impact_xs.at(ch)) {
      auto wc = wc_bin_xs.first;
      if (wc == WilsonCoefficientsCode::SM)
        continue;
      
      for (const auto& bin_xs : wc_bin_xs.second) {
	bin = bin_xs.first;
	double impact_xs_    = bin_xs.second;
	double impact_widht_ = 0.;
  if (bin == "NONE")
    continue;

	if (impact_br_channel.find(ch) != impact_br_channel.end()) {
	  if (impact_br_channel.at(ch).find(wc) != impact_br_channel.at(ch).end()) {
	    impact_widht_ = impact_br_channel.at(ch).at(wc);
	  } // try to find impact_br[ch] on this wc
	} // try to find impact_br on this ch
	param_ch_wc_bin[ch][wc][bin] = impact_xs_ + impact_widht_;
	//param_ch_wc_bin[ch][wc][bin] = impact_widht_;
	cout << "impact[" << setw(7) << enumToStr(ch) << "][" << setw(8) << enumToStr(wc) << "][" 
	     << setw(35) << bin << "] = "
	     << setw(10) << impact_xs_ << " - " 
	     << setw(10) << impact_widht_ << " = "
	     << param_ch_wc_bin[ch][wc][bin] << endl;
      } // bin
    } // wc
  } // channel
  cout << "ok" << endl;

  // print parametrisation
  os << "parametrisation:" << endl;
  for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
    for (const string& bin_ : bins_per_chan[ch]) {
      if (bin == "NONE")
        continue;
      os << " - [";
      bool isFirst = true;
      for (const WC wc : regWC) {
        if (wc == WilsonCoefficientsCode::SM)
          continue;
	      if (param_ch_wc_bin[ch].find(wc) != param_ch_wc_bin[ch].end()) {
	        if ( !isFirst )
	          os << ", ";
	        else
	          isFirst = false;

	        os << param_ch_wc_bin[ch][wc][bin_];
	      } 
      } // wc
      os << "]" << endl;
    } // bins 
  } // channels

  // pring xpars...
  os << "xpars: [";
  bool isFirst = true;
  for (const WC wc : regWC) {
    if (wc == WilsonCoefficientsCode::SM)
        continue;
    if ( ! isFirst )
      os << ", ";
    else
      isFirst = false;

    os << enumToStr(wc);
  } // wc
  os << "]\n";

  // PRINT ypars
  os << "ypars: [";
  isFirst = true;
  for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
    for (const string& bin_ : bins_per_chan[ch]) {
      if (bin == "NONE")
        continue;
    if ( ! isFirst )
      os << ", ";
    else
      isFirst = false;
    
    os << bin_;
    } // bins
    os << endl;
    
  } // ch
  os << "]\n";

  // put the sm predictions...
  os << "SM_predictions: [";
  isFirst = true;
  for (const CH ch : {CH::GAM_GAM, CH::LLLL, CH::B_B}) {
    for (const string& bin_ : bins_per_chan[ch]) {
       if (bin == "NONE")
        continue;
      if ( ! isFirst )
        os << ", ";
      else
        isFirst = false;

      os << xs_ch_sm[ch][bin_];
    } // bin
  } // ch
  os << "]\n";
}
//////////////////////////////////////////////////////////////////////////////////////
//void EFTManager::PrintYaml(ostream& os, map<CH, map<WC, map<string, double>>>& impact) const {
//  cout << "[PrintYaml]
//}
//////////////////////////////////////////////////////////////////////////////////////
//map<WilsonCoefficientsCode, CrossSections> 
map<WilsonCoefficientsCode, map<string, ValErr<double>>>
EFTManager::Merge(const XSperWC& interfXs, 
                  const string& channel) const
{
  LOG_DURATION("Merge");

  const auto merger = GetMerger().GetMergingScheme(channel);
  cout << "[Merge] for " << channel << " channel" << endl;

  map<WilsonCoefficientsCode, map<string, ValErr<double>>> res;

  CrossSections xss;
  //cross_section xs;
  truth_bin     bin;
  WilsonCoefficientsCode wc;
  size_t idx_bin;

  string bin_init, bin_merged;

  for (const auto& wc_wcVal_xss : interfXs) {
    wc = wc_wcVal_xss.first;
    cout << "wc: " << wc << " available: " << wc_wcVal_xss.second.at(WCvalue::AVERAGED).size() 
         << " truth bins" << endl;
    res[wc];
    //if (wc == WilsonCoefficientsCode::SM)
    //  continue;

    size_t maxBins = 0;
    if (channel == "yy")
        maxBins = eft::fineNames.size();
      else
        maxBins = eft::coarseNames.size();


    cout << " - loop over " << wc_wcVal_xss.second.at(WCvalue::AVERAGED).size() << " bins" << endl;
    for (const auto& bin_xs : wc_wcVal_xss.second.at(WCvalue::AVERAGED)) {
      idx_bin = bin_xs.first;
      cout << "idx_bin = " << idx_bin << endl;

      if (idx_bin >= maxBins)
        continue;

      if (channel == "yy")
        bin_init = eft::fineNames.at(idx_bin);
      else
        bin_init = eft::coarseNames.at(idx_bin);

      bin_merged = merger.GetMergedFromInit(bin_init);
      //bin_init   = merger.GetInitStrNb(idx_bin);
      //bin_merged = merger.GetMergedFromInit(bin_init);
      cout << setw(25) << bin_init << " --> " << setw(25) << bin_merged << "| ==> ";
      //cout << " * idx: " << idx_bin << " --> " << bin_init << " ===> " << bin_merged;
      cout << " val: |" << res[wc][bin_merged] << "| + |" << bin_xs.second << "| ==> ";
      res[wc][bin_merged] += bin_xs.second;
      cout << res[wc][bin_merged] << endl;
      cout << "*****************************" << endl;
    } // bins
  } // wc

  return res;
}
//////////////////////////////////////////////////////////////////////////////////////
map<string, ValErr<double>> 
EFTManager::Merge(const CrossSections& smXs, const string& channel) const {
  cout << "Merge SM xs for " << channel << " channel" << endl;
  XSperWC xsPerWC_;
  xsPerWC_[WilsonCoefficientsCode::SM];
  xsPerWC_[WilsonCoefficientsCode::SM][WCvalue::AVERAGED] = smXs;
  
  auto res = EFTManager::Merge(xsPerWC_, channel);
  return res[WilsonCoefficientsCode::SM];
}
//////////////////////////////////////////////////////////////////////////////////////
    
void EFTManager::ComputeEV()
{
  LOG_DURATION("ComputeEV");

  cout << "[ComputeEV] for the hessian matrix: " << EFTHessian_.GetNrows()
       << " x " << EFTHessian_.GetNcols() << endl;

  MatrixManager::GetEigenVecsVals(EFTHessian_, EFTevecs_, EFTevals_);
  MatrixManager::DrawMatrix(EFTevecs_, "figures/EFTevecs.pdf", "colz text");

  /*{
    TCanvas c("c", "c", 1200, 800);
    c.SetGrid();

    EFTevecs_.Draw("colz text");
    c.SaveAs("figures/evecs.pdf");
  }

  {
    TCanvas c("c", "c", 1200, 800);
    c.SetGrid();

    EFTHessian_.Draw("colz text");
    EFTHessian_.SetMarkerSize(0.10);
    c.SaveAs("figures/Hessian.pdf");
  }*/

  size_t nbRows = EFTevecs_.GetNrows();
  //size_t nbCols = EFTevecs_.GetNcols();

  cout << "obtained evals and evecs: " << endl;
  for (size_t idx_v = 0; idx_v < nbRows; ++idx_v)
  {
    cout << EFTevals_[idx_v] << " " << endl;
    // cout << " * vec #" << idx_v << " with value: " << EFTevals_[idx_v] << ":" << endl;
    // for (size_t idx = 0; idx < nbCols; ++idx) {
    //   cout << " - " << EFTevecs_[idx_v][idx] << endl;
    // }
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::WriteXMLwsReparametrisation(const string &filename,
                                             XMLConfig config) const
{
  LOG_DURATION("WriteXMLwsReparametrisation");

  /*
  XMLwriteRequest:
      string          channel = "none";
      vector<string>  wcNames;
      vector<string>  muNames;
      bool            includeSM     = true;
      bool            includeBSM    = false;
      bool            includeInterf = true;
      TMatrixD        parametrisation;
  */

  map<string, XMLwriteRequest> reqs;

  ostringstream os;
  ofstream res(filename, std::ios_base::out);

  vector<string> wcNames;
  wcNames.reserve(regWCcode_.size());

  for (const auto wc : regWCcode_)
  {
    const string wcName = wcToStr(wc);
    wcNames.push_back(move(wcName));
  }

  config.POIsName = wcNames;
  config.muNames = mergedBinsName;

  XMLwriteRequest req_yy;
  req_yy.channel = "yy";
  req_yy.muNames = mergedBinsName;
  // req_yy.parametrisation = transWcBin_;
  req_yy.wcNames = wcNames;

  cout << "size of transWcBin_: " << transWcBin_.GetNrows() << " * " << transWcBin_.GetNcols() << endl;
  cout << "size of req_yy.parametrisation before resc: " << req_yy.parametrisation.GetNrows() << " * " << req_yy.parametrisation.GetNcols() << endl;

  // req_yy.parametrisation.ResizeTo(transWcBin_.GetNrows(), transWcBin_.GetNcols());
  req_yy.parametrisation.ResizeTo(transWcBin_);
  // cout << "size of transWcBin_: " << transWcBin_.GetNcols() << " * " << transWcBin_.GetNrows() << endl;
  cout << "size of req_yy.parametrisation after resc: " << req_yy.parametrisation.GetNrows() << " * " << req_yy.parametrisation.GetNcols() << endl;
  req_yy.parametrisation += transWcBin_;
  cout << "size of req_yy.parametrisation after  = : " << req_yy.parametrisation.GetNrows() << " * " << req_yy.parametrisation.GetNcols() << endl;
  // cout << "size of transWcBin_: " << transWcBin_.GetNcols() << " * " << transWcBin_.GetNrows() << endl;

  req_yy.parametrisation.Print();

  // cout << "wc names: " << endl;
  // for (const string& name : req_yy.wcNames) {
  //	cout << name << endl;
  // }
  // exit(1);
  cout << "before map" << endl;
  // reqs.insert()
  reqs["yy"] = req_yy;
  cout << "after map" << endl;
  reqs["yy"].parametrisation.Print();

  // exit(1);
  XMLwriter::Write(os, config, reqs);
  res << os.str() << endl;
  res.close();
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::DrawXS(TH1D &h, const WCvalue val, const WilsonCoefficientsCode code) const
{
  LOG_DURATION("DrawXS");

  TCanvas c("c", "c", 1200, 800);

  c.SetBottomMargin(0.30);
  // EFTManager::SetLabels(crossSections.get(), eft::binsNamesLatex, 'x');
  h.SetTitle(("Interference XS induced by: " + wcToStr(code)).c_str());
  h.GetXaxis()->SetLabelSize(0.02);
  h.GetYaxis()->SetTitle("Interference Cross-section [pb]");
  SetLabels(&h, fineNames, 'x');
  h.Draw("HIST E text");
  const string name = "Ana_" + wcToStr(code) + "_" + enumToStr<WCvalue>(val);
  cout << "[INFO] save as: " << name << endl;
  c.SaveAs(("figures/res_" + name + ".pdf").c_str());
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::ReadBrParametrisation(istream& is, DecayChannel chan) {
  LOG_DURATION("ReadBrParametrisation");
  using namespace eft::serialisation;
  std::vector<std::string> names;
  std::vector<double> vals;
  parseExpression(is, names, vals);
  cout << "names: " << endl;
  //for (const string& name : names) {
  for (size_t idx = 0; idx < names.size(); ++idx) {
    string name = names[idx];
  	cout << name << endl;
    auto wc = strToWC(name);
    brPerChanWcVal_[chan][wc] = vals[idx];
    cout << "assign to: " << setw(15) << name << " -> " << vals[idx] << endl;
  }  
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::SaveSmXs(const string& path) const {
  LOG_DURATION("SaveSmXs");
  using namespace eft::serialisation;

  ofstream fs(path, std::fstream::in | std::fstream::out | std::fstream::app);

  WidthOrXSsEntry xsEntry;
  xsEntry.setEntryType(EntryType::XSS);
	xsEntry.setVersion("version_test");
	xsEntry.setThRegime(TheoryRegime::SM);
	xsEntry.setChannel("yy");	  
  xsEntry.setWCcode(WilsonCoefficientsCode::SM);

  map<size_t, ValErr<double>> xsToWrite;

  for (int idx_bin = 1; idx_bin <= hSM_->GetNbinsX(); ++idx_bin) {
    ValErr<double> xs {hSM_->GetBinContent(idx_bin), hSM_->GetBinError(idx_bin)};
    xsToWrite[idx_bin - 1] = move(xs);
  }

  xsEntry.setXss(xsToWrite);
  xsEntry.serialise(fs);
  fs.close();
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::SaveIntXs(const string& path) const {
  LOG_DURATION("SaveIntXs");
  cout << "inteff xs contains: " << interfXS_.size() << " entries" << endl;

  using namespace eft::serialisation;

  ofstream fs(path, std::fstream::in | std::fstream::out | std::fstream::app);
  WilsonCoefficientsCode wc;
  CrossSections xss;
  ValErr<double> xs;
  size_t bin;
  for (const auto& wc_xss : interfXS_)
  {
    cout << "====================================" << endl;
    wc = wc_xss.first;
    cout << " * " << wc << endl;

    if (wc == WilsonCoefficientsCode::SM) {
      cout << " --> skip SM" << endl;
    }

    for (const auto& wcVal_xss : wc_xss.second) {
      auto wcVal = wcVal_xss.first;
      xss = wcVal_xss.second;

      cout << "[" << setw(10) << wc << "][" << setw(5) << wcVal << "]\n"; 

      WidthOrXSsEntry xsEntry;
      xsEntry.setEntryType(EntryType::XSS);
	    xsEntry.setVersion("version_test");
	    xsEntry.setThRegime(TheoryRegime::INT);
	    xsEntry.setChannel("yy");	  
      xsEntry.setWCcode(wc);

      xsEntry.setWCvalue(wcVal);


      //CrossSections xsToWrite;
      /*cout << "before find ONE\n";
      if (wc_xss.second.find(WCvalue::ONE) != wc_xss.second.end())
        xsEntry.setWCvalue(WCvalue::ONE);

      cout << "after find ONE\n";
      if (wc == WilsonCoefficientsCode::NONE)
        continue;

      cout << "try to find total\n";
      xss = wc_xss.second.at(WCvalue::TOTAL);
      cout << "after total\n"; */
      //wcIdx++;

      const string wcName = wcToStr(wc);
      // WIRTE XS(bin) for a given WC
      map<size_t, ValErr<double>> xsToWrite;
      for (size_t bin_idx = 0; bin_idx < mergedBinsName.size(); ++bin_idx) {
        if (xss.find(bin_idx) != xss.end()) {
          bin = bin_idx;
          xs = xss.at(bin_idx);
        } 
        else {
          bin = bin_idx;
          xs.val = 0.f;
          xs.err = 0.f;
        }

        xsToWrite[bin] = xs;
      } // bins
      //xsEntry.setXss(xsToWrite);
      xsEntry.setXss(xsToWrite);
      xsEntry.serialise(fs);
    } // WC value
  } // WC
  fs.close();
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::SaveIntWidths(const string& path) const {
  LOG_DURATION("SaveIntWidhts");
  using namespace eft::serialisation;
  ofstream fs(path, std::fstream::in | std::fstream::out | std::fstream::app);

  WilsonCoefficientsCode wc;
  DecayChannel ch;
  WCvalue val;
  Width width;

  for (const auto& ch_rest : widthPerChanWcVal_) {
    ch = ch_rest.first;
    for (const auto& wc_rest : ch_rest.second) {
      wc = wc_rest.first;
      for (const auto& val_width : wc_rest.second) {
        val = val_width.first;
        width = val_width.second;
	      cout << "[SaveIntWidhts][" << ch << "]["<< wc << "][" << val << "]" << endl;
        WidthOrXSsEntry wEntry;
        wEntry.setEntryType(EntryType::WIDTH);
	      wEntry.setVersion("version_test");
	      wEntry.setThRegime(TheoryRegime::INT);
	      wEntry.setChannel(enumToStr<DecayChannel>(ch));	  
        wEntry.setWCcode(wc);
        wEntry.setWCvalue(val);
        wEntry.setWidth(width);
        wEntry.serialise(fs);
      } // wc vals
    } // wc
  } // channels 
}
//////////////////////////////////////////////////////////////////////////////////////
void EFTManager::SaveDataBase(const string& path) const {
  LOG_DURATION("SaveDataBase");
  //SaveSmXs(path);
  SaveIntXs(path);
  SaveIntWidths(path);
} // function
//////////////////////////////////////////////////////////////////////////////////////
// void EFTManager::DrawImpactComparison() const {
//   using namespace eft::plot;
  
//   //auto res = eft::serialisation::TEntry::readDB("test_db_10k_no_ana.txt");
//   auto res = eft::serialisation::TEntry::readDB("like_this.txt");

//   ImpactPlotConfig cfg;
//   cfg.channels = {"yy"};
//   cfg.db_ = res;
//   std::vector<size_t> truth_bins(28);
//   std::iota(truth_bins.begin(), truth_bins.end(), 0);
//   cfg.truth_bins = truth_bins;
//   cfg.groupsWCcodes = {
//     //{WilsonCoefficientsCode::CHG, WilsonCoefficientsCode::CUGRE, WilsonCoefficientsCode::CUHRE, WilsonCoefficientsCode::CG, WilsonCoefficientsCode::CQQ3, WilsonCoefficientsCode::CQQ1},
//     //{WilsonCoefficientsCode::CHW, WilsonCoefficientsCode::CHWB, WilsonCoefficientsCode::CHB},
//     //{WilsonCoefficientsCode::CHQ3, WilsonCoefficientsCode::CHU, WilsonCoefficientsCode::CHQ1}
//   };

//   cfg.wcScaling_ = {
//     // {WilsonCoefficientsCode::CHG, 0.005},
//     // {WilsonCoefficientsCode::CUGRE, 0.1},
//     // {WilsonCoefficientsCode::CQQ3, 0.3},
//     // {WilsonCoefficientsCode::CHW, 0.5},
//     // {WilsonCoefficientsCode::CHQ3, 0.1},
//     // {WilsonCoefficientsCode::CHQ1, 0.2},
//     // {WilsonCoefficientsCode::CQQ1, 0.2},
//   }; 

//   ImpactPlotter plot(cfg);
//   plot.Plot();
// }
//////////////////////////////////////////////////////////////////////////////////////
