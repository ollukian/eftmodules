echo "[INFO][Manager]"
filenames=("Parser" "MatrixManager" "XMLwriter" "EFTManager" "WilsonCoefficients" "ParseResults" "CrossSections")

for filename in ${filenames[@]}; do
    echo "[INFO][Manager] try to compile: " $filename
    if g++ -c -g  @../Utils/compilerFlags.sh `root-config --cflags` $filename.cpp; then
        #echo "${filename} has been successfully compiled. Copy .o file"
        echo "[INFO][Manager] {${filename}} has been successfully compiled."
    else
        echo "**********************************************"
        echo "*** ERROR in compilation of: ${filename} *****"
        echo "**********************************************"
        exit
    fi
done
echo "[INFO][Manager] *****************************************"
echo "[INFO][Manager] *         successfully compiled         *"
echo "[INFO][Manager] *****************************************"



