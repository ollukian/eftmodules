#pragma once

#include <string>
#include <set>
#include <iostream>

using std::string;
using std::set;

class TODO {
public:
    explicit TODO(const string& msg = "")
        : message(msg)
    {
        todos_.insert(message);
    }

    ~TODO() = default;

    static void Print(ostream& os) {
        for (const string& todo : todos) {
            os << todo << endl;
        }
    }
private:
    const string             message;

    static set<string> todos_;
};

set<string> TODO::todos_;

#ifndef UNIQ_ID_IMPL
#define UNIQ_ID_IMPL(lineno) _a_local_var_##lineno
#define UNIQ_ID(lineno) UNIQ_ID_IMPL(lineno)

#define LOG_TODO(message) \
  TODO UNIQ_ID(__LINE__){message};

#endif // ifndef UNIQ_ID_IMPL