#pragma once

#ifndef CROSS_SECTIONS_H
#define CROSS_SECTIONS_H

#ifndef	_MSC_VER
#include "../DataBase/ValErr.h"
#else
#include "../DataBase/ValErr.h"
#endif
#include "TruthBin.h"

using truth_bin     = eft::Label;
using cross_section = ValErr<double>;

class CrossSections {
public:
    inline map<truth_bin, cross_section> data() { return data_; }
    inline const map<truth_bin, cross_section> data() const { return data_; }

    using iterator = map<truth_bin, cross_section>::iterator;
    using const_iterator = map<truth_bin, cross_section>::const_iterator;

    CrossSections() = default;
    CrossSections(const CrossSections& other) = default;
    CrossSections(CrossSections&& other) = default;

    CrossSections& operator = (const CrossSections& other) noexcept{  data_ = other.data(); return *this; }
    CrossSections& operator = (CrossSections&& other) noexcept { data_ = other.data(); return *this; }

    CrossSections& operator += (const CrossSections& other);
    CrossSections  operator +  (const CrossSections& other);


          cross_section& operator [] (const truth_bin& bin)        { return data_[bin]; }
    const cross_section& operator [] (const truth_bin& bin) const  { return data_.at(bin); }
          cross_section& operator [] (const size_t& bin)        { eft::Label l(bin); return data_[move(l)]; }
    const cross_section& operator [] (const size_t& bin) const  { eft::Label l(bin); return data_.at(move(l));}

          cross_section& at(const truth_bin& bin)        { return data_.at(bin); }
    const cross_section& at(const truth_bin& bin) const  { return data_.at(bin); }
          cross_section& at(const size_t& bin)           { eft::Label l(bin); return data_.at(move(l)); }
    const cross_section& at(const size_t& bin)    const  { eft::Label l(bin); return data_.at(move(l));}

    iterator       begin()       { return data_.begin(); };
    const_iterator begin() const { return data_.begin(); };

    iterator       end()         { return data_.end(); };
    const_iterator end()  const  { return data_.end(); };

    const_iterator find(const truth_bin& bin) const { return data_.find(bin); }
    const_iterator find(const size_t bin)     const { return data_.find(eft::Label(bin)); }

    size_t size() const { return data_.size(); }
private:    
    map<truth_bin, cross_section> data_;
};

#endif