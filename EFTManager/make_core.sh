echo "[INFO][Core] make Core"

# we're at root/EFTManager
# go to    root/Core

cd ../Core

filenames=("WCpair" "Impact")

for filename in ${filenames[@]}; do
    echo "[INFO][Core] try to compile: " $filename
    if g++ -c -g  @../Utils/compilerFlags.sh `root-config --cflags` $filename.cpp; then 
        #echo "${filename} has been successfully compiled. Copy .o file"
        echo "[INFO][Core] {${filename}} has been successfully compiled"
        cp ${filename}.o ../EFTManager
    else
        echo "**********************************************"
        echo "*** ERROR in compilation of: ${filename} *****"
        echo "**********************************************"
        exit
    fi
done
echo "[INFO][Core] *****************************************"
echo "[INFO][Core] *         successfully compiled         *"
echo "[INFO][Core] *****************************************"

cd ../EFTManager
