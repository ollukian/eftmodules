echo "[Info] compile single file: $1 and run"
#g++ -c -g -std=c++17 -Wall -Wextra  -Wshadow -Wunreachable-code -pedantic `root-config --cflags` $1
g++ -c -g  @Utils/compilerFlags.sh `root-config --cflags` $1
echo "[Info] compilation is done"
echo "[Info] link..."
g++ @Utils/compilerFlags.sh  `root-config --glibs` TEntry.o EntryParser.o WilsonCoefficients.o WIdthOrXSsEntry.o  QuadParametrisation.o LinearParametrisation.o ImpactPlotter.o Parser.o MatrixManager.o XMLwriter.o EFTManager.o Merger.o HiggsConfig.o HiggsModule.o EWModule.o TChainModulesOnePhysics.o HiggsChainModules.o SandboxHiggsChain.o RunEFT.o
echo "[Info] linking is done"
echo "[Info] run!"
./a.out
