echo "[INFO][DataBase]"
cd ../DataBase

filenames=("TEntry" "EntryParser" "WIdthOrXSsEntry" "QuadParametrisation" "LinearParametrisation" "ImpactPlotter" "Merger")
for filename in ${filenames[@]}; do
    echo "[INFO][DataBase] try to compile: " $filename
    if g++ -c -g  @../Utils/compilerFlags.sh `root-config --cflags` $filename.cpp; then
        #echo "${filename} has been successfully compiled. Copy .o file"
        echo "[INFO][DataBase] {${filename}} has been successfully compiled. Copy .o file"
        cp ${filename}.o ../EFTManager
    else
        echo "**********************************************"
        echo "*** ERROR in compilation of: ${filename} *****"
        echo "**********************************************"
        exit
    fi
done
echo "[INFO][DataBase] *****************************************"
echo "[INFO][DataBase] *         successfully compiled         *"
echo "[INFO][DataBase] *****************************************"

cd ../EFTManager


