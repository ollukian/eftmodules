#pragma once

#ifndef PARSER_H
#define PARSER_H

#include "WilsonCoefficients.h"
#include "TruthBin.h"
#include "CrossSections.h"

#include "../DataBase/ValErr.h"
#include "../DataBase/ParseResults.h"

#include <string>
#include <vector>
#include <map>

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <fstream>

//#ifndef	_MSC_VER
#include "TH1D.h"
#include "TFile.h"
#include "TCanvas.h"

#include "TSystem.h"
//#endif

//using truth_bin     = std::string;
////using truth_bin     = double;
using truth_bin     = eft::Label;
//using cross_section = double;
using cross_section = ValErr<double>;

using Width         = ValErr<double>;

using std::vector;
using std::map;
using std::string;

/*struct ParseResults {
    size_t                      nbGeneratedEvents = 0;
    std::string                 filename;

    eft::Regime                 regime; // PRODUCTION / DECAY
    eft::ProdMode               prodMode = eft::ProdMode::NONE;
    eft::DecayChannel           decayChannel = eft::DecayChannel::NONE;
    eft::TheoryRegime           theoryRegime = eft::TheoryRegime::SM;

    CrossSections               xsPerTB {};
    CrossSections               xsPerTBcoarse {};

    CrossSections               xsErrorPerTB {};
    Width                       width_ {};

    double                      wcValue = 0;
    eft::WCvalue                wcValueCode = eft::WCvalue::SM;
    eft::WilsonCoefficientsCode wcCode = eft::WilsonCoefficientsCode::NONE;
    eft::WilsonCoefficient      wc {};

    double                      wcValue2 = 0;
    eft::WCvalue                wcValueCode2 = eft::WCvalue::SM;
    eft::WilsonCoefficientsCode wcCode2 = eft::WilsonCoefficientsCode::NONE;
    eft::WilsonCoefficient      wc2 {};
 };

ostream& operator << (ostream& os, const ParseResults& res);*/

using namespace eft;

vector<string>                      Tokenize(string line); // copy, so that it can be modified!
vector<string>                      Tokenize(string line, const vector<string>& delims);
vector<string>                      GetListFiles(const string& path, 
                                                 Regime reg = Regime::NONE, 
                                                 ProdMode mode = ProdMode::NONE,
                                                 string specification = "",
                                                 /*bool isModeAsNb = false,*/
                                                 WCvalue val = WCvalue::NONE, 
                                                 size_t nbEvents = 0
                                                 );
vector<string>                      GetListFilesAna(const string& path, ProdMode mode = ProdMode::NONE, WCvalue val = WCvalue::NONE);
vector<string>                      GetListFiles(const string& path, WilsonCoefficientsCode code, size_t nbEvents, Regime reg = Regime::PRODUCTION, WCvalue val = WCvalue::NONE);
inline eft::ProdMode                ParseProductionMode(char code);
inline eft::ProdMode                ParseProductionMode(const string& line);
std::vector<eft::ProdMode>          ParseProductionModesFromNames(const string& names);
inline eft::DecayChannel            ParseDecayChannel(short int code);
       eft::DecayChannel            ParseDecayChannelEleonora(const string& name);
inline eft::WilsonCoefficientsCode  ParseWCcode(short int code); 
inline eft::WilsonCoefficientsCode  ParseWCcode(const string& code); 
inline eft::WCvalue                 ParseWCvalueAna(const string& val);
inline eft::TheoryRegime            ParseTheoryRegime(const string& reg);
inline size_t                       ParseWCid(const string& wc) { return stoi(wc.substr(1, 2)); }
eft::WCvalue                        ParseWCvalue(float val);
//#ifndef	_MSC_VER
TH1D*                               GetHistFromFile(const string& path, const string& name = "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_fine_pTjet30");
shared_ptr<TH1D>                    MapToHisto(const CrossSections& xs);
shared_ptr<TH1D>                    GetTotalXsGivenWC(const string& path, WilsonCoefficientsCode code, size_t nbEvent, WCvalue val = WCvalue::NONE);
TH1D*                               ComputeSignificanceDifferencesHistos(const TH1D& h1, const TH1D& h2);
void                                Test();
//#endif
//shared_ptr<TH2D>            MapToHisto(const XSperWC& xsPerWc);

CrossSections                       GetCrossSection(
        const string& filename,
        bool  doDraw = false, 
        const string& varName = "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_fine_pTjet30"
        /*const string& varName = "HiggsTemplateCrossSectionsStage12/STXS_stage1_2_pTjet30"*/
        );
Width                               GetWidth(const string& filename);
CrossSections                       Merge(CrossSections& xs, const std::map<size_t, size_t> mg = eft::mergingFineIdxIdx/*eft::mergingIdxIdx*/);
vector<double>                      WcValsToDouble(const vector<eft::WCvalue>& vals);

inline void                         DeleteBlankChars(std::string& line);
inline void                         DeleteBlankChars(const char* line);
inline string                       CapitaliseString(const char* line);
inline string                       CapitaliseString(string& line);
inline void                         ToLowerCaseString(string& line);
inline void                         ToLowerCaseString(const char* line);
//#ifndef	_MSC_VER
void SetLabels(TH1* h, const vector<string>& labels, char axis = 'x');
//#endif

//////////////////////////////////////////////////////////////////////////////////////
inline eft::TheoryRegime ParseTheoryRegime(const string& reg) {
    if (reg == "SM")  return TheoryRegime::SM;
    if (reg == "INT") return TheoryRegime::INT;
    if (reg == "BSM") return TheoryRegime::BSM;
    if (reg == "sm")  return TheoryRegime::SM;
    if (reg == "int") return TheoryRegime::INT;
    if (reg == "bsm") return TheoryRegime::BSM;
    throw std::runtime_error("Impossible to parse Theory regime: |"
        + reg + "|. Available: SM, INT, BSM");
}
//////////////////////////////////////////////////////////////////////////////////////
inline eft::WCvalue ParseWCvalueAna(const string& val) {
    if (val == "ci1")     return WCvalue::ONE;
    if (val == "ci01")    return WCvalue::DOTONE;
    if (val == "ci05neg") return WCvalue::MINHALF;
     throw std::runtime_error("Impossible to parse WC value from Ana: |"
        + val + "|. Available: ci1, ci01, ci05neg");
}
//////////////////////////////////////////////////////////////////////////////////////
inline eft::WilsonCoefficientsCode ParseWCcode(const string& code) {
    try {
        short int int_code = stoi(code);
        return ParseWCcode(int_code);
    }
    catch(...) {
        throw std::runtime_error("Impossible to parse: " + code + " - not a number");
    }
    return WilsonCoefficientsCode::NONE;
}
//////////////////////////////////////////////////////////////////////////////////////
inline void DeleteBlankChars(std::string& line) {
    while (line[0] == ' ')
        line.erase(0, 1);

    while (line.back() == ' ')
    {
        line.pop_back();
    }
}
//////////////////////////////////////////////////////////////////////////////////////
inline void DeleteBlankChars(const char* line) {
    std::string string_line(line);
    return DeleteBlankChars(string_line);
}
//////////////////////////////////////////////////////////////////////////////////////
inline string CapitaliseString(string& line) {
    std::transform(line.begin(), line.end(), line.begin(), ::toupper);
    return line;
}
//////////////////////////////////////////////////////////////////////////////////////
inline string CapitaliseString(const char* line_) {
    std::string line(line_);
    return CapitaliseString(line);
}
//////////////////////////////////////////////////////////////////////////////////////
inline void ToLowerCaseString(string& line) {
    std::transform(line.begin(), line.end(), line.begin(), ::tolower);
}
//////////////////////////////////////////////////////////////////////////////////////
inline void ToLowerCaseString(const char* line_) {
    std::string line(line_);
    ToLowerCaseString(line);
}
//////////////////////////////////////////////////////////////////////////////////////
inline ProdMode ParseProductionMode(char code) {
    size_t nb = code - '0';
    if (nb == 1) return eft::ProdMode::GGF; 
    if (nb == 2) return eft::ProdMode::VBF;
    if (nb == 4) return eft::ProdMode::ZH;
    if (nb == 3) return eft::ProdMode::WH;
    if (nb == 5) return eft::ProdMode::TTH;
    if (nb == 6) return eft::ProdMode::THJB;
    if (nb == 7) return eft::ProdMode::THW;

    string res;
    res.push_back(code);
    throw std::runtime_error("[ParseProdMode(char)] ERROR: code: " + res + " is not recognised. Available: 1-7");
}
//////////////////////////////////////////////////////////////////////////////////////
inline eft::WilsonCoefficientsCode ParseWCcode(short int code) {
  //cout << "[ParseWCcode] " << code << endl;
  if (code == 0) return eft::WilsonCoefficientsCode::SM;
  if (code < 19 || code > 84) throw std::out_of_range("code for WC: " + to_string(code) + " is outside of [19, 84]");
  return static_cast<WilsonCoefficientsCode>(code);
}
//////////////////////////////////////////////////////////////////////////////////////
eft::DecayChannel ParseDecayChannel(short int code) {
    if (code < 10 || code > 31)
        throw std::out_of_range("code for decay: " + to_string(code) + " is outside of [10, 30]");
    return static_cast<DecayChannel>(code);
}
//////////////////////////////////////////////////////////////////////////////////////
eft::ProdMode ParseProductionMode(const string& line) {
    cout << "parse prod mode: " << line << endl;
    string lower_case_line = line;
    ToLowerCaseString(lower_case_line);
    if (lower_case_line == "ggh")       return eft::ProdMode::GGF; 
    if (lower_case_line == "ggf")       return eft::ProdMode::GGF; 
    if (lower_case_line == "vbf")       return eft::ProdMode::VBF;
    if (lower_case_line == "zhlep")     return eft::ProdMode::ZH;
    if (lower_case_line == "zh")        return eft::ProdMode::ZH;
    if (lower_case_line == "whlep")     return eft::ProdMode::WH;
    if (lower_case_line == "wh")        return eft::ProdMode::WH;
    if (lower_case_line == "gg2zh")     return eft::ProdMode::GG2ZH; 
    if (lower_case_line == "qq2zh")     return eft::ProdMode::QQ2ZH; 
    if (lower_case_line == "tth")       return eft::ProdMode::TTH;
    if (lower_case_line == "thjb")      return eft::ProdMode::THJB;
    if (lower_case_line == "thw")       return eft::ProdMode::THW;
    if (lower_case_line == "bbh")       return eft::ProdMode::BBH; 
    throw std::runtime_error("production mode: " + line + " is not recognised. Available: \n"
      + "ggH, VBF, ZH, WH, ttH, tHjb, tHW, bbH, GG2ZH, QQ2ZH");
}
//////////////////////////////////////////////////////////////////////////////////////
#endif
