echo "[INFO][EFTModules] make EFTModules"
cd ../DataBase/EFTModules

filenames=("HiggsConfig" "HiggsModule" "EWConfig" "EWModule" "TChainModulesOnePhysics" "HiggsChainModules" "SandboxHiggsChain")

for filename in ${filenames[@]}; do
    echo "[INFO][EFTModules] try to compile: " $filename
    if g++ -c -g  @../../Utils/compilerFlags.sh `root-config --cflags` $filename.cpp; then
        #echo "${filename} has been successfully compiled. Copy .o file"
        echo "[INFO][EFTModules] {${filename}} has been successfully compiled. Copy .o file"
        cp ${filename}.o ../../EFTManager
    else
        echo "**********************************************"
        echo "*** ERROR in compilation of: ${filename} *****"
        echo "**********************************************"
        exit
    fi
done
echo "[INFO][EFTModules] *****************************************"
echo "[INFO][EFTModules] *         successfully compiled         *"
echo "[INFO][EFTModules] *****************************************"

cd ../../EFTManager


