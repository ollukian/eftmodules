#include "MatrixManager.h"

// TMatrixD MatrixManager::MakeTMatrixD(const vector<vector<double>>& mat) {
//     LOG_DURATION("MakeTMatrixD");

//         size_t nbRows = mat.size();
//         size_t nbCols = mat.front().size();

//         cout << "make TMatrixD: " << nbRows << " x " << nbCols << endl;
//         TMatrixD res(nbRows, nbCols);

//         for (size_t idx_row = 0; idx_row < nbRows; ++idx_row) {
//             for (size_t idx_col = 0; idx_col < nbRows; ++idx_col) {
//                 res(idx_row, idx_col) = mat[idx_row][idx_col];
//                 cout << "set " << setw(2) << idx_row << "][" << setw(2) << "] to " << mat[idx_row][idx_col] << endl; 
//             }    
//         }
//         return move(res);
// }
//////////////////////////////////////////////////////////////////////////////////////
// TMatrixD MatrixManager::MakeTMatrixD(const map<size_t, map<size_t, double>>& mat,) {
//     LOG_DURATION("MakeTMatrixD(map)");
//     return move(MakeTMatrixD(Map2DToVec(mat)));
// }
//////////////////////////////////////////////////////////////////////////////////////
TH2D MatrixManager::MakeHistFromTMatrixD(TMatrixD& mat) {
    LOG_DURATION("MakeHistFromTMatrixD");

    size_t nbCols = mat.GetNcols();
    size_t nbRows = mat.GetNrows();

    TH2D res("htmp", "htmp", nbCols, 0, nbCols, nbRows, 0, nbRows);

    for (size_t idx_row = 0; idx_row < nbRows; ++idx_row) {
        for (size_t idx_col = 0; idx_col < nbRows; ++idx_col) {
                res.SetBinContent(idx_row + 1, idx_col + 1, mat[idx_row][idx_col]);
                cout << "set [" << setw(2) << idx_row << "][" 
                     << setw(2) << idx_col << "] to " << mat[idx_row][idx_col] << endl; 
        }    
    }
    return res;
}
//////////////////////////////////////////////////////////////////////////////////////