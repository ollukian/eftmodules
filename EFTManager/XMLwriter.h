

#ifndef XMLWRITER_H
#define XMLWRITER_H

#include <string>
#include <vector>
#include <set>
#include <map>

#include <fstream>
#include <sstream>

//#pragma warning(push, 0)
//#ifndef	_MSC_VER
#include "TMatrixD.h"
//#endif
//#pragma warning(pop)

#include "../Utils/profile.h"
#include "../Utils/test_runner.h"

using std::map;

//class XMLriter;
//class XMLConfig;

struct XMLConfig {
    string          inFile = "ws.root";
    string          outFile = "newWs.root";
    string          WSname = "combWS";
    string          modelConfigName = "ModelConfig";
    string          dataName = "combData";
    string          modelName = "SMEFT";
    vector<string>  POIsName;
    vector<string>  muNames;
};

struct XMLwriteRequest {
    string          channel = "none";
    vector<string>  wcNames;
    vector<string>  muNames;
    bool            includeSM     = true;
    bool            includeBSM    = false;
    bool            includeInterf = true;
//#ifndef	_MSC_VER
    TMatrixD        parametrisation;
//#endif

    XMLwriteRequest& operator = (const XMLwriteRequest& other);
};

class XMLwriter {
public:
    static void Write(ostringstream& os, 
                      const XMLConfig& config, 
                      const map<string, XMLwriteRequest>& reqPerChannnel);
private:

};

#endif
